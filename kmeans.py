import tensorflow as tf
# import argparse
import numpy as np
# the following line allows all cluster labels to be printed
np.set_printoptions(threshold=np.nan)
import time

#parser = argparse.ArgumentParser(description='Process num: pts, clusters, iterations.')
#parser.add_argument('--num_points', type=int, help='number of random 3D points')
#parser.add_argument('--num_clusters', type=int, help='number of clusters')
#parser.add_argument('--num_iterations', type=int, help='number of iterations')
#args = parser.parse_args()
# access wth args.num_*

#N = args.num_points
N = 50000
#K = args.num_clusters
K = 100
#ITERS = args.num_iterations
ITERS = 1000
# 3D points:
DIM = 3

# start the timer of the execution
start = time.time()

# generate N random 3D points, uniform default range is fraction in range(0,1)
points = tf.Variable(tf.random_uniform([N,DIM]))
# generate N random cluster labels
cluster_labels = tf.Variable(tf.zeros([N],dtype=tf.int64))

# initialize k cluster centroids
# because the points are random, use the first k points
# tf.slice(input_, begin, size) copies a subtensor of input_
centroids = tf.Variable(tf.slice(points.initialized_value(), [0,0], [K,DIM]))

# In order to compute the sum of euclidean distances between points and centroids
# it is easiest to create tensors that contain every element that will factor in to
# the subtraction of points from centroids
# Use reshape and tile to create compatible tensors of dimension [N,K,DIM]
# then compute the euclidean distances.
# tf.tile(input, multiples), tf.reshape(input, dims)
reshaped_centroids = tf.reshape(tf.tile(centroids, [N, 1]), [N, K, DIM])
reshaped_points = tf.reshape(tf.tile(points, [1, K]), [N, K, DIM])
# tf.square is element wise
# tf.reduce_sum reduces the input tensor along the axis dim
# axis=2 sums across the DIM dimensions for each point-centroid distance
sum_euclid = tf.reduce_sum(tf.square(reshaped_centroids-reshaped_points), axis=2)

# Use tf.argmin to find the lowest euclidean distance point
# tf.argmin(input, axis=1)
# axis=1 finds the closest centroid for each point, therefore the label                      $
best_labels = tf.argmin(sum_euclid, 1)

# tf.reduce_any(input) = logical OR of all input elements
# tf.not_equal(comp1, comp2) = not equal for each element of the input groups
# change labels will be True if any label changed, and False if no label changed
change_labels = tf.reduce_any(tf.not_equal(best_labels, cluster_labels))

# setup the operations to recalculate the centroids by finding
# the mean for each cluster

# build a function that calulates these means
# tf.unsorted_segment_sum(data, segment_ids, num_segments) allows for sums
# to be calculated for each cluster, which is what we want to be able to calculate
# means by cluster
def cluster_means(data, cluster_ids, num_clusters):
	# the total sum for each cluster
    total = tf.unsorted_segment_sum(data, cluster_ids, num_clusters)
    # count is a factor for the number of points in the centroid
    # by using tf.ones_like to convert all point elements to 1
    count = tf.unsorted_segment_sum(tf.ones_like(data), cluster_ids, num_clusters)
    # calculate means by cluster
    return total / count

# the means for each cluster are equivalent to the centroids for each cluster
means = cluster_means(points, best_labels, K)

# Update variable is used to trigger the assigment of values of centroids and labels
# after they have been recalulated by another sess.run() iteration
# Use tf.control_dependencies to ensure that the centroids and labels variables
# are modified after the centroid recalculation and relabeling
with tf.control_dependencies([change_labels]):
    update = tf.group(centroids.assign(means), cluster_labels.assign(best_labels))

# now that the graph is established, run the iterations
sess = tf.Session()
sess.run(tf.global_variables_initializer())
change = True
iters = 0

while change and iters < ITERS:
    iters += 1
    [change, _] = sess.run([change_labels, update])

# extract the variable values
[ctrds, labels] = sess.run([centroids, cluster_labels])

# stop execution timer
stop = time.time()

print ("Execution time:  %.2f s" % (stop-start))
print "Required",  iters, "iterations"
#print "Final Centroids:", ctrds
#print "Cluster Labels:", labels
