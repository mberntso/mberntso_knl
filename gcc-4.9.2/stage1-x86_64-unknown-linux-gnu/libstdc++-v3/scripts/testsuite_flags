#!/bin/sh

#
# This script computes the various flags needed to run GNU C++ testsuites
# (compiler specific as well as library specific). 
#
# Written by Benjamin Kosnik <bkoz@redhat.com>
#            Gabriel Dos Reis <gdr@codesourcery.com>
#

# Print a message saying how this script is intended to be invoked
print_usage() {
    cat <<EOF
Usage: 
    testsuite_flags --install-includes
		    --build-includes
		    --build-cxx
		    --build-cc
		    --install-cxx
		    --cxxflags
		    --cxxldflags
		    --cxxpchflags
		    --cxxvtvflags

EOF
}

# Establish configure-generated directory structure.
BUILD_DIR=/home/mberntso/gcc-4.9.2/x86_64-unknown-linux-gnu/libstdc++-v3
SRC_DIR=/home/mberntso/gcc-4.9.2/libstdc++-v3
PREFIX_DIR=NONE
query=$1

case ${query} in
    --install-includes)
      INCLUDES="-I${SRC_DIR}/testsuite/util"
      echo ${INCLUDES}
      ;;
    --build-includes)
      INCLUDES="-nostdinc++ -I/home/mberntso/gcc-4.9.2/x86_64-unknown-linux-gnu/libstdc++-v3/include/x86_64-unknown-linux-gnu -I/home/mberntso/gcc-4.9.2/x86_64-unknown-linux-gnu/libstdc++-v3/include -I/home/mberntso/gcc-4.9.2/libstdc++-v3/libsupc++ 
                -I${SRC_DIR}/include/backward -I${SRC_DIR}/testsuite/util"
      echo ${INCLUDES}
      ;;
    --install-cxx)
      CXX=${PREFIX_DIR}/bin/g++
      echo ${CXX}
      ;;
    --build-cxx)
      CXX_build=" /home/mberntso/gcc-4.9.2/host-x86_64-unknown-linux-gnu/gcc/xgcc -shared-libgcc -B/home/mberntso/gcc-4.9.2/host-x86_64-unknown-linux-gnu/gcc -nostdinc++ -L/home/mberntso/gcc-4.9.2/x86_64-unknown-linux-gnu/libstdc++-v3/src -L/home/mberntso/gcc-4.9.2/x86_64-unknown-linux-gnu/libstdc++-v3/src/.libs -L/home/mberntso/gcc-4.9.2/x86_64-unknown-linux-gnu/libstdc++-v3/libsupc++/.libs -B/usr/local/x86_64-unknown-linux-gnu/bin/ -B/usr/local/x86_64-unknown-linux-gnu/lib/ -isystem /usr/local/x86_64-unknown-linux-gnu/include -isystem /usr/local/x86_64-unknown-linux-gnu/sys-include   "
      CXX=`echo "$CXX_build" | sed 's,gcc/xgcc ,gcc/xg++ ,'`
      echo ${CXX}
      ;;
    --build-cc)
      CC_build="/home/mberntso/gcc-4.9.2/host-x86_64-unknown-linux-gnu/gcc/xgcc -B/home/mberntso/gcc-4.9.2/host-x86_64-unknown-linux-gnu/gcc/ -B/usr/local/x86_64-unknown-linux-gnu/bin/ -B/usr/local/x86_64-unknown-linux-gnu/lib/ -isystem /usr/local/x86_64-unknown-linux-gnu/include -isystem /usr/local/x86_64-unknown-linux-gnu/sys-include   "
      CC="$CC_build"
      echo ${CC}
      ;;
    --cxxflags)
      CXXFLAGS_default="-D_GLIBCXX_ASSERT -fmessage-length=0"
      CXXFLAGS_config="-ffunction-sections -fdata-sections -g -O2 -D_GNU_SOURCE "
      echo ${CXXFLAGS_default} ${CXXFLAGS_config} 
      ;;
    --cxxvtvflags)
      CXXFLAGS_vtv=""
      LDFLAGS_vtv=""
      echo ${CXXFLAGS_vtv} ${LDFLAGS_vtv}
      ;;
    --cxxparallelflags)
      CXXFLAGS_parallel="-D_GLIBCXX_PARALLEL -fopenmp
			 -B${BUILD_DIR}/../libgomp 
                         -I${BUILD_DIR}/../libgomp 
			 -L${BUILD_DIR}/../libgomp/.libs -lgomp"
      echo ${CXXFLAGS_parallel}
      ;;
    --cxxpchflags)
      PCHFLAGS="-include bits/stdc++.h"
      echo ${PCHFLAGS}
      ;;
    --cxxldflags)
      SECTIONLDFLAGS="-Wl,--gc-sections  "
      echo ${SECTIONLDFLAGS}
      ;;
    *)
      print_usage
      ;;
esac

exit 0
