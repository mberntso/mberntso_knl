/* Generated automatically by the program `genopinit'
   from the machine description file `md'.  */

#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "tm.h"
#include "tree.h"
#include "varasm.h"
#include "stor-layout.h"
#include "calls.h"
#include "rtl.h"
#include "tm_p.h"
#include "flags.h"
#include "insn-config.h"
#include "expr.h"
#include "optabs.h"

struct optab_pat {
  unsigned scode;
  enum insn_code icode;
};

static const struct optab_pat pats[NUM_OPTAB_PATTERNS] = {
  { 0x010e0f, CODE_FOR_extendqihi2 },
  { 0x010e10, CODE_FOR_extendqisi2 },
  { 0x010e11, CODE_FOR_extendqidi2 },
  { 0x010f10, CODE_FOR_extendhisi2 },
  { 0x010f11, CODE_FOR_extendhidi2 },
  { 0x011011, CODE_FOR_extendsidi2 },
  { 0x012728, CODE_FOR_extendsfdf2 },
  { 0x012729, CODE_FOR_extendsfxf2 },
  { 0x012829, CODE_FOR_extenddfxf2 },
  { 0x022827, CODE_FOR_truncdfsf2 },
  { 0x022927, CODE_FOR_truncxfsf2 },
  { 0x022928, CODE_FOR_truncxfdf2 },
  { 0x030e0f, CODE_FOR_zero_extendqihi2 },
  { 0x030e10, CODE_FOR_zero_extendqisi2 },
  { 0x030e11, CODE_FOR_zero_extendqidi2 },
  { 0x030f10, CODE_FOR_zero_extendhisi2 },
  { 0x030f11, CODE_FOR_zero_extendhidi2 },
  { 0x031011, CODE_FOR_zero_extendsidi2 },
  { 0x060f27, CODE_FOR_floathisf2 },
  { 0x060f28, CODE_FOR_floathidf2 },
  { 0x060f29, CODE_FOR_floathixf2 },
  { 0x061027, CODE_FOR_floatsisf2 },
  { 0x061028, CODE_FOR_floatsidf2 },
  { 0x061029, CODE_FOR_floatsixf2 },
  { 0x061127, CODE_FOR_floatdisf2 },
  { 0x061128, CODE_FOR_floatdidf2 },
  { 0x061129, CODE_FOR_floatdixf2 },
  { 0x064356, CODE_FOR_floatv4siv4sf2 },
  { 0x064359, CODE_FOR_floatv4siv4df2 },
  { 0x064858, CODE_FOR_floatv8siv8sf2 },
  { 0x06485c, CODE_FOR_floatv8siv8df2 },
  { 0x064d5b, CODE_FOR_floatv16siv16sf2 },
  { 0x070e27, CODE_FOR_floatunsqisf2 },
  { 0x070e28, CODE_FOR_floatunsqidf2 },
  { 0x070f27, CODE_FOR_floatunshisf2 },
  { 0x070f28, CODE_FOR_floatunshidf2 },
  { 0x071027, CODE_FOR_floatunssisf2 },
  { 0x071028, CODE_FOR_floatunssidf2 },
  { 0x071029, CODE_FOR_floatunssixf2 },
  { 0x071127, CODE_FOR_floatunsdisf2 },
  { 0x071128, CODE_FOR_floatunsdidf2 },
  { 0x074356, CODE_FOR_floatunsv4siv4sf2 },
  { 0x074858, CODE_FOR_floatunsv8siv8sf2 },
  { 0x074d5b, CODE_FOR_floatunsv16siv16sf2 },
  { 0x082710, CODE_FOR_lrintsfsi2 },
  { 0x082711, CODE_FOR_lrintsfdi2 },
  { 0x082810, CODE_FOR_lrintdfsi2 },
  { 0x082811, CODE_FOR_lrintdfdi2 },
  { 0x08290f, CODE_FOR_lrintxfhi2 },
  { 0x082910, CODE_FOR_lrintxfsi2 },
  { 0x082911, CODE_FOR_lrintxfdi2 },
  { 0x09270f, CODE_FOR_lroundsfhi2 },
  { 0x092710, CODE_FOR_lroundsfsi2 },
  { 0x092711, CODE_FOR_lroundsfdi2 },
  { 0x09280f, CODE_FOR_lrounddfhi2 },
  { 0x092810, CODE_FOR_lrounddfsi2 },
  { 0x092811, CODE_FOR_lrounddfdi2 },
  { 0x09290f, CODE_FOR_lroundxfhi2 },
  { 0x092910, CODE_FOR_lroundxfsi2 },
  { 0x092911, CODE_FOR_lroundxfdi2 },
  { 0x0a2710, CODE_FOR_lfloorsfsi2 },
  { 0x0a2711, CODE_FOR_lfloorsfdi2 },
  { 0x0a2810, CODE_FOR_lfloordfsi2 },
  { 0x0a2811, CODE_FOR_lfloordfdi2 },
  { 0x0a290f, CODE_FOR_lfloorxfhi2 },
  { 0x0a2910, CODE_FOR_lfloorxfsi2 },
  { 0x0a2911, CODE_FOR_lfloorxfdi2 },
  { 0x0b2710, CODE_FOR_lceilsfsi2 },
  { 0x0b2711, CODE_FOR_lceilsfdi2 },
  { 0x0b2810, CODE_FOR_lceildfsi2 },
  { 0x0b2811, CODE_FOR_lceildfdi2 },
  { 0x0b290f, CODE_FOR_lceilxfhi2 },
  { 0x0b2910, CODE_FOR_lceilxfsi2 },
  { 0x0b2911, CODE_FOR_lceilxfdi2 },
  { 0x10270f, CODE_FOR_fix_truncsfhi2 },
  { 0x102710, CODE_FOR_fix_truncsfsi2 },
  { 0x102711, CODE_FOR_fix_truncsfdi2 },
  { 0x10280f, CODE_FOR_fix_truncdfhi2 },
  { 0x102810, CODE_FOR_fix_truncdfsi2 },
  { 0x102811, CODE_FOR_fix_truncdfdi2 },
  { 0x10290f, CODE_FOR_fix_truncxfhi2 },
  { 0x102910, CODE_FOR_fix_truncxfsi2 },
  { 0x102911, CODE_FOR_fix_truncxfdi2 },
  { 0x105643, CODE_FOR_fix_truncv4sfv4si2 },
  { 0x105848, CODE_FOR_fix_truncv8sfv8si2 },
  { 0x105943, CODE_FOR_fix_truncv4dfv4si2 },
  { 0x105b4d, CODE_FOR_fix_truncv16sfv16si2 },
  { 0x105c48, CODE_FOR_fix_truncv8dfv8si2 },
  { 0x11270f, CODE_FOR_fixuns_truncsfhi2 },
  { 0x112710, CODE_FOR_fixuns_truncsfsi2 },
  { 0x11280f, CODE_FOR_fixuns_truncdfhi2 },
  { 0x112810, CODE_FOR_fixuns_truncdfsi2 },
  { 0x115643, CODE_FOR_fixuns_truncv4sfv4si2 },
  { 0x115848, CODE_FOR_fixuns_truncv8sfv8si2 },
  { 0x115b4d, CODE_FOR_fixuns_truncv16sfv16si2 },
  { 0x120e0f, CODE_FOR_mulqihi3 },
  { 0x121011, CODE_FOR_mulsidi3 },
  { 0x121112, CODE_FOR_mulditi3 },
  { 0x130e0f, CODE_FOR_umulqihi3 },
  { 0x131011, CODE_FOR_umulsidi3 },
  { 0x131112, CODE_FOR_umulditi3 },
  { 0x1f4141, CODE_FOR_vcondv16qiv16qi },
  { 0x1f4142, CODE_FOR_vcondv8hiv16qi },
  { 0x1f4143, CODE_FOR_vcondv4siv16qi },
  { 0x1f4144, CODE_FOR_vcondv2div16qi },
  { 0x1f4156, CODE_FOR_vcondv4sfv16qi },
  { 0x1f4157, CODE_FOR_vcondv2dfv16qi },
  { 0x1f4241, CODE_FOR_vcondv16qiv8hi },
  { 0x1f4242, CODE_FOR_vcondv8hiv8hi },
  { 0x1f4243, CODE_FOR_vcondv4siv8hi },
  { 0x1f4244, CODE_FOR_vcondv2div8hi },
  { 0x1f4256, CODE_FOR_vcondv4sfv8hi },
  { 0x1f4257, CODE_FOR_vcondv2dfv8hi },
  { 0x1f4341, CODE_FOR_vcondv16qiv4si },
  { 0x1f4342, CODE_FOR_vcondv8hiv4si },
  { 0x1f4343, CODE_FOR_vcondv4siv4si },
  { 0x1f4344, CODE_FOR_vcondv2div4si },
  { 0x1f4356, CODE_FOR_vcondv4sfv4si },
  { 0x1f4357, CODE_FOR_vcondv2dfv4si },
  { 0x1f4444, CODE_FOR_vcondv2div2di },
  { 0x1f4457, CODE_FOR_vcondv2dfv2di },
  { 0x1f4646, CODE_FOR_vcondv32qiv32qi },
  { 0x1f4647, CODE_FOR_vcondv16hiv32qi },
  { 0x1f4648, CODE_FOR_vcondv8siv32qi },
  { 0x1f4649, CODE_FOR_vcondv4div32qi },
  { 0x1f4658, CODE_FOR_vcondv8sfv32qi },
  { 0x1f4659, CODE_FOR_vcondv4dfv32qi },
  { 0x1f4746, CODE_FOR_vcondv32qiv16hi },
  { 0x1f4747, CODE_FOR_vcondv16hiv16hi },
  { 0x1f4748, CODE_FOR_vcondv8siv16hi },
  { 0x1f4749, CODE_FOR_vcondv4div16hi },
  { 0x1f4758, CODE_FOR_vcondv8sfv16hi },
  { 0x1f4759, CODE_FOR_vcondv4dfv16hi },
  { 0x1f4846, CODE_FOR_vcondv32qiv8si },
  { 0x1f4847, CODE_FOR_vcondv16hiv8si },
  { 0x1f4848, CODE_FOR_vcondv8siv8si },
  { 0x1f4849, CODE_FOR_vcondv4div8si },
  { 0x1f4858, CODE_FOR_vcondv8sfv8si },
  { 0x1f4859, CODE_FOR_vcondv4dfv8si },
  { 0x1f4946, CODE_FOR_vcondv32qiv4di },
  { 0x1f4947, CODE_FOR_vcondv16hiv4di },
  { 0x1f4948, CODE_FOR_vcondv8siv4di },
  { 0x1f4949, CODE_FOR_vcondv4div4di },
  { 0x1f4958, CODE_FOR_vcondv8sfv4di },
  { 0x1f4959, CODE_FOR_vcondv4dfv4di },
  { 0x1f4b4b, CODE_FOR_vcondv64qiv64qi },
  { 0x1f4b4c, CODE_FOR_vcondv32hiv64qi },
  { 0x1f4b4d, CODE_FOR_vcondv16siv64qi },
  { 0x1f4b4e, CODE_FOR_vcondv8div64qi },
  { 0x1f4b5b, CODE_FOR_vcondv16sfv64qi },
  { 0x1f4b5c, CODE_FOR_vcondv8dfv64qi },
  { 0x1f4c4b, CODE_FOR_vcondv64qiv32hi },
  { 0x1f4c4c, CODE_FOR_vcondv32hiv32hi },
  { 0x1f4c4d, CODE_FOR_vcondv16siv32hi },
  { 0x1f4c4e, CODE_FOR_vcondv8div32hi },
  { 0x1f4c5b, CODE_FOR_vcondv16sfv32hi },
  { 0x1f4c5c, CODE_FOR_vcondv8dfv32hi },
  { 0x1f4d4b, CODE_FOR_vcondv64qiv16si },
  { 0x1f4d4c, CODE_FOR_vcondv32hiv16si },
  { 0x1f4d4d, CODE_FOR_vcondv16siv16si },
  { 0x1f4d4e, CODE_FOR_vcondv8div16si },
  { 0x1f4d5b, CODE_FOR_vcondv16sfv16si },
  { 0x1f4d5c, CODE_FOR_vcondv8dfv16si },
  { 0x1f4e4b, CODE_FOR_vcondv64qiv8di },
  { 0x1f4e4c, CODE_FOR_vcondv32hiv8di },
  { 0x1f4e4d, CODE_FOR_vcondv16siv8di },
  { 0x1f4e4e, CODE_FOR_vcondv8div8di },
  { 0x1f4e5b, CODE_FOR_vcondv16sfv8di },
  { 0x1f4e5c, CODE_FOR_vcondv8dfv8di },
  { 0x1f5641, CODE_FOR_vcondv16qiv4sf },
  { 0x1f5642, CODE_FOR_vcondv8hiv4sf },
  { 0x1f5643, CODE_FOR_vcondv4siv4sf },
  { 0x1f5644, CODE_FOR_vcondv2div4sf },
  { 0x1f5656, CODE_FOR_vcondv4sfv4sf },
  { 0x1f5657, CODE_FOR_vcondv2dfv4sf },
  { 0x1f5741, CODE_FOR_vcondv16qiv2df },
  { 0x1f5742, CODE_FOR_vcondv8hiv2df },
  { 0x1f5743, CODE_FOR_vcondv4siv2df },
  { 0x1f5744, CODE_FOR_vcondv2div2df },
  { 0x1f5756, CODE_FOR_vcondv4sfv2df },
  { 0x1f5757, CODE_FOR_vcondv2dfv2df },
  { 0x1f5846, CODE_FOR_vcondv32qiv8sf },
  { 0x1f5847, CODE_FOR_vcondv16hiv8sf },
  { 0x1f5848, CODE_FOR_vcondv8siv8sf },
  { 0x1f5849, CODE_FOR_vcondv4div8sf },
  { 0x1f5858, CODE_FOR_vcondv8sfv8sf },
  { 0x1f5859, CODE_FOR_vcondv4dfv8sf },
  { 0x1f5946, CODE_FOR_vcondv32qiv4df },
  { 0x1f5947, CODE_FOR_vcondv16hiv4df },
  { 0x1f5948, CODE_FOR_vcondv8siv4df },
  { 0x1f5949, CODE_FOR_vcondv4div4df },
  { 0x1f5958, CODE_FOR_vcondv8sfv4df },
  { 0x1f5959, CODE_FOR_vcondv4dfv4df },
  { 0x1f5b4b, CODE_FOR_vcondv64qiv16sf },
  { 0x1f5b4c, CODE_FOR_vcondv32hiv16sf },
  { 0x1f5b4d, CODE_FOR_vcondv16siv16sf },
  { 0x1f5b4e, CODE_FOR_vcondv8div16sf },
  { 0x1f5b5b, CODE_FOR_vcondv16sfv16sf },
  { 0x1f5b5c, CODE_FOR_vcondv8dfv16sf },
  { 0x1f5c4b, CODE_FOR_vcondv64qiv8df },
  { 0x1f5c4c, CODE_FOR_vcondv32hiv8df },
  { 0x1f5c4d, CODE_FOR_vcondv16siv8df },
  { 0x1f5c4e, CODE_FOR_vcondv8div8df },
  { 0x1f5c5b, CODE_FOR_vcondv16sfv8df },
  { 0x1f5c5c, CODE_FOR_vcondv8dfv8df },
  { 0x204141, CODE_FOR_vconduv16qiv16qi },
  { 0x204142, CODE_FOR_vconduv8hiv16qi },
  { 0x204143, CODE_FOR_vconduv4siv16qi },
  { 0x204144, CODE_FOR_vconduv2div16qi },
  { 0x204156, CODE_FOR_vconduv4sfv16qi },
  { 0x204157, CODE_FOR_vconduv2dfv16qi },
  { 0x204241, CODE_FOR_vconduv16qiv8hi },
  { 0x204242, CODE_FOR_vconduv8hiv8hi },
  { 0x204243, CODE_FOR_vconduv4siv8hi },
  { 0x204244, CODE_FOR_vconduv2div8hi },
  { 0x204256, CODE_FOR_vconduv4sfv8hi },
  { 0x204257, CODE_FOR_vconduv2dfv8hi },
  { 0x204341, CODE_FOR_vconduv16qiv4si },
  { 0x204342, CODE_FOR_vconduv8hiv4si },
  { 0x204343, CODE_FOR_vconduv4siv4si },
  { 0x204344, CODE_FOR_vconduv2div4si },
  { 0x204356, CODE_FOR_vconduv4sfv4si },
  { 0x204357, CODE_FOR_vconduv2dfv4si },
  { 0x204444, CODE_FOR_vconduv2div2di },
  { 0x204457, CODE_FOR_vconduv2dfv2di },
  { 0x204646, CODE_FOR_vconduv32qiv32qi },
  { 0x204647, CODE_FOR_vconduv16hiv32qi },
  { 0x204648, CODE_FOR_vconduv8siv32qi },
  { 0x204649, CODE_FOR_vconduv4div32qi },
  { 0x204658, CODE_FOR_vconduv8sfv32qi },
  { 0x204659, CODE_FOR_vconduv4dfv32qi },
  { 0x204746, CODE_FOR_vconduv32qiv16hi },
  { 0x204747, CODE_FOR_vconduv16hiv16hi },
  { 0x204748, CODE_FOR_vconduv8siv16hi },
  { 0x204749, CODE_FOR_vconduv4div16hi },
  { 0x204758, CODE_FOR_vconduv8sfv16hi },
  { 0x204759, CODE_FOR_vconduv4dfv16hi },
  { 0x204846, CODE_FOR_vconduv32qiv8si },
  { 0x204847, CODE_FOR_vconduv16hiv8si },
  { 0x204848, CODE_FOR_vconduv8siv8si },
  { 0x204849, CODE_FOR_vconduv4div8si },
  { 0x204858, CODE_FOR_vconduv8sfv8si },
  { 0x204859, CODE_FOR_vconduv4dfv8si },
  { 0x204946, CODE_FOR_vconduv32qiv4di },
  { 0x204947, CODE_FOR_vconduv16hiv4di },
  { 0x204948, CODE_FOR_vconduv8siv4di },
  { 0x204949, CODE_FOR_vconduv4div4di },
  { 0x204958, CODE_FOR_vconduv8sfv4di },
  { 0x204959, CODE_FOR_vconduv4dfv4di },
  { 0x204b4b, CODE_FOR_vconduv64qiv64qi },
  { 0x204b4c, CODE_FOR_vconduv32hiv64qi },
  { 0x204b4d, CODE_FOR_vconduv16siv64qi },
  { 0x204b4e, CODE_FOR_vconduv8div64qi },
  { 0x204b5b, CODE_FOR_vconduv16sfv64qi },
  { 0x204b5c, CODE_FOR_vconduv8dfv64qi },
  { 0x204c4b, CODE_FOR_vconduv64qiv32hi },
  { 0x204c4c, CODE_FOR_vconduv32hiv32hi },
  { 0x204c4d, CODE_FOR_vconduv16siv32hi },
  { 0x204c4e, CODE_FOR_vconduv8div32hi },
  { 0x204c5b, CODE_FOR_vconduv16sfv32hi },
  { 0x204c5c, CODE_FOR_vconduv8dfv32hi },
  { 0x204d4b, CODE_FOR_vconduv64qiv16si },
  { 0x204d4c, CODE_FOR_vconduv32hiv16si },
  { 0x204d4d, CODE_FOR_vconduv16siv16si },
  { 0x204d4e, CODE_FOR_vconduv8div16si },
  { 0x204d5b, CODE_FOR_vconduv16sfv16si },
  { 0x204d5c, CODE_FOR_vconduv8dfv16si },
  { 0x204e4b, CODE_FOR_vconduv64qiv8di },
  { 0x204e4c, CODE_FOR_vconduv32hiv8di },
  { 0x204e4d, CODE_FOR_vconduv16siv8di },
  { 0x204e4e, CODE_FOR_vconduv8div8di },
  { 0x204e5b, CODE_FOR_vconduv16sfv8di },
  { 0x204e5c, CODE_FOR_vconduv8dfv8di },
  { 0x21000e, CODE_FOR_addqi3 },
  { 0x21000f, CODE_FOR_addhi3 },
  { 0x210010, CODE_FOR_addsi3 },
  { 0x210011, CODE_FOR_adddi3 },
  { 0x210012, CODE_FOR_addti3 },
  { 0x210027, CODE_FOR_addsf3 },
  { 0x210028, CODE_FOR_adddf3 },
  { 0x210029, CODE_FOR_addxf3 },
  { 0x210041, CODE_FOR_addv16qi3 },
  { 0x210042, CODE_FOR_addv8hi3 },
  { 0x210043, CODE_FOR_addv4si3 },
  { 0x210044, CODE_FOR_addv2di3 },
  { 0x210046, CODE_FOR_addv32qi3 },
  { 0x210047, CODE_FOR_addv16hi3 },
  { 0x210048, CODE_FOR_addv8si3 },
  { 0x210049, CODE_FOR_addv4di3 },
  { 0x21004d, CODE_FOR_addv16si3 },
  { 0x21004e, CODE_FOR_addv8di3 },
  { 0x210056, CODE_FOR_addv4sf3 },
  { 0x210057, CODE_FOR_addv2df3 },
  { 0x210058, CODE_FOR_addv8sf3 },
  { 0x210059, CODE_FOR_addv4df3 },
  { 0x21005b, CODE_FOR_addv16sf3 },
  { 0x21005c, CODE_FOR_addv8df3 },
  { 0x25000e, CODE_FOR_subqi3 },
  { 0x25000f, CODE_FOR_subhi3 },
  { 0x250010, CODE_FOR_subsi3 },
  { 0x250011, CODE_FOR_subdi3 },
  { 0x250012, CODE_FOR_subti3 },
  { 0x250027, CODE_FOR_subsf3 },
  { 0x250028, CODE_FOR_subdf3 },
  { 0x250029, CODE_FOR_subxf3 },
  { 0x250041, CODE_FOR_subv16qi3 },
  { 0x250042, CODE_FOR_subv8hi3 },
  { 0x250043, CODE_FOR_subv4si3 },
  { 0x250044, CODE_FOR_subv2di3 },
  { 0x250046, CODE_FOR_subv32qi3 },
  { 0x250047, CODE_FOR_subv16hi3 },
  { 0x250048, CODE_FOR_subv8si3 },
  { 0x250049, CODE_FOR_subv4di3 },
  { 0x25004d, CODE_FOR_subv16si3 },
  { 0x25004e, CODE_FOR_subv8di3 },
  { 0x250056, CODE_FOR_subv4sf3 },
  { 0x250057, CODE_FOR_subv2df3 },
  { 0x250058, CODE_FOR_subv8sf3 },
  { 0x250059, CODE_FOR_subv4df3 },
  { 0x25005b, CODE_FOR_subv16sf3 },
  { 0x25005c, CODE_FOR_subv8df3 },
  { 0x29000e, CODE_FOR_mulqi3 },
  { 0x29000f, CODE_FOR_mulhi3 },
  { 0x290010, CODE_FOR_mulsi3 },
  { 0x290011, CODE_FOR_muldi3 },
  { 0x290027, CODE_FOR_mulsf3 },
  { 0x290028, CODE_FOR_muldf3 },
  { 0x290029, CODE_FOR_mulxf3 },
  { 0x290041, CODE_FOR_mulv16qi3 },
  { 0x290042, CODE_FOR_mulv8hi3 },
  { 0x290043, CODE_FOR_mulv4si3 },
  { 0x290044, CODE_FOR_mulv2di3 },
  { 0x290046, CODE_FOR_mulv32qi3 },
  { 0x290047, CODE_FOR_mulv16hi3 },
  { 0x290048, CODE_FOR_mulv8si3 },
  { 0x290049, CODE_FOR_mulv4di3 },
  { 0x29004d, CODE_FOR_mulv16si3 },
  { 0x29004e, CODE_FOR_mulv8di3 },
  { 0x290056, CODE_FOR_mulv4sf3 },
  { 0x290057, CODE_FOR_mulv2df3 },
  { 0x290058, CODE_FOR_mulv8sf3 },
  { 0x290059, CODE_FOR_mulv4df3 },
  { 0x29005b, CODE_FOR_mulv16sf3 },
  { 0x29005c, CODE_FOR_mulv8df3 },
  { 0x2d0027, CODE_FOR_divsf3 },
  { 0x2d0028, CODE_FOR_divdf3 },
  { 0x2d0029, CODE_FOR_divxf3 },
  { 0x2d0056, CODE_FOR_divv4sf3 },
  { 0x2d0057, CODE_FOR_divv2df3 },
  { 0x2d0058, CODE_FOR_divv8sf3 },
  { 0x2d0059, CODE_FOR_divv4df3 },
  { 0x2d005b, CODE_FOR_divv16sf3 },
  { 0x2d005c, CODE_FOR_divv8df3 },
  { 0x32000e, CODE_FOR_divmodqi4 },
  { 0x32000f, CODE_FOR_divmodhi4 },
  { 0x320010, CODE_FOR_divmodsi4 },
  { 0x320011, CODE_FOR_divmoddi4 },
  { 0x33000e, CODE_FOR_udivmodqi4 },
  { 0x33000f, CODE_FOR_udivmodhi4 },
  { 0x330010, CODE_FOR_udivmodsi4 },
  { 0x330011, CODE_FOR_udivmoddi4 },
  { 0x37000e, CODE_FOR_andqi3 },
  { 0x37000f, CODE_FOR_andhi3 },
  { 0x370010, CODE_FOR_andsi3 },
  { 0x370011, CODE_FOR_anddi3 },
  { 0x37002a, CODE_FOR_andtf3 },
  { 0x370041, CODE_FOR_andv16qi3 },
  { 0x370042, CODE_FOR_andv8hi3 },
  { 0x370043, CODE_FOR_andv4si3 },
  { 0x370044, CODE_FOR_andv2di3 },
  { 0x370046, CODE_FOR_andv32qi3 },
  { 0x370047, CODE_FOR_andv16hi3 },
  { 0x370048, CODE_FOR_andv8si3 },
  { 0x370049, CODE_FOR_andv4di3 },
  { 0x37004d, CODE_FOR_andv16si3 },
  { 0x37004e, CODE_FOR_andv8di3 },
  { 0x370056, CODE_FOR_andv4sf3 },
  { 0x370057, CODE_FOR_andv2df3 },
  { 0x370058, CODE_FOR_andv8sf3 },
  { 0x370059, CODE_FOR_andv4df3 },
  { 0x37005b, CODE_FOR_andv16sf3 },
  { 0x37005c, CODE_FOR_andv8df3 },
  { 0x38000e, CODE_FOR_iorqi3 },
  { 0x38000f, CODE_FOR_iorhi3 },
  { 0x380010, CODE_FOR_iorsi3 },
  { 0x380011, CODE_FOR_iordi3 },
  { 0x38002a, CODE_FOR_iortf3 },
  { 0x380041, CODE_FOR_iorv16qi3 },
  { 0x380042, CODE_FOR_iorv8hi3 },
  { 0x380043, CODE_FOR_iorv4si3 },
  { 0x380044, CODE_FOR_iorv2di3 },
  { 0x380046, CODE_FOR_iorv32qi3 },
  { 0x380047, CODE_FOR_iorv16hi3 },
  { 0x380048, CODE_FOR_iorv8si3 },
  { 0x380049, CODE_FOR_iorv4di3 },
  { 0x38004d, CODE_FOR_iorv16si3 },
  { 0x38004e, CODE_FOR_iorv8di3 },
  { 0x380056, CODE_FOR_iorv4sf3 },
  { 0x380057, CODE_FOR_iorv2df3 },
  { 0x380058, CODE_FOR_iorv8sf3 },
  { 0x380059, CODE_FOR_iorv4df3 },
  { 0x39000e, CODE_FOR_xorqi3 },
  { 0x39000f, CODE_FOR_xorhi3 },
  { 0x390010, CODE_FOR_xorsi3 },
  { 0x390011, CODE_FOR_xordi3 },
  { 0x39002a, CODE_FOR_xortf3 },
  { 0x390041, CODE_FOR_xorv16qi3 },
  { 0x390042, CODE_FOR_xorv8hi3 },
  { 0x390043, CODE_FOR_xorv4si3 },
  { 0x390044, CODE_FOR_xorv2di3 },
  { 0x390046, CODE_FOR_xorv32qi3 },
  { 0x390047, CODE_FOR_xorv16hi3 },
  { 0x390048, CODE_FOR_xorv8si3 },
  { 0x390049, CODE_FOR_xorv4di3 },
  { 0x39004d, CODE_FOR_xorv16si3 },
  { 0x39004e, CODE_FOR_xorv8di3 },
  { 0x390056, CODE_FOR_xorv4sf3 },
  { 0x390057, CODE_FOR_xorv2df3 },
  { 0x390058, CODE_FOR_xorv8sf3 },
  { 0x390059, CODE_FOR_xorv4df3 },
  { 0x39005b, CODE_FOR_xorv16sf3 },
  { 0x39005c, CODE_FOR_xorv8df3 },
  { 0x3a000e, CODE_FOR_ashlqi3 },
  { 0x3a000f, CODE_FOR_ashlhi3 },
  { 0x3a0010, CODE_FOR_ashlsi3 },
  { 0x3a0011, CODE_FOR_ashldi3 },
  { 0x3a0012, CODE_FOR_ashlti3 },
  { 0x3a0041, CODE_FOR_ashlv16qi3 },
  { 0x3a0042, CODE_FOR_ashlv8hi3 },
  { 0x3a0043, CODE_FOR_ashlv4si3 },
  { 0x3a0044, CODE_FOR_ashlv2di3 },
  { 0x3a0046, CODE_FOR_ashlv32qi3 },
  { 0x3a0047, CODE_FOR_ashlv16hi3 },
  { 0x3a0048, CODE_FOR_ashlv8si3 },
  { 0x3a0049, CODE_FOR_ashlv4di3 },
  { 0x3a004d, CODE_FOR_ashlv16si3 },
  { 0x3a004e, CODE_FOR_ashlv8di3 },
  { 0x3d000e, CODE_FOR_ashrqi3 },
  { 0x3d000f, CODE_FOR_ashrhi3 },
  { 0x3d0010, CODE_FOR_ashrsi3 },
  { 0x3d0011, CODE_FOR_ashrdi3 },
  { 0x3d0012, CODE_FOR_ashrti3 },
  { 0x3d0041, CODE_FOR_ashrv16qi3 },
  { 0x3d0042, CODE_FOR_ashrv8hi3 },
  { 0x3d0043, CODE_FOR_ashrv4si3 },
  { 0x3d0044, CODE_FOR_ashrv2di3 },
  { 0x3d0046, CODE_FOR_ashrv32qi3 },
  { 0x3d0047, CODE_FOR_ashrv16hi3 },
  { 0x3d0048, CODE_FOR_ashrv8si3 },
  { 0x3d004d, CODE_FOR_ashrv16si3 },
  { 0x3d004e, CODE_FOR_ashrv8di3 },
  { 0x3e000e, CODE_FOR_lshrqi3 },
  { 0x3e000f, CODE_FOR_lshrhi3 },
  { 0x3e0010, CODE_FOR_lshrsi3 },
  { 0x3e0011, CODE_FOR_lshrdi3 },
  { 0x3e0012, CODE_FOR_lshrti3 },
  { 0x3e0041, CODE_FOR_lshrv16qi3 },
  { 0x3e0042, CODE_FOR_lshrv8hi3 },
  { 0x3e0043, CODE_FOR_lshrv4si3 },
  { 0x3e0044, CODE_FOR_lshrv2di3 },
  { 0x3e0046, CODE_FOR_lshrv32qi3 },
  { 0x3e0047, CODE_FOR_lshrv16hi3 },
  { 0x3e0048, CODE_FOR_lshrv8si3 },
  { 0x3e0049, CODE_FOR_lshrv4di3 },
  { 0x3e004d, CODE_FOR_lshrv16si3 },
  { 0x3e004e, CODE_FOR_lshrv8di3 },
  { 0x3f000e, CODE_FOR_rotlqi3 },
  { 0x3f000f, CODE_FOR_rotlhi3 },
  { 0x3f0010, CODE_FOR_rotlsi3 },
  { 0x3f0011, CODE_FOR_rotldi3 },
  { 0x3f0012, CODE_FOR_rotlti3 },
  { 0x3f0041, CODE_FOR_rotlv16qi3 },
  { 0x3f0042, CODE_FOR_rotlv8hi3 },
  { 0x3f0043, CODE_FOR_rotlv4si3 },
  { 0x3f0044, CODE_FOR_rotlv2di3 },
  { 0x40000e, CODE_FOR_rotrqi3 },
  { 0x40000f, CODE_FOR_rotrhi3 },
  { 0x400010, CODE_FOR_rotrsi3 },
  { 0x400011, CODE_FOR_rotrdi3 },
  { 0x400012, CODE_FOR_rotrti3 },
  { 0x400041, CODE_FOR_rotrv16qi3 },
  { 0x400042, CODE_FOR_rotrv8hi3 },
  { 0x400043, CODE_FOR_rotrv4si3 },
  { 0x400044, CODE_FOR_rotrv2di3 },
  { 0x410041, CODE_FOR_vashlv16qi3 },
  { 0x410042, CODE_FOR_vashlv8hi3 },
  { 0x410043, CODE_FOR_vashlv4si3 },
  { 0x410044, CODE_FOR_vashlv2di3 },
  { 0x410048, CODE_FOR_vashlv8si3 },
  { 0x410049, CODE_FOR_vashlv4di3 },
  { 0x41004d, CODE_FOR_vashlv16si3 },
  { 0x41004e, CODE_FOR_vashlv8di3 },
  { 0x420041, CODE_FOR_vashrv16qi3 },
  { 0x420042, CODE_FOR_vashrv8hi3 },
  { 0x420043, CODE_FOR_vashrv4si3 },
  { 0x420044, CODE_FOR_vashrv2di3 },
  { 0x420048, CODE_FOR_vashrv8si3 },
  { 0x42004d, CODE_FOR_vashrv16si3 },
  { 0x430041, CODE_FOR_vlshrv16qi3 },
  { 0x430042, CODE_FOR_vlshrv8hi3 },
  { 0x430043, CODE_FOR_vlshrv4si3 },
  { 0x430044, CODE_FOR_vlshrv2di3 },
  { 0x430048, CODE_FOR_vlshrv8si3 },
  { 0x430049, CODE_FOR_vlshrv4di3 },
  { 0x43004d, CODE_FOR_vlshrv16si3 },
  { 0x43004e, CODE_FOR_vlshrv8di3 },
  { 0x440041, CODE_FOR_vrotlv16qi3 },
  { 0x440042, CODE_FOR_vrotlv8hi3 },
  { 0x440043, CODE_FOR_vrotlv4si3 },
  { 0x440044, CODE_FOR_vrotlv2di3 },
  { 0x450041, CODE_FOR_vrotrv16qi3 },
  { 0x450042, CODE_FOR_vrotrv8hi3 },
  { 0x450043, CODE_FOR_vrotrv4si3 },
  { 0x450044, CODE_FOR_vrotrv2di3 },
  { 0x460027, CODE_FOR_sminsf3 },
  { 0x460028, CODE_FOR_smindf3 },
  { 0x460041, CODE_FOR_sminv16qi3 },
  { 0x460042, CODE_FOR_sminv8hi3 },
  { 0x460043, CODE_FOR_sminv4si3 },
  { 0x460044, CODE_FOR_sminv2di3 },
  { 0x460046, CODE_FOR_sminv32qi3 },
  { 0x460047, CODE_FOR_sminv16hi3 },
  { 0x460048, CODE_FOR_sminv8si3 },
  { 0x460049, CODE_FOR_sminv4di3 },
  { 0x46004d, CODE_FOR_sminv16si3 },
  { 0x46004e, CODE_FOR_sminv8di3 },
  { 0x460056, CODE_FOR_sminv4sf3 },
  { 0x460057, CODE_FOR_sminv2df3 },
  { 0x460058, CODE_FOR_sminv8sf3 },
  { 0x460059, CODE_FOR_sminv4df3 },
  { 0x46005b, CODE_FOR_sminv16sf3 },
  { 0x46005c, CODE_FOR_sminv8df3 },
  { 0x470027, CODE_FOR_smaxsf3 },
  { 0x470028, CODE_FOR_smaxdf3 },
  { 0x470041, CODE_FOR_smaxv16qi3 },
  { 0x470042, CODE_FOR_smaxv8hi3 },
  { 0x470043, CODE_FOR_smaxv4si3 },
  { 0x470044, CODE_FOR_smaxv2di3 },
  { 0x470046, CODE_FOR_smaxv32qi3 },
  { 0x470047, CODE_FOR_smaxv16hi3 },
  { 0x470048, CODE_FOR_smaxv8si3 },
  { 0x470049, CODE_FOR_smaxv4di3 },
  { 0x47004d, CODE_FOR_smaxv16si3 },
  { 0x47004e, CODE_FOR_smaxv8di3 },
  { 0x470056, CODE_FOR_smaxv4sf3 },
  { 0x470057, CODE_FOR_smaxv2df3 },
  { 0x470058, CODE_FOR_smaxv8sf3 },
  { 0x470059, CODE_FOR_smaxv4df3 },
  { 0x47005b, CODE_FOR_smaxv16sf3 },
  { 0x47005c, CODE_FOR_smaxv8df3 },
  { 0x480041, CODE_FOR_uminv16qi3 },
  { 0x480042, CODE_FOR_uminv8hi3 },
  { 0x480043, CODE_FOR_uminv4si3 },
  { 0x480044, CODE_FOR_uminv2di3 },
  { 0x480046, CODE_FOR_uminv32qi3 },
  { 0x480047, CODE_FOR_uminv16hi3 },
  { 0x480048, CODE_FOR_uminv8si3 },
  { 0x480049, CODE_FOR_uminv4di3 },
  { 0x48004d, CODE_FOR_uminv16si3 },
  { 0x48004e, CODE_FOR_uminv8di3 },
  { 0x490041, CODE_FOR_umaxv16qi3 },
  { 0x490042, CODE_FOR_umaxv8hi3 },
  { 0x490043, CODE_FOR_umaxv4si3 },
  { 0x490044, CODE_FOR_umaxv2di3 },
  { 0x490046, CODE_FOR_umaxv32qi3 },
  { 0x490047, CODE_FOR_umaxv16hi3 },
  { 0x490048, CODE_FOR_umaxv8si3 },
  { 0x490049, CODE_FOR_umaxv4di3 },
  { 0x49004d, CODE_FOR_umaxv16si3 },
  { 0x49004e, CODE_FOR_umaxv8di3 },
  { 0x4a000e, CODE_FOR_negqi2 },
  { 0x4a000f, CODE_FOR_neghi2 },
  { 0x4a0010, CODE_FOR_negsi2 },
  { 0x4a0011, CODE_FOR_negdi2 },
  { 0x4a0012, CODE_FOR_negti2 },
  { 0x4a0027, CODE_FOR_negsf2 },
  { 0x4a0028, CODE_FOR_negdf2 },
  { 0x4a0029, CODE_FOR_negxf2 },
  { 0x4a002a, CODE_FOR_negtf2 },
  { 0x4a0041, CODE_FOR_negv16qi2 },
  { 0x4a0042, CODE_FOR_negv8hi2 },
  { 0x4a0043, CODE_FOR_negv4si2 },
  { 0x4a0044, CODE_FOR_negv2di2 },
  { 0x4a0046, CODE_FOR_negv32qi2 },
  { 0x4a0047, CODE_FOR_negv16hi2 },
  { 0x4a0048, CODE_FOR_negv8si2 },
  { 0x4a0049, CODE_FOR_negv4di2 },
  { 0x4a004d, CODE_FOR_negv16si2 },
  { 0x4a004e, CODE_FOR_negv8di2 },
  { 0x4a0056, CODE_FOR_negv4sf2 },
  { 0x4a0057, CODE_FOR_negv2df2 },
  { 0x4a0058, CODE_FOR_negv8sf2 },
  { 0x4a0059, CODE_FOR_negv4df2 },
  { 0x4a005b, CODE_FOR_negv16sf2 },
  { 0x4a005c, CODE_FOR_negv8df2 },
  { 0x4e0027, CODE_FOR_abssf2 },
  { 0x4e0028, CODE_FOR_absdf2 },
  { 0x4e0029, CODE_FOR_absxf2 },
  { 0x4e002a, CODE_FOR_abstf2 },
  { 0x4e003d, CODE_FOR_absv8qi2 },
  { 0x4e003e, CODE_FOR_absv4hi2 },
  { 0x4e003f, CODE_FOR_absv2si2 },
  { 0x4e0041, CODE_FOR_absv16qi2 },
  { 0x4e0042, CODE_FOR_absv8hi2 },
  { 0x4e0043, CODE_FOR_absv4si2 },
  { 0x4e0046, CODE_FOR_absv32qi2 },
  { 0x4e0047, CODE_FOR_absv16hi2 },
  { 0x4e0048, CODE_FOR_absv8si2 },
  { 0x4e004d, CODE_FOR_absv16si2 },
  { 0x4e004e, CODE_FOR_absv8di2 },
  { 0x4e0056, CODE_FOR_absv4sf2 },
  { 0x4e0057, CODE_FOR_absv2df2 },
  { 0x4e0058, CODE_FOR_absv8sf2 },
  { 0x4e0059, CODE_FOR_absv4df2 },
  { 0x4e005b, CODE_FOR_absv16sf2 },
  { 0x4e005c, CODE_FOR_absv8df2 },
  { 0x50000e, CODE_FOR_one_cmplqi2 },
  { 0x50000f, CODE_FOR_one_cmplhi2 },
  { 0x500010, CODE_FOR_one_cmplsi2 },
  { 0x500011, CODE_FOR_one_cmpldi2 },
  { 0x500041, CODE_FOR_one_cmplv16qi2 },
  { 0x500042, CODE_FOR_one_cmplv8hi2 },
  { 0x500043, CODE_FOR_one_cmplv4si2 },
  { 0x500044, CODE_FOR_one_cmplv2di2 },
  { 0x500046, CODE_FOR_one_cmplv32qi2 },
  { 0x500047, CODE_FOR_one_cmplv16hi2 },
  { 0x500048, CODE_FOR_one_cmplv8si2 },
  { 0x500049, CODE_FOR_one_cmplv4di2 },
  { 0x50004d, CODE_FOR_one_cmplv16si2 },
  { 0x50004e, CODE_FOR_one_cmplv8di2 },
  { 0x510010, CODE_FOR_bswapsi2 },
  { 0x510011, CODE_FOR_bswapdi2 },
  { 0x520010, CODE_FOR_ffssi2 },
  { 0x520011, CODE_FOR_ffsdi2 },
  { 0x53000f, CODE_FOR_clzhi2 },
  { 0x530010, CODE_FOR_clzsi2 },
  { 0x530011, CODE_FOR_clzdi2 },
  { 0x53004d, CODE_FOR_clzv16si2 },
  { 0x53004e, CODE_FOR_clzv8di2 },
  { 0x54000f, CODE_FOR_ctzhi2 },
  { 0x540010, CODE_FOR_ctzsi2 },
  { 0x540011, CODE_FOR_ctzdi2 },
  { 0x56000f, CODE_FOR_popcounthi2 },
  { 0x560010, CODE_FOR_popcountsi2 },
  { 0x560011, CODE_FOR_popcountdi2 },
  { 0x570010, CODE_FOR_paritysi2 },
  { 0x570011, CODE_FOR_paritydi2 },
  { 0x620027, CODE_FOR_sqrtsf2 },
  { 0x620028, CODE_FOR_sqrtdf2 },
  { 0x620029, CODE_FOR_sqrtxf2 },
  { 0x620056, CODE_FOR_sqrtv4sf2 },
  { 0x620057, CODE_FOR_sqrtv2df2 },
  { 0x620058, CODE_FOR_sqrtv8sf2 },
  { 0x620059, CODE_FOR_sqrtv4df2 },
  { 0x62005b, CODE_FOR_sqrtv16sf2 },
  { 0x62005c, CODE_FOR_sqrtv8df2 },
  { 0x71000e, CODE_FOR_movqi },
  { 0x71000f, CODE_FOR_movhi },
  { 0x710010, CODE_FOR_movsi },
  { 0x710011, CODE_FOR_movdi },
  { 0x710012, CODE_FOR_movti },
  { 0x710013, CODE_FOR_movoi },
  { 0x710014, CODE_FOR_movxi },
  { 0x710027, CODE_FOR_movsf },
  { 0x710028, CODE_FOR_movdf },
  { 0x710029, CODE_FOR_movxf },
  { 0x71002a, CODE_FOR_movtf },
  { 0x710031, CODE_FOR_movcdi },
  { 0x71003d, CODE_FOR_movv8qi },
  { 0x71003e, CODE_FOR_movv4hi },
  { 0x71003f, CODE_FOR_movv2si },
  { 0x710040, CODE_FOR_movv1di },
  { 0x710041, CODE_FOR_movv16qi },
  { 0x710042, CODE_FOR_movv8hi },
  { 0x710043, CODE_FOR_movv4si },
  { 0x710044, CODE_FOR_movv2di },
  { 0x710045, CODE_FOR_movv1ti },
  { 0x710046, CODE_FOR_movv32qi },
  { 0x710047, CODE_FOR_movv16hi },
  { 0x710048, CODE_FOR_movv8si },
  { 0x710049, CODE_FOR_movv4di },
  { 0x71004a, CODE_FOR_movv2ti },
  { 0x71004b, CODE_FOR_movv64qi },
  { 0x71004c, CODE_FOR_movv32hi },
  { 0x71004d, CODE_FOR_movv16si },
  { 0x71004e, CODE_FOR_movv8di },
  { 0x710055, CODE_FOR_movv2sf },
  { 0x710056, CODE_FOR_movv4sf },
  { 0x710057, CODE_FOR_movv2df },
  { 0x710058, CODE_FOR_movv8sf },
  { 0x710059, CODE_FOR_movv4df },
  { 0x71005b, CODE_FOR_movv16sf },
  { 0x71005c, CODE_FOR_movv8df },
  { 0x72000e, CODE_FOR_movstrictqi },
  { 0x72000f, CODE_FOR_movstricthi },
  { 0x73003d, CODE_FOR_movmisalignv8qi },
  { 0x73003e, CODE_FOR_movmisalignv4hi },
  { 0x73003f, CODE_FOR_movmisalignv2si },
  { 0x730040, CODE_FOR_movmisalignv1di },
  { 0x730041, CODE_FOR_movmisalignv16qi },
  { 0x730042, CODE_FOR_movmisalignv8hi },
  { 0x730043, CODE_FOR_movmisalignv4si },
  { 0x730044, CODE_FOR_movmisalignv2di },
  { 0x730045, CODE_FOR_movmisalignv1ti },
  { 0x730046, CODE_FOR_movmisalignv32qi },
  { 0x730047, CODE_FOR_movmisalignv16hi },
  { 0x730048, CODE_FOR_movmisalignv8si },
  { 0x730049, CODE_FOR_movmisalignv4di },
  { 0x73004a, CODE_FOR_movmisalignv2ti },
  { 0x73004b, CODE_FOR_movmisalignv64qi },
  { 0x73004c, CODE_FOR_movmisalignv32hi },
  { 0x73004d, CODE_FOR_movmisalignv16si },
  { 0x73004e, CODE_FOR_movmisalignv8di },
  { 0x730055, CODE_FOR_movmisalignv2sf },
  { 0x730056, CODE_FOR_movmisalignv4sf },
  { 0x730057, CODE_FOR_movmisalignv2df },
  { 0x730058, CODE_FOR_movmisalignv8sf },
  { 0x730059, CODE_FOR_movmisalignv4df },
  { 0x73005b, CODE_FOR_movmisalignv16sf },
  { 0x73005c, CODE_FOR_movmisalignv8df },
  { 0x740010, CODE_FOR_storentsi },
  { 0x740011, CODE_FOR_storentdi },
  { 0x740027, CODE_FOR_storentsf },
  { 0x740028, CODE_FOR_storentdf },
  { 0x740044, CODE_FOR_storentv2di },
  { 0x740049, CODE_FOR_storentv4di },
  { 0x74004e, CODE_FOR_storentv8di },
  { 0x740056, CODE_FOR_storentv4sf },
  { 0x740057, CODE_FOR_storentv2df },
  { 0x740058, CODE_FOR_storentv8sf },
  { 0x740059, CODE_FOR_storentv4df },
  { 0x74005b, CODE_FOR_storentv16sf },
  { 0x74005c, CODE_FOR_storentv8df },
  { 0x7e0002, CODE_FOR_cbranchcc4 },
  { 0x7e000e, CODE_FOR_cbranchqi4 },
  { 0x7e000f, CODE_FOR_cbranchhi4 },
  { 0x7e0010, CODE_FOR_cbranchsi4 },
  { 0x7e0011, CODE_FOR_cbranchdi4 },
  { 0x7e0012, CODE_FOR_cbranchti4 },
  { 0x7e0027, CODE_FOR_cbranchsf4 },
  { 0x7e0028, CODE_FOR_cbranchdf4 },
  { 0x7e0029, CODE_FOR_cbranchxf4 },
  { 0x7f000e, CODE_FOR_addqicc },
  { 0x7f000f, CODE_FOR_addhicc },
  { 0x7f0010, CODE_FOR_addsicc },
  { 0x7f0011, CODE_FOR_adddicc },
  { 0x80000e, CODE_FOR_movqicc },
  { 0x80000f, CODE_FOR_movhicc },
  { 0x800010, CODE_FOR_movsicc },
  { 0x800011, CODE_FOR_movdicc },
  { 0x800027, CODE_FOR_movsfcc },
  { 0x800028, CODE_FOR_movdfcc },
  { 0x800029, CODE_FOR_movxfcc },
  { 0x820002, CODE_FOR_cstorecc4 },
  { 0x82000e, CODE_FOR_cstoreqi4 },
  { 0x82000f, CODE_FOR_cstorehi4 },
  { 0x820010, CODE_FOR_cstoresi4 },
  { 0x820011, CODE_FOR_cstoredi4 },
  { 0x820027, CODE_FOR_cstoresf4 },
  { 0x820028, CODE_FOR_cstoredf4 },
  { 0x820029, CODE_FOR_cstorexf4 },
  { 0x84000e, CODE_FOR_addvqi4 },
  { 0x84000f, CODE_FOR_addvhi4 },
  { 0x840010, CODE_FOR_addvsi4 },
  { 0x840011, CODE_FOR_addvdi4 },
  { 0x85000e, CODE_FOR_subvqi4 },
  { 0x85000f, CODE_FOR_subvhi4 },
  { 0x850010, CODE_FOR_subvsi4 },
  { 0x850011, CODE_FOR_subvdi4 },
  { 0x860010, CODE_FOR_mulvsi4 },
  { 0x860011, CODE_FOR_mulvdi4 },
  { 0x87000e, CODE_FOR_negvqi3 },
  { 0x87000f, CODE_FOR_negvhi3 },
  { 0x870010, CODE_FOR_negvsi3 },
  { 0x870011, CODE_FOR_negvdi3 },
  { 0x890010, CODE_FOR_smulsi3_highpart },
  { 0x890011, CODE_FOR_smuldi3_highpart },
  { 0x890042, CODE_FOR_smulv8hi3_highpart },
  { 0x890047, CODE_FOR_smulv16hi3_highpart },
  { 0x8a0010, CODE_FOR_umulsi3_highpart },
  { 0x8a0011, CODE_FOR_umuldi3_highpart },
  { 0x8a0042, CODE_FOR_umulv8hi3_highpart },
  { 0x8a0047, CODE_FOR_umulv16hi3_highpart },
  { 0x8d0010, CODE_FOR_cmpstrnsi },
  { 0x8e0010, CODE_FOR_movmemsi },
  { 0x8e0011, CODE_FOR_movmemdi },
  { 0x8f0010, CODE_FOR_setmemsi },
  { 0x8f0011, CODE_FOR_setmemdi },
  { 0x900010, CODE_FOR_strlensi },
  { 0x900011, CODE_FOR_strlendi },
  { 0x910027, CODE_FOR_fmasf4 },
  { 0x910028, CODE_FOR_fmadf4 },
  { 0x910056, CODE_FOR_fmav4sf4 },
  { 0x910057, CODE_FOR_fmav2df4 },
  { 0x910058, CODE_FOR_fmav8sf4 },
  { 0x910059, CODE_FOR_fmav4df4 },
  { 0x91005b, CODE_FOR_fmav16sf4 },
  { 0x91005c, CODE_FOR_fmav8df4 },
  { 0x920027, CODE_FOR_fmssf4 },
  { 0x920028, CODE_FOR_fmsdf4 },
  { 0x920056, CODE_FOR_fmsv4sf4 },
  { 0x920057, CODE_FOR_fmsv2df4 },
  { 0x920058, CODE_FOR_fmsv8sf4 },
  { 0x920059, CODE_FOR_fmsv4df4 },
  { 0x92005b, CODE_FOR_fmsv16sf4 },
  { 0x92005c, CODE_FOR_fmsv8df4 },
  { 0x930027, CODE_FOR_fnmasf4 },
  { 0x930028, CODE_FOR_fnmadf4 },
  { 0x930056, CODE_FOR_fnmav4sf4 },
  { 0x930057, CODE_FOR_fnmav2df4 },
  { 0x930058, CODE_FOR_fnmav8sf4 },
  { 0x930059, CODE_FOR_fnmav4df4 },
  { 0x93005b, CODE_FOR_fnmav16sf4 },
  { 0x93005c, CODE_FOR_fnmav8df4 },
  { 0x940027, CODE_FOR_fnmssf4 },
  { 0x940028, CODE_FOR_fnmsdf4 },
  { 0x940056, CODE_FOR_fnmsv4sf4 },
  { 0x940057, CODE_FOR_fnmsv2df4 },
  { 0x940058, CODE_FOR_fnmsv8sf4 },
  { 0x940059, CODE_FOR_fnmsv4df4 },
  { 0x94005b, CODE_FOR_fnmsv16sf4 },
  { 0x94005c, CODE_FOR_fnmsv8df4 },
  { 0x950027, CODE_FOR_rintsf2 },
  { 0x950028, CODE_FOR_rintdf2 },
  { 0x950029, CODE_FOR_rintxf2 },
  { 0x960027, CODE_FOR_roundsf2 },
  { 0x960028, CODE_FOR_rounddf2 },
  { 0x960029, CODE_FOR_roundxf2 },
  { 0x960056, CODE_FOR_roundv4sf2 },
  { 0x960057, CODE_FOR_roundv2df2 },
  { 0x960058, CODE_FOR_roundv8sf2 },
  { 0x960059, CODE_FOR_roundv4df2 },
  { 0x96005b, CODE_FOR_roundv16sf2 },
  { 0x96005c, CODE_FOR_roundv8df2 },
  { 0x970027, CODE_FOR_floorsf2 },
  { 0x970028, CODE_FOR_floordf2 },
  { 0x970029, CODE_FOR_floorxf2 },
  { 0x980027, CODE_FOR_ceilsf2 },
  { 0x980028, CODE_FOR_ceildf2 },
  { 0x980029, CODE_FOR_ceilxf2 },
  { 0x990027, CODE_FOR_btruncsf2 },
  { 0x990028, CODE_FOR_btruncdf2 },
  { 0x990029, CODE_FOR_btruncxf2 },
  { 0x9a0027, CODE_FOR_nearbyintsf2 },
  { 0x9a0028, CODE_FOR_nearbyintdf2 },
  { 0x9a0029, CODE_FOR_nearbyintxf2 },
  { 0x9b0027, CODE_FOR_acossf2 },
  { 0x9b0028, CODE_FOR_acosdf2 },
  { 0x9b0029, CODE_FOR_acosxf2 },
  { 0x9c0027, CODE_FOR_asinsf2 },
  { 0x9c0028, CODE_FOR_asindf2 },
  { 0x9c0029, CODE_FOR_asinxf2 },
  { 0x9d0027, CODE_FOR_atan2sf3 },
  { 0x9d0028, CODE_FOR_atan2df3 },
  { 0x9d0029, CODE_FOR_atan2xf3 },
  { 0x9e0027, CODE_FOR_atansf2 },
  { 0x9e0028, CODE_FOR_atandf2 },
  { 0x9e0029, CODE_FOR_atanxf2 },
  { 0x9f0027, CODE_FOR_copysignsf3 },
  { 0x9f0028, CODE_FOR_copysigndf3 },
  { 0x9f002a, CODE_FOR_copysigntf3 },
  { 0x9f0056, CODE_FOR_copysignv4sf3 },
  { 0x9f0057, CODE_FOR_copysignv2df3 },
  { 0x9f0058, CODE_FOR_copysignv8sf3 },
  { 0x9f0059, CODE_FOR_copysignv4df3 },
  { 0x9f005b, CODE_FOR_copysignv16sf3 },
  { 0x9f005c, CODE_FOR_copysignv8df3 },
  { 0xa10027, CODE_FOR_exp10sf2 },
  { 0xa10028, CODE_FOR_exp10df2 },
  { 0xa10029, CODE_FOR_exp10xf2 },
  { 0xa20027, CODE_FOR_exp2sf2 },
  { 0xa20028, CODE_FOR_exp2df2 },
  { 0xa20029, CODE_FOR_exp2xf2 },
  { 0xa30027, CODE_FOR_expsf2 },
  { 0xa30028, CODE_FOR_expdf2 },
  { 0xa30029, CODE_FOR_expxf2 },
  { 0xa40027, CODE_FOR_expm1sf2 },
  { 0xa40028, CODE_FOR_expm1df2 },
  { 0xa40029, CODE_FOR_expm1xf2 },
  { 0xa50027, CODE_FOR_fmodsf3 },
  { 0xa50028, CODE_FOR_fmoddf3 },
  { 0xa50029, CODE_FOR_fmodxf3 },
  { 0xa60027, CODE_FOR_ilogbsf2 },
  { 0xa60028, CODE_FOR_ilogbdf2 },
  { 0xa60029, CODE_FOR_ilogbxf2 },
  { 0xa70027, CODE_FOR_isinfsf2 },
  { 0xa70028, CODE_FOR_isinfdf2 },
  { 0xa70029, CODE_FOR_isinfxf2 },
  { 0xa80027, CODE_FOR_ldexpsf3 },
  { 0xa80028, CODE_FOR_ldexpdf3 },
  { 0xa80029, CODE_FOR_ldexpxf3 },
  { 0xa90027, CODE_FOR_log10sf2 },
  { 0xa90028, CODE_FOR_log10df2 },
  { 0xa90029, CODE_FOR_log10xf2 },
  { 0xaa0027, CODE_FOR_log1psf2 },
  { 0xaa0028, CODE_FOR_log1pdf2 },
  { 0xaa0029, CODE_FOR_log1pxf2 },
  { 0xab0027, CODE_FOR_log2sf2 },
  { 0xab0028, CODE_FOR_log2df2 },
  { 0xab0029, CODE_FOR_log2xf2 },
  { 0xac0027, CODE_FOR_logsf2 },
  { 0xac0028, CODE_FOR_logdf2 },
  { 0xac0029, CODE_FOR_logxf2 },
  { 0xad0027, CODE_FOR_logbsf2 },
  { 0xad0028, CODE_FOR_logbdf2 },
  { 0xad0029, CODE_FOR_logbxf2 },
  { 0xaf0027, CODE_FOR_remaindersf3 },
  { 0xaf0028, CODE_FOR_remainderdf3 },
  { 0xaf0029, CODE_FOR_remainderxf3 },
  { 0xb00027, CODE_FOR_scalbsf3 },
  { 0xb00028, CODE_FOR_scalbdf3 },
  { 0xb00029, CODE_FOR_scalbxf3 },
  { 0xb10027, CODE_FOR_signbitsf2 },
  { 0xb10028, CODE_FOR_signbitdf2 },
  { 0xb10029, CODE_FOR_signbitxf2 },
  { 0xb20027, CODE_FOR_significandsf2 },
  { 0xb20028, CODE_FOR_significanddf2 },
  { 0xb20029, CODE_FOR_significandxf2 },
  { 0xb40027, CODE_FOR_sincossf3 },
  { 0xb40028, CODE_FOR_sincosdf3 },
  { 0xb40029, CODE_FOR_sincosxf3 },
  { 0xb50027, CODE_FOR_tansf2 },
  { 0xb50028, CODE_FOR_tandf2 },
  { 0xb50029, CODE_FOR_tanxf2 },
  { 0xb60046, CODE_FOR_reduc_smax_v32qi },
  { 0xb60047, CODE_FOR_reduc_smax_v16hi },
  { 0xb60048, CODE_FOR_reduc_smax_v8si },
  { 0xb60049, CODE_FOR_reduc_smax_v4di },
  { 0xb6004d, CODE_FOR_reduc_smax_v16si },
  { 0xb6004e, CODE_FOR_reduc_smax_v8di },
  { 0xb60056, CODE_FOR_reduc_smax_v4sf },
  { 0xb60058, CODE_FOR_reduc_smax_v8sf },
  { 0xb60059, CODE_FOR_reduc_smax_v4df },
  { 0xb6005b, CODE_FOR_reduc_smax_v16sf },
  { 0xb6005c, CODE_FOR_reduc_smax_v8df },
  { 0xb70046, CODE_FOR_reduc_smin_v32qi },
  { 0xb70047, CODE_FOR_reduc_smin_v16hi },
  { 0xb70048, CODE_FOR_reduc_smin_v8si },
  { 0xb70049, CODE_FOR_reduc_smin_v4di },
  { 0xb7004d, CODE_FOR_reduc_smin_v16si },
  { 0xb7004e, CODE_FOR_reduc_smin_v8di },
  { 0xb70056, CODE_FOR_reduc_smin_v4sf },
  { 0xb70058, CODE_FOR_reduc_smin_v8sf },
  { 0xb70059, CODE_FOR_reduc_smin_v4df },
  { 0xb7005b, CODE_FOR_reduc_smin_v16sf },
  { 0xb7005c, CODE_FOR_reduc_smin_v8df },
  { 0xb80056, CODE_FOR_reduc_splus_v4sf },
  { 0xb80057, CODE_FOR_reduc_splus_v2df },
  { 0xb80058, CODE_FOR_reduc_splus_v8sf },
  { 0xb80059, CODE_FOR_reduc_splus_v4df },
  { 0xb8005b, CODE_FOR_reduc_splus_v16sf },
  { 0xb8005c, CODE_FOR_reduc_splus_v8df },
  { 0xb90046, CODE_FOR_reduc_umax_v32qi },
  { 0xb90047, CODE_FOR_reduc_umax_v16hi },
  { 0xb90048, CODE_FOR_reduc_umax_v8si },
  { 0xb90049, CODE_FOR_reduc_umax_v4di },
  { 0xb9004d, CODE_FOR_reduc_umax_v16si },
  { 0xb9004e, CODE_FOR_reduc_umax_v8di },
  { 0xba0042, CODE_FOR_reduc_umin_v8hi },
  { 0xba0046, CODE_FOR_reduc_umin_v32qi },
  { 0xba0047, CODE_FOR_reduc_umin_v16hi },
  { 0xba0048, CODE_FOR_reduc_umin_v8si },
  { 0xba0049, CODE_FOR_reduc_umin_v4di },
  { 0xba004d, CODE_FOR_reduc_umin_v16si },
  { 0xba004e, CODE_FOR_reduc_umin_v8di },
  { 0xbc0042, CODE_FOR_sdot_prodv8hi },
  { 0xbc0043, CODE_FOR_sdot_prodv4si },
  { 0xbc0047, CODE_FOR_sdot_prodv16hi },
  { 0xc00043, CODE_FOR_maskloadv4si },
  { 0xc00044, CODE_FOR_maskloadv2di },
  { 0xc00048, CODE_FOR_maskloadv8si },
  { 0xc00049, CODE_FOR_maskloadv4di },
  { 0xc00056, CODE_FOR_maskloadv4sf },
  { 0xc00057, CODE_FOR_maskloadv2df },
  { 0xc00058, CODE_FOR_maskloadv8sf },
  { 0xc00059, CODE_FOR_maskloadv4df },
  { 0xc10043, CODE_FOR_maskstorev4si },
  { 0xc10044, CODE_FOR_maskstorev2di },
  { 0xc10048, CODE_FOR_maskstorev8si },
  { 0xc10049, CODE_FOR_maskstorev4di },
  { 0xc10056, CODE_FOR_maskstorev4sf },
  { 0xc10057, CODE_FOR_maskstorev2df },
  { 0xc10058, CODE_FOR_maskstorev8sf },
  { 0xc10059, CODE_FOR_maskstorev4df },
  { 0xc2003d, CODE_FOR_vec_extractv8qi },
  { 0xc2003e, CODE_FOR_vec_extractv4hi },
  { 0xc2003f, CODE_FOR_vec_extractv2si },
  { 0xc20041, CODE_FOR_vec_extractv16qi },
  { 0xc20042, CODE_FOR_vec_extractv8hi },
  { 0xc20043, CODE_FOR_vec_extractv4si },
  { 0xc20044, CODE_FOR_vec_extractv2di },
  { 0xc20046, CODE_FOR_vec_extractv32qi },
  { 0xc20047, CODE_FOR_vec_extractv16hi },
  { 0xc20048, CODE_FOR_vec_extractv8si },
  { 0xc20049, CODE_FOR_vec_extractv4di },
  { 0xc2004d, CODE_FOR_vec_extractv16si },
  { 0xc2004e, CODE_FOR_vec_extractv8di },
  { 0xc20055, CODE_FOR_vec_extractv2sf },
  { 0xc20056, CODE_FOR_vec_extractv4sf },
  { 0xc20057, CODE_FOR_vec_extractv2df },
  { 0xc20058, CODE_FOR_vec_extractv8sf },
  { 0xc20059, CODE_FOR_vec_extractv4df },
  { 0xc2005b, CODE_FOR_vec_extractv16sf },
  { 0xc2005c, CODE_FOR_vec_extractv8df },
  { 0xc3003d, CODE_FOR_vec_initv8qi },
  { 0xc3003e, CODE_FOR_vec_initv4hi },
  { 0xc3003f, CODE_FOR_vec_initv2si },
  { 0xc30041, CODE_FOR_vec_initv16qi },
  { 0xc30042, CODE_FOR_vec_initv8hi },
  { 0xc30043, CODE_FOR_vec_initv4si },
  { 0xc30044, CODE_FOR_vec_initv2di },
  { 0xc30046, CODE_FOR_vec_initv32qi },
  { 0xc30047, CODE_FOR_vec_initv16hi },
  { 0xc30048, CODE_FOR_vec_initv8si },
  { 0xc30049, CODE_FOR_vec_initv4di },
  { 0xc3004d, CODE_FOR_vec_initv16si },
  { 0xc3004e, CODE_FOR_vec_initv8di },
  { 0xc30055, CODE_FOR_vec_initv2sf },
  { 0xc30056, CODE_FOR_vec_initv4sf },
  { 0xc30057, CODE_FOR_vec_initv2df },
  { 0xc30058, CODE_FOR_vec_initv8sf },
  { 0xc30059, CODE_FOR_vec_initv4df },
  { 0xc3005b, CODE_FOR_vec_initv16sf },
  { 0xc3005c, CODE_FOR_vec_initv8df },
  { 0xc40057, CODE_FOR_vec_pack_sfix_trunc_v2df },
  { 0xc40059, CODE_FOR_vec_pack_sfix_trunc_v4df },
  { 0xc4005c, CODE_FOR_vec_pack_sfix_trunc_v8df },
  { 0xc60042, CODE_FOR_vec_pack_trunc_v8hi },
  { 0xc60043, CODE_FOR_vec_pack_trunc_v4si },
  { 0xc60044, CODE_FOR_vec_pack_trunc_v2di },
  { 0xc60047, CODE_FOR_vec_pack_trunc_v16hi },
  { 0xc60048, CODE_FOR_vec_pack_trunc_v8si },
  { 0xc60049, CODE_FOR_vec_pack_trunc_v4di },
  { 0xc6004e, CODE_FOR_vec_pack_trunc_v8di },
  { 0xc60057, CODE_FOR_vec_pack_trunc_v2df },
  { 0xc60059, CODE_FOR_vec_pack_trunc_v4df },
  { 0xc6005c, CODE_FOR_vec_pack_trunc_v8df },
  { 0xc70057, CODE_FOR_vec_pack_ufix_trunc_v2df },
  { 0xc70059, CODE_FOR_vec_pack_ufix_trunc_v4df },
  { 0xc7005c, CODE_FOR_vec_pack_ufix_trunc_v8df },
  { 0xc90041, CODE_FOR_vec_perm_constv16qi },
  { 0xc90042, CODE_FOR_vec_perm_constv8hi },
  { 0xc90043, CODE_FOR_vec_perm_constv4si },
  { 0xc90044, CODE_FOR_vec_perm_constv2di },
  { 0xc90046, CODE_FOR_vec_perm_constv32qi },
  { 0xc90047, CODE_FOR_vec_perm_constv16hi },
  { 0xc90048, CODE_FOR_vec_perm_constv8si },
  { 0xc90049, CODE_FOR_vec_perm_constv4di },
  { 0xc9004d, CODE_FOR_vec_perm_constv16si },
  { 0xc9004e, CODE_FOR_vec_perm_constv8di },
  { 0xc90056, CODE_FOR_vec_perm_constv4sf },
  { 0xc90057, CODE_FOR_vec_perm_constv2df },
  { 0xc90058, CODE_FOR_vec_perm_constv8sf },
  { 0xc90059, CODE_FOR_vec_perm_constv4df },
  { 0xc9005b, CODE_FOR_vec_perm_constv16sf },
  { 0xc9005c, CODE_FOR_vec_perm_constv8df },
  { 0xca0041, CODE_FOR_vec_permv16qi },
  { 0xca0042, CODE_FOR_vec_permv8hi },
  { 0xca0043, CODE_FOR_vec_permv4si },
  { 0xca0044, CODE_FOR_vec_permv2di },
  { 0xca0046, CODE_FOR_vec_permv32qi },
  { 0xca0047, CODE_FOR_vec_permv16hi },
  { 0xca0048, CODE_FOR_vec_permv8si },
  { 0xca0049, CODE_FOR_vec_permv4di },
  { 0xca004d, CODE_FOR_vec_permv16si },
  { 0xca004e, CODE_FOR_vec_permv8di },
  { 0xca0056, CODE_FOR_vec_permv4sf },
  { 0xca0057, CODE_FOR_vec_permv2df },
  { 0xca0058, CODE_FOR_vec_permv8sf },
  { 0xca0059, CODE_FOR_vec_permv4df },
  { 0xca005b, CODE_FOR_vec_permv16sf },
  { 0xca005c, CODE_FOR_vec_permv8df },
  { 0xcc003d, CODE_FOR_vec_setv8qi },
  { 0xcc003e, CODE_FOR_vec_setv4hi },
  { 0xcc003f, CODE_FOR_vec_setv2si },
  { 0xcc0041, CODE_FOR_vec_setv16qi },
  { 0xcc0042, CODE_FOR_vec_setv8hi },
  { 0xcc0043, CODE_FOR_vec_setv4si },
  { 0xcc0044, CODE_FOR_vec_setv2di },
  { 0xcc0046, CODE_FOR_vec_setv32qi },
  { 0xcc0047, CODE_FOR_vec_setv16hi },
  { 0xcc0048, CODE_FOR_vec_setv8si },
  { 0xcc0049, CODE_FOR_vec_setv4di },
  { 0xcc004d, CODE_FOR_vec_setv16si },
  { 0xcc004e, CODE_FOR_vec_setv8di },
  { 0xcc0055, CODE_FOR_vec_setv2sf },
  { 0xcc0056, CODE_FOR_vec_setv4sf },
  { 0xcc0057, CODE_FOR_vec_setv2df },
  { 0xcc0058, CODE_FOR_vec_setv8sf },
  { 0xcc0059, CODE_FOR_vec_setv4df },
  { 0xcc005b, CODE_FOR_vec_setv16sf },
  { 0xcc005c, CODE_FOR_vec_setv8df },
  { 0xcd0041, CODE_FOR_vec_shl_v16qi },
  { 0xcd0042, CODE_FOR_vec_shl_v8hi },
  { 0xcd0043, CODE_FOR_vec_shl_v4si },
  { 0xcd0044, CODE_FOR_vec_shl_v2di },
  { 0xce0041, CODE_FOR_vec_shr_v16qi },
  { 0xce0042, CODE_FOR_vec_shr_v8hi },
  { 0xce0043, CODE_FOR_vec_shr_v4si },
  { 0xce0044, CODE_FOR_vec_shr_v2di },
  { 0xcf0042, CODE_FOR_vec_unpacks_float_hi_v8hi },
  { 0xcf0043, CODE_FOR_vec_unpacks_float_hi_v4si },
  { 0xcf0047, CODE_FOR_vec_unpacks_float_hi_v16hi },
  { 0xcf0048, CODE_FOR_vec_unpacks_float_hi_v8si },
  { 0xcf004c, CODE_FOR_vec_unpacks_float_hi_v32hi },
  { 0xcf004d, CODE_FOR_vec_unpacks_float_hi_v16si },
  { 0xd00042, CODE_FOR_vec_unpacks_float_lo_v8hi },
  { 0xd00043, CODE_FOR_vec_unpacks_float_lo_v4si },
  { 0xd00047, CODE_FOR_vec_unpacks_float_lo_v16hi },
  { 0xd00048, CODE_FOR_vec_unpacks_float_lo_v8si },
  { 0xd0004c, CODE_FOR_vec_unpacks_float_lo_v32hi },
  { 0xd0004d, CODE_FOR_vec_unpacks_float_lo_v16si },
  { 0xd10041, CODE_FOR_vec_unpacks_hi_v16qi },
  { 0xd10042, CODE_FOR_vec_unpacks_hi_v8hi },
  { 0xd10043, CODE_FOR_vec_unpacks_hi_v4si },
  { 0xd10046, CODE_FOR_vec_unpacks_hi_v32qi },
  { 0xd10047, CODE_FOR_vec_unpacks_hi_v16hi },
  { 0xd10048, CODE_FOR_vec_unpacks_hi_v8si },
  { 0xd1004c, CODE_FOR_vec_unpacks_hi_v32hi },
  { 0xd1004d, CODE_FOR_vec_unpacks_hi_v16si },
  { 0xd10056, CODE_FOR_vec_unpacks_hi_v4sf },
  { 0xd10058, CODE_FOR_vec_unpacks_hi_v8sf },
  { 0xd1005b, CODE_FOR_vec_unpacks_hi_v16sf },
  { 0xd20041, CODE_FOR_vec_unpacks_lo_v16qi },
  { 0xd20042, CODE_FOR_vec_unpacks_lo_v8hi },
  { 0xd20043, CODE_FOR_vec_unpacks_lo_v4si },
  { 0xd20046, CODE_FOR_vec_unpacks_lo_v32qi },
  { 0xd20047, CODE_FOR_vec_unpacks_lo_v16hi },
  { 0xd20048, CODE_FOR_vec_unpacks_lo_v8si },
  { 0xd2004c, CODE_FOR_vec_unpacks_lo_v32hi },
  { 0xd2004d, CODE_FOR_vec_unpacks_lo_v16si },
  { 0xd20056, CODE_FOR_vec_unpacks_lo_v4sf },
  { 0xd20058, CODE_FOR_vec_unpacks_lo_v8sf },
  { 0xd2005b, CODE_FOR_vec_unpacks_lo_v16sf },
  { 0xd30042, CODE_FOR_vec_unpacku_float_hi_v8hi },
  { 0xd30043, CODE_FOR_vec_unpacku_float_hi_v4si },
  { 0xd30047, CODE_FOR_vec_unpacku_float_hi_v16hi },
  { 0xd30048, CODE_FOR_vec_unpacku_float_hi_v8si },
  { 0xd3004c, CODE_FOR_vec_unpacku_float_hi_v32hi },
  { 0xd3004d, CODE_FOR_vec_unpacku_float_hi_v16si },
  { 0xd40042, CODE_FOR_vec_unpacku_float_lo_v8hi },
  { 0xd40043, CODE_FOR_vec_unpacku_float_lo_v4si },
  { 0xd40047, CODE_FOR_vec_unpacku_float_lo_v16hi },
  { 0xd40048, CODE_FOR_vec_unpacku_float_lo_v8si },
  { 0xd4004c, CODE_FOR_vec_unpacku_float_lo_v32hi },
  { 0xd4004d, CODE_FOR_vec_unpacku_float_lo_v16si },
  { 0xd50041, CODE_FOR_vec_unpacku_hi_v16qi },
  { 0xd50042, CODE_FOR_vec_unpacku_hi_v8hi },
  { 0xd50043, CODE_FOR_vec_unpacku_hi_v4si },
  { 0xd50046, CODE_FOR_vec_unpacku_hi_v32qi },
  { 0xd50047, CODE_FOR_vec_unpacku_hi_v16hi },
  { 0xd50048, CODE_FOR_vec_unpacku_hi_v8si },
  { 0xd5004c, CODE_FOR_vec_unpacku_hi_v32hi },
  { 0xd5004d, CODE_FOR_vec_unpacku_hi_v16si },
  { 0xd60041, CODE_FOR_vec_unpacku_lo_v16qi },
  { 0xd60042, CODE_FOR_vec_unpacku_lo_v8hi },
  { 0xd60043, CODE_FOR_vec_unpacku_lo_v4si },
  { 0xd60046, CODE_FOR_vec_unpacku_lo_v32qi },
  { 0xd60047, CODE_FOR_vec_unpacku_lo_v16hi },
  { 0xd60048, CODE_FOR_vec_unpacku_lo_v8si },
  { 0xd6004c, CODE_FOR_vec_unpacku_lo_v32hi },
  { 0xd6004d, CODE_FOR_vec_unpacku_lo_v16si },
  { 0xd70043, CODE_FOR_vec_widen_smult_even_v4si },
  { 0xd70048, CODE_FOR_vec_widen_smult_even_v8si },
  { 0xd7004d, CODE_FOR_vec_widen_smult_even_v16si },
  { 0xd80041, CODE_FOR_vec_widen_smult_hi_v16qi },
  { 0xd80042, CODE_FOR_vec_widen_smult_hi_v8hi },
  { 0xd80043, CODE_FOR_vec_widen_smult_hi_v4si },
  { 0xd80046, CODE_FOR_vec_widen_smult_hi_v32qi },
  { 0xd80047, CODE_FOR_vec_widen_smult_hi_v16hi },
  { 0xd80048, CODE_FOR_vec_widen_smult_hi_v8si },
  { 0xd90041, CODE_FOR_vec_widen_smult_lo_v16qi },
  { 0xd90042, CODE_FOR_vec_widen_smult_lo_v8hi },
  { 0xd90043, CODE_FOR_vec_widen_smult_lo_v4si },
  { 0xd90046, CODE_FOR_vec_widen_smult_lo_v32qi },
  { 0xd90047, CODE_FOR_vec_widen_smult_lo_v16hi },
  { 0xd90048, CODE_FOR_vec_widen_smult_lo_v8si },
  { 0xda0043, CODE_FOR_vec_widen_smult_odd_v4si },
  { 0xda0048, CODE_FOR_vec_widen_smult_odd_v8si },
  { 0xda004d, CODE_FOR_vec_widen_smult_odd_v16si },
  { 0xdd0043, CODE_FOR_vec_widen_umult_even_v4si },
  { 0xdd0048, CODE_FOR_vec_widen_umult_even_v8si },
  { 0xdd004d, CODE_FOR_vec_widen_umult_even_v16si },
  { 0xde0041, CODE_FOR_vec_widen_umult_hi_v16qi },
  { 0xde0042, CODE_FOR_vec_widen_umult_hi_v8hi },
  { 0xde0043, CODE_FOR_vec_widen_umult_hi_v4si },
  { 0xde0046, CODE_FOR_vec_widen_umult_hi_v32qi },
  { 0xde0047, CODE_FOR_vec_widen_umult_hi_v16hi },
  { 0xde0048, CODE_FOR_vec_widen_umult_hi_v8si },
  { 0xdf0041, CODE_FOR_vec_widen_umult_lo_v16qi },
  { 0xdf0042, CODE_FOR_vec_widen_umult_lo_v8hi },
  { 0xdf0043, CODE_FOR_vec_widen_umult_lo_v4si },
  { 0xdf0046, CODE_FOR_vec_widen_umult_lo_v32qi },
  { 0xdf0047, CODE_FOR_vec_widen_umult_lo_v16hi },
  { 0xdf0048, CODE_FOR_vec_widen_umult_lo_v8si },
  { 0xe00043, CODE_FOR_vec_widen_umult_odd_v4si },
  { 0xe00048, CODE_FOR_vec_widen_umult_odd_v8si },
  { 0xe0004d, CODE_FOR_vec_widen_umult_odd_v16si },
  { 0xeb000e, CODE_FOR_atomic_addqi },
  { 0xeb000f, CODE_FOR_atomic_addhi },
  { 0xeb0010, CODE_FOR_atomic_addsi },
  { 0xeb0011, CODE_FOR_atomic_adddi },
  { 0xed000e, CODE_FOR_atomic_andqi },
  { 0xed000f, CODE_FOR_atomic_andhi },
  { 0xed0010, CODE_FOR_atomic_andsi },
  { 0xed0011, CODE_FOR_atomic_anddi },
  { 0xee000e, CODE_FOR_atomic_compare_and_swapqi },
  { 0xee000f, CODE_FOR_atomic_compare_and_swaphi },
  { 0xee0010, CODE_FOR_atomic_compare_and_swapsi },
  { 0xee0011, CODE_FOR_atomic_compare_and_swapdi },
  { 0xee0012, CODE_FOR_atomic_compare_and_swapti },
  { 0xef000e, CODE_FOR_atomic_exchangeqi },
  { 0xef000f, CODE_FOR_atomic_exchangehi },
  { 0xef0010, CODE_FOR_atomic_exchangesi },
  { 0xef0011, CODE_FOR_atomic_exchangedi },
  { 0xf0000e, CODE_FOR_atomic_fetch_addqi },
  { 0xf0000f, CODE_FOR_atomic_fetch_addhi },
  { 0xf00010, CODE_FOR_atomic_fetch_addsi },
  { 0xf00011, CODE_FOR_atomic_fetch_adddi },
  { 0xf6000e, CODE_FOR_atomic_loadqi },
  { 0xf6000f, CODE_FOR_atomic_loadhi },
  { 0xf60010, CODE_FOR_atomic_loadsi },
  { 0xf60011, CODE_FOR_atomic_loaddi },
  { 0xfa000e, CODE_FOR_atomic_orqi },
  { 0xfa000f, CODE_FOR_atomic_orhi },
  { 0xfa0010, CODE_FOR_atomic_orsi },
  { 0xfa0011, CODE_FOR_atomic_ordi },
  { 0xfb000e, CODE_FOR_atomic_storeqi },
  { 0xfb000f, CODE_FOR_atomic_storehi },
  { 0xfb0010, CODE_FOR_atomic_storesi },
  { 0xfb0011, CODE_FOR_atomic_storedi },
  { 0xfd000e, CODE_FOR_atomic_subqi },
  { 0xfd000f, CODE_FOR_atomic_subhi },
  { 0xfd0010, CODE_FOR_atomic_subsi },
  { 0xfd0011, CODE_FOR_atomic_subdi },
  { 0xff000e, CODE_FOR_atomic_xorqi },
  { 0xff000f, CODE_FOR_atomic_xorhi },
  { 0xff0010, CODE_FOR_atomic_xorsi },
  { 0xff0011, CODE_FOR_atomic_xordi },
};

void
init_all_optabs (struct target_optabs *optabs)
{
  bool *ena = optabs->pat_enable;
  ena[0] = HAVE_extendqihi2;
  ena[1] = HAVE_extendqisi2;
  ena[2] = HAVE_extendqidi2;
  ena[3] = HAVE_extendhisi2;
  ena[4] = HAVE_extendhidi2;
  ena[5] = HAVE_extendsidi2;
  ena[6] = HAVE_extendsfdf2;
  ena[7] = HAVE_extendsfxf2;
  ena[8] = HAVE_extenddfxf2;
  ena[9] = HAVE_truncdfsf2;
  ena[10] = HAVE_truncxfsf2;
  ena[11] = HAVE_truncxfdf2;
  ena[12] = HAVE_zero_extendqihi2;
  ena[13] = HAVE_zero_extendqisi2;
  ena[14] = HAVE_zero_extendqidi2;
  ena[15] = HAVE_zero_extendhisi2;
  ena[16] = HAVE_zero_extendhidi2;
  ena[17] = HAVE_zero_extendsidi2;
  ena[18] = HAVE_floathisf2;
  ena[19] = HAVE_floathidf2;
  ena[20] = HAVE_floathixf2;
  ena[21] = HAVE_floatsisf2;
  ena[22] = HAVE_floatsidf2;
  ena[23] = HAVE_floatsixf2;
  ena[24] = HAVE_floatdisf2;
  ena[25] = HAVE_floatdidf2;
  ena[26] = HAVE_floatdixf2;
  ena[27] = HAVE_floatv4siv4sf2;
  ena[28] = HAVE_floatv4siv4df2;
  ena[29] = HAVE_floatv8siv8sf2;
  ena[30] = HAVE_floatv8siv8df2;
  ena[31] = HAVE_floatv16siv16sf2;
  ena[32] = HAVE_floatunsqisf2;
  ena[33] = HAVE_floatunsqidf2;
  ena[34] = HAVE_floatunshisf2;
  ena[35] = HAVE_floatunshidf2;
  ena[36] = HAVE_floatunssisf2;
  ena[37] = HAVE_floatunssidf2;
  ena[38] = HAVE_floatunssixf2;
  ena[39] = HAVE_floatunsdisf2;
  ena[40] = HAVE_floatunsdidf2;
  ena[41] = HAVE_floatunsv4siv4sf2;
  ena[42] = HAVE_floatunsv8siv8sf2;
  ena[43] = HAVE_floatunsv16siv16sf2;
  ena[44] = HAVE_lrintsfsi2;
  ena[45] = HAVE_lrintsfdi2;
  ena[46] = HAVE_lrintdfsi2;
  ena[47] = HAVE_lrintdfdi2;
  ena[48] = HAVE_lrintxfhi2;
  ena[49] = HAVE_lrintxfsi2;
  ena[50] = HAVE_lrintxfdi2;
  ena[51] = HAVE_lroundsfhi2;
  ena[52] = HAVE_lroundsfsi2;
  ena[53] = HAVE_lroundsfdi2;
  ena[54] = HAVE_lrounddfhi2;
  ena[55] = HAVE_lrounddfsi2;
  ena[56] = HAVE_lrounddfdi2;
  ena[57] = HAVE_lroundxfhi2;
  ena[58] = HAVE_lroundxfsi2;
  ena[59] = HAVE_lroundxfdi2;
  ena[60] = HAVE_lfloorsfsi2;
  ena[61] = HAVE_lfloorsfdi2;
  ena[62] = HAVE_lfloordfsi2;
  ena[63] = HAVE_lfloordfdi2;
  ena[64] = HAVE_lfloorxfhi2;
  ena[65] = HAVE_lfloorxfsi2;
  ena[66] = HAVE_lfloorxfdi2;
  ena[67] = HAVE_lceilsfsi2;
  ena[68] = HAVE_lceilsfdi2;
  ena[69] = HAVE_lceildfsi2;
  ena[70] = HAVE_lceildfdi2;
  ena[71] = HAVE_lceilxfhi2;
  ena[72] = HAVE_lceilxfsi2;
  ena[73] = HAVE_lceilxfdi2;
  ena[74] = HAVE_fix_truncsfhi2;
  ena[75] = HAVE_fix_truncsfsi2;
  ena[76] = HAVE_fix_truncsfdi2;
  ena[77] = HAVE_fix_truncdfhi2;
  ena[78] = HAVE_fix_truncdfsi2;
  ena[79] = HAVE_fix_truncdfdi2;
  ena[80] = HAVE_fix_truncxfhi2;
  ena[81] = HAVE_fix_truncxfsi2;
  ena[82] = HAVE_fix_truncxfdi2;
  ena[83] = HAVE_fix_truncv4sfv4si2;
  ena[84] = HAVE_fix_truncv8sfv8si2;
  ena[85] = HAVE_fix_truncv4dfv4si2;
  ena[86] = HAVE_fix_truncv16sfv16si2;
  ena[87] = HAVE_fix_truncv8dfv8si2;
  ena[88] = HAVE_fixuns_truncsfhi2;
  ena[89] = HAVE_fixuns_truncsfsi2;
  ena[90] = HAVE_fixuns_truncdfhi2;
  ena[91] = HAVE_fixuns_truncdfsi2;
  ena[92] = HAVE_fixuns_truncv4sfv4si2;
  ena[93] = HAVE_fixuns_truncv8sfv8si2;
  ena[94] = HAVE_fixuns_truncv16sfv16si2;
  ena[95] = HAVE_mulqihi3;
  ena[96] = HAVE_mulsidi3;
  ena[97] = HAVE_mulditi3;
  ena[98] = HAVE_umulqihi3;
  ena[99] = HAVE_umulsidi3;
  ena[100] = HAVE_umulditi3;
  ena[101] = HAVE_vcondv16qiv16qi;
  ena[102] = HAVE_vcondv8hiv16qi;
  ena[103] = HAVE_vcondv4siv16qi;
  ena[104] = HAVE_vcondv2div16qi;
  ena[105] = HAVE_vcondv4sfv16qi;
  ena[106] = HAVE_vcondv2dfv16qi;
  ena[107] = HAVE_vcondv16qiv8hi;
  ena[108] = HAVE_vcondv8hiv8hi;
  ena[109] = HAVE_vcondv4siv8hi;
  ena[110] = HAVE_vcondv2div8hi;
  ena[111] = HAVE_vcondv4sfv8hi;
  ena[112] = HAVE_vcondv2dfv8hi;
  ena[113] = HAVE_vcondv16qiv4si;
  ena[114] = HAVE_vcondv8hiv4si;
  ena[115] = HAVE_vcondv4siv4si;
  ena[116] = HAVE_vcondv2div4si;
  ena[117] = HAVE_vcondv4sfv4si;
  ena[118] = HAVE_vcondv2dfv4si;
  ena[119] = HAVE_vcondv2div2di;
  ena[120] = HAVE_vcondv2dfv2di;
  ena[121] = HAVE_vcondv32qiv32qi;
  ena[122] = HAVE_vcondv16hiv32qi;
  ena[123] = HAVE_vcondv8siv32qi;
  ena[124] = HAVE_vcondv4div32qi;
  ena[125] = HAVE_vcondv8sfv32qi;
  ena[126] = HAVE_vcondv4dfv32qi;
  ena[127] = HAVE_vcondv32qiv16hi;
  ena[128] = HAVE_vcondv16hiv16hi;
  ena[129] = HAVE_vcondv8siv16hi;
  ena[130] = HAVE_vcondv4div16hi;
  ena[131] = HAVE_vcondv8sfv16hi;
  ena[132] = HAVE_vcondv4dfv16hi;
  ena[133] = HAVE_vcondv32qiv8si;
  ena[134] = HAVE_vcondv16hiv8si;
  ena[135] = HAVE_vcondv8siv8si;
  ena[136] = HAVE_vcondv4div8si;
  ena[137] = HAVE_vcondv8sfv8si;
  ena[138] = HAVE_vcondv4dfv8si;
  ena[139] = HAVE_vcondv32qiv4di;
  ena[140] = HAVE_vcondv16hiv4di;
  ena[141] = HAVE_vcondv8siv4di;
  ena[142] = HAVE_vcondv4div4di;
  ena[143] = HAVE_vcondv8sfv4di;
  ena[144] = HAVE_vcondv4dfv4di;
  ena[145] = HAVE_vcondv64qiv64qi;
  ena[146] = HAVE_vcondv32hiv64qi;
  ena[147] = HAVE_vcondv16siv64qi;
  ena[148] = HAVE_vcondv8div64qi;
  ena[149] = HAVE_vcondv16sfv64qi;
  ena[150] = HAVE_vcondv8dfv64qi;
  ena[151] = HAVE_vcondv64qiv32hi;
  ena[152] = HAVE_vcondv32hiv32hi;
  ena[153] = HAVE_vcondv16siv32hi;
  ena[154] = HAVE_vcondv8div32hi;
  ena[155] = HAVE_vcondv16sfv32hi;
  ena[156] = HAVE_vcondv8dfv32hi;
  ena[157] = HAVE_vcondv64qiv16si;
  ena[158] = HAVE_vcondv32hiv16si;
  ena[159] = HAVE_vcondv16siv16si;
  ena[160] = HAVE_vcondv8div16si;
  ena[161] = HAVE_vcondv16sfv16si;
  ena[162] = HAVE_vcondv8dfv16si;
  ena[163] = HAVE_vcondv64qiv8di;
  ena[164] = HAVE_vcondv32hiv8di;
  ena[165] = HAVE_vcondv16siv8di;
  ena[166] = HAVE_vcondv8div8di;
  ena[167] = HAVE_vcondv16sfv8di;
  ena[168] = HAVE_vcondv8dfv8di;
  ena[169] = HAVE_vcondv16qiv4sf;
  ena[170] = HAVE_vcondv8hiv4sf;
  ena[171] = HAVE_vcondv4siv4sf;
  ena[172] = HAVE_vcondv2div4sf;
  ena[173] = HAVE_vcondv4sfv4sf;
  ena[174] = HAVE_vcondv2dfv4sf;
  ena[175] = HAVE_vcondv16qiv2df;
  ena[176] = HAVE_vcondv8hiv2df;
  ena[177] = HAVE_vcondv4siv2df;
  ena[178] = HAVE_vcondv2div2df;
  ena[179] = HAVE_vcondv4sfv2df;
  ena[180] = HAVE_vcondv2dfv2df;
  ena[181] = HAVE_vcondv32qiv8sf;
  ena[182] = HAVE_vcondv16hiv8sf;
  ena[183] = HAVE_vcondv8siv8sf;
  ena[184] = HAVE_vcondv4div8sf;
  ena[185] = HAVE_vcondv8sfv8sf;
  ena[186] = HAVE_vcondv4dfv8sf;
  ena[187] = HAVE_vcondv32qiv4df;
  ena[188] = HAVE_vcondv16hiv4df;
  ena[189] = HAVE_vcondv8siv4df;
  ena[190] = HAVE_vcondv4div4df;
  ena[191] = HAVE_vcondv8sfv4df;
  ena[192] = HAVE_vcondv4dfv4df;
  ena[193] = HAVE_vcondv64qiv16sf;
  ena[194] = HAVE_vcondv32hiv16sf;
  ena[195] = HAVE_vcondv16siv16sf;
  ena[196] = HAVE_vcondv8div16sf;
  ena[197] = HAVE_vcondv16sfv16sf;
  ena[198] = HAVE_vcondv8dfv16sf;
  ena[199] = HAVE_vcondv64qiv8df;
  ena[200] = HAVE_vcondv32hiv8df;
  ena[201] = HAVE_vcondv16siv8df;
  ena[202] = HAVE_vcondv8div8df;
  ena[203] = HAVE_vcondv16sfv8df;
  ena[204] = HAVE_vcondv8dfv8df;
  ena[205] = HAVE_vconduv16qiv16qi;
  ena[206] = HAVE_vconduv8hiv16qi;
  ena[207] = HAVE_vconduv4siv16qi;
  ena[208] = HAVE_vconduv2div16qi;
  ena[209] = HAVE_vconduv4sfv16qi;
  ena[210] = HAVE_vconduv2dfv16qi;
  ena[211] = HAVE_vconduv16qiv8hi;
  ena[212] = HAVE_vconduv8hiv8hi;
  ena[213] = HAVE_vconduv4siv8hi;
  ena[214] = HAVE_vconduv2div8hi;
  ena[215] = HAVE_vconduv4sfv8hi;
  ena[216] = HAVE_vconduv2dfv8hi;
  ena[217] = HAVE_vconduv16qiv4si;
  ena[218] = HAVE_vconduv8hiv4si;
  ena[219] = HAVE_vconduv4siv4si;
  ena[220] = HAVE_vconduv2div4si;
  ena[221] = HAVE_vconduv4sfv4si;
  ena[222] = HAVE_vconduv2dfv4si;
  ena[223] = HAVE_vconduv2div2di;
  ena[224] = HAVE_vconduv2dfv2di;
  ena[225] = HAVE_vconduv32qiv32qi;
  ena[226] = HAVE_vconduv16hiv32qi;
  ena[227] = HAVE_vconduv8siv32qi;
  ena[228] = HAVE_vconduv4div32qi;
  ena[229] = HAVE_vconduv8sfv32qi;
  ena[230] = HAVE_vconduv4dfv32qi;
  ena[231] = HAVE_vconduv32qiv16hi;
  ena[232] = HAVE_vconduv16hiv16hi;
  ena[233] = HAVE_vconduv8siv16hi;
  ena[234] = HAVE_vconduv4div16hi;
  ena[235] = HAVE_vconduv8sfv16hi;
  ena[236] = HAVE_vconduv4dfv16hi;
  ena[237] = HAVE_vconduv32qiv8si;
  ena[238] = HAVE_vconduv16hiv8si;
  ena[239] = HAVE_vconduv8siv8si;
  ena[240] = HAVE_vconduv4div8si;
  ena[241] = HAVE_vconduv8sfv8si;
  ena[242] = HAVE_vconduv4dfv8si;
  ena[243] = HAVE_vconduv32qiv4di;
  ena[244] = HAVE_vconduv16hiv4di;
  ena[245] = HAVE_vconduv8siv4di;
  ena[246] = HAVE_vconduv4div4di;
  ena[247] = HAVE_vconduv8sfv4di;
  ena[248] = HAVE_vconduv4dfv4di;
  ena[249] = HAVE_vconduv64qiv64qi;
  ena[250] = HAVE_vconduv32hiv64qi;
  ena[251] = HAVE_vconduv16siv64qi;
  ena[252] = HAVE_vconduv8div64qi;
  ena[253] = HAVE_vconduv16sfv64qi;
  ena[254] = HAVE_vconduv8dfv64qi;
  ena[255] = HAVE_vconduv64qiv32hi;
  ena[256] = HAVE_vconduv32hiv32hi;
  ena[257] = HAVE_vconduv16siv32hi;
  ena[258] = HAVE_vconduv8div32hi;
  ena[259] = HAVE_vconduv16sfv32hi;
  ena[260] = HAVE_vconduv8dfv32hi;
  ena[261] = HAVE_vconduv64qiv16si;
  ena[262] = HAVE_vconduv32hiv16si;
  ena[263] = HAVE_vconduv16siv16si;
  ena[264] = HAVE_vconduv8div16si;
  ena[265] = HAVE_vconduv16sfv16si;
  ena[266] = HAVE_vconduv8dfv16si;
  ena[267] = HAVE_vconduv64qiv8di;
  ena[268] = HAVE_vconduv32hiv8di;
  ena[269] = HAVE_vconduv16siv8di;
  ena[270] = HAVE_vconduv8div8di;
  ena[271] = HAVE_vconduv16sfv8di;
  ena[272] = HAVE_vconduv8dfv8di;
  ena[273] = HAVE_addqi3;
  ena[274] = HAVE_addhi3;
  ena[275] = HAVE_addsi3;
  ena[276] = HAVE_adddi3;
  ena[277] = HAVE_addti3;
  ena[278] = HAVE_addsf3;
  ena[279] = HAVE_adddf3;
  ena[280] = HAVE_addxf3;
  ena[281] = HAVE_addv16qi3;
  ena[282] = HAVE_addv8hi3;
  ena[283] = HAVE_addv4si3;
  ena[284] = HAVE_addv2di3;
  ena[285] = HAVE_addv32qi3;
  ena[286] = HAVE_addv16hi3;
  ena[287] = HAVE_addv8si3;
  ena[288] = HAVE_addv4di3;
  ena[289] = HAVE_addv16si3;
  ena[290] = HAVE_addv8di3;
  ena[291] = HAVE_addv4sf3;
  ena[292] = HAVE_addv2df3;
  ena[293] = HAVE_addv8sf3;
  ena[294] = HAVE_addv4df3;
  ena[295] = HAVE_addv16sf3;
  ena[296] = HAVE_addv8df3;
  ena[297] = HAVE_subqi3;
  ena[298] = HAVE_subhi3;
  ena[299] = HAVE_subsi3;
  ena[300] = HAVE_subdi3;
  ena[301] = HAVE_subti3;
  ena[302] = HAVE_subsf3;
  ena[303] = HAVE_subdf3;
  ena[304] = HAVE_subxf3;
  ena[305] = HAVE_subv16qi3;
  ena[306] = HAVE_subv8hi3;
  ena[307] = HAVE_subv4si3;
  ena[308] = HAVE_subv2di3;
  ena[309] = HAVE_subv32qi3;
  ena[310] = HAVE_subv16hi3;
  ena[311] = HAVE_subv8si3;
  ena[312] = HAVE_subv4di3;
  ena[313] = HAVE_subv16si3;
  ena[314] = HAVE_subv8di3;
  ena[315] = HAVE_subv4sf3;
  ena[316] = HAVE_subv2df3;
  ena[317] = HAVE_subv8sf3;
  ena[318] = HAVE_subv4df3;
  ena[319] = HAVE_subv16sf3;
  ena[320] = HAVE_subv8df3;
  ena[321] = HAVE_mulqi3;
  ena[322] = HAVE_mulhi3;
  ena[323] = HAVE_mulsi3;
  ena[324] = HAVE_muldi3;
  ena[325] = HAVE_mulsf3;
  ena[326] = HAVE_muldf3;
  ena[327] = HAVE_mulxf3;
  ena[328] = HAVE_mulv16qi3;
  ena[329] = HAVE_mulv8hi3;
  ena[330] = HAVE_mulv4si3;
  ena[331] = HAVE_mulv2di3;
  ena[332] = HAVE_mulv32qi3;
  ena[333] = HAVE_mulv16hi3;
  ena[334] = HAVE_mulv8si3;
  ena[335] = HAVE_mulv4di3;
  ena[336] = HAVE_mulv16si3;
  ena[337] = HAVE_mulv8di3;
  ena[338] = HAVE_mulv4sf3;
  ena[339] = HAVE_mulv2df3;
  ena[340] = HAVE_mulv8sf3;
  ena[341] = HAVE_mulv4df3;
  ena[342] = HAVE_mulv16sf3;
  ena[343] = HAVE_mulv8df3;
  ena[344] = HAVE_divsf3;
  ena[345] = HAVE_divdf3;
  ena[346] = HAVE_divxf3;
  ena[347] = HAVE_divv4sf3;
  ena[348] = HAVE_divv2df3;
  ena[349] = HAVE_divv8sf3;
  ena[350] = HAVE_divv4df3;
  ena[351] = HAVE_divv16sf3;
  ena[352] = HAVE_divv8df3;
  ena[353] = HAVE_divmodqi4;
  ena[354] = HAVE_divmodhi4;
  ena[355] = HAVE_divmodsi4;
  ena[356] = HAVE_divmoddi4;
  ena[357] = HAVE_udivmodqi4;
  ena[358] = HAVE_udivmodhi4;
  ena[359] = HAVE_udivmodsi4;
  ena[360] = HAVE_udivmoddi4;
  ena[361] = HAVE_andqi3;
  ena[362] = HAVE_andhi3;
  ena[363] = HAVE_andsi3;
  ena[364] = HAVE_anddi3;
  ena[365] = HAVE_andtf3;
  ena[366] = HAVE_andv16qi3;
  ena[367] = HAVE_andv8hi3;
  ena[368] = HAVE_andv4si3;
  ena[369] = HAVE_andv2di3;
  ena[370] = HAVE_andv32qi3;
  ena[371] = HAVE_andv16hi3;
  ena[372] = HAVE_andv8si3;
  ena[373] = HAVE_andv4di3;
  ena[374] = HAVE_andv16si3;
  ena[375] = HAVE_andv8di3;
  ena[376] = HAVE_andv4sf3;
  ena[377] = HAVE_andv2df3;
  ena[378] = HAVE_andv8sf3;
  ena[379] = HAVE_andv4df3;
  ena[380] = HAVE_andv16sf3;
  ena[381] = HAVE_andv8df3;
  ena[382] = HAVE_iorqi3;
  ena[383] = HAVE_iorhi3;
  ena[384] = HAVE_iorsi3;
  ena[385] = HAVE_iordi3;
  ena[386] = HAVE_iortf3;
  ena[387] = HAVE_iorv16qi3;
  ena[388] = HAVE_iorv8hi3;
  ena[389] = HAVE_iorv4si3;
  ena[390] = HAVE_iorv2di3;
  ena[391] = HAVE_iorv32qi3;
  ena[392] = HAVE_iorv16hi3;
  ena[393] = HAVE_iorv8si3;
  ena[394] = HAVE_iorv4di3;
  ena[395] = HAVE_iorv16si3;
  ena[396] = HAVE_iorv8di3;
  ena[397] = HAVE_iorv4sf3;
  ena[398] = HAVE_iorv2df3;
  ena[399] = HAVE_iorv8sf3;
  ena[400] = HAVE_iorv4df3;
  ena[401] = HAVE_xorqi3;
  ena[402] = HAVE_xorhi3;
  ena[403] = HAVE_xorsi3;
  ena[404] = HAVE_xordi3;
  ena[405] = HAVE_xortf3;
  ena[406] = HAVE_xorv16qi3;
  ena[407] = HAVE_xorv8hi3;
  ena[408] = HAVE_xorv4si3;
  ena[409] = HAVE_xorv2di3;
  ena[410] = HAVE_xorv32qi3;
  ena[411] = HAVE_xorv16hi3;
  ena[412] = HAVE_xorv8si3;
  ena[413] = HAVE_xorv4di3;
  ena[414] = HAVE_xorv16si3;
  ena[415] = HAVE_xorv8di3;
  ena[416] = HAVE_xorv4sf3;
  ena[417] = HAVE_xorv2df3;
  ena[418] = HAVE_xorv8sf3;
  ena[419] = HAVE_xorv4df3;
  ena[420] = HAVE_xorv16sf3;
  ena[421] = HAVE_xorv8df3;
  ena[422] = HAVE_ashlqi3;
  ena[423] = HAVE_ashlhi3;
  ena[424] = HAVE_ashlsi3;
  ena[425] = HAVE_ashldi3;
  ena[426] = HAVE_ashlti3;
  ena[427] = HAVE_ashlv16qi3;
  ena[428] = HAVE_ashlv8hi3;
  ena[429] = HAVE_ashlv4si3;
  ena[430] = HAVE_ashlv2di3;
  ena[431] = HAVE_ashlv32qi3;
  ena[432] = HAVE_ashlv16hi3;
  ena[433] = HAVE_ashlv8si3;
  ena[434] = HAVE_ashlv4di3;
  ena[435] = HAVE_ashlv16si3;
  ena[436] = HAVE_ashlv8di3;
  ena[437] = HAVE_ashrqi3;
  ena[438] = HAVE_ashrhi3;
  ena[439] = HAVE_ashrsi3;
  ena[440] = HAVE_ashrdi3;
  ena[441] = HAVE_ashrti3;
  ena[442] = HAVE_ashrv16qi3;
  ena[443] = HAVE_ashrv8hi3;
  ena[444] = HAVE_ashrv4si3;
  ena[445] = HAVE_ashrv2di3;
  ena[446] = HAVE_ashrv32qi3;
  ena[447] = HAVE_ashrv16hi3;
  ena[448] = HAVE_ashrv8si3;
  ena[449] = HAVE_ashrv16si3;
  ena[450] = HAVE_ashrv8di3;
  ena[451] = HAVE_lshrqi3;
  ena[452] = HAVE_lshrhi3;
  ena[453] = HAVE_lshrsi3;
  ena[454] = HAVE_lshrdi3;
  ena[455] = HAVE_lshrti3;
  ena[456] = HAVE_lshrv16qi3;
  ena[457] = HAVE_lshrv8hi3;
  ena[458] = HAVE_lshrv4si3;
  ena[459] = HAVE_lshrv2di3;
  ena[460] = HAVE_lshrv32qi3;
  ena[461] = HAVE_lshrv16hi3;
  ena[462] = HAVE_lshrv8si3;
  ena[463] = HAVE_lshrv4di3;
  ena[464] = HAVE_lshrv16si3;
  ena[465] = HAVE_lshrv8di3;
  ena[466] = HAVE_rotlqi3;
  ena[467] = HAVE_rotlhi3;
  ena[468] = HAVE_rotlsi3;
  ena[469] = HAVE_rotldi3;
  ena[470] = HAVE_rotlti3;
  ena[471] = HAVE_rotlv16qi3;
  ena[472] = HAVE_rotlv8hi3;
  ena[473] = HAVE_rotlv4si3;
  ena[474] = HAVE_rotlv2di3;
  ena[475] = HAVE_rotrqi3;
  ena[476] = HAVE_rotrhi3;
  ena[477] = HAVE_rotrsi3;
  ena[478] = HAVE_rotrdi3;
  ena[479] = HAVE_rotrti3;
  ena[480] = HAVE_rotrv16qi3;
  ena[481] = HAVE_rotrv8hi3;
  ena[482] = HAVE_rotrv4si3;
  ena[483] = HAVE_rotrv2di3;
  ena[484] = HAVE_vashlv16qi3;
  ena[485] = HAVE_vashlv8hi3;
  ena[486] = HAVE_vashlv4si3;
  ena[487] = HAVE_vashlv2di3;
  ena[488] = HAVE_vashlv8si3;
  ena[489] = HAVE_vashlv4di3;
  ena[490] = HAVE_vashlv16si3;
  ena[491] = HAVE_vashlv8di3;
  ena[492] = HAVE_vashrv16qi3;
  ena[493] = HAVE_vashrv8hi3;
  ena[494] = HAVE_vashrv4si3;
  ena[495] = HAVE_vashrv2di3;
  ena[496] = HAVE_vashrv8si3;
  ena[497] = HAVE_vashrv16si3;
  ena[498] = HAVE_vlshrv16qi3;
  ena[499] = HAVE_vlshrv8hi3;
  ena[500] = HAVE_vlshrv4si3;
  ena[501] = HAVE_vlshrv2di3;
  ena[502] = HAVE_vlshrv8si3;
  ena[503] = HAVE_vlshrv4di3;
  ena[504] = HAVE_vlshrv16si3;
  ena[505] = HAVE_vlshrv8di3;
  ena[506] = HAVE_vrotlv16qi3;
  ena[507] = HAVE_vrotlv8hi3;
  ena[508] = HAVE_vrotlv4si3;
  ena[509] = HAVE_vrotlv2di3;
  ena[510] = HAVE_vrotrv16qi3;
  ena[511] = HAVE_vrotrv8hi3;
  ena[512] = HAVE_vrotrv4si3;
  ena[513] = HAVE_vrotrv2di3;
  ena[514] = HAVE_sminsf3;
  ena[515] = HAVE_smindf3;
  ena[516] = HAVE_sminv16qi3;
  ena[517] = HAVE_sminv8hi3;
  ena[518] = HAVE_sminv4si3;
  ena[519] = HAVE_sminv2di3;
  ena[520] = HAVE_sminv32qi3;
  ena[521] = HAVE_sminv16hi3;
  ena[522] = HAVE_sminv8si3;
  ena[523] = HAVE_sminv4di3;
  ena[524] = HAVE_sminv16si3;
  ena[525] = HAVE_sminv8di3;
  ena[526] = HAVE_sminv4sf3;
  ena[527] = HAVE_sminv2df3;
  ena[528] = HAVE_sminv8sf3;
  ena[529] = HAVE_sminv4df3;
  ena[530] = HAVE_sminv16sf3;
  ena[531] = HAVE_sminv8df3;
  ena[532] = HAVE_smaxsf3;
  ena[533] = HAVE_smaxdf3;
  ena[534] = HAVE_smaxv16qi3;
  ena[535] = HAVE_smaxv8hi3;
  ena[536] = HAVE_smaxv4si3;
  ena[537] = HAVE_smaxv2di3;
  ena[538] = HAVE_smaxv32qi3;
  ena[539] = HAVE_smaxv16hi3;
  ena[540] = HAVE_smaxv8si3;
  ena[541] = HAVE_smaxv4di3;
  ena[542] = HAVE_smaxv16si3;
  ena[543] = HAVE_smaxv8di3;
  ena[544] = HAVE_smaxv4sf3;
  ena[545] = HAVE_smaxv2df3;
  ena[546] = HAVE_smaxv8sf3;
  ena[547] = HAVE_smaxv4df3;
  ena[548] = HAVE_smaxv16sf3;
  ena[549] = HAVE_smaxv8df3;
  ena[550] = HAVE_uminv16qi3;
  ena[551] = HAVE_uminv8hi3;
  ena[552] = HAVE_uminv4si3;
  ena[553] = HAVE_uminv2di3;
  ena[554] = HAVE_uminv32qi3;
  ena[555] = HAVE_uminv16hi3;
  ena[556] = HAVE_uminv8si3;
  ena[557] = HAVE_uminv4di3;
  ena[558] = HAVE_uminv16si3;
  ena[559] = HAVE_uminv8di3;
  ena[560] = HAVE_umaxv16qi3;
  ena[561] = HAVE_umaxv8hi3;
  ena[562] = HAVE_umaxv4si3;
  ena[563] = HAVE_umaxv2di3;
  ena[564] = HAVE_umaxv32qi3;
  ena[565] = HAVE_umaxv16hi3;
  ena[566] = HAVE_umaxv8si3;
  ena[567] = HAVE_umaxv4di3;
  ena[568] = HAVE_umaxv16si3;
  ena[569] = HAVE_umaxv8di3;
  ena[570] = HAVE_negqi2;
  ena[571] = HAVE_neghi2;
  ena[572] = HAVE_negsi2;
  ena[573] = HAVE_negdi2;
  ena[574] = HAVE_negti2;
  ena[575] = HAVE_negsf2;
  ena[576] = HAVE_negdf2;
  ena[577] = HAVE_negxf2;
  ena[578] = HAVE_negtf2;
  ena[579] = HAVE_negv16qi2;
  ena[580] = HAVE_negv8hi2;
  ena[581] = HAVE_negv4si2;
  ena[582] = HAVE_negv2di2;
  ena[583] = HAVE_negv32qi2;
  ena[584] = HAVE_negv16hi2;
  ena[585] = HAVE_negv8si2;
  ena[586] = HAVE_negv4di2;
  ena[587] = HAVE_negv16si2;
  ena[588] = HAVE_negv8di2;
  ena[589] = HAVE_negv4sf2;
  ena[590] = HAVE_negv2df2;
  ena[591] = HAVE_negv8sf2;
  ena[592] = HAVE_negv4df2;
  ena[593] = HAVE_negv16sf2;
  ena[594] = HAVE_negv8df2;
  ena[595] = HAVE_abssf2;
  ena[596] = HAVE_absdf2;
  ena[597] = HAVE_absxf2;
  ena[598] = HAVE_abstf2;
  ena[599] = HAVE_absv8qi2;
  ena[600] = HAVE_absv4hi2;
  ena[601] = HAVE_absv2si2;
  ena[602] = HAVE_absv16qi2;
  ena[603] = HAVE_absv8hi2;
  ena[604] = HAVE_absv4si2;
  ena[605] = HAVE_absv32qi2;
  ena[606] = HAVE_absv16hi2;
  ena[607] = HAVE_absv8si2;
  ena[608] = HAVE_absv16si2;
  ena[609] = HAVE_absv8di2;
  ena[610] = HAVE_absv4sf2;
  ena[611] = HAVE_absv2df2;
  ena[612] = HAVE_absv8sf2;
  ena[613] = HAVE_absv4df2;
  ena[614] = HAVE_absv16sf2;
  ena[615] = HAVE_absv8df2;
  ena[616] = HAVE_one_cmplqi2;
  ena[617] = HAVE_one_cmplhi2;
  ena[618] = HAVE_one_cmplsi2;
  ena[619] = HAVE_one_cmpldi2;
  ena[620] = HAVE_one_cmplv16qi2;
  ena[621] = HAVE_one_cmplv8hi2;
  ena[622] = HAVE_one_cmplv4si2;
  ena[623] = HAVE_one_cmplv2di2;
  ena[624] = HAVE_one_cmplv32qi2;
  ena[625] = HAVE_one_cmplv16hi2;
  ena[626] = HAVE_one_cmplv8si2;
  ena[627] = HAVE_one_cmplv4di2;
  ena[628] = HAVE_one_cmplv16si2;
  ena[629] = HAVE_one_cmplv8di2;
  ena[630] = HAVE_bswapsi2;
  ena[631] = HAVE_bswapdi2;
  ena[632] = HAVE_ffssi2;
  ena[633] = HAVE_ffsdi2;
  ena[634] = HAVE_clzhi2;
  ena[635] = HAVE_clzsi2;
  ena[636] = HAVE_clzdi2;
  ena[637] = HAVE_clzv16si2;
  ena[638] = HAVE_clzv8di2;
  ena[639] = HAVE_ctzhi2;
  ena[640] = HAVE_ctzsi2;
  ena[641] = HAVE_ctzdi2;
  ena[642] = HAVE_popcounthi2;
  ena[643] = HAVE_popcountsi2;
  ena[644] = HAVE_popcountdi2;
  ena[645] = HAVE_paritysi2;
  ena[646] = HAVE_paritydi2;
  ena[647] = HAVE_sqrtsf2;
  ena[648] = HAVE_sqrtdf2;
  ena[649] = HAVE_sqrtxf2;
  ena[650] = HAVE_sqrtv4sf2;
  ena[651] = HAVE_sqrtv2df2;
  ena[652] = HAVE_sqrtv8sf2;
  ena[653] = HAVE_sqrtv4df2;
  ena[654] = HAVE_sqrtv16sf2;
  ena[655] = HAVE_sqrtv8df2;
  ena[656] = HAVE_movqi;
  ena[657] = HAVE_movhi;
  ena[658] = HAVE_movsi;
  ena[659] = HAVE_movdi;
  ena[660] = HAVE_movti;
  ena[661] = HAVE_movoi;
  ena[662] = HAVE_movxi;
  ena[663] = HAVE_movsf;
  ena[664] = HAVE_movdf;
  ena[665] = HAVE_movxf;
  ena[666] = HAVE_movtf;
  ena[667] = HAVE_movcdi;
  ena[668] = HAVE_movv8qi;
  ena[669] = HAVE_movv4hi;
  ena[670] = HAVE_movv2si;
  ena[671] = HAVE_movv1di;
  ena[672] = HAVE_movv16qi;
  ena[673] = HAVE_movv8hi;
  ena[674] = HAVE_movv4si;
  ena[675] = HAVE_movv2di;
  ena[676] = HAVE_movv1ti;
  ena[677] = HAVE_movv32qi;
  ena[678] = HAVE_movv16hi;
  ena[679] = HAVE_movv8si;
  ena[680] = HAVE_movv4di;
  ena[681] = HAVE_movv2ti;
  ena[682] = HAVE_movv64qi;
  ena[683] = HAVE_movv32hi;
  ena[684] = HAVE_movv16si;
  ena[685] = HAVE_movv8di;
  ena[686] = HAVE_movv2sf;
  ena[687] = HAVE_movv4sf;
  ena[688] = HAVE_movv2df;
  ena[689] = HAVE_movv8sf;
  ena[690] = HAVE_movv4df;
  ena[691] = HAVE_movv16sf;
  ena[692] = HAVE_movv8df;
  ena[693] = HAVE_movstrictqi;
  ena[694] = HAVE_movstricthi;
  ena[695] = HAVE_movmisalignv8qi;
  ena[696] = HAVE_movmisalignv4hi;
  ena[697] = HAVE_movmisalignv2si;
  ena[698] = HAVE_movmisalignv1di;
  ena[699] = HAVE_movmisalignv16qi;
  ena[700] = HAVE_movmisalignv8hi;
  ena[701] = HAVE_movmisalignv4si;
  ena[702] = HAVE_movmisalignv2di;
  ena[703] = HAVE_movmisalignv1ti;
  ena[704] = HAVE_movmisalignv32qi;
  ena[705] = HAVE_movmisalignv16hi;
  ena[706] = HAVE_movmisalignv8si;
  ena[707] = HAVE_movmisalignv4di;
  ena[708] = HAVE_movmisalignv2ti;
  ena[709] = HAVE_movmisalignv64qi;
  ena[710] = HAVE_movmisalignv32hi;
  ena[711] = HAVE_movmisalignv16si;
  ena[712] = HAVE_movmisalignv8di;
  ena[713] = HAVE_movmisalignv2sf;
  ena[714] = HAVE_movmisalignv4sf;
  ena[715] = HAVE_movmisalignv2df;
  ena[716] = HAVE_movmisalignv8sf;
  ena[717] = HAVE_movmisalignv4df;
  ena[718] = HAVE_movmisalignv16sf;
  ena[719] = HAVE_movmisalignv8df;
  ena[720] = HAVE_storentsi;
  ena[721] = HAVE_storentdi;
  ena[722] = HAVE_storentsf;
  ena[723] = HAVE_storentdf;
  ena[724] = HAVE_storentv2di;
  ena[725] = HAVE_storentv4di;
  ena[726] = HAVE_storentv8di;
  ena[727] = HAVE_storentv4sf;
  ena[728] = HAVE_storentv2df;
  ena[729] = HAVE_storentv8sf;
  ena[730] = HAVE_storentv4df;
  ena[731] = HAVE_storentv16sf;
  ena[732] = HAVE_storentv8df;
  ena[733] = HAVE_cbranchcc4;
  ena[734] = HAVE_cbranchqi4;
  ena[735] = HAVE_cbranchhi4;
  ena[736] = HAVE_cbranchsi4;
  ena[737] = HAVE_cbranchdi4;
  ena[738] = HAVE_cbranchti4;
  ena[739] = HAVE_cbranchsf4;
  ena[740] = HAVE_cbranchdf4;
  ena[741] = HAVE_cbranchxf4;
  ena[742] = HAVE_addqicc;
  ena[743] = HAVE_addhicc;
  ena[744] = HAVE_addsicc;
  ena[745] = HAVE_adddicc;
  ena[746] = HAVE_movqicc;
  ena[747] = HAVE_movhicc;
  ena[748] = HAVE_movsicc;
  ena[749] = HAVE_movdicc;
  ena[750] = HAVE_movsfcc;
  ena[751] = HAVE_movdfcc;
  ena[752] = HAVE_movxfcc;
  ena[753] = HAVE_cstorecc4;
  ena[754] = HAVE_cstoreqi4;
  ena[755] = HAVE_cstorehi4;
  ena[756] = HAVE_cstoresi4;
  ena[757] = HAVE_cstoredi4;
  ena[758] = HAVE_cstoresf4;
  ena[759] = HAVE_cstoredf4;
  ena[760] = HAVE_cstorexf4;
  ena[761] = HAVE_addvqi4;
  ena[762] = HAVE_addvhi4;
  ena[763] = HAVE_addvsi4;
  ena[764] = HAVE_addvdi4;
  ena[765] = HAVE_subvqi4;
  ena[766] = HAVE_subvhi4;
  ena[767] = HAVE_subvsi4;
  ena[768] = HAVE_subvdi4;
  ena[769] = HAVE_mulvsi4;
  ena[770] = HAVE_mulvdi4;
  ena[771] = HAVE_negvqi3;
  ena[772] = HAVE_negvhi3;
  ena[773] = HAVE_negvsi3;
  ena[774] = HAVE_negvdi3;
  ena[775] = HAVE_smulsi3_highpart;
  ena[776] = HAVE_smuldi3_highpart;
  ena[777] = HAVE_smulv8hi3_highpart;
  ena[778] = HAVE_smulv16hi3_highpart;
  ena[779] = HAVE_umulsi3_highpart;
  ena[780] = HAVE_umuldi3_highpart;
  ena[781] = HAVE_umulv8hi3_highpart;
  ena[782] = HAVE_umulv16hi3_highpart;
  ena[783] = HAVE_cmpstrnsi;
  ena[784] = HAVE_movmemsi;
  ena[785] = HAVE_movmemdi;
  ena[786] = HAVE_setmemsi;
  ena[787] = HAVE_setmemdi;
  ena[788] = HAVE_strlensi;
  ena[789] = HAVE_strlendi;
  ena[790] = HAVE_fmasf4;
  ena[791] = HAVE_fmadf4;
  ena[792] = HAVE_fmav4sf4;
  ena[793] = HAVE_fmav2df4;
  ena[794] = HAVE_fmav8sf4;
  ena[795] = HAVE_fmav4df4;
  ena[796] = HAVE_fmav16sf4;
  ena[797] = HAVE_fmav8df4;
  ena[798] = HAVE_fmssf4;
  ena[799] = HAVE_fmsdf4;
  ena[800] = HAVE_fmsv4sf4;
  ena[801] = HAVE_fmsv2df4;
  ena[802] = HAVE_fmsv8sf4;
  ena[803] = HAVE_fmsv4df4;
  ena[804] = HAVE_fmsv16sf4;
  ena[805] = HAVE_fmsv8df4;
  ena[806] = HAVE_fnmasf4;
  ena[807] = HAVE_fnmadf4;
  ena[808] = HAVE_fnmav4sf4;
  ena[809] = HAVE_fnmav2df4;
  ena[810] = HAVE_fnmav8sf4;
  ena[811] = HAVE_fnmav4df4;
  ena[812] = HAVE_fnmav16sf4;
  ena[813] = HAVE_fnmav8df4;
  ena[814] = HAVE_fnmssf4;
  ena[815] = HAVE_fnmsdf4;
  ena[816] = HAVE_fnmsv4sf4;
  ena[817] = HAVE_fnmsv2df4;
  ena[818] = HAVE_fnmsv8sf4;
  ena[819] = HAVE_fnmsv4df4;
  ena[820] = HAVE_fnmsv16sf4;
  ena[821] = HAVE_fnmsv8df4;
  ena[822] = HAVE_rintsf2;
  ena[823] = HAVE_rintdf2;
  ena[824] = HAVE_rintxf2;
  ena[825] = HAVE_roundsf2;
  ena[826] = HAVE_rounddf2;
  ena[827] = HAVE_roundxf2;
  ena[828] = HAVE_roundv4sf2;
  ena[829] = HAVE_roundv2df2;
  ena[830] = HAVE_roundv8sf2;
  ena[831] = HAVE_roundv4df2;
  ena[832] = HAVE_roundv16sf2;
  ena[833] = HAVE_roundv8df2;
  ena[834] = HAVE_floorsf2;
  ena[835] = HAVE_floordf2;
  ena[836] = HAVE_floorxf2;
  ena[837] = HAVE_ceilsf2;
  ena[838] = HAVE_ceildf2;
  ena[839] = HAVE_ceilxf2;
  ena[840] = HAVE_btruncsf2;
  ena[841] = HAVE_btruncdf2;
  ena[842] = HAVE_btruncxf2;
  ena[843] = HAVE_nearbyintsf2;
  ena[844] = HAVE_nearbyintdf2;
  ena[845] = HAVE_nearbyintxf2;
  ena[846] = HAVE_acossf2;
  ena[847] = HAVE_acosdf2;
  ena[848] = HAVE_acosxf2;
  ena[849] = HAVE_asinsf2;
  ena[850] = HAVE_asindf2;
  ena[851] = HAVE_asinxf2;
  ena[852] = HAVE_atan2sf3;
  ena[853] = HAVE_atan2df3;
  ena[854] = HAVE_atan2xf3;
  ena[855] = HAVE_atansf2;
  ena[856] = HAVE_atandf2;
  ena[857] = HAVE_atanxf2;
  ena[858] = HAVE_copysignsf3;
  ena[859] = HAVE_copysigndf3;
  ena[860] = HAVE_copysigntf3;
  ena[861] = HAVE_copysignv4sf3;
  ena[862] = HAVE_copysignv2df3;
  ena[863] = HAVE_copysignv8sf3;
  ena[864] = HAVE_copysignv4df3;
  ena[865] = HAVE_copysignv16sf3;
  ena[866] = HAVE_copysignv8df3;
  ena[867] = HAVE_exp10sf2;
  ena[868] = HAVE_exp10df2;
  ena[869] = HAVE_exp10xf2;
  ena[870] = HAVE_exp2sf2;
  ena[871] = HAVE_exp2df2;
  ena[872] = HAVE_exp2xf2;
  ena[873] = HAVE_expsf2;
  ena[874] = HAVE_expdf2;
  ena[875] = HAVE_expxf2;
  ena[876] = HAVE_expm1sf2;
  ena[877] = HAVE_expm1df2;
  ena[878] = HAVE_expm1xf2;
  ena[879] = HAVE_fmodsf3;
  ena[880] = HAVE_fmoddf3;
  ena[881] = HAVE_fmodxf3;
  ena[882] = HAVE_ilogbsf2;
  ena[883] = HAVE_ilogbdf2;
  ena[884] = HAVE_ilogbxf2;
  ena[885] = HAVE_isinfsf2;
  ena[886] = HAVE_isinfdf2;
  ena[887] = HAVE_isinfxf2;
  ena[888] = HAVE_ldexpsf3;
  ena[889] = HAVE_ldexpdf3;
  ena[890] = HAVE_ldexpxf3;
  ena[891] = HAVE_log10sf2;
  ena[892] = HAVE_log10df2;
  ena[893] = HAVE_log10xf2;
  ena[894] = HAVE_log1psf2;
  ena[895] = HAVE_log1pdf2;
  ena[896] = HAVE_log1pxf2;
  ena[897] = HAVE_log2sf2;
  ena[898] = HAVE_log2df2;
  ena[899] = HAVE_log2xf2;
  ena[900] = HAVE_logsf2;
  ena[901] = HAVE_logdf2;
  ena[902] = HAVE_logxf2;
  ena[903] = HAVE_logbsf2;
  ena[904] = HAVE_logbdf2;
  ena[905] = HAVE_logbxf2;
  ena[906] = HAVE_remaindersf3;
  ena[907] = HAVE_remainderdf3;
  ena[908] = HAVE_remainderxf3;
  ena[909] = HAVE_scalbsf3;
  ena[910] = HAVE_scalbdf3;
  ena[911] = HAVE_scalbxf3;
  ena[912] = HAVE_signbitsf2;
  ena[913] = HAVE_signbitdf2;
  ena[914] = HAVE_signbitxf2;
  ena[915] = HAVE_significandsf2;
  ena[916] = HAVE_significanddf2;
  ena[917] = HAVE_significandxf2;
  ena[918] = HAVE_sincossf3;
  ena[919] = HAVE_sincosdf3;
  ena[920] = HAVE_sincosxf3;
  ena[921] = HAVE_tansf2;
  ena[922] = HAVE_tandf2;
  ena[923] = HAVE_tanxf2;
  ena[924] = HAVE_reduc_smax_v32qi;
  ena[925] = HAVE_reduc_smax_v16hi;
  ena[926] = HAVE_reduc_smax_v8si;
  ena[927] = HAVE_reduc_smax_v4di;
  ena[928] = HAVE_reduc_smax_v16si;
  ena[929] = HAVE_reduc_smax_v8di;
  ena[930] = HAVE_reduc_smax_v4sf;
  ena[931] = HAVE_reduc_smax_v8sf;
  ena[932] = HAVE_reduc_smax_v4df;
  ena[933] = HAVE_reduc_smax_v16sf;
  ena[934] = HAVE_reduc_smax_v8df;
  ena[935] = HAVE_reduc_smin_v32qi;
  ena[936] = HAVE_reduc_smin_v16hi;
  ena[937] = HAVE_reduc_smin_v8si;
  ena[938] = HAVE_reduc_smin_v4di;
  ena[939] = HAVE_reduc_smin_v16si;
  ena[940] = HAVE_reduc_smin_v8di;
  ena[941] = HAVE_reduc_smin_v4sf;
  ena[942] = HAVE_reduc_smin_v8sf;
  ena[943] = HAVE_reduc_smin_v4df;
  ena[944] = HAVE_reduc_smin_v16sf;
  ena[945] = HAVE_reduc_smin_v8df;
  ena[946] = HAVE_reduc_splus_v4sf;
  ena[947] = HAVE_reduc_splus_v2df;
  ena[948] = HAVE_reduc_splus_v8sf;
  ena[949] = HAVE_reduc_splus_v4df;
  ena[950] = HAVE_reduc_splus_v16sf;
  ena[951] = HAVE_reduc_splus_v8df;
  ena[952] = HAVE_reduc_umax_v32qi;
  ena[953] = HAVE_reduc_umax_v16hi;
  ena[954] = HAVE_reduc_umax_v8si;
  ena[955] = HAVE_reduc_umax_v4di;
  ena[956] = HAVE_reduc_umax_v16si;
  ena[957] = HAVE_reduc_umax_v8di;
  ena[958] = HAVE_reduc_umin_v8hi;
  ena[959] = HAVE_reduc_umin_v32qi;
  ena[960] = HAVE_reduc_umin_v16hi;
  ena[961] = HAVE_reduc_umin_v8si;
  ena[962] = HAVE_reduc_umin_v4di;
  ena[963] = HAVE_reduc_umin_v16si;
  ena[964] = HAVE_reduc_umin_v8di;
  ena[965] = HAVE_sdot_prodv8hi;
  ena[966] = HAVE_sdot_prodv4si;
  ena[967] = HAVE_sdot_prodv16hi;
  ena[968] = HAVE_maskloadv4si;
  ena[969] = HAVE_maskloadv2di;
  ena[970] = HAVE_maskloadv8si;
  ena[971] = HAVE_maskloadv4di;
  ena[972] = HAVE_maskloadv4sf;
  ena[973] = HAVE_maskloadv2df;
  ena[974] = HAVE_maskloadv8sf;
  ena[975] = HAVE_maskloadv4df;
  ena[976] = HAVE_maskstorev4si;
  ena[977] = HAVE_maskstorev2di;
  ena[978] = HAVE_maskstorev8si;
  ena[979] = HAVE_maskstorev4di;
  ena[980] = HAVE_maskstorev4sf;
  ena[981] = HAVE_maskstorev2df;
  ena[982] = HAVE_maskstorev8sf;
  ena[983] = HAVE_maskstorev4df;
  ena[984] = HAVE_vec_extractv8qi;
  ena[985] = HAVE_vec_extractv4hi;
  ena[986] = HAVE_vec_extractv2si;
  ena[987] = HAVE_vec_extractv16qi;
  ena[988] = HAVE_vec_extractv8hi;
  ena[989] = HAVE_vec_extractv4si;
  ena[990] = HAVE_vec_extractv2di;
  ena[991] = HAVE_vec_extractv32qi;
  ena[992] = HAVE_vec_extractv16hi;
  ena[993] = HAVE_vec_extractv8si;
  ena[994] = HAVE_vec_extractv4di;
  ena[995] = HAVE_vec_extractv16si;
  ena[996] = HAVE_vec_extractv8di;
  ena[997] = HAVE_vec_extractv2sf;
  ena[998] = HAVE_vec_extractv4sf;
  ena[999] = HAVE_vec_extractv2df;
  ena[1000] = HAVE_vec_extractv8sf;
  ena[1001] = HAVE_vec_extractv4df;
  ena[1002] = HAVE_vec_extractv16sf;
  ena[1003] = HAVE_vec_extractv8df;
  ena[1004] = HAVE_vec_initv8qi;
  ena[1005] = HAVE_vec_initv4hi;
  ena[1006] = HAVE_vec_initv2si;
  ena[1007] = HAVE_vec_initv16qi;
  ena[1008] = HAVE_vec_initv8hi;
  ena[1009] = HAVE_vec_initv4si;
  ena[1010] = HAVE_vec_initv2di;
  ena[1011] = HAVE_vec_initv32qi;
  ena[1012] = HAVE_vec_initv16hi;
  ena[1013] = HAVE_vec_initv8si;
  ena[1014] = HAVE_vec_initv4di;
  ena[1015] = HAVE_vec_initv16si;
  ena[1016] = HAVE_vec_initv8di;
  ena[1017] = HAVE_vec_initv2sf;
  ena[1018] = HAVE_vec_initv4sf;
  ena[1019] = HAVE_vec_initv2df;
  ena[1020] = HAVE_vec_initv8sf;
  ena[1021] = HAVE_vec_initv4df;
  ena[1022] = HAVE_vec_initv16sf;
  ena[1023] = HAVE_vec_initv8df;
  ena[1024] = HAVE_vec_pack_sfix_trunc_v2df;
  ena[1025] = HAVE_vec_pack_sfix_trunc_v4df;
  ena[1026] = HAVE_vec_pack_sfix_trunc_v8df;
  ena[1027] = HAVE_vec_pack_trunc_v8hi;
  ena[1028] = HAVE_vec_pack_trunc_v4si;
  ena[1029] = HAVE_vec_pack_trunc_v2di;
  ena[1030] = HAVE_vec_pack_trunc_v16hi;
  ena[1031] = HAVE_vec_pack_trunc_v8si;
  ena[1032] = HAVE_vec_pack_trunc_v4di;
  ena[1033] = HAVE_vec_pack_trunc_v8di;
  ena[1034] = HAVE_vec_pack_trunc_v2df;
  ena[1035] = HAVE_vec_pack_trunc_v4df;
  ena[1036] = HAVE_vec_pack_trunc_v8df;
  ena[1037] = HAVE_vec_pack_ufix_trunc_v2df;
  ena[1038] = HAVE_vec_pack_ufix_trunc_v4df;
  ena[1039] = HAVE_vec_pack_ufix_trunc_v8df;
  ena[1040] = HAVE_vec_perm_constv16qi;
  ena[1041] = HAVE_vec_perm_constv8hi;
  ena[1042] = HAVE_vec_perm_constv4si;
  ena[1043] = HAVE_vec_perm_constv2di;
  ena[1044] = HAVE_vec_perm_constv32qi;
  ena[1045] = HAVE_vec_perm_constv16hi;
  ena[1046] = HAVE_vec_perm_constv8si;
  ena[1047] = HAVE_vec_perm_constv4di;
  ena[1048] = HAVE_vec_perm_constv16si;
  ena[1049] = HAVE_vec_perm_constv8di;
  ena[1050] = HAVE_vec_perm_constv4sf;
  ena[1051] = HAVE_vec_perm_constv2df;
  ena[1052] = HAVE_vec_perm_constv8sf;
  ena[1053] = HAVE_vec_perm_constv4df;
  ena[1054] = HAVE_vec_perm_constv16sf;
  ena[1055] = HAVE_vec_perm_constv8df;
  ena[1056] = HAVE_vec_permv16qi;
  ena[1057] = HAVE_vec_permv8hi;
  ena[1058] = HAVE_vec_permv4si;
  ena[1059] = HAVE_vec_permv2di;
  ena[1060] = HAVE_vec_permv32qi;
  ena[1061] = HAVE_vec_permv16hi;
  ena[1062] = HAVE_vec_permv8si;
  ena[1063] = HAVE_vec_permv4di;
  ena[1064] = HAVE_vec_permv16si;
  ena[1065] = HAVE_vec_permv8di;
  ena[1066] = HAVE_vec_permv4sf;
  ena[1067] = HAVE_vec_permv2df;
  ena[1068] = HAVE_vec_permv8sf;
  ena[1069] = HAVE_vec_permv4df;
  ena[1070] = HAVE_vec_permv16sf;
  ena[1071] = HAVE_vec_permv8df;
  ena[1072] = HAVE_vec_setv8qi;
  ena[1073] = HAVE_vec_setv4hi;
  ena[1074] = HAVE_vec_setv2si;
  ena[1075] = HAVE_vec_setv16qi;
  ena[1076] = HAVE_vec_setv8hi;
  ena[1077] = HAVE_vec_setv4si;
  ena[1078] = HAVE_vec_setv2di;
  ena[1079] = HAVE_vec_setv32qi;
  ena[1080] = HAVE_vec_setv16hi;
  ena[1081] = HAVE_vec_setv8si;
  ena[1082] = HAVE_vec_setv4di;
  ena[1083] = HAVE_vec_setv16si;
  ena[1084] = HAVE_vec_setv8di;
  ena[1085] = HAVE_vec_setv2sf;
  ena[1086] = HAVE_vec_setv4sf;
  ena[1087] = HAVE_vec_setv2df;
  ena[1088] = HAVE_vec_setv8sf;
  ena[1089] = HAVE_vec_setv4df;
  ena[1090] = HAVE_vec_setv16sf;
  ena[1091] = HAVE_vec_setv8df;
  ena[1092] = HAVE_vec_shl_v16qi;
  ena[1093] = HAVE_vec_shl_v8hi;
  ena[1094] = HAVE_vec_shl_v4si;
  ena[1095] = HAVE_vec_shl_v2di;
  ena[1096] = HAVE_vec_shr_v16qi;
  ena[1097] = HAVE_vec_shr_v8hi;
  ena[1098] = HAVE_vec_shr_v4si;
  ena[1099] = HAVE_vec_shr_v2di;
  ena[1100] = HAVE_vec_unpacks_float_hi_v8hi;
  ena[1101] = HAVE_vec_unpacks_float_hi_v4si;
  ena[1102] = HAVE_vec_unpacks_float_hi_v16hi;
  ena[1103] = HAVE_vec_unpacks_float_hi_v8si;
  ena[1104] = HAVE_vec_unpacks_float_hi_v32hi;
  ena[1105] = HAVE_vec_unpacks_float_hi_v16si;
  ena[1106] = HAVE_vec_unpacks_float_lo_v8hi;
  ena[1107] = HAVE_vec_unpacks_float_lo_v4si;
  ena[1108] = HAVE_vec_unpacks_float_lo_v16hi;
  ena[1109] = HAVE_vec_unpacks_float_lo_v8si;
  ena[1110] = HAVE_vec_unpacks_float_lo_v32hi;
  ena[1111] = HAVE_vec_unpacks_float_lo_v16si;
  ena[1112] = HAVE_vec_unpacks_hi_v16qi;
  ena[1113] = HAVE_vec_unpacks_hi_v8hi;
  ena[1114] = HAVE_vec_unpacks_hi_v4si;
  ena[1115] = HAVE_vec_unpacks_hi_v32qi;
  ena[1116] = HAVE_vec_unpacks_hi_v16hi;
  ena[1117] = HAVE_vec_unpacks_hi_v8si;
  ena[1118] = HAVE_vec_unpacks_hi_v32hi;
  ena[1119] = HAVE_vec_unpacks_hi_v16si;
  ena[1120] = HAVE_vec_unpacks_hi_v4sf;
  ena[1121] = HAVE_vec_unpacks_hi_v8sf;
  ena[1122] = HAVE_vec_unpacks_hi_v16sf;
  ena[1123] = HAVE_vec_unpacks_lo_v16qi;
  ena[1124] = HAVE_vec_unpacks_lo_v8hi;
  ena[1125] = HAVE_vec_unpacks_lo_v4si;
  ena[1126] = HAVE_vec_unpacks_lo_v32qi;
  ena[1127] = HAVE_vec_unpacks_lo_v16hi;
  ena[1128] = HAVE_vec_unpacks_lo_v8si;
  ena[1129] = HAVE_vec_unpacks_lo_v32hi;
  ena[1130] = HAVE_vec_unpacks_lo_v16si;
  ena[1131] = HAVE_vec_unpacks_lo_v4sf;
  ena[1132] = HAVE_vec_unpacks_lo_v8sf;
  ena[1133] = HAVE_vec_unpacks_lo_v16sf;
  ena[1134] = HAVE_vec_unpacku_float_hi_v8hi;
  ena[1135] = HAVE_vec_unpacku_float_hi_v4si;
  ena[1136] = HAVE_vec_unpacku_float_hi_v16hi;
  ena[1137] = HAVE_vec_unpacku_float_hi_v8si;
  ena[1138] = HAVE_vec_unpacku_float_hi_v32hi;
  ena[1139] = HAVE_vec_unpacku_float_hi_v16si;
  ena[1140] = HAVE_vec_unpacku_float_lo_v8hi;
  ena[1141] = HAVE_vec_unpacku_float_lo_v4si;
  ena[1142] = HAVE_vec_unpacku_float_lo_v16hi;
  ena[1143] = HAVE_vec_unpacku_float_lo_v8si;
  ena[1144] = HAVE_vec_unpacku_float_lo_v32hi;
  ena[1145] = HAVE_vec_unpacku_float_lo_v16si;
  ena[1146] = HAVE_vec_unpacku_hi_v16qi;
  ena[1147] = HAVE_vec_unpacku_hi_v8hi;
  ena[1148] = HAVE_vec_unpacku_hi_v4si;
  ena[1149] = HAVE_vec_unpacku_hi_v32qi;
  ena[1150] = HAVE_vec_unpacku_hi_v16hi;
  ena[1151] = HAVE_vec_unpacku_hi_v8si;
  ena[1152] = HAVE_vec_unpacku_hi_v32hi;
  ena[1153] = HAVE_vec_unpacku_hi_v16si;
  ena[1154] = HAVE_vec_unpacku_lo_v16qi;
  ena[1155] = HAVE_vec_unpacku_lo_v8hi;
  ena[1156] = HAVE_vec_unpacku_lo_v4si;
  ena[1157] = HAVE_vec_unpacku_lo_v32qi;
  ena[1158] = HAVE_vec_unpacku_lo_v16hi;
  ena[1159] = HAVE_vec_unpacku_lo_v8si;
  ena[1160] = HAVE_vec_unpacku_lo_v32hi;
  ena[1161] = HAVE_vec_unpacku_lo_v16si;
  ena[1162] = HAVE_vec_widen_smult_even_v4si;
  ena[1163] = HAVE_vec_widen_smult_even_v8si;
  ena[1164] = HAVE_vec_widen_smult_even_v16si;
  ena[1165] = HAVE_vec_widen_smult_hi_v16qi;
  ena[1166] = HAVE_vec_widen_smult_hi_v8hi;
  ena[1167] = HAVE_vec_widen_smult_hi_v4si;
  ena[1168] = HAVE_vec_widen_smult_hi_v32qi;
  ena[1169] = HAVE_vec_widen_smult_hi_v16hi;
  ena[1170] = HAVE_vec_widen_smult_hi_v8si;
  ena[1171] = HAVE_vec_widen_smult_lo_v16qi;
  ena[1172] = HAVE_vec_widen_smult_lo_v8hi;
  ena[1173] = HAVE_vec_widen_smult_lo_v4si;
  ena[1174] = HAVE_vec_widen_smult_lo_v32qi;
  ena[1175] = HAVE_vec_widen_smult_lo_v16hi;
  ena[1176] = HAVE_vec_widen_smult_lo_v8si;
  ena[1177] = HAVE_vec_widen_smult_odd_v4si;
  ena[1178] = HAVE_vec_widen_smult_odd_v8si;
  ena[1179] = HAVE_vec_widen_smult_odd_v16si;
  ena[1180] = HAVE_vec_widen_umult_even_v4si;
  ena[1181] = HAVE_vec_widen_umult_even_v8si;
  ena[1182] = HAVE_vec_widen_umult_even_v16si;
  ena[1183] = HAVE_vec_widen_umult_hi_v16qi;
  ena[1184] = HAVE_vec_widen_umult_hi_v8hi;
  ena[1185] = HAVE_vec_widen_umult_hi_v4si;
  ena[1186] = HAVE_vec_widen_umult_hi_v32qi;
  ena[1187] = HAVE_vec_widen_umult_hi_v16hi;
  ena[1188] = HAVE_vec_widen_umult_hi_v8si;
  ena[1189] = HAVE_vec_widen_umult_lo_v16qi;
  ena[1190] = HAVE_vec_widen_umult_lo_v8hi;
  ena[1191] = HAVE_vec_widen_umult_lo_v4si;
  ena[1192] = HAVE_vec_widen_umult_lo_v32qi;
  ena[1193] = HAVE_vec_widen_umult_lo_v16hi;
  ena[1194] = HAVE_vec_widen_umult_lo_v8si;
  ena[1195] = HAVE_vec_widen_umult_odd_v4si;
  ena[1196] = HAVE_vec_widen_umult_odd_v8si;
  ena[1197] = HAVE_vec_widen_umult_odd_v16si;
  ena[1198] = HAVE_atomic_addqi;
  ena[1199] = HAVE_atomic_addhi;
  ena[1200] = HAVE_atomic_addsi;
  ena[1201] = HAVE_atomic_adddi;
  ena[1202] = HAVE_atomic_andqi;
  ena[1203] = HAVE_atomic_andhi;
  ena[1204] = HAVE_atomic_andsi;
  ena[1205] = HAVE_atomic_anddi;
  ena[1206] = HAVE_atomic_compare_and_swapqi;
  ena[1207] = HAVE_atomic_compare_and_swaphi;
  ena[1208] = HAVE_atomic_compare_and_swapsi;
  ena[1209] = HAVE_atomic_compare_and_swapdi;
  ena[1210] = HAVE_atomic_compare_and_swapti;
  ena[1211] = HAVE_atomic_exchangeqi;
  ena[1212] = HAVE_atomic_exchangehi;
  ena[1213] = HAVE_atomic_exchangesi;
  ena[1214] = HAVE_atomic_exchangedi;
  ena[1215] = HAVE_atomic_fetch_addqi;
  ena[1216] = HAVE_atomic_fetch_addhi;
  ena[1217] = HAVE_atomic_fetch_addsi;
  ena[1218] = HAVE_atomic_fetch_adddi;
  ena[1219] = HAVE_atomic_loadqi;
  ena[1220] = HAVE_atomic_loadhi;
  ena[1221] = HAVE_atomic_loadsi;
  ena[1222] = HAVE_atomic_loaddi;
  ena[1223] = HAVE_atomic_orqi;
  ena[1224] = HAVE_atomic_orhi;
  ena[1225] = HAVE_atomic_orsi;
  ena[1226] = HAVE_atomic_ordi;
  ena[1227] = HAVE_atomic_storeqi;
  ena[1228] = HAVE_atomic_storehi;
  ena[1229] = HAVE_atomic_storesi;
  ena[1230] = HAVE_atomic_storedi;
  ena[1231] = HAVE_atomic_subqi;
  ena[1232] = HAVE_atomic_subhi;
  ena[1233] = HAVE_atomic_subsi;
  ena[1234] = HAVE_atomic_subdi;
  ena[1235] = HAVE_atomic_xorqi;
  ena[1236] = HAVE_atomic_xorhi;
  ena[1237] = HAVE_atomic_xorsi;
  ena[1238] = HAVE_atomic_xordi;
}

static int
lookup_handler (unsigned scode)
{
  int l = 0, h = ARRAY_SIZE (pats), m;
  while (h > l)
    {
      m = (h + l) / 2;
      if (scode == pats[m].scode)
        return m;
      else if (scode < pats[m].scode)
        h = m;
      else
        l = m + 1;
    }
  return -1;
}

enum insn_code
raw_optab_handler (unsigned scode)
{
  int i = lookup_handler (scode);
  return (i >= 0 && this_fn_optabs->pat_enable[i]
          ? pats[i].icode : CODE_FOR_nothing);
}

bool
swap_optab_enable (optab op, enum machine_mode m, bool set)
{
  unsigned scode = (op << 16) | m;
  int i = lookup_handler (scode);
  if (i >= 0)
    {
      bool ret = this_fn_optabs->pat_enable[i];
      this_fn_optabs->pat_enable[i] = set;
      return ret;
    }
  else
    {
      gcc_assert (!set);
      return false;
    }
}

const struct convert_optab_libcall_d convlib_def[NUM_CONVLIB_OPTABS] = {
  { "extend", gen_extend_conv_libfunc },
  { "trunc", gen_trunc_conv_libfunc },
  { NULL, NULL },
  { "fix", gen_fp_to_int_conv_libfunc },
  { "fixuns", gen_fp_to_int_conv_libfunc },
  { "float", gen_int_to_fp_conv_libfunc },
  { NULL, gen_ufloat_conv_libfunc },
  { "lrint", gen_int_to_fp_nondecimal_conv_libfunc },
  { "lround", gen_int_to_fp_nondecimal_conv_libfunc },
  { "lfloor", gen_int_to_fp_nondecimal_conv_libfunc },
  { "lceil", gen_int_to_fp_nondecimal_conv_libfunc },
  { "fract", gen_fract_conv_libfunc },
  { "fractuns", gen_fractuns_conv_libfunc },
  { "satfract", gen_satfract_conv_libfunc },
  { "satfractuns", gen_satfractuns_conv_libfunc },
};

const struct optab_libcall_d normlib_def[NUM_NORMLIB_OPTABS] = {
  { '3', "add", gen_int_fp_fixed_libfunc },
  { '3', "add", gen_intv_fp_libfunc },
  { '3', "ssadd", gen_signed_fixed_libfunc },
  { '3', "usadd", gen_unsigned_fixed_libfunc },
  { '3', "sub", gen_int_fp_fixed_libfunc },
  { '3', "sub", gen_intv_fp_libfunc },
  { '3', "sssub", gen_signed_fixed_libfunc },
  { '3', "ussub", gen_unsigned_fixed_libfunc },
  { '3', "mul", gen_int_fp_fixed_libfunc },
  { '3', "mul", gen_intv_fp_libfunc },
  { '3', "ssmul", gen_signed_fixed_libfunc },
  { '3', "usmul", gen_unsigned_fixed_libfunc },
  { '3', "div", gen_int_fp_signed_fixed_libfunc },
  { '3', "divv", gen_int_libfunc },
  { '3', "ssdiv", gen_signed_fixed_libfunc },
  { '3', "udiv", gen_int_unsigned_fixed_libfunc },
  { '3', "usdiv", gen_unsigned_fixed_libfunc },
  { '4', "divmod", gen_int_libfunc },
  { '4', "udivmod", gen_int_libfunc },
  { '3', "mod", gen_int_libfunc },
  { '3', "umod", gen_int_libfunc },
  { '2', "ftrunc", gen_fp_libfunc },
  { '3', "and", gen_int_libfunc },
  { '3', "ior", gen_int_libfunc },
  { '3', "xor", gen_int_libfunc },
  { '3', "ashl", gen_int_fixed_libfunc },
  { '3', "ssashl", gen_signed_fixed_libfunc },
  { '3', "usashl", gen_unsigned_fixed_libfunc },
  { '3', "ashr", gen_int_signed_fixed_libfunc },
  { '3', "lshr", gen_int_unsigned_fixed_libfunc },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '3', "min", gen_int_fp_libfunc },
  { '3', "max", gen_int_fp_libfunc },
  { '3', "umin", gen_int_libfunc },
  { '3', "umax", gen_int_libfunc },
  { '2', "neg", gen_int_fp_fixed_libfunc },
  { '2', "neg", gen_intv_fp_libfunc },
  { '2', "ssneg", gen_signed_fixed_libfunc },
  { '2', "usneg", gen_unsigned_fixed_libfunc },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '2', "one_cmpl", gen_int_libfunc },
  { '\0', NULL, NULL },
  { '2', "ffs", gen_int_libfunc },
  { '2', "clz", gen_int_libfunc },
  { '2', "ctz", gen_int_libfunc },
  { '2', "clrsb", gen_int_libfunc },
  { '2', "popcount", gen_int_libfunc },
  { '2', "parity", gen_int_libfunc },
  { '2', "cmp", gen_int_fp_fixed_libfunc },
  { '2', "ucmp", gen_int_libfunc },
  { '2', "eq", gen_fp_libfunc },
  { '2', "ne", gen_fp_libfunc },
  { '2', "gt", gen_fp_libfunc },
  { '2', "ge", gen_fp_libfunc },
  { '2', "lt", gen_fp_libfunc },
  { '2', "le", gen_fp_libfunc },
  { '2', "unord", gen_fp_libfunc },
  { '2', "powi", gen_fp_libfunc },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
  { '\0', NULL, NULL },
};

enum rtx_code const optab_to_code_[NUM_OPTABS] = {
  UNKNOWN,
  SIGN_EXTEND,
  TRUNCATE,
  ZERO_EXTEND,
  FIX,
  UNSIGNED_FIX,
  FLOAT,
  UNSIGNED_FLOAT,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  FRACT_CONVERT,
  UNSIGNED_FRACT_CONVERT,
  SAT_FRACT,
  UNSIGNED_SAT_FRACT,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  PLUS,
  PLUS,
  SS_PLUS,
  US_PLUS,
  MINUS,
  MINUS,
  SS_MINUS,
  US_MINUS,
  MULT,
  MULT,
  SS_MULT,
  US_MULT,
  DIV,
  DIV,
  SS_DIV,
  UDIV,
  US_DIV,
  UNKNOWN,
  UNKNOWN,
  MOD,
  UMOD,
  UNKNOWN,
  AND,
  IOR,
  XOR,
  ASHIFT,
  SS_ASHIFT,
  US_ASHIFT,
  ASHIFTRT,
  LSHIFTRT,
  ROTATE,
  ROTATERT,
  ASHIFT,
  ASHIFTRT,
  LSHIFTRT,
  ROTATE,
  ROTATERT,
  SMIN,
  SMAX,
  UMIN,
  UMAX,
  NEG,
  NEG,
  SS_NEG,
  US_NEG,
  ABS,
  ABS,
  NOT,
  BSWAP,
  FFS,
  CLZ,
  CTZ,
  CLRSB,
  POPCOUNT,
  PARITY,
  UNKNOWN,
  UNKNOWN,
  EQ,
  NE,
  GT,
  GE,
  LT,
  LE,
  UNORDERED,
  UNKNOWN,
  SQRT,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  SET,
  STRICT_LOW_PART,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  COMPARE,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  FMA,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
  UNKNOWN,
};

const optab code_to_optab_[NUM_RTX_CODE] = {
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  mov_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  movstrict_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  cbranch_optab,
  add_optab,
  sub_optab,
  neg_optab,
  smul_optab,
  ssmul_optab,
  usmul_optab,
  sdiv_optab,
  ssdiv_optab,
  usdiv_optab,
  smod_optab,
  udiv_optab,
  umod_optab,
  and_optab,
  ior_optab,
  xor_optab,
  one_cmpl_optab,
  ashl_optab,
  rotl_optab,
  ashr_optab,
  lshr_optab,
  rotr_optab,
  smin_optab,
  smax_optab,
  umin_optab,
  umax_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  ne_optab,
  eq_optab,
  ge_optab,
  gt_optab,
  le_optab,
  lt_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unord_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  abs_optab,
  sqrt_optab,
  bswap_optab,
  ffs_optab,
  clrsb_optab,
  clz_optab,
  ctz_optab,
  popcount_optab,
  parity_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  ssadd_optab,
  usadd_optab,
  sssub_optab,
  ssneg_optab,
  usneg_optab,
  unknown_optab,
  ssashl_optab,
  usashl_optab,
  ussub_optab,
  unknown_optab,
  unknown_optab,
  fma_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
  unknown_optab,
};

