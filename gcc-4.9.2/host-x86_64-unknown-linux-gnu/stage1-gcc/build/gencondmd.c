/* Generated automatically by the program `genconditions' from the target
   machine description file.  */

#include "bconfig.h"
#include "system.h"

/* It is necessary, but not entirely safe, to include the headers below
   in a generator program.  As a defensive measure, don't do so when the
   table isn't going to have anything in it.  */
#if GCC_VERSION >= 3001

/* Do not allow checking to confuse the issue.  */
#undef ENABLE_CHECKING
#undef ENABLE_TREE_CHECKING
#undef ENABLE_RTL_CHECKING
#undef ENABLE_RTL_FLAG_CHECKING
#undef ENABLE_GC_CHECKING
#undef ENABLE_GC_ALWAYS_COLLECT

#include "coretypes.h"
#include "tm.h"
#include "insn-constants.h"
#include "rtl.h"
#include "tm_p.h"
#include "function.h"

/* Fake - insn-config.h doesn't exist yet.  */
#define MAX_RECOG_OPERANDS 10
#define MAX_DUP_OPERANDS 10
#define MAX_INSNS_PER_SPLIT 5

#include "regs.h"
#include "recog.h"
#include "output.h"
#include "flags.h"
#include "hard-reg-set.h"
#include "resource.h"
#include "diagnostic-core.h"
#include "reload.h"
#include "tm-constrs.h"

#define HAVE_eh_return 1
#include "except.h"

/* Dummy external declarations.  */
extern rtx insn;
extern rtx ins1;
extern rtx operands[];

#endif /* gcc >= 3.0.1 */

/* Structure definition duplicated from gensupport.h rather than
   drag in that file and its dependencies.  */
struct c_test
{
  const char *expr;
  int value;
};

/* This table lists each condition found in the machine description.
   Each condition is mapped to its truth value (0 or 1), or -1 if that
   cannot be calculated at compile time.
   If we don't have __builtin_constant_p, or it's not acceptable in array
   initializers, fall back to assuming that all conditions potentially
   vary at run time.  It works in 3.0.1 and later; 3.0 only when not
   optimizing.  */

#if GCC_VERSION >= 3001
static const struct c_test insn_conditions[] = {

#line 1003 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (AND, V4HImode, operands)",
    __builtin_constant_p 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (AND, V4HImode, operands))
    ? (int) 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (AND, V4HImode, operands))
    : -1 },
  { "(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && ((((((((word_mode == SImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17371 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17371 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 1058 "../.././gcc/config/i386/i386.md"
  { "Pmode == DImode",
    __builtin_constant_p 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)
    ? (int) 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)
    : -1 },
#line 11429 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && !SIBLING_CALL_P (insn)",
    __builtin_constant_p 
#line 11429 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !SIBLING_CALL_P (insn))
    ? (int) 
#line 11429 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !SIBLING_CALL_P (insn))
    : -1 },
  { "(TARGET_AVOID_VECTOR_DECODE\n\
   && SSE_FLOAT_MODE_P (SFmode)\n\
   && optimize_insn_for_speed_p ()) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 4416 "../.././gcc/config/i386/i386.md"
(TARGET_AVOID_VECTOR_DECODE
   && SSE_FLOAT_MODE_P (SFmode)
   && optimize_insn_for_speed_p ()) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 4416 "../.././gcc/config/i386/i386.md"
(TARGET_AVOID_VECTOR_DECODE
   && SSE_FLOAT_MODE_P (SFmode)
   && optimize_insn_for_speed_p ()) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_USE_BT || optimize_function_for_size_p (cfun)) && ( 1)",
    __builtin_constant_p (
#line 10863 "../.././gcc/config/i386/i386.md"
(TARGET_USE_BT || optimize_function_for_size_p (cfun)) && 
#line 10865 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 10863 "../.././gcc/config/i386/i386.md"
(TARGET_USE_BT || optimize_function_for_size_p (cfun)) && 
#line 10865 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "(TARGET_AVX && 1\n\
   && avx_vpermilp_parallel (operands[2], V16SFmode)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V16SFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V16SFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "((optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& (TARGET_SHIFT1\n\
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFT, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9410 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9410 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 1602 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)
   && 1 && 1)
    ? (int) 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)
   && 1 && 1)
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)
   && 1 && 1)
    : -1 },
  { "(TARGET_SSE2) && (Pmode == DImode)",
    __builtin_constant_p (
#line 10790 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 10790 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(TARGET_CMPXCHG) && (TARGET_64BIT && TARGET_CMPXCHG16B)",
    __builtin_constant_p (
#line 343 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG) && 
#line 331 "../.././gcc/config/i386/sync.md"
(TARGET_64BIT && TARGET_CMPXCHG16B))
    ? (int) (
#line 343 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG) && 
#line 331 "../.././gcc/config/i386/sync.md"
(TARGET_64BIT && TARGET_CMPXCHG16B))
    : -1 },
#line 14335 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX && 1",
    __builtin_constant_p 
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1)
    ? (int) 
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1)
    : -1 },
  { "(ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (XOR, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_SSE_MATH) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1523 "../.././gcc/config/i386/sse.md"
(TARGET_SSE_MATH) && 
#line 195 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1523 "../.././gcc/config/i386/sse.md"
(TARGET_SSE_MATH) && 
#line 195 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 14832 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH",
    __builtin_constant_p 
#line 14832 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
    ? (int) 
#line 14832 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    : -1 },
#line 7469 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
    && GET_MODE (operands[2]) != QImode\n\
    && (!REG_P (operands[2]) || ANY_QI_REG_P (operands[2]))\n\
    && ((ix86_match_ccmode (insn, CCZmode)\n\
	 && !(INTVAL (operands[3]) & ~255))\n\
	|| (ix86_match_ccmode (insn, CCNOmode)\n\
	    && !(INTVAL (operands[3]) & ~127)))",
    __builtin_constant_p 
#line 7469 "../.././gcc/config/i386/i386.md"
(reload_completed
    && GET_MODE (operands[2]) != QImode
    && (!REG_P (operands[2]) || ANY_QI_REG_P (operands[2]))
    && ((ix86_match_ccmode (insn, CCZmode)
	 && !(INTVAL (operands[3]) & ~255))
	|| (ix86_match_ccmode (insn, CCNOmode)
	    && !(INTVAL (operands[3]) & ~127))))
    ? (int) 
#line 7469 "../.././gcc/config/i386/i386.md"
(reload_completed
    && GET_MODE (operands[2]) != QImode
    && (!REG_P (operands[2]) || ANY_QI_REG_P (operands[2]))
    && ((ix86_match_ccmode (insn, CCZmode)
	 && !(INTVAL (operands[3]) & ~255))
	|| (ix86_match_ccmode (insn, CCNOmode)
	    && !(INTVAL (operands[3]) & ~127))))
    : -1 },
#line 1499 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE\n\
   && (TARGET_USE_SIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 1499 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 1499 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
#line 6215 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (MINUS, HImode, operands)",
    __builtin_constant_p 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, HImode, operands))
    ? (int) 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)
   && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)
   && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V32QImode)\n\
       == GET_MODE_NUNITS (V32QImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V32QImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V32QImode)))
    : -1 },
#line 14943 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH\n\
       && !flag_trapping_math)",
    __builtin_constant_p 
#line 14943 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && !flag_trapping_math))
    ? (int) 
#line 14943 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && !flag_trapping_math))
    : -1 },
#line 17242 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT\n\
   && exact_log2 (INTVAL (operands[2])) >= 0\n\
   && REGNO (operands[0]) == REGNO (operands[1])\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 17242 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && exact_log2 (INTVAL (operands[2])) >= 0
   && REGNO (operands[0]) == REGNO (operands[1])
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 17242 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && exact_log2 (INTVAL (operands[2])) >= 0
   && REGNO (operands[0]) == REGNO (operands[1])
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
#line 17612 "../.././gcc/config/i386/i386.md"
  { "TARGET_PRFCHW",
    __builtin_constant_p 
#line 17612 "../.././gcc/config/i386/i386.md"
(TARGET_PRFCHW)
    ? (int) 
#line 17612 "../.././gcc/config/i386/i386.md"
(TARGET_PRFCHW)
    : -1 },
#line 5859 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (PLUS, HImode, operands)\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])",
    __builtin_constant_p 
#line 5859 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, HImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    ? (int) 
#line 5859 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, HImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    : -1 },
#line 757 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_SSE || TARGET_3DNOW_A)\n\
   && ix86_binary_operator_ok (MULT, V4HImode, operands)",
    __builtin_constant_p 
#line 757 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (MULT, V4HImode, operands))
    ? (int) 
#line 757 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (MULT, V4HImode, operands))
    : -1 },
#line 14672 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14672 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14672 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
  { "(TARGET_64BIT && TARGET_XSAVE) && (TARGET_XSAVEOPT)",
    __builtin_constant_p (
#line 17910 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_XSAVE) && 
#line 17871 "../.././gcc/config/i386/i386.md"
(TARGET_XSAVEOPT))
    ? (int) (
#line 17910 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_XSAVE) && 
#line 17871 "../.././gcc/config/i386/i386.md"
(TARGET_XSAVEOPT))
    : -1 },
#line 3299 "../.././gcc/config/i386/sse.md"
  { "TARGET_FMA || TARGET_AVX512F",
    __builtin_constant_p 
#line 3299 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_AVX512F)
    ? (int) 
#line 3299 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_AVX512F)
    : -1 },
  { "(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == DImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17383 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17383 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 7234 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 7234 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 7234 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
  { "(TARGET_80387 || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 4692 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 4692 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 7287 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 7287 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 7287 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
  { "(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE) && ( reload_completed)",
    __builtin_constant_p (
#line 1461 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE) && 
#line 1463 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 1461 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE) && 
#line 1463 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V32QImode)\n\
       == GET_MODE_NUNITS (V4DFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V4DFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V4DFmode)))
    : -1 },
#line 9882 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (LSHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, SImode, operands))
    ? (int) 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, SImode, operands))
    : -1 },
  { "((16 == 64) && 1) && (TARGET_FMA || TARGET_FMA4)",
    __builtin_constant_p (
#line 2986 "../.././gcc/config/i386/sse.md"
((16 == 64) && 1) && 
#line 2774 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4))
    ? (int) (
#line 2986 "../.././gcc/config/i386/sse.md"
((16 == 64) && 1) && 
#line 2774 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4))
    : -1 },
#line 10215 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && TARGET_BMI2 && reload_completed",
    __builtin_constant_p 
#line 10215 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_BMI2 && reload_completed)
    ? (int) 
#line 10215 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_BMI2 && reload_completed)
    : -1 },
  { "(TARGET_XOP) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 13717 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 2747 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 13717 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 2747 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 928 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1",
    __builtin_constant_p 
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1)
    ? (int) 
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1)
    : -1 },
  { "(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == DImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17383 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17383 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
#line 12307 "../.././gcc/config/i386/i386.md"
  { "TARGET_TBM",
    __builtin_constant_p 
#line 12307 "../.././gcc/config/i386/i386.md"
(TARGET_TBM)
    ? (int) 
#line 12307 "../.././gcc/config/i386/i386.md"
(TARGET_TBM)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 4425 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || DImode != DImode))\n\
	&& TARGET_SSE_MATH)\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ())
    ? (int) 
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ())
    : -1 },
#line 12176 "../.././gcc/config/i386/sse.md"
  { "TARGET_ROUND && !flag_trapping_math",
    __builtin_constant_p 
#line 12176 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND && !flag_trapping_math)
    ? (int) 
#line 12176 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND && !flag_trapping_math)
    : -1 },
#line 4060 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSE2 && TARGET_MIX_SSE_I387 && flag_unsafe_math_optimizations",
    __builtin_constant_p 
#line 4060 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_MIX_SSE_I387 && flag_unsafe_math_optimizations)
    ? (int) 
#line 4060 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_MIX_SSE_I387 && flag_unsafe_math_optimizations)
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V32HImode)\n\
       == GET_MODE_NUNITS (V8DFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V8DFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V8DFmode)))
    : -1 },
  { "(TARGET_LWP) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 18093 "../.././gcc/config/i386/i386.md"
(TARGET_LWP) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 18093 "../.././gcc/config/i386/i386.md"
(TARGET_LWP) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 15257 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 15257 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && can_create_pseudo_p ())
    ? (int) 
#line 15257 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && can_create_pseudo_p ())
    : -1 },
#line 9598 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && ((unsigned) exact_log2 (INTVAL (operands[3]))\n\
       < GET_MODE_NUNITS (V8HImode))",
    __builtin_constant_p 
#line 9598 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V8HImode)))
    ? (int) 
#line 9598 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V8HImode)))
    : -1 },
#line 8181 "../.././gcc/config/i386/i386.md"
  { "TARGET_AVX512F && ix86_match_ccmode (insn, CCZmode)",
    __builtin_constant_p 
#line 8181 "../.././gcc/config/i386/i386.md"
(TARGET_AVX512F && ix86_match_ccmode (insn, CCZmode))
    ? (int) 
#line 8181 "../.././gcc/config/i386/i386.md"
(TARGET_AVX512F && ix86_match_ccmode (insn, CCZmode))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V16HImode)\n\
       == GET_MODE_NUNITS (V16HImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V16HImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V16HImode)))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V8SImode)\n\
       == GET_MODE_NUNITS (V16HImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V16HImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V16HImode)))
    : -1 },
#line 17231 "../.././gcc/config/i386/i386.md"
  { "exact_log2 (INTVAL (operands[1])) >= 0\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 17231 "../.././gcc/config/i386/i386.md"
(exact_log2 (INTVAL (operands[1])) >= 0
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 17231 "../.././gcc/config/i386/i386.md"
(exact_log2 (INTVAL (operands[1])) >= 0
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
  { "(TARGET_SSE && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && ( reload_completed)",
    __builtin_constant_p (
#line 5777 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 5779 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) (
#line 5777 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 5779 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
#line 9410 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& (TARGET_SHIFT1\n\
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFT, QImode, operands)",
    __builtin_constant_p 
#line 9410 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, QImode, operands))
    ? (int) 
#line 9410 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, QImode, operands))
    : -1 },
#line 3267 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
   && (GET_MODE (operands[0]) == TFmode\n\
       || GET_MODE (operands[0]) == XFmode\n\
       || GET_MODE (operands[0]) == DFmode)\n\
   && (operands[2] = find_constant_src (insn))",
    __builtin_constant_p 
#line 3267 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (GET_MODE (operands[0]) == TFmode
       || GET_MODE (operands[0]) == XFmode
       || GET_MODE (operands[0]) == DFmode)
   && (operands[2] = find_constant_src (insn)))
    ? (int) 
#line 3267 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (GET_MODE (operands[0]) == TFmode
       || GET_MODE (operands[0]) == XFmode
       || GET_MODE (operands[0]) == DFmode)
   && (operands[2] = find_constant_src (insn)))
    : -1 },
  { "((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == SImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17345 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17345 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 8546 "../.././gcc/config/i386/i386.md"
  { "TARGET_MIX_SSE_I387 && SSE_FLOAT_MODE_P (DFmode)",
    __builtin_constant_p 
#line 8546 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387 && SSE_FLOAT_MODE_P (DFmode))
    ? (int) 
#line 8546 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387 && SSE_FLOAT_MODE_P (DFmode))
    : -1 },
#line 8352 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
    && ANY_QI_REG_P (operands[0])\n\
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
    && !(INTVAL (operands[2]) & ~255)\n\
    && (INTVAL (operands[2]) & 128)\n\
    && GET_MODE (operands[0]) != QImode",
    __builtin_constant_p 
#line 8352 "../.././gcc/config/i386/i386.md"
(reload_completed
    && ANY_QI_REG_P (operands[0])
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    && !(INTVAL (operands[2]) & ~255)
    && (INTVAL (operands[2]) & 128)
    && GET_MODE (operands[0]) != QImode)
    ? (int) 
#line 8352 "../.././gcc/config/i386/i386.md"
(reload_completed
    && ANY_QI_REG_P (operands[0])
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    && !(INTVAL (operands[2]) & ~255)
    && (INTVAL (operands[2]) & 128)
    && GET_MODE (operands[0]) != QImode)
    : -1 },
  { "(TARGET_SSE2 && (V16SFmode == V4SFmode || TARGET_AVX2)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 3610 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (V16SFmode == V4SFmode || TARGET_AVX2)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 3610 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (V16SFmode == V4SFmode || TARGET_AVX2)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)\n\
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)\n\
   && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)
   && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)
   && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(((TARGET_BMI || TARGET_GENERIC)\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && (TARGET_64BIT)) && ( reload_completed)",
    __builtin_constant_p ((
#line 11939 "../.././gcc/config/i386/i386.md"
((TARGET_BMI || TARGET_GENERIC)
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 11942 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) ((
#line 11939 "../.././gcc/config/i386/i386.md"
((TARGET_BMI || TARGET_GENERIC)
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 11942 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V32QImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V32QImode, operands)) && 
#line 223 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V32QImode, operands)) && 
#line 223 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 16883 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && (true_regnum (operands[2]) != AX_REG\n\
       || satisfies_constraint_K (operands[3]))\n\
   && peep2_reg_dead_p (1, operands[2])",
    __builtin_constant_p 
#line 16883 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && (true_regnum (operands[2]) != AX_REG
       || satisfies_constraint_K (operands[3]))
   && peep2_reg_dead_p (1, operands[2]))
    ? (int) 
#line 16883 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && (true_regnum (operands[2]) != AX_REG
       || satisfies_constraint_K (operands[3]))
   && peep2_reg_dead_p (1, operands[2]))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F\n\
   && INTVAL (operands[2]) + 4 == INTVAL (operands[6])\n\
   && INTVAL (operands[3]) + 4 == INTVAL (operands[7])\n\
   && INTVAL (operands[4]) + 4 == INTVAL (operands[8])\n\
   && INTVAL (operands[5]) + 4 == INTVAL (operands[9])\n\
   && INTVAL (operands[2]) + 8 == INTVAL (operands[10])\n\
   && INTVAL (operands[3]) + 8 == INTVAL (operands[11])\n\
   && INTVAL (operands[4]) + 8 == INTVAL (operands[12])\n\
   && INTVAL (operands[5]) + 8 == INTVAL (operands[13])\n\
   && INTVAL (operands[2]) + 12 == INTVAL (operands[14])\n\
   && INTVAL (operands[3]) + 12 == INTVAL (operands[15])\n\
   && INTVAL (operands[4]) + 12 == INTVAL (operands[16])\n\
   && INTVAL (operands[5]) + 12 == INTVAL (operands[17]))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9955 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && INTVAL (operands[2]) + 4 == INTVAL (operands[6])
   && INTVAL (operands[3]) + 4 == INTVAL (operands[7])
   && INTVAL (operands[4]) + 4 == INTVAL (operands[8])
   && INTVAL (operands[5]) + 4 == INTVAL (operands[9])
   && INTVAL (operands[2]) + 8 == INTVAL (operands[10])
   && INTVAL (operands[3]) + 8 == INTVAL (operands[11])
   && INTVAL (operands[4]) + 8 == INTVAL (operands[12])
   && INTVAL (operands[5]) + 8 == INTVAL (operands[13])
   && INTVAL (operands[2]) + 12 == INTVAL (operands[14])
   && INTVAL (operands[3]) + 12 == INTVAL (operands[15])
   && INTVAL (operands[4]) + 12 == INTVAL (operands[16])
   && INTVAL (operands[5]) + 12 == INTVAL (operands[17])))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9955 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && INTVAL (operands[2]) + 4 == INTVAL (operands[6])
   && INTVAL (operands[3]) + 4 == INTVAL (operands[7])
   && INTVAL (operands[4]) + 4 == INTVAL (operands[8])
   && INTVAL (operands[5]) + 4 == INTVAL (operands[9])
   && INTVAL (operands[2]) + 8 == INTVAL (operands[10])
   && INTVAL (operands[3]) + 8 == INTVAL (operands[11])
   && INTVAL (operands[4]) + 8 == INTVAL (operands[12])
   && INTVAL (operands[5]) + 8 == INTVAL (operands[13])
   && INTVAL (operands[2]) + 12 == INTVAL (operands[14])
   && INTVAL (operands[3]) + 12 == INTVAL (operands[15])
   && INTVAL (operands[4]) + 12 == INTVAL (operands[16])
   && INTVAL (operands[5]) + 12 == INTVAL (operands[17])))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V4DImode)\n\
       == GET_MODE_NUNITS (V4DFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V4DFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V4DFmode)))
    : -1 },
#line 666 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_MMX || (TARGET_SSE2 && V2SImode == V1DImode))\n\
   && ix86_binary_operator_ok (MINUS, V2SImode, operands)",
    __builtin_constant_p 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V2SImode == V1DImode))
   && ix86_binary_operator_ok (MINUS, V2SImode, operands))
    ? (int) 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V2SImode == V1DImode))
   && ix86_binary_operator_ok (MINUS, V2SImode, operands))
    : -1 },
#line 8609 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (UMAX, V4SImode, operands)",
    __builtin_constant_p 
#line 8609 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (UMAX, V4SImode, operands))
    ? (int) 
#line 8609 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (UMAX, V4SImode, operands))
    : -1 },
#line 9882 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (LSHIFTRT, QImode, operands)",
    __builtin_constant_p 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, QImode, operands))
    ? (int) 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, QImode, operands))
    : -1 },
  { "(TARGET_SSP_TLS_GUARD) && (ptr_mode == SImode)",
    __builtin_constant_p (
#line 17711 "../.././gcc/config/i386/i386.md"
(TARGET_SSP_TLS_GUARD) && 
#line 1068 "../.././gcc/config/i386/i386.md"
(ptr_mode == SImode))
    ? (int) (
#line 17711 "../.././gcc/config/i386/i386.md"
(TARGET_SSP_TLS_GUARD) && 
#line 1068 "../.././gcc/config/i386/i386.md"
(ptr_mode == SImode))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V16HImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 4416 "../.././gcc/config/i386/i386.md"
  { "TARGET_AVOID_VECTOR_DECODE\n\
   && SSE_FLOAT_MODE_P (DFmode)\n\
   && optimize_insn_for_speed_p ()",
    __builtin_constant_p 
#line 4416 "../.././gcc/config/i386/i386.md"
(TARGET_AVOID_VECTOR_DECODE
   && SSE_FLOAT_MODE_P (DFmode)
   && optimize_insn_for_speed_p ())
    ? (int) 
#line 4416 "../.././gcc/config/i386/i386.md"
(TARGET_AVOID_VECTOR_DECODE
   && SSE_FLOAT_MODE_P (DFmode)
   && optimize_insn_for_speed_p ())
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V32HImode)\n\
       == GET_MODE_NUNITS (V16SImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V16SImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V16SImode)))
    : -1 },
#line 4966 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 4966 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 4966 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)\n\
   && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(ix86_binary_operator_ok (ASHIFT, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9084 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9084 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && TARGET_64BIT)",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3898 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && TARGET_64BIT))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3898 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && TARGET_64BIT))
    : -1 },
  { "(TARGET_SSE && !flag_finite_math_only\n\
   && 1 && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 16298 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && TARGET_CMOVE\n\
   && !(MEM_P (operands[2]) && MEM_P (operands[3]))",
    __builtin_constant_p 
#line 16298 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE
   && !(MEM_P (operands[2]) && MEM_P (operands[3])))
    ? (int) 
#line 16298 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE
   && !(MEM_P (operands[2]) && MEM_P (operands[3])))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_SSE && !flag_finite_math_only\n\
   && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    : -1 },
#line 959 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (EQ, V8QImode, operands)",
    __builtin_constant_p 
#line 959 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (EQ, V8QImode, operands))
    ? (int) 
#line 959 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (EQ, V8QImode, operands))
    : -1 },
#line 4416 "../.././gcc/config/i386/i386.md"
  { "TARGET_AVOID_VECTOR_DECODE\n\
   && SSE_FLOAT_MODE_P (SFmode)\n\
   && optimize_insn_for_speed_p ()",
    __builtin_constant_p 
#line 4416 "../.././gcc/config/i386/i386.md"
(TARGET_AVOID_VECTOR_DECODE
   && SSE_FLOAT_MODE_P (SFmode)
   && optimize_insn_for_speed_p ())
    ? (int) 
#line 4416 "../.././gcc/config/i386/i386.md"
(TARGET_AVOID_VECTOR_DECODE
   && SSE_FLOAT_MODE_P (SFmode)
   && optimize_insn_for_speed_p ())
    : -1 },
#line 13192 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (DFmode, SImode)\n\
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
   && (TARGET_USE_SIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 13192 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, SImode)
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 13192 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, SImode)
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)\n\
   && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)
   && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)
   && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 658 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX || (TARGET_SSE2 && V1DImode == V1DImode)",
    __builtin_constant_p 
#line 658 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX || (TARGET_SSE2 && V1DImode == V1DImode))
    ? (int) 
#line 658 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX || (TARGET_SSE2 && V1DImode == V1DImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 9316 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[1] == const1_rtx\n\
	&& (TARGET_SHIFT1\n\
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))",
    __builtin_constant_p 
#line 9316 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[1] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0]))))))
    ? (int) 
#line 9316 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[1] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0]))))))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V32QImode)\n\
       == GET_MODE_NUNITS (V4DImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V4DImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V4DImode)))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V32QImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 10715 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 10715 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(ix86_unary_operator_ok (NEG, TImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8427 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, TImode, operands)) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8427 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, TImode, operands)) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 1504 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_SSE || TARGET_3DNOW_A)\n\
   && ix86_binary_operator_ok (PLUS, V4HImode, operands)",
    __builtin_constant_p 
#line 1504 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (PLUS, V4HImode, operands))
    ? (int) 
#line 1504 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (PLUS, V4HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (V4SFmode == V4SFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3567 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V4SFmode == V4SFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3567 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V4SFmode == V4SFmode)))
    : -1 },
#line 5436 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (PLUS, HImode, operands)",
    __builtin_constant_p 
#line 5436 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, HImode, operands))
    ? (int) 
#line 5436 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, HImode, operands))
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)\n\
   && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)
   && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)
   && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_SSE2 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V4DFmode)\n\
       == GET_MODE_NUNITS (V4DFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V4DFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V4DFmode)))
    : -1 },
#line 9882 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    ? (int) 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    : -1 },
  { "(TARGET_SSE || TARGET_3DNOW_A) && (Pmode == DImode)",
    __builtin_constant_p (
#line 1543 "../.././gcc/config/i386/mmx.md"
(TARGET_SSE || TARGET_3DNOW_A) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 1543 "../.././gcc/config/i386/mmx.md"
(TARGET_SSE || TARGET_3DNOW_A) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
#line 8526 "../.././gcc/config/i386/i386.md"
  { "ix86_unary_operator_ok (NEG, SImode, operands)\n\
   && mode_signbit_p (SImode, operands[2])",
    __builtin_constant_p 
#line 8526 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, SImode, operands)
   && mode_signbit_p (SImode, operands[2]))
    ? (int) 
#line 8526 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, SImode, operands)
   && mode_signbit_p (SImode, operands[2]))
    : -1 },
#line 9458 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT\n\
   && (optimize_function_for_size_p (cfun)\n\
       || !TARGET_PARTIAL_FLAG_REG_STALL\n\
       || (operands[2] == const1_rtx\n\
	   && (TARGET_SHIFT1\n\
	       || TARGET_DOUBLE_WITH_ADD)))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFT, SImode, operands)",
    __builtin_constant_p 
#line 9458 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && (optimize_function_for_size_p (cfun)
       || !TARGET_PARTIAL_FLAG_REG_STALL
       || (operands[2] == const1_rtx
	   && (TARGET_SHIFT1
	       || TARGET_DOUBLE_WITH_ADD)))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, SImode, operands))
    ? (int) 
#line 9458 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && (optimize_function_for_size_p (cfun)
       || !TARGET_PARTIAL_FLAG_REG_STALL
       || (operands[2] == const1_rtx
	   && (TARGET_SHIFT1
	       || TARGET_DOUBLE_WITH_ADD)))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 16826 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && ((HImode == HImode\n\
       && TARGET_LCP_STALL)\n\
       || (TARGET_SPLIT_LONG_MOVES\n\
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))",
    __builtin_constant_p 
#line 16826 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((HImode == HImode
       && TARGET_LCP_STALL)
       || (TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn)))
    ? (int) 
#line 16826 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((HImode == HImode
       && TARGET_LCP_STALL)
       || (TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn)))
    : -1 },
  { "((TARGET_SINGLE_POP || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && ((((((((word_mode == SImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17330 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17330 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
  { "(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 3068 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 3068 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "((optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (LSHIFTRT, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 6851 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 6851 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 9155 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (ASHIFT, SImode, operands)",
    __builtin_constant_p 
#line 9155 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (ASHIFT, SImode, operands))
    ? (int) 
#line 9155 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (ASHIFT, SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && !TARGET_FISTTP\n\
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || DImode != DImode))\n\
   && can_create_pseudo_p ()) && ( 1)",
    __builtin_constant_p (
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
   && can_create_pseudo_p ()) && 
#line 4510 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
   && can_create_pseudo_p ()) && 
#line 4510 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_ROUND) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 12070 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 12070 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SImode)\n\
       == GET_MODE_NUNITS (V16SImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V16SImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V16SImode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX512F && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DFmode == V16SFmode || V8DFmode == V8DFmode))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DFmode == V16SFmode || V8DFmode == V8DFmode))))
    : -1 },
#line 7591 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (AND, SImode, operands)",
    __builtin_constant_p 
#line 7591 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (AND, SImode, operands))
    ? (int) 
#line 7591 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (AND, SImode, operands))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V8SFmode)\n\
       == GET_MODE_NUNITS (V4DFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V4DFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V4DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)\n\
   && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)
   && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)
   && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX2 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1) && 
#line 326 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1) && 
#line 326 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSP_TLS_GUARD) && (ptr_mode == DImode)",
    __builtin_constant_p (
#line 17711 "../.././gcc/config/i386/i386.md"
(TARGET_SSP_TLS_GUARD) && 
#line 1068 "../.././gcc/config/i386/i386.md"
(ptr_mode == DImode))
    ? (int) (
#line 17711 "../.././gcc/config/i386/i386.md"
(TARGET_SSP_TLS_GUARD) && 
#line 1068 "../.././gcc/config/i386/i386.md"
(ptr_mode == DImode))
    : -1 },
#line 8138 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (XOR, QImode, operands)",
    __builtin_constant_p 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, QImode, operands))
    ? (int) 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, QImode, operands))
    : -1 },
  { "(optimize_insn_for_size_p ()\n\
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == SImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17393 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_size_p ()
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17393 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_size_p ()
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
  { "((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)\n\
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[1])\n\
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])\n\
       || GET_MODE (operands[3]) == VOIDmode)) && ( reload_completed)",
    __builtin_constant_p (
#line 5912 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])
       || GET_MODE (operands[3]) == VOIDmode)) && 
#line 5918 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 5912 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])
       || GET_MODE (operands[3]) == VOIDmode)) && 
#line 5918 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 16248 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && TARGET_CMOVE",
    __builtin_constant_p 
#line 16248 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE)
    ? (int) 
#line 16248 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE)
    : -1 },
  { "(ix86_target_stack_probe ()) && (Pmode == SImode)",
    __builtin_constant_p (
#line 16535 "../.././gcc/config/i386/i386.md"
(ix86_target_stack_probe ()) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 16535 "../.././gcc/config/i386/i386.md"
(ix86_target_stack_probe ()) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 4757 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)",
    __builtin_constant_p 
#line 4757 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode))
    ? (int) 
#line 4757 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
  { "(TARGET_LZCNT) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 12028 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 12028 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 319 "../.././gcc/config/i386/sync.md"
  { "TARGET_CMPXCHG",
    __builtin_constant_p 
#line 319 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG)
    ? (int) 
#line 319 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG)
    : -1 },
  { "((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == DImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17316 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17316 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
#line 4504 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && !TARGET_FISTTP\n\
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || SImode != DImode))\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
   && can_create_pseudo_p ())
    ? (int) 
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
   && can_create_pseudo_p ())
    : -1 },
  { "(TARGET_AVX && 1\n\
   && avx_vpermilp_parallel (operands[2], V2DFmode)) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V2DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V2DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (64 == 64)\n\
   && ix86_binary_operator_ok (IOR, V8DImode, operands)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (IOR, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (IOR, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16SImode, operands) && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16SImode, operands) && 1) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16SImode, operands) && 1) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V8SImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 10524 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && reload_completed",
    __builtin_constant_p 
#line 10524 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && reload_completed)
    ? (int) 
#line 10524 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && reload_completed)
    : -1 },
#line 17001 "../.././gcc/config/i386/i386.md"
  { "REGNO (operands[0]) != REGNO (operands[1])\n\
   && ((MMX_REG_P (operands[0]) && MMX_REG_P (operands[1])) \n\
       || (SSE_REG_P (operands[0]) && SSE_REG_P (operands[1])))",
    __builtin_constant_p 
#line 17001 "../.././gcc/config/i386/i386.md"
(REGNO (operands[0]) != REGNO (operands[1])
   && ((MMX_REG_P (operands[0]) && MMX_REG_P (operands[1])) 
       || (SSE_REG_P (operands[0]) && SSE_REG_P (operands[1]))))
    ? (int) 
#line 17001 "../.././gcc/config/i386/i386.md"
(REGNO (operands[0]) != REGNO (operands[1])
   && ((MMX_REG_P (operands[0]) && MMX_REG_P (operands[1])) 
       || (SSE_REG_P (operands[0]) && SSE_REG_P (operands[1]))))
    : -1 },
  { "(TARGET_AVX && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1 && 1) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1 && 1) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DImode)\n\
       == GET_MODE_NUNITS (V8DFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V8DFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (IOR, V2DImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (IOR, V2DImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (IOR, V2DImode, operands)))
    : -1 },
#line 8901 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)",
    __builtin_constant_p 
#line 8901 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode))
    ? (int) 
#line 8901 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 16841 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p () && ix86_match_ccmode (insn, CCNOmode)",
    __builtin_constant_p 
#line 16841 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p () && ix86_match_ccmode (insn, CCNOmode))
    ? (int) 
#line 16841 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p () && ix86_match_ccmode (insn, CCNOmode))
    : -1 },
  { "(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V4SImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V4SImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V4SImode, operands))
    : -1 },
  { "((32 == 64) && 1) && (TARGET_FMA || TARGET_FMA4)",
    __builtin_constant_p (
#line 2986 "../.././gcc/config/i386/sse.md"
((32 == 64) && 1) && 
#line 2776 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4))
    ? (int) (
#line 2986 "../.././gcc/config/i386/sse.md"
((32 == 64) && 1) && 
#line 2776 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4))
    : -1 },
#line 1345 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && 1 && 1",
    __builtin_constant_p 
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && 1 && 1)
    ? (int) 
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && 1 && 1)
    : -1 },
  { "((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && ((((((((word_mode == SImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17305 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17305 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 251 "../.././gcc/config/i386/sync.md"
  { "!TARGET_64BIT && (TARGET_80387 || TARGET_SSE)",
    __builtin_constant_p 
#line 251 "../.././gcc/config/i386/sync.md"
(!TARGET_64BIT && (TARGET_80387 || TARGET_SSE))
    ? (int) 
#line 251 "../.././gcc/config/i386/sync.md"
(!TARGET_64BIT && (TARGET_80387 || TARGET_SSE))
    : -1 },
#line 959 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (EQ, V2SImode, operands)",
    __builtin_constant_p 
#line 959 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (EQ, V2SImode, operands))
    ? (int) 
#line 959 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (EQ, V2SImode, operands))
    : -1 },
#line 3005 "../.././gcc/config/i386/sse.md"
  { "1 && 1",
    __builtin_constant_p 
#line 3005 "../.././gcc/config/i386/sse.md"
(1 && 1)
    ? (int) 
#line 3005 "../.././gcc/config/i386/sse.md"
(1 && 1)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 13037 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (DFmode) && TARGET_MIX_SSE_I387\n\
   && COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13037 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_MIX_SSE_I387
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13037 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_MIX_SSE_I387
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 13730 "../.././gcc/config/i386/sse.md"
  { "TARGET_XOP",
    __builtin_constant_p 
#line 13730 "../.././gcc/config/i386/sse.md"
(TARGET_XOP)
    ? (int) 
#line 13730 "../.././gcc/config/i386/sse.md"
(TARGET_XOP)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX512F && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SFmode == V16SFmode || V16SFmode == V8DFmode))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SFmode == V16SFmode || V16SFmode == V8DFmode))))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V64QImode)\n\
       == GET_MODE_NUNITS (V8DImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V8DImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V8DImode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F\n\
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)\n\
       && INTVAL (operands[5]) == (INTVAL (operands[6]) - 1)\n\
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)\n\
       && INTVAL (operands[9]) == (INTVAL (operands[10]) - 1)))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9803 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[5]) == (INTVAL (operands[6]) - 1)
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)
       && INTVAL (operands[9]) == (INTVAL (operands[10]) - 1))))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9803 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[5]) == (INTVAL (operands[6]) - 1)
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)
       && INTVAL (operands[9]) == (INTVAL (operands[10]) - 1))))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V32QImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V32QImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V32QImode, operands)
   && 1 && 1)
    : -1 },
#line 1590 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1 && 1",
    __builtin_constant_p 
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && 1)
    ? (int) 
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && 1)
    : -1 },
  { "(TARGET_USE_BT || optimize_function_for_size_p (cfun)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10795 "../.././gcc/config/i386/i386.md"
(TARGET_USE_BT || optimize_function_for_size_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10795 "../.././gcc/config/i386/i386.md"
(TARGET_USE_BT || optimize_function_for_size_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 1003 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (IOR, V8QImode, operands)",
    __builtin_constant_p 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (IOR, V8QImode, operands))
    ? (int) 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (IOR, V8QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)\n\
   && (32 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)
   && (32 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)
   && (32 == 64) && 1))
    : -1 },
#line 12070 "../.././gcc/config/i386/sse.md"
  { "TARGET_ROUND",
    __builtin_constant_p 
#line 12070 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND)
    ? (int) 
#line 12070 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND)
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SImode)\n\
       == GET_MODE_NUNITS (V64QImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V64QImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V64QImode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_SSE4_1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 11594 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 11594 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 16703 "../.././gcc/config/i386/i386.md"
  { "! TARGET_PARTIAL_REG_STALL && reload_completed\n\
   && optimize_insn_for_speed_p ()\n\
   && ((GET_MODE (operands[1]) == HImode && ! TARGET_FAST_PREFIX)\n\
       || (GET_MODE (operands[1]) == QImode && TARGET_PROMOTE_QImode))\n\
   /* Ensure that the operand will remain sign-extended immediate.  */\n\
   && ix86_match_ccmode (insn, INTVAL (operands[4]) >= 0 ? CCNOmode : CCZmode)",
    __builtin_constant_p 
#line 16703 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL && reload_completed
   && optimize_insn_for_speed_p ()
   && ((GET_MODE (operands[1]) == HImode && ! TARGET_FAST_PREFIX)
       || (GET_MODE (operands[1]) == QImode && TARGET_PROMOTE_QImode))
   /* Ensure that the operand will remain sign-extended immediate.  */
   && ix86_match_ccmode (insn, INTVAL (operands[4]) >= 0 ? CCNOmode : CCZmode))
    ? (int) 
#line 16703 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL && reload_completed
   && optimize_insn_for_speed_p ()
   && ((GET_MODE (operands[1]) == HImode && ! TARGET_FAST_PREFIX)
       || (GET_MODE (operands[1]) == QImode && TARGET_PROMOTE_QImode))
   /* Ensure that the operand will remain sign-extended immediate.  */
   && ix86_match_ccmode (insn, INTVAL (operands[4]) >= 0 ? CCNOmode : CCZmode))
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)
   && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)
   && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 4396 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (DFmode)\n\
   && (!TARGET_FISTTP || TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 4396 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode)
   && (!TARGET_FISTTP || TARGET_SSE_MATH))
    ? (int) 
#line 4396 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode)
   && (!TARGET_FISTTP || TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_FMA)",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3236 "../.././gcc/config/i386/sse.md"
(TARGET_FMA))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3236 "../.././gcc/config/i386/sse.md"
(TARGET_FMA))
    : -1 },
  { "(!TARGET_64BIT\n\
   && TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && ( reload_completed)",
    __builtin_constant_p (
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && 
#line 4949 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && 
#line 4949 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V8SFmode)\n\
       || register_operand (operands[1], V8SFmode))) && (TARGET_AVX)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8SFmode)
       || register_operand (operands[1], V8SFmode))) && 
#line 150 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8SFmode)
       || register_operand (operands[1], V8SFmode))) && 
#line 150 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 10147 "../.././gcc/config/i386/i386.md"
  { "TARGET_BMI2 && reload_completed",
    __builtin_constant_p 
#line 10147 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2 && reload_completed)
    ? (int) 
#line 10147 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2 && reload_completed)
    : -1 },
  { "(TARGET_AVX && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && ( reload_completed)",
    __builtin_constant_p (
#line 6332 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 6334 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) (
#line 6332 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 6334 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
  { "(X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || SImode != DImode))\n\
	&& TARGET_SSE_MATH)\n\
   && can_create_pseudo_p ()) && ( 1)",
    __builtin_constant_p (
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ()) && 
#line 4432 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ()) && 
#line 4432 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8528 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (SMAX, V16QImode, operands)",
    __builtin_constant_p 
#line 8528 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (SMAX, V16QImode, operands))
    ? (int) 
#line 8528 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (SMAX, V16QImode, operands))
    : -1 },
  { "(TARGET_SSE2 && TARGET_SSE_MATH\n\
   && TARGET_SSE_PARTIAL_REG_DEPENDENCY\n\
   && optimize_function_for_speed_p (cfun)\n\
   && reload_completed && SSE_REG_P (operands[0])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 4793 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH
   && TARGET_SSE_PARTIAL_REG_DEPENDENCY
   && optimize_function_for_speed_p (cfun)
   && reload_completed && SSE_REG_P (operands[0])) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 4793 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH
   && TARGET_SSE_PARTIAL_REG_DEPENDENCY
   && optimize_function_for_speed_p (cfun)
   && reload_completed && SSE_REG_P (operands[0])) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(ix86_binary_operator_ok (IOR, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8062 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (IOR, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8062 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (IOR, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 13150 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_ARITH (DFmode)\n\
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
   && !COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13150 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (DFmode)
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   && !COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13150 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (DFmode)
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   && !COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 1565 "../.././gcc/config/i386/i386.md"
  { "TARGET_MIX_SSE_I387\n\
   && SSE_FLOAT_MODE_P (SFmode)",
    __builtin_constant_p 
#line 1565 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387
   && SSE_FLOAT_MODE_P (SFmode))
    ? (int) 
#line 1565 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387
   && SSE_FLOAT_MODE_P (SFmode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && (16 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && (16 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && (16 == 64) && 1))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V64QImode)\n\
       == GET_MODE_NUNITS (V16SFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V16SFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V16SFmode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (AND, V2DImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (AND, V2DImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (AND, V2DImode, operands)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_FMA) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3236 "../.././gcc/config/i386/sse.md"
(TARGET_FMA) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3236 "../.././gcc/config/i386/sse.md"
(TARGET_FMA) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4DImode, operands) && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4DImode, operands) && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4DImode, operands) && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)\n\
   && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)
   && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)
   && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V4DFmode)\n\
       == GET_MODE_NUNITS (V16HImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V16HImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V16HImode)))
    : -1 },
#line 3318 "../.././gcc/config/i386/sse.md"
  { "TARGET_FMA4",
    __builtin_constant_p 
#line 3318 "../.././gcc/config/i386/sse.md"
(TARGET_FMA4)
    ? (int) 
#line 3318 "../.././gcc/config/i386/sse.md"
(TARGET_FMA4)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX && 1\n\
   && avx_vpermilp_parallel (operands[2], V8DFmode)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 17422 "../.././gcc/config/i386/i386.md"
  { "!TARGET_PARTIAL_REG_STALL\n\
   || SImode == SImode\n\
   || optimize_function_for_size_p (cfun)",
    __builtin_constant_p 
#line 17422 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   || SImode == SImode
   || optimize_function_for_size_p (cfun))
    ? (int) 
#line 17422 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   || SImode == SImode
   || optimize_function_for_size_p (cfun))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V32QImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V32QImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V32QImode, operands)
   && 1 && 1)
    : -1 },
#line 11536 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4A",
    __builtin_constant_p 
#line 11536 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4A)
    ? (int) 
#line 11536 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4A)
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V8SImode)\n\
       == GET_MODE_NUNITS (V4DImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V4DImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V4DImode)))
    : -1 },
  { "(SIBLING_CALL_P (insn)) && (word_mode == SImode)",
    __builtin_constant_p (
#line 11419 "../.././gcc/config/i386/i386.md"
(SIBLING_CALL_P (insn)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode))
    ? (int) (
#line 11419 "../.././gcc/config/i386/i386.md"
(SIBLING_CALL_P (insn)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode))
    : -1 },
#line 5626 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT\n\
   && ix86_match_ccmode (insn, CCGCmode)",
    __builtin_constant_p 
#line 5626 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && ix86_match_ccmode (insn, CCGCmode))
    ? (int) 
#line 5626 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && ix86_match_ccmode (insn, CCGCmode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (64 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8317 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (64 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8317 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (64 == 64)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (XOR, V8DFmode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V8DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V8DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 2010 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
   && !SSE_REG_P (operands[0]) && !SSE_REG_P (operands[1])",
    __builtin_constant_p 
#line 2010 "../.././gcc/config/i386/i386.md"
(reload_completed
   && !SSE_REG_P (operands[0]) && !SSE_REG_P (operands[1]))
    ? (int) 
#line 2010 "../.././gcc/config/i386/i386.md"
(reload_completed
   && !SSE_REG_P (operands[0]) && !SSE_REG_P (operands[1]))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SImode)\n\
       == GET_MODE_NUNITS (V8DFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V8DFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V8DFmode)))
    : -1 },
#line 9410 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& (TARGET_SHIFT1\n\
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFT, SImode, operands)",
    __builtin_constant_p 
#line 9410 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, SImode, operands))
    ? (int) 
#line 9410 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, SImode, operands))
    : -1 },
  { "(TARGET_XADD) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 465 "../.././gcc/config/i386/sync.md"
(TARGET_XADD) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 465 "../.././gcc/config/i386/sync.md"
(TARGET_XADD) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 17922 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && TARGET_XSAVE",
    __builtin_constant_p 
#line 17922 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_XSAVE)
    ? (int) 
#line 17922 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_XSAVE)
    : -1 },
#line 11939 "../.././gcc/config/i386/i386.md"
  { "(TARGET_BMI || TARGET_GENERIC)\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)",
    __builtin_constant_p 
#line 11939 "../.././gcc/config/i386/i386.md"
((TARGET_BMI || TARGET_GENERIC)
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun))
    ? (int) 
#line 11939 "../.././gcc/config/i386/i386.md"
((TARGET_BMI || TARGET_GENERIC)
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && TARGET_64BIT)",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3549 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && TARGET_64BIT))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3549 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && TARGET_64BIT))
    : -1 },
  { "((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == DImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17345 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17345 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 5423 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && reload_completed && ix86_lea_for_add_ok (insn, operands)",
    __builtin_constant_p 
#line 5423 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && reload_completed && ix86_lea_for_add_ok (insn, operands))
    ? (int) 
#line 5423 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && reload_completed && ix86_lea_for_add_ok (insn, operands))
    : -1 },
  { "(TARGET_SSE && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
#line 4927 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT\n\
   && SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH",
    __builtin_constant_p 
#line 4927 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
    ? (int) 
#line 4927 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
    : -1 },
  { "(TARGET_SSE\n\
   && (GET_MODE_NUNITS (V2DImode)\n\
       == GET_MODE_NUNITS (V2DFmode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DFmode)\n\
       == GET_MODE_NUNITS (V32HImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V32HImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V32HImode)))
    : -1 },
#line 8654 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (EQ, V8SImode, operands)",
    __builtin_constant_p 
#line 8654 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (EQ, V8SImode, operands))
    ? (int) 
#line 8654 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (EQ, V8SImode, operands))
    : -1 },
  { "(TARGET_SSE\n\
   && (GET_MODE_NUNITS (V8HImode)\n\
       == GET_MODE_NUNITS (V2DFmode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 7672 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16QImode, operands) && 1",
    __builtin_constant_p 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16QImode, operands) && 1)
    ? (int) 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16QImode, operands) && 1)
    : -1 },
  { "(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 1068 "../.././gcc/config/i386/i386.md"
  { "ptr_mode == DImode",
    __builtin_constant_p 
#line 1068 "../.././gcc/config/i386/i386.md"
(ptr_mode == DImode)
    ? (int) 
#line 1068 "../.././gcc/config/i386/i386.md"
(ptr_mode == DImode)
    : -1 },
#line 5943 "../.././gcc/config/i386/i386.md"
  { "(GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)\n\
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[1])\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[3])",
    __builtin_constant_p 
#line 5943 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && GET_MODE (operands[0]) == GET_MODE (operands[3]))
    ? (int) 
#line 5943 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && GET_MODE (operands[0]) == GET_MODE (operands[3]))
    : -1 },
#line 2410 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && ix86_binary_operator_ok (AND, V4SFmode, operands)",
    __builtin_constant_p 
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V4SFmode, operands))
    ? (int) 
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V4SFmode, operands))
    : -1 },
#line 3979 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 || (TARGET_SSE2 && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 3979 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (TARGET_SSE2 && TARGET_SSE_MATH))
    ? (int) 
#line 3979 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (TARGET_SSE2 && TARGET_SSE_MATH))
    : -1 },
  { "(1 && 1) && (TARGET_FMA || TARGET_FMA4)",
    __builtin_constant_p (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && 1) && 
#line 2776 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4))
    ? (int) (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && 1) && 
#line 2776 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4))
    : -1 },
  { "(TARGET_SSE && 1 && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 5370 "../.././gcc/config/i386/i386.md"
  { "reload_completed && ix86_avoid_lea_for_add (insn, operands)",
    __builtin_constant_p 
#line 5370 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_avoid_lea_for_add (insn, operands))
    ? (int) 
#line 5370 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_avoid_lea_for_add (insn, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)\n\
   && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)
   && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)
   && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    : -1 },
#line 7672 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8HImode, operands) && 1",
    __builtin_constant_p 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8HImode, operands) && 1)
    ? (int) 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8HImode, operands) && 1)
    : -1 },
  { "(TARGET_FMA || TARGET_AVX512F) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 3299 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_AVX512F) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 3299 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_AVX512F) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V32QImode)\n\
       == GET_MODE_NUNITS (V8SImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V8SImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V8SImode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    : -1 },
  { "(ix86_match_ccmode (insn, CCmode)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 1148 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 1148 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 904 "../.././gcc/config/i386/i386.md"
  { "TARGET_QIMODE_MATH",
    __builtin_constant_p 
#line 904 "../.././gcc/config/i386/i386.md"
(TARGET_QIMODE_MATH)
    ? (int) 
#line 904 "../.././gcc/config/i386/i386.md"
(TARGET_QIMODE_MATH)
    : -1 },
  { "(TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V2DFmode)\n\
       == GET_MODE_NUNITS (V4SImode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V4SImode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V4SImode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 4730 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 4730 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 4757 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (SFmode, SImode)",
    __builtin_constant_p 
#line 4757 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, SImode))
    ? (int) 
#line 4757 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, SImode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (AND, V16QImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (AND, V16QImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (AND, V16QImode, operands)))
    : -1 },
  { "(TARGET_SSE2 && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 1584 "../.././gcc/config/i386/mmx.md"
  { "TARGET_3DNOW",
    __builtin_constant_p 
#line 1584 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW)
    ? (int) 
#line 1584 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW)
    : -1 },
  { "((TARGET_BMI || TARGET_GENERIC)\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && ( reload_completed)",
    __builtin_constant_p (
#line 11939 "../.././gcc/config/i386/i386.md"
((TARGET_BMI || TARGET_GENERIC)
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 11942 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 11939 "../.././gcc/config/i386/i386.md"
((TARGET_BMI || TARGET_GENERIC)
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 11942 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V16HImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_64BIT && TARGET_SSE) && ( reload_completed)",
    __builtin_constant_p (
#line 10488 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE) && 
#line 10490 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) (
#line 10488 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE) && 
#line 10490 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(optimize_insn_for_size_p ()\n\
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == DImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17393 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_size_p ()
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17393 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_size_p ()
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)\n\
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DFmode)\n\
       == GET_MODE_NUNITS (V16SFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V16SFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V16SFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)) && 
#line 2773 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)) && 
#line 2773 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    : -1 },
  { "(ix86_binary_operator_ok (PLUS, DImode, operands)) && (!TARGET_64BIT)",
    __builtin_constant_p (
#line 5061 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, DImode, operands)) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    ? (int) (
#line 5061 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, DImode, operands)) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    : -1 },
#line 13130 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH\n\
   && !COMMUTATIVE_ARITH_P (operands[3])",
    __builtin_constant_p 
#line 13130 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
   && !COMMUTATIVE_ARITH_P (operands[3]))
    ? (int) 
#line 13130 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
   && !COMMUTATIVE_ARITH_P (operands[3]))
    : -1 },
#line 11718 "../.././gcc/config/i386/i386.md"
  { "TARGET_LP64",
    __builtin_constant_p 
#line 11718 "../.././gcc/config/i386/i386.md"
(TARGET_LP64)
    ? (int) 
#line 11718 "../.././gcc/config/i386/i386.md"
(TARGET_LP64)
    : -1 },
#line 3333 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 || reload_completed",
    __builtin_constant_p 
#line 3333 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || reload_completed)
    ? (int) 
#line 3333 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || reload_completed)
    : -1 },
#line 6215 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (PLUS, QImode, operands)",
    __builtin_constant_p 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, QImode, operands))
    ? (int) 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, QImode, operands))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V8SFmode)\n\
       == GET_MODE_NUNITS (V16HImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V16HImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V16HImode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)\n\
   && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)
   && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)
   && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    : -1 },
#line 6256 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (MINUS, SImode, operands)",
    __builtin_constant_p 
#line 6256 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (MINUS, SImode, operands))
    ? (int) 
#line 6256 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (MINUS, SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)\n\
   && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)
   && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)
   && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 320 "../.././gcc/config/i386/mmx.md"
  { "TARGET_3DNOW && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V2SFmode, operands)",
    __builtin_constant_p 
#line 320 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2SFmode, operands))
    ? (int) 
#line 320 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2SFmode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 10101 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ROTATE, SImode, operands)",
    __builtin_constant_p 
#line 10101 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, SImode, operands))
    ? (int) 
#line 10101 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, SImode, operands))
    : -1 },
#line 6215 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (MINUS, SImode, operands)",
    __builtin_constant_p 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, SImode, operands))
    ? (int) 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, SImode, operands))
    : -1 },
#line 4004 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_VECTOR_FP_CONVERTS\n\
   && optimize_insn_for_speed_p ()\n\
   && reload_completed && SSE_REG_P (operands[0])",
    __builtin_constant_p 
#line 4004 "../.././gcc/config/i386/i386.md"
(TARGET_USE_VECTOR_FP_CONVERTS
   && optimize_insn_for_speed_p ()
   && reload_completed && SSE_REG_P (operands[0]))
    ? (int) 
#line 4004 "../.././gcc/config/i386/i386.md"
(TARGET_USE_VECTOR_FP_CONVERTS
   && optimize_insn_for_speed_p ()
   && reload_completed && SSE_REG_P (operands[0]))
    : -1 },
  { "(!TARGET_64BIT && (TARGET_80387 || TARGET_SSE)) && ( reload_completed)",
    __builtin_constant_p (
#line 251 "../.././gcc/config/i386/sync.md"
(!TARGET_64BIT && (TARGET_80387 || TARGET_SSE)) && 
#line 253 "../.././gcc/config/i386/sync.md"
( reload_completed))
    ? (int) (
#line 251 "../.././gcc/config/i386/sync.md"
(!TARGET_64BIT && (TARGET_80387 || TARGET_SSE)) && 
#line 253 "../.././gcc/config/i386/sync.md"
( reload_completed))
    : -1 },
#line 4396 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (SFmode)\n\
   && (!TARGET_FISTTP || TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 4396 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode)
   && (!TARGET_FISTTP || TARGET_SSE_MATH))
    ? (int) 
#line 4396 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode)
   && (!TARGET_FISTTP || TARGET_SSE_MATH))
    : -1 },
#line 906 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_SSE || TARGET_3DNOW_A)\n\
   && ix86_binary_operator_ok (UMAX, V8QImode, operands)",
    __builtin_constant_p 
#line 906 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (UMAX, V8QImode, operands))
    ? (int) 
#line 906 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (UMAX, V8QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)\n\
   && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)
   && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)
   && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(ix86_match_ccmode (insn, CCGOCmode)\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 5710 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 5710 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 659 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (register_operand (operands[0], V1TImode)\n\
       || register_operand (operands[1], V1TImode))",
    __builtin_constant_p 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V1TImode)
       || register_operand (operands[1], V1TImode)))
    ? (int) 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V1TImode)
       || register_operand (operands[1], V1TImode)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(exact_log2 (INTVAL (operands[1])) >= 0\n\
   && peep2_regno_dead_p (0, FLAGS_REG)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 17231 "../.././gcc/config/i386/i386.md"
(exact_log2 (INTVAL (operands[1])) >= 0
   && peep2_regno_dead_p (0, FLAGS_REG)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 17231 "../.././gcc/config/i386/i386.md"
(exact_log2 (INTVAL (operands[1])) >= 0
   && peep2_regno_dead_p (0, FLAGS_REG)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 6619 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && TARGET_BMI2\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 6619 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_BMI2
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 6619 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_BMI2
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)\n\
   && ix86_binary_operator_ok (AND, V8SImode, operands)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (AND, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (AND, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 10014 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && INTVAL (operands[2]) + 4 == INTVAL (operands[6])\n\
   && INTVAL (operands[3]) + 4 == INTVAL (operands[7])\n\
   && INTVAL (operands[4]) + 4 == INTVAL (operands[8])\n\
   && INTVAL (operands[5]) + 4 == INTVAL (operands[9])",
    __builtin_constant_p 
#line 10014 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && INTVAL (operands[2]) + 4 == INTVAL (operands[6])
   && INTVAL (operands[3]) + 4 == INTVAL (operands[7])
   && INTVAL (operands[4]) + 4 == INTVAL (operands[8])
   && INTVAL (operands[5]) + 4 == INTVAL (operands[9]))
    ? (int) 
#line 10014 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && INTVAL (operands[2]) + 4 == INTVAL (operands[6])
   && INTVAL (operands[3]) + 4 == INTVAL (operands[7])
   && INTVAL (operands[4]) + 4 == INTVAL (operands[8])
   && INTVAL (operands[5]) + 4 == INTVAL (operands[9]))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V32QImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (64 == 64)) && 
#line 326 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (64 == 64)) && 
#line 326 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8HImode, operands) && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8HImode, operands) && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8HImode, operands) && (16 == 64)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(peep2_reg_dead_p (3, operands[0])\n\
   && (unsigned HOST_WIDE_INT) INTVAL (operands[2])\n\
      == -(unsigned HOST_WIDE_INT) INTVAL (operands[3])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 486 "../.././gcc/config/i386/sync.md"
(peep2_reg_dead_p (3, operands[0])
   && (unsigned HOST_WIDE_INT) INTVAL (operands[2])
      == -(unsigned HOST_WIDE_INT) INTVAL (operands[3])
   && !reg_overlap_mentioned_p (operands[0], operands[1])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 486 "../.././gcc/config/i386/sync.md"
(peep2_reg_dead_p (3, operands[0])
   && (unsigned HOST_WIDE_INT) INTVAL (operands[2])
      == -(unsigned HOST_WIDE_INT) INTVAL (operands[3])
   && !reg_overlap_mentioned_p (operands[0], operands[1])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8479 "../.././gcc/config/i386/i386.md"
  { "ix86_unary_operator_ok (NEG, HImode, operands)",
    __builtin_constant_p 
#line 8479 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, HImode, operands))
    ? (int) 
#line 8479 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, HImode, operands))
    : -1 },
  { "(!TARGET_PARTIAL_REG_STALL\n\
   && TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)) && ( reload_completed)",
    __builtin_constant_p (
#line 10528 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   && TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)) && 
#line 10531 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 10528 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   && TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)) && 
#line 10531 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)\n\
   && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)
   && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)
   && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
#line 7672 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4SImode, operands) && 1",
    __builtin_constant_p 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4SImode, operands) && 1)
    ? (int) 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4SImode, operands) && 1)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 9782 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (LSHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9782 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (LSHIFTRT, SImode, operands))
    ? (int) 
#line 9782 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (LSHIFTRT, SImode, operands))
    : -1 },
#line 13090 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (SFmode) && TARGET_MIX_SSE_I387\n\
   && !COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13090 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_MIX_SSE_I387
   && !COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13090 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_MIX_SSE_I387
   && !COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V4DImode)\n\
       == GET_MODE_NUNITS (V16HImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V16HImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V16HImode)))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DFmode)\n\
       == GET_MODE_NUNITS (V8DFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V8DFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V8DFmode)))
    : -1 },
#line 6164 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (MINUS, QImode, operands)\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])",
    __builtin_constant_p 
#line 6164 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, QImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    ? (int) 
#line 6164 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, QImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V2DImode)\n\
       == GET_MODE_NUNITS (V16QImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V16QImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V16QImode)))
    : -1 },
#line 13783 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
       || TARGET_MIX_SSE_I387)\n\
   && flag_unsafe_math_optimizations\n\
   && standard_80387_constant_p (operands[3]) == 2",
    __builtin_constant_p 
#line 13783 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations
   && standard_80387_constant_p (operands[3]) == 2)
    ? (int) 
#line 13783 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations
   && standard_80387_constant_p (operands[3]) == 2)
    : -1 },
  { "(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && ((((((((word_mode == SImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17371 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17371 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
  { "(TARGET_CMPXCHG8B) && (!TARGET_64BIT)",
    __builtin_constant_p (
#line 427 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG8B) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    ? (int) (
#line 427 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG8B) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (XOR, V16QImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (XOR, V16QImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (XOR, V16QImode, operands)))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V2DImode)\n\
       == GET_MODE_NUNITS (V4SImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V4SImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V4SImode)))
    : -1 },
#line 12907 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && TARGET_GNU2_TLS",
    __builtin_constant_p 
#line 12907 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_GNU2_TLS)
    ? (int) 
#line 12907 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_GNU2_TLS)
    : -1 },
#line 10416 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && !TARGET_64BIT\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 10416 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !TARGET_64BIT
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 10416 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !TARGET_64BIT
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_XOP) && (TARGET_FMA || TARGET_FMA4)",
    __builtin_constant_p (
#line 13717 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 2745 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4))
    ? (int) (
#line 13717 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 2745 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4))
    : -1 },
#line 4672 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
       || TARGET_MIX_SSE_I387)",
    __builtin_constant_p 
#line 4672 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387))
    ? (int) 
#line 4672 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V4SImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V4SImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V4SImode, operands))
    : -1 },
  { "(ix86_binary_operator_ok (ASHIFTRT, DImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))\n\
      == GET_MODE_BITSIZE (DImode)-1) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9563 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9563 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_BMI) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 12133 "../.././gcc/config/i386/i386.md"
(TARGET_BMI) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 12133 "../.././gcc/config/i386/i386.md"
(TARGET_BMI) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 14532 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && avx_vperm2f128_parallel (operands[3], V8SImode)",
    __builtin_constant_p 
#line 14532 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && avx_vperm2f128_parallel (operands[3], V8SImode))
    ? (int) 
#line 14532 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && avx_vperm2f128_parallel (operands[3], V8SImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
  { "((optimize_insn_for_size_p () || TARGET_MOVE_M1_VIA_OR)\n\
   && peep2_regno_dead_p (0, FLAGS_REG)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 17171 "../.././gcc/config/i386/i386.md"
((optimize_insn_for_size_p () || TARGET_MOVE_M1_VIA_OR)
   && peep2_regno_dead_p (0, FLAGS_REG)) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 17171 "../.././gcc/config/i386/i386.md"
((optimize_insn_for_size_p () || TARGET_MOVE_M1_VIA_OR)
   && peep2_regno_dead_p (0, FLAGS_REG)) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V32QImode)\n\
       == GET_MODE_NUNITS (V16HImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V16HImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V16HImode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
  { "((optimize && flag_peephole2) ? epilogue_completed : reload_completed) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9579 "../.././gcc/config/i386/i386.md"
((optimize && flag_peephole2) ? epilogue_completed : reload_completed) && 
#line 919 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9579 "../.././gcc/config/i386/i386.md"
((optimize && flag_peephole2) ? epilogue_completed : reload_completed) && 
#line 919 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 1003 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (XOR, V4HImode, operands)",
    __builtin_constant_p 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (XOR, V4HImode, operands))
    ? (int) 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (XOR, V4HImode, operands))
    : -1 },
#line 10217 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && INTVAL (operands[2]) + 8 == INTVAL (operands[6])\n\
   && INTVAL (operands[3]) + 8 == INTVAL (operands[7])\n\
   && INTVAL (operands[4]) + 8 == INTVAL (operands[8])\n\
   && INTVAL (operands[5]) + 8 == INTVAL (operands[9])",
    __builtin_constant_p 
#line 10217 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && INTVAL (operands[2]) + 8 == INTVAL (operands[6])
   && INTVAL (operands[3]) + 8 == INTVAL (operands[7])
   && INTVAL (operands[4]) + 8 == INTVAL (operands[8])
   && INTVAL (operands[5]) + 8 == INTVAL (operands[9]))
    ? (int) 
#line 10217 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && INTVAL (operands[2]) + 8 == INTVAL (operands[6])
   && INTVAL (operands[3]) + 8 == INTVAL (operands[7])
   && INTVAL (operands[4]) + 8 == INTVAL (operands[8])
   && INTVAL (operands[5]) + 8 == INTVAL (operands[9]))
    : -1 },
  { "(IN_RANGE (INTVAL (operands[2]), 1, 3)\n\
   /* Validate MODE for lea.  */\n\
   && ((!TARGET_PARTIAL_REG_STALL\n\
	&& (GET_MODE (operands[0]) == QImode\n\
	    || GET_MODE (operands[0]) == HImode))\n\
       || GET_MODE (operands[0]) == SImode\n\
       || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))\n\
   && (rtx_equal_p (operands[0], operands[3])\n\
       || peep2_reg_dead_p (2, operands[0]))\n\
   /* We reorder load and the shift.  */\n\
   && !reg_overlap_mentioned_p (operands[0], operands[4])) && (word_mode == DImode)",
    __builtin_constant_p (
#line 17516 "../.././gcc/config/i386/i386.md"
(IN_RANGE (INTVAL (operands[2]), 1, 3)
   /* Validate MODE for lea.  */
   && ((!TARGET_PARTIAL_REG_STALL
	&& (GET_MODE (operands[0]) == QImode
	    || GET_MODE (operands[0]) == HImode))
       || GET_MODE (operands[0]) == SImode
       || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))
   && (rtx_equal_p (operands[0], operands[3])
       || peep2_reg_dead_p (2, operands[0]))
   /* We reorder load and the shift.  */
   && !reg_overlap_mentioned_p (operands[0], operands[4])) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode))
    ? (int) (
#line 17516 "../.././gcc/config/i386/i386.md"
(IN_RANGE (INTVAL (operands[2]), 1, 3)
   /* Validate MODE for lea.  */
   && ((!TARGET_PARTIAL_REG_STALL
	&& (GET_MODE (operands[0]) == QImode
	    || GET_MODE (operands[0]) == HImode))
       || GET_MODE (operands[0]) == SImode
       || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))
   && (rtx_equal_p (operands[0], operands[3])
       || peep2_reg_dead_p (2, operands[0]))
   /* We reorder load and the shift.  */
   && !reg_overlap_mentioned_p (operands[0], operands[4])) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 6811 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 6811 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 6811 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_SSE4_1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 11675 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 247 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 11675 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 247 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_CMPXCHG16B) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 427 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG16B) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 427 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG16B) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 7071 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_8BIT_IDIV\n\
   && TARGET_QIMODE_MATH\n\
   && can_create_pseudo_p ()\n\
   && !optimize_insn_for_size_p ()",
    __builtin_constant_p 
#line 7071 "../.././gcc/config/i386/i386.md"
(TARGET_USE_8BIT_IDIV
   && TARGET_QIMODE_MATH
   && can_create_pseudo_p ()
   && !optimize_insn_for_size_p ())
    ? (int) 
#line 7071 "../.././gcc/config/i386/i386.md"
(TARGET_USE_8BIT_IDIV
   && TARGET_QIMODE_MATH
   && can_create_pseudo_p ()
   && !optimize_insn_for_size_p ())
    : -1 },
#line 8624 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (UMAX, V16QImode, operands)",
    __builtin_constant_p 
#line 8624 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (UMAX, V16QImode, operands))
    ? (int) 
#line 8624 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (UMAX, V16QImode, operands))
    : -1 },
#line 16810 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && ((SImode == HImode\n\
       && TARGET_LCP_STALL)\n\
       || (!TARGET_USE_MOV0\n\
          && TARGET_SPLIT_LONG_MOVES\n\
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 16810 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((SImode == HImode
       && TARGET_LCP_STALL)
       || (!TARGET_USE_MOV0
          && TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 16810 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((SImode == HImode
       && TARGET_LCP_STALL)
       || (!TARGET_USE_MOV0
          && TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V8SFmode)\n\
       == GET_MODE_NUNITS (V4DImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V4DImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V4DImode)))
    : -1 },
#line 5859 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (PLUS, QImode, operands)\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])",
    __builtin_constant_p 
#line 5859 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, QImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    ? (int) 
#line 5859 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, QImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V4DImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(ix86_unary_operator_ok (NEG, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8479 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8479 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (32 == 64) && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (32 == 64) && 1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (32 == 64) && 1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(ix86_match_ccmode (insn, CCGOCmode)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 1159 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 1159 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "((TARGET_SINGLE_POP || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && ((((((((word_mode == DImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17330 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17330 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
  { "(TARGET_AVX512F) && (((16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
((16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)) && 
#line 2773 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
((16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)) && 
#line 2773 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    : -1 },
  { "(TARGET_LWP) && (Pmode == DImode)",
    __builtin_constant_p (
#line 18058 "../.././gcc/config/i386/i386.md"
(TARGET_LWP) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 18058 "../.././gcc/config/i386/i386.md"
(TARGET_LWP) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
#line 1499 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE\n\
   && (TARGET_USE_HIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 1499 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 1499 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SFmode)\n\
       == GET_MODE_NUNITS (V16SImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V16SImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V16SImode)))
    : -1 },
#line 13213 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_ARITH (DFmode)\n\
   && !(TARGET_SSE2 && TARGET_SSE_MATH)\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13213 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (DFmode)
   && !(TARGET_SSE2 && TARGET_SSE_MATH)
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13213 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (DFmode)
   && !(TARGET_SSE2 && TARGET_SSE_MATH)
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)\n\
   && ix86_binary_operator_ok (IOR, V8SImode, operands)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (IOR, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (IOR, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)\n\
   && ix86_binary_operator_ok (IOR, V4DImode, operands)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (IOR, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (IOR, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 7795 "../.././gcc/config/i386/i386.md"
  { "(!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && reload_completed",
    __builtin_constant_p 
#line 7795 "../.././gcc/config/i386/i386.md"
((!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && reload_completed)
    ? (int) 
#line 7795 "../.././gcc/config/i386/i386.md"
((!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && reload_completed)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8SImode, operands) && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8SImode, operands) && (32 == 64)) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8SImode, operands) && (32 == 64)) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && TARGET_64BIT)",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3988 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && TARGET_64BIT))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3988 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && TARGET_64BIT))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)
   && 1 && 1)
    : -1 },
  { "(TARGET_CMOVE && TARGET_AVOID_MEM_OPND_FOR_CMOVE\n\
   && optimize_insn_for_speed_p ()) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 16226 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && optimize_insn_for_speed_p ()) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 16226 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && optimize_insn_for_speed_p ()) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 2483 "../.././gcc/config/i386/i386.md"
  { "TARGET_LP64 && ix86_check_movabs (insn, 1)",
    __builtin_constant_p 
#line 2483 "../.././gcc/config/i386/i386.md"
(TARGET_LP64 && ix86_check_movabs (insn, 1))
    ? (int) 
#line 2483 "../.././gcc/config/i386/i386.md"
(TARGET_LP64 && ix86_check_movabs (insn, 1))
    : -1 },
#line 8073 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (XOR, HImode, operands)",
    __builtin_constant_p 
#line 8073 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (XOR, HImode, operands))
    ? (int) 
#line 8073 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (XOR, HImode, operands))
    : -1 },
#line 1063 "../.././gcc/config/i386/i386.md"
  { "word_mode == DImode",
    __builtin_constant_p 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)
    ? (int) 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)
    : -1 },
  { "(!(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15979 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15979 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DFmode)\n\
       == GET_MODE_NUNITS (V16SImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V16SImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V16SImode)))
    : -1 },
#line 17118 "../.././gcc/config/i386/i386.md"
  { "(TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())\n\
   && REG_P (operands[0]) && REG_P (operands[4])\n\
   && REGNO (operands[0]) == REGNO (operands[4])\n\
   && peep2_reg_dead_p (4, operands[0])\n\
   && (QImode != QImode\n\
       || immediate_operand (operands[2], SImode)\n\
       || q_regs_operand (operands[2], SImode))\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[2])\n\
   && ix86_match_ccmode (peep2_next_insn (3),\n\
			 (GET_CODE (operands[3]) == PLUS\n\
			  || GET_CODE (operands[3]) == MINUS)\n\
			 ? CCGOCmode : CCNOmode)",
    __builtin_constant_p 
#line 17118 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && REG_P (operands[0]) && REG_P (operands[4])
   && REGNO (operands[0]) == REGNO (operands[4])
   && peep2_reg_dead_p (4, operands[0])
   && (QImode != QImode
       || immediate_operand (operands[2], SImode)
       || q_regs_operand (operands[2], SImode))
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    ? (int) 
#line 17118 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && REG_P (operands[0]) && REG_P (operands[4])
   && REGNO (operands[0]) == REGNO (operands[4])
   && peep2_reg_dead_p (4, operands[0])
   && (QImode != QImode
       || immediate_operand (operands[2], SImode)
       || q_regs_operand (operands[2], SImode))
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    : -1 },
  { "(TARGET_AVX && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 10513 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && !TARGET_PARTIAL_REG_STALL",
    __builtin_constant_p 
#line 10513 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_PARTIAL_REG_STALL)
    ? (int) 
#line 10513 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_PARTIAL_REG_STALL)
    : -1 },
#line 5694 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1\n\
   && ((unsigned) exact_log2 (INTVAL (operands[3]))\n\
       < GET_MODE_NUNITS (V4SFmode))",
    __builtin_constant_p 
#line 5694 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V4SFmode)))
    ? (int) 
#line 5694 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V4SFmode)))
    : -1 },
#line 16053 "../.././gcc/config/i386/i386.md"
  { "peep2_reg_dead_p (4, operands[7]) && peep2_reg_dead_p (4, operands[8])",
    __builtin_constant_p 
#line 16053 "../.././gcc/config/i386/i386.md"
(peep2_reg_dead_p (4, operands[7]) && peep2_reg_dead_p (4, operands[8]))
    ? (int) 
#line 16053 "../.././gcc/config/i386/i386.md"
(peep2_reg_dead_p (4, operands[7]) && peep2_reg_dead_p (4, operands[8]))
    : -1 },
#line 11465 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && SIBLING_CALL_P (insn)",
    __builtin_constant_p 
#line 11465 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && SIBLING_CALL_P (insn))
    ? (int) 
#line 11465 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && SIBLING_CALL_P (insn))
    : -1 },
#line 4608 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && !TARGET_FISTTP\n\
   && !SSE_FLOAT_MODE_P (GET_MODE (operands[1]))",
    __builtin_constant_p 
#line 4608 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !SSE_FLOAT_MODE_P (GET_MODE (operands[1])))
    ? (int) 
#line 4608 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !SSE_FLOAT_MODE_P (GET_MODE (operands[1])))
    : -1 },
  { "(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && ((((((((word_mode == DImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17371 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17371 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V8SFmode)\n\
       == GET_MODE_NUNITS (V8SImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V8SImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V8SImode)))
    : -1 },
  { "(TARGET_64BIT\n\
   && !(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15561 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15561 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (AND, V16SFmode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V16SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V16SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)\n\
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64)) && 
#line 326 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64)) && 
#line 326 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SFmode)\n\
       == GET_MODE_NUNITS (V32HImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V32HImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V32HImode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_SSE\n\
   && (GET_MODE_NUNITS (V4SFmode)\n\
       == GET_MODE_NUNITS (V2DFmode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(!SIBLING_CALL_P (insn)) && (word_mode == SImode)",
    __builtin_constant_p (
#line 11411 "../.././gcc/config/i386/i386.md"
(!SIBLING_CALL_P (insn)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode))
    ? (int) (
#line 11411 "../.././gcc/config/i386/i386.md"
(!SIBLING_CALL_P (insn)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode))
    : -1 },
#line 16195 "../.././gcc/config/i386/i386.md"
  { "TARGET_CMOVE && !TARGET_PARTIAL_REG_STALL\n\
   && reload_completed",
    __builtin_constant_p 
#line 16195 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && !TARGET_PARTIAL_REG_STALL
   && reload_completed)
    ? (int) 
#line 16195 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && !TARGET_PARTIAL_REG_STALL
   && reload_completed)
    : -1 },
#line 17436 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && (!TARGET_PARTIAL_REG_STALL || SImode == SImode)",
    __builtin_constant_p 
#line 17436 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && (!TARGET_PARTIAL_REG_STALL || SImode == SImode))
    ? (int) 
#line 17436 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && (!TARGET_PARTIAL_REG_STALL || SImode == SImode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_FMA || TARGET_AVX512F)",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3299 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_AVX512F))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3299 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)\n\
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    : -1 },
#line 13783 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
       || TARGET_MIX_SSE_I387)\n\
   && flag_unsafe_math_optimizations\n\
   && standard_80387_constant_p (operands[3]) == 2",
    __builtin_constant_p 
#line 13783 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations
   && standard_80387_constant_p (operands[3]) == 2)
    ? (int) 
#line 13783 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations
   && standard_80387_constant_p (operands[3]) == 2)
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V8DImode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 12836 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512PF",
    __builtin_constant_p 
#line 12836 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512PF)
    ? (int) 
#line 12836 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512PF)
    : -1 },
#line 10167 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (ROTATE, SImode, operands)",
    __builtin_constant_p 
#line 10167 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (ROTATE, SImode, operands))
    ? (int) 
#line 10167 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (ROTATE, SImode, operands))
    : -1 },
#line 9390 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && reload_completed\n\
   && true_regnum (operands[0]) != true_regnum (operands[1])",
    __builtin_constant_p 
#line 9390 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && reload_completed
   && true_regnum (operands[0]) != true_regnum (operands[1]))
    ? (int) 
#line 9390 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && reload_completed
   && true_regnum (operands[0]) != true_regnum (operands[1]))
    : -1 },
#line 1327 "../.././gcc/config/i386/mmx.md"
  { "TARGET_64BIT && TARGET_MMX",
    __builtin_constant_p 
#line 1327 "../.././gcc/config/i386/mmx.md"
(TARGET_64BIT && TARGET_MMX)
    ? (int) 
#line 1327 "../.././gcc/config/i386/mmx.md"
(TARGET_64BIT && TARGET_MMX)
    : -1 },
#line 2808 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
   && (operands[2] = find_constant_src (insn))",
    __builtin_constant_p 
#line 2808 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (operands[2] = find_constant_src (insn)))
    ? (int) 
#line 2808 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (operands[2] = find_constant_src (insn)))
    : -1 },
#line 4266 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 || (TARGET_64BIT && SSE_FLOAT_MODE_P (DFmode))",
    __builtin_constant_p 
#line 4266 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (TARGET_64BIT && SSE_FLOAT_MODE_P (DFmode)))
    ? (int) 
#line 4266 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (TARGET_64BIT && SSE_FLOAT_MODE_P (DFmode)))
    : -1 },
#line 16238 "../.././gcc/config/i386/i386.md"
  { "(TARGET_80387 && TARGET_CMOVE)\n\
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 16238 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && TARGET_CMOVE)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 16238 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && TARGET_CMOVE)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512ER) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 12957 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 12957 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 1221 "../.././gcc/config/i386/mmx.md"
  { "TARGET_3DNOW_A",
    __builtin_constant_p 
#line 1221 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW_A)
    ? (int) 
#line 1221 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW_A)
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V8DImode)\n\
       || register_operand (operands[1], V8DImode))) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8DImode)
       || register_operand (operands[1], V8DImode))) && 
#line 148 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8DImode)
       || register_operand (operands[1], V8DImode))) && 
#line 148 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 2702 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE_MATH && (TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F)",
    __builtin_constant_p 
#line 2702 "../.././gcc/config/i386/sse.md"
(TARGET_SSE_MATH && (TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F))
    ? (int) 
#line 2702 "../.././gcc/config/i386/sse.md"
(TARGET_SSE_MATH && (TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 6103 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (MINUS, SImode, operands)",
    __builtin_constant_p 
#line 6103 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, SImode, operands))
    ? (int) 
#line 6103 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, SImode, operands))
    : -1 },
#line 14359 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX && 1\n\
   && avx_vpermilp_parallel (operands[2], V4SFmode)",
    __builtin_constant_p 
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V4SFmode))
    ? (int) 
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V4SFmode))
    : -1 },
  { "(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE\n\
   && (TARGET_USE_HIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))) && ( reload_completed)",
    __builtin_constant_p (
#line 1499 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun))) && 
#line 1503 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 1499 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun))) && 
#line 1503 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V8HImode)\n\
       == GET_MODE_NUNITS (V16QImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V16QImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V16QImode)))
    : -1 },
  { "(TARGET_POPCNT) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 12351 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 12351 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (V8DImode != V8DImode || TARGET_64BIT))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14158 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DImode != V8DImode || TARGET_64BIT)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14158 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DImode != V8DImode || TARGET_64BIT)))
    : -1 },
  { "(TARGET_AVX && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "((TARGET_POPCNT\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && (TARGET_64BIT)) && ( reload_completed)",
    __builtin_constant_p ((
#line 12358 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 12361 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) ((
#line 12358 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 12361 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4DImode, operands) && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4DImode, operands) && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4DImode, operands) && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 6332 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 6332 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 6332 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (V8DImode == V16SFmode || V8DImode == V8DFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DImode == V16SFmode || V8DImode == V8DFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DImode == V16SFmode || V8DImode == V8DFmode)))
    : -1 },
#line 2317 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (GET_MODE_NUNITS (V2DImode)\n\
       == GET_MODE_NUNITS (V4SFmode))",
    __builtin_constant_p 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V4SFmode)))
    ? (int) 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V4SFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 2840 "../.././gcc/config/i386/i386.md"
  { "(TARGET_64BIT || TARGET_SSE)\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))\n\
   && (!can_create_pseudo_p ()\n\
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)\n\
       || GET_CODE (operands[1]) != CONST_DOUBLE\n\
       || (optimize_function_for_size_p (cfun)\n\
	   && standard_sse_constant_p (operands[1])\n\
	   && !memory_operand (operands[0], TFmode))\n\
       || (!TARGET_MEMORY_MISMATCH_STALL\n\
	   && memory_operand (operands[0], TFmode)))",
    __builtin_constant_p 
#line 2840 "../.././gcc/config/i386/i386.md"
((TARGET_64BIT || TARGET_SSE)
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))
   && (!can_create_pseudo_p ()
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)
       || GET_CODE (operands[1]) != CONST_DOUBLE
       || (optimize_function_for_size_p (cfun)
	   && standard_sse_constant_p (operands[1])
	   && !memory_operand (operands[0], TFmode))
       || (!TARGET_MEMORY_MISMATCH_STALL
	   && memory_operand (operands[0], TFmode))))
    ? (int) 
#line 2840 "../.././gcc/config/i386/i386.md"
((TARGET_64BIT || TARGET_SSE)
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))
   && (!can_create_pseudo_p ()
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)
       || GET_CODE (operands[1]) != CONST_DOUBLE
       || (optimize_function_for_size_p (cfun)
	   && standard_sse_constant_p (operands[1])
	   && !memory_operand (operands[0], TFmode))
       || (!TARGET_MEMORY_MISMATCH_STALL
	   && memory_operand (operands[0], TFmode))))
    : -1 },
#line 4467 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	&& (TARGET_64BIT || HImode != DImode))\n\
	&& TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 4467 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	&& (TARGET_64BIT || HImode != DImode))
	&& TARGET_SSE_MATH))
    ? (int) 
#line 4467 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	&& (TARGET_64BIT || HImode != DImode))
	&& TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V16HImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V16SFmode, operands) && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (16 == 64)))
    : -1 },
  { "(TARGET_TBM) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 12307 "../.././gcc/config/i386/i386.md"
(TARGET_TBM) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 12307 "../.././gcc/config/i386/i386.md"
(TARGET_TBM) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX2) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15236 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15236 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 10900 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_BT || optimize_function_for_size_p (cfun))\n\
   && (INTVAL (operands[3]) & 0x1f) == 0x1f",
    __builtin_constant_p 
#line 10900 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & 0x1f) == 0x1f)
    ? (int) 
#line 10900 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & 0x1f) == 0x1f)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && (16 == 64) && 1) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(! TARGET_POPCNT) && ( reload_completed)",
    __builtin_constant_p (
#line 12566 "../.././gcc/config/i386/i386.md"
(! TARGET_POPCNT) && 
#line 12568 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 12566 "../.././gcc/config/i386/i386.md"
(! TARGET_POPCNT) && 
#line 12568 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 4425 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || HImode != DImode))\n\
	&& TARGET_SSE_MATH)\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ())
    ? (int) 
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ())
    : -1 },
  { "((unsigned HOST_WIDE_INT) INTVAL (operands[1])\n\
   == -(unsigned HOST_WIDE_INT) INTVAL (operands[2])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 510 "../.././gcc/config/i386/sync.md"
((unsigned HOST_WIDE_INT) INTVAL (operands[1])
   == -(unsigned HOST_WIDE_INT) INTVAL (operands[2])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 510 "../.././gcc/config/i386/sync.md"
((unsigned HOST_WIDE_INT) INTVAL (operands[1])
   == -(unsigned HOST_WIDE_INT) INTVAL (operands[2])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)\n\
   && (32 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)
   && (32 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)
   && (32 == 64) && 1))
    : -1 },
  { "(TARGET_AVX512F) && (((16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
((16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 2774 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
((16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 2774 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && (16 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && (16 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && (16 == 64) && 1))
    : -1 },
#line 17731 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSE4_2 || TARGET_CRC32",
    __builtin_constant_p 
#line 17731 "../.././gcc/config/i386/i386.md"
(TARGET_SSE4_2 || TARGET_CRC32)
    ? (int) 
#line 17731 "../.././gcc/config/i386/i386.md"
(TARGET_SSE4_2 || TARGET_CRC32)
    : -1 },
#line 10224 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ROTATERT, HImode, operands)",
    __builtin_constant_p 
#line 10224 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, HImode, operands))
    ? (int) 
#line 10224 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, HImode, operands))
    : -1 },
#line 9826 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (LSHIFTRT, HImode, operands)",
    __builtin_constant_p 
#line 9826 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, HImode, operands))
    ? (int) 
#line 9826 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, HImode, operands))
    : -1 },
  { "(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V8SImode, operands) && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V8SImode, operands) && 1) && 
#line 262 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V8SImode, operands) && 1) && 
#line 262 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_XOP) && (TARGET_AVX)",
    __builtin_constant_p (
#line 13821 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 13821 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 16826 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && ((QImode == HImode\n\
       && TARGET_LCP_STALL)\n\
       || (TARGET_SPLIT_LONG_MOVES\n\
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))",
    __builtin_constant_p 
#line 16826 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((QImode == HImode
       && TARGET_LCP_STALL)
       || (TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn)))
    ? (int) 
#line 16826 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((QImode == HImode
       && TARGET_LCP_STALL)
       || (TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn)))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V32QImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 8609 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (UMIN, V8HImode, operands)",
    __builtin_constant_p 
#line 8609 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (UMIN, V8HImode, operands))
    ? (int) 
#line 8609 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (UMIN, V8HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)\n\
   && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)
   && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)
   && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
#line 16968 "../.././gcc/config/i386/i386.md"
  { "!(TARGET_READ_MODIFY || optimize_insn_for_size_p ())",
    __builtin_constant_p 
#line 16968 "../.././gcc/config/i386/i386.md"
(!(TARGET_READ_MODIFY || optimize_insn_for_size_p ()))
    ? (int) 
#line 16968 "../.././gcc/config/i386/i386.md"
(!(TARGET_READ_MODIFY || optimize_insn_for_size_p ()))
    : -1 },
#line 8598 "../.././gcc/config/i386/i386.md"
  { "reload_completed && SSE_REG_P (operands[0])",
    __builtin_constant_p 
#line 8598 "../.././gcc/config/i386/i386.md"
(reload_completed && SSE_REG_P (operands[0]))
    ? (int) 
#line 8598 "../.././gcc/config/i386/i386.md"
(reload_completed && SSE_REG_P (operands[0]))
    : -1 },
  { "(TARGET_AVX512F) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15427 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15427 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(reload_completed) && (!TARGET_64BIT)",
    __builtin_constant_p (
#line 10066 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    ? (int) (
#line 10066 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    : -1 },
#line 8121 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (MULT, V8HImode, operands)",
    __builtin_constant_p 
#line 8121 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MULT, V8HImode, operands))
    ? (int) 
#line 8121 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MULT, V8HImode, operands))
    : -1 },
#line 17058 "../.././gcc/config/i386/i386.md"
  { "(TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())\n\
   && peep2_reg_dead_p (4, operands[0])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[2])\n\
   && (SImode != QImode\n\
       || immediate_operand (operands[2], QImode)\n\
       || q_regs_operand (operands[2], QImode))\n\
   && ix86_match_ccmode (peep2_next_insn (3),\n\
			 (GET_CODE (operands[3]) == PLUS\n\
			  || GET_CODE (operands[3]) == MINUS)\n\
			 ? CCGOCmode : CCNOmode)",
    __builtin_constant_p 
#line 17058 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && peep2_reg_dead_p (4, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && (SImode != QImode
       || immediate_operand (operands[2], QImode)
       || q_regs_operand (operands[2], QImode))
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    ? (int) 
#line 17058 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && peep2_reg_dead_p (4, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && (SImode != QImode
       || immediate_operand (operands[2], QImode)
       || q_regs_operand (operands[2], QImode))
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    : -1 },
#line 2317 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (GET_MODE_NUNITS (V4SFmode)\n\
       == GET_MODE_NUNITS (V4SFmode))",
    __builtin_constant_p 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V4SFmode)))
    ? (int) 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V4SFmode)))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V32QImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V32QImode, operands)) && 
#line 223 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V32QImode, operands)) && 
#line 223 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_SSE\n\
   && (GET_MODE_NUNITS (V2DFmode)\n\
       == GET_MODE_NUNITS (V4SFmode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V4SFmode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V4SFmode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V4SFmode)\n\
       == GET_MODE_NUNITS (V16QImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V16QImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V16QImode)))
    : -1 },
#line 2828 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT || TARGET_SSE",
    __builtin_constant_p 
#line 2828 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT || TARGET_SSE)
    ? (int) 
#line 2828 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT || TARGET_SSE)
    : -1 },
  { "(TARGET_SSE && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10395 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10395 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && (((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 1613 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && TARGET_CMOVE\n\
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 1613 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 1613 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    : -1 },
#line 3160 "../.././gcc/config/i386/sse.md"
  { "(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && 1",
    __builtin_constant_p 
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && 1)
    ? (int) 
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && 1)
    : -1 },
#line 9638 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && INTVAL (operands[2]) == 63\n\
   && (TARGET_USE_CLTD || optimize_function_for_size_p (cfun))\n\
   && ix86_binary_operator_ok (ASHIFTRT, DImode, operands)",
    __builtin_constant_p 
#line 9638 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && INTVAL (operands[2]) == 63
   && (TARGET_USE_CLTD || optimize_function_for_size_p (cfun))
   && ix86_binary_operator_ok (ASHIFTRT, DImode, operands))
    ? (int) 
#line 9638 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && INTVAL (operands[2]) == 63
   && (TARGET_USE_CLTD || optimize_function_for_size_p (cfun))
   && ix86_binary_operator_ok (ASHIFTRT, DImode, operands))
    : -1 },
#line 4303 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 || SSE_FLOAT_MODE_P (SFmode)",
    __builtin_constant_p 
#line 4303 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || SSE_FLOAT_MODE_P (SFmode))
    ? (int) 
#line 4303 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || SSE_FLOAT_MODE_P (SFmode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (16 == 64)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V16HImode)\n\
       == GET_MODE_NUNITS (V4DFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V4DFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V4DFmode)))
    : -1 },
  { "(!TARGET_64BIT\n\
   && TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && ( reload_completed)",
    __builtin_constant_p (
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && 
#line 4949 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && 
#line 4949 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 8537 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 8537 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 8537 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_FMA || TARGET_AVX512F) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3299 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_AVX512F) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3299 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_AVX512F) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (AND, V8SFmode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V8SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V8SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V8SImode)\n\
       == GET_MODE_NUNITS (V8SImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V8SImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V8SImode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (64 == 64)\n\
   && ix86_binary_operator_ok (AND, V8DImode, operands)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (AND, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (AND, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8609 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (UMAX, V8HImode, operands)",
    __builtin_constant_p 
#line 8609 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (UMAX, V8HImode, operands))
    ? (int) 
#line 8609 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (UMAX, V8HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)\n\
   && (32 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)
   && (32 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)
   && (32 == 64) && 1))
    : -1 },
#line 16543 "../.././gcc/config/i386/i386.md"
  { "ix86_target_stack_probe ()",
    __builtin_constant_p 
#line 16543 "../.././gcc/config/i386/i386.md"
(ix86_target_stack_probe ())
    ? (int) 
#line 16543 "../.././gcc/config/i386/i386.md"
(ix86_target_stack_probe ())
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSSE3 && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && (16 == 64)))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V2DImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V2DImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V2DImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V64QImode)\n\
       == GET_MODE_NUNITS (V32HImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V32HImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V32HImode)))
    : -1 },
  { "(TARGET_SSE && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (32 == 64) && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (32 == 64) && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)\n\
   && (32 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)
   && (32 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)
   && (32 == 64) && 1))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V8SImode)\n\
       == GET_MODE_NUNITS (V4DFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V4DFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V4DFmode)))
    : -1 },
#line 2529 "../.././gcc/config/i386/i386.md"
  { "TARGET_PARTIAL_REG_STALL",
    __builtin_constant_p 
#line 2529 "../.././gcc/config/i386/i386.md"
(TARGET_PARTIAL_REG_STALL)
    ? (int) 
#line 2529 "../.././gcc/config/i386/i386.md"
(TARGET_PARTIAL_REG_STALL)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)\n\
   && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)
   && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)
   && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSSE3 && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && (64 == 64)) && 
#line 302 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && (64 == 64)) && 
#line 302 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_RDSEED) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 18153 "../.././gcc/config/i386/i386.md"
(TARGET_RDSEED) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 18153 "../.././gcc/config/i386/i386.md"
(TARGET_RDSEED) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 12466 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_XCHGB || optimize_function_for_size_p (cfun)",
    __builtin_constant_p 
#line 12466 "../.././gcc/config/i386/i386.md"
(TARGET_USE_XCHGB || optimize_function_for_size_p (cfun))
    ? (int) 
#line 12466 "../.././gcc/config/i386/i386.md"
(TARGET_USE_XCHGB || optimize_function_for_size_p (cfun))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 8774 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && !TARGET_XOP",
    __builtin_constant_p 
#line 8774 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP)
    ? (int) 
#line 8774 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP)
    : -1 },
  { "(TARGET_AVX512F) && (((32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
((32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 2776 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
((32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 2776 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    : -1 },
  { "(TARGET_AVX512F) && (((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 17405 "../.././gcc/config/i386/i386.md"
  { "(((!TARGET_FUSE_CMP_AND_BRANCH || optimize_insn_for_size_p ())\n\
     && incdec_operand (operands[3], GET_MODE (operands[3])))\n\
    || (!TARGET_FUSE_CMP_AND_BRANCH\n\
	&& INTVAL (operands[3]) == 128))\n\
   && ix86_match_ccmode (insn, CCGCmode)\n\
   && peep2_reg_dead_p (1, operands[2])",
    __builtin_constant_p 
#line 17405 "../.././gcc/config/i386/i386.md"
((((!TARGET_FUSE_CMP_AND_BRANCH || optimize_insn_for_size_p ())
     && incdec_operand (operands[3], GET_MODE (operands[3])))
    || (!TARGET_FUSE_CMP_AND_BRANCH
	&& INTVAL (operands[3]) == 128))
   && ix86_match_ccmode (insn, CCGCmode)
   && peep2_reg_dead_p (1, operands[2]))
    ? (int) 
#line 17405 "../.././gcc/config/i386/i386.md"
((((!TARGET_FUSE_CMP_AND_BRANCH || optimize_insn_for_size_p ())
     && incdec_operand (operands[3], GET_MODE (operands[3])))
    || (!TARGET_FUSE_CMP_AND_BRANCH
	&& INTVAL (operands[3]) == 128))
   && ix86_match_ccmode (insn, CCGCmode)
   && peep2_reg_dead_p (1, operands[2]))
    : -1 },
#line 15525 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512CD",
    __builtin_constant_p 
#line 15525 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512CD)
    ? (int) 
#line 15525 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512CD)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)\n\
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8DImode, operands)
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 18093 "../.././gcc/config/i386/i386.md"
  { "TARGET_LWP",
    __builtin_constant_p 
#line 18093 "../.././gcc/config/i386/i386.md"
(TARGET_LWP)
    ? (int) 
#line 18093 "../.././gcc/config/i386/i386.md"
(TARGET_LWP)
    : -1 },
#line 17752 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && (TARGET_SSE4_2 || TARGET_CRC32)",
    __builtin_constant_p 
#line 17752 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && (TARGET_SSE4_2 || TARGET_CRC32))
    ? (int) 
#line 17752 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && (TARGET_SSE4_2 || TARGET_CRC32))
    : -1 },
#line 6448 "../.././gcc/config/i386/i386.md"
  { "TARGET_HIMODE_MATH\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 6448 "../.././gcc/config/i386/i386.md"
(TARGET_HIMODE_MATH
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 6448 "../.././gcc/config/i386/i386.md"
(TARGET_HIMODE_MATH
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 8543 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (SMIN, V8HImode, operands)",
    __builtin_constant_p 
#line 8543 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SMIN, V8HImode, operands))
    ? (int) 
#line 8543 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SMIN, V8HImode, operands))
    : -1 },
#line 4145 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && !(TARGET_SSE2 && TARGET_SSE_MATH)\n\
   && !TARGET_MIX_SSE_I387",
    __builtin_constant_p 
#line 4145 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !(TARGET_SSE2 && TARGET_SSE_MATH)
   && !TARGET_MIX_SSE_I387)
    ? (int) 
#line 4145 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !(TARGET_SSE2 && TARGET_SSE_MATH)
   && !TARGET_MIX_SSE_I387)
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V8DImode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 1003 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (AND, V8QImode, operands)",
    __builtin_constant_p 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (AND, V8QImode, operands))
    ? (int) 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (AND, V8QImode, operands))
    : -1 },
#line 4877 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC\n\
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)",
    __builtin_constant_p 
#line 4877 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun))
    ? (int) 
#line 4877 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V32HImode)\n\
       == GET_MODE_NUNITS (V16SFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V16SFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V16SFmode)))
    : -1 },
  { "((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && ((((((((word_mode == DImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17305 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17305 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
#line 4327 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && !(SSE_FLOAT_MODE_P (SFmode) && (!TARGET_FISTTP || TARGET_SSE_MATH))",
    __builtin_constant_p 
#line 4327 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !(SSE_FLOAT_MODE_P (SFmode) && (!TARGET_FISTTP || TARGET_SSE_MATH)))
    ? (int) 
#line 4327 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !(SSE_FLOAT_MODE_P (SFmode) && (!TARGET_FISTTP || TARGET_SSE_MATH)))
    : -1 },
  { "(TARGET_SSE4_1) && ( reload_completed && SSE_REG_P (operands[0]))",
    __builtin_constant_p (
#line 5793 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 5798 "../.././gcc/config/i386/sse.md"
( reload_completed && SSE_REG_P (operands[0])))
    ? (int) (
#line 5793 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 5798 "../.././gcc/config/i386/sse.md"
( reload_completed && SSE_REG_P (operands[0])))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && ix86_binary_operator_ok (EQ, V8DImode, operands))",
    __builtin_constant_p (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8676 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (EQ, V8DImode, operands)))
    ? (int) (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8676 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (EQ, V8DImode, operands)))
    : -1 },
#line 3650 "../.././gcc/config/i386/i386.md"
  { "/* cltd is shorter than sarl $31, %eax */\n\
   !optimize_function_for_size_p (cfun)\n\
   && true_regnum (operands[1]) == AX_REG\n\
   && true_regnum (operands[2]) == DX_REG\n\
   && peep2_reg_dead_p (2, operands[1])\n\
   && peep2_reg_dead_p (3, operands[2])\n\
   && !reg_mentioned_p (operands[2], operands[3])",
    __builtin_constant_p 
#line 3650 "../.././gcc/config/i386/i386.md"
(/* cltd is shorter than sarl $31, %eax */
   !optimize_function_for_size_p (cfun)
   && true_regnum (operands[1]) == AX_REG
   && true_regnum (operands[2]) == DX_REG
   && peep2_reg_dead_p (2, operands[1])
   && peep2_reg_dead_p (3, operands[2])
   && !reg_mentioned_p (operands[2], operands[3]))
    ? (int) 
#line 3650 "../.././gcc/config/i386/i386.md"
(/* cltd is shorter than sarl $31, %eax */
   !optimize_function_for_size_p (cfun)
   && true_regnum (operands[1]) == AX_REG
   && true_regnum (operands[2]) == DX_REG
   && peep2_reg_dead_p (2, operands[1])
   && peep2_reg_dead_p (3, operands[2])
   && !reg_mentioned_p (operands[2], operands[3]))
    : -1 },
#line 9877 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[5]) - 2)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[6]) - 3)\n\
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)\n\
       && INTVAL (operands[7]) == (INTVAL (operands[9]) - 2)\n\
       && INTVAL (operands[7]) == (INTVAL (operands[10]) - 3)\n\
       && INTVAL (operands[11]) == (INTVAL (operands[12]) - 1)\n\
       && INTVAL (operands[11]) == (INTVAL (operands[13]) - 2)\n\
       && INTVAL (operands[11]) == (INTVAL (operands[14]) - 3)\n\
       && INTVAL (operands[15]) == (INTVAL (operands[16]) - 1)\n\
       && INTVAL (operands[15]) == (INTVAL (operands[17]) - 2)\n\
       && INTVAL (operands[15]) == (INTVAL (operands[18]) - 3))",
    __builtin_constant_p 
#line 9877 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[3]) == (INTVAL (operands[5]) - 2)
       && INTVAL (operands[3]) == (INTVAL (operands[6]) - 3)
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)
       && INTVAL (operands[7]) == (INTVAL (operands[9]) - 2)
       && INTVAL (operands[7]) == (INTVAL (operands[10]) - 3)
       && INTVAL (operands[11]) == (INTVAL (operands[12]) - 1)
       && INTVAL (operands[11]) == (INTVAL (operands[13]) - 2)
       && INTVAL (operands[11]) == (INTVAL (operands[14]) - 3)
       && INTVAL (operands[15]) == (INTVAL (operands[16]) - 1)
       && INTVAL (operands[15]) == (INTVAL (operands[17]) - 2)
       && INTVAL (operands[15]) == (INTVAL (operands[18]) - 3)))
    ? (int) 
#line 9877 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[3]) == (INTVAL (operands[5]) - 2)
       && INTVAL (operands[3]) == (INTVAL (operands[6]) - 3)
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)
       && INTVAL (operands[7]) == (INTVAL (operands[9]) - 2)
       && INTVAL (operands[7]) == (INTVAL (operands[10]) - 3)
       && INTVAL (operands[11]) == (INTVAL (operands[12]) - 1)
       && INTVAL (operands[11]) == (INTVAL (operands[13]) - 2)
       && INTVAL (operands[11]) == (INTVAL (operands[14]) - 3)
       && INTVAL (operands[15]) == (INTVAL (operands[16]) - 1)
       && INTVAL (operands[15]) == (INTVAL (operands[17]) - 2)
       && INTVAL (operands[15]) == (INTVAL (operands[18]) - 3)))
    : -1 },
  { "(TARGET_64BIT && !TARGET_PARTIAL_REG_STALL) && ( reload_completed)",
    __builtin_constant_p (
#line 10513 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_PARTIAL_REG_STALL) && 
#line 10515 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 10513 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_PARTIAL_REG_STALL) && 
#line 10515 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 17149 "../.././gcc/config/i386/i386.md"
  { "GET_MODE_SIZE (GET_MODE (operands[0])) <= UNITS_PER_WORD\n\
   && (! TARGET_USE_MOV0 || optimize_insn_for_size_p ())\n\
   && GENERAL_REG_P (operands[0])\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 17149 "../.././gcc/config/i386/i386.md"
(GET_MODE_SIZE (GET_MODE (operands[0])) <= UNITS_PER_WORD
   && (! TARGET_USE_MOV0 || optimize_insn_for_size_p ())
   && GENERAL_REG_P (operands[0])
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 17149 "../.././gcc/config/i386/i386.md"
(GET_MODE_SIZE (GET_MODE (operands[0])) <= UNITS_PER_WORD
   && (! TARGET_USE_MOV0 || optimize_insn_for_size_p ())
   && GENERAL_REG_P (operands[0])
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
#line 4467 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	&& (TARGET_64BIT || DImode != DImode))\n\
	&& TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 4467 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	&& (TARGET_64BIT || DImode != DImode))
	&& TARGET_SSE_MATH))
    ? (int) 
#line 4467 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	&& (TARGET_64BIT || DImode != DImode))
	&& TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && !flag_finite_math_only\n\
   && (16 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (16 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (16 == 64) && 1))
    : -1 },
  { "(!(MEM_P (operands[1]) && MEM_P (operands[2]))) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 6703 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 6703 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 1461 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE",
    __builtin_constant_p 
#line 1461 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE)
    ? (int) 
#line 1461 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE)
    : -1 },
  { "(TARGET_80387 || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 4692 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 4692 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_SSE && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)\n\
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[1])\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[2])\n\
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])\n\
       || GET_MODE (operands[3]) == VOIDmode)) && ( reload_completed)",
    __builtin_constant_p (
#line 5880 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && GET_MODE (operands[0]) == GET_MODE (operands[2])
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])
       || GET_MODE (operands[3]) == VOIDmode)) && 
#line 5887 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 5880 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && GET_MODE (operands[0]) == GET_MODE (operands[2])
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])
       || GET_MODE (operands[3]) == VOIDmode)) && 
#line 5887 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 7871 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (AND, SImode, operands)",
    __builtin_constant_p 
#line 7871 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (AND, SImode, operands))
    ? (int) 
#line 7871 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (AND, SImode, operands))
    : -1 },
  { "(TARGET_SSSE3) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 11386 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3) && 
#line 288 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 11386 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3) && 
#line 288 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)\n\
   && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)
   && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)
   && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 1702 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && reload_completed\n\
   && !(MMX_REG_P (operands[1]) || SSE_REG_P (operands[1]))",
    __builtin_constant_p 
#line 1702 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && reload_completed
   && !(MMX_REG_P (operands[1]) || SSE_REG_P (operands[1])))
    ? (int) 
#line 1702 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && reload_completed
   && !(MMX_REG_P (operands[1]) || SSE_REG_P (operands[1])))
    : -1 },
#line 12168 "../.././gcc/config/i386/i386.md"
  { "TARGET_BMI2",
    __builtin_constant_p 
#line 12168 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2)
    ? (int) 
#line 12168 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2)
    : -1 },
#line 685 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (SS_PLUS, V8QImode, operands)",
    __builtin_constant_p 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (SS_PLUS, V8QImode, operands))
    ? (int) 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (SS_PLUS, V8QImode, operands))
    : -1 },
#line 3305 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
   && (GET_MODE (operands[0]) == TFmode\n\
       || GET_MODE (operands[0]) == XFmode\n\
       || GET_MODE (operands[0]) == DFmode)\n\
   && !(ANY_FP_REG_P (operands[0]) || ANY_FP_REG_P (operands[1]))",
    __builtin_constant_p 
#line 3305 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (GET_MODE (operands[0]) == TFmode
       || GET_MODE (operands[0]) == XFmode
       || GET_MODE (operands[0]) == DFmode)
   && !(ANY_FP_REG_P (operands[0]) || ANY_FP_REG_P (operands[1])))
    ? (int) 
#line 3305 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (GET_MODE (operands[0]) == TFmode
       || GET_MODE (operands[0]) == XFmode
       || GET_MODE (operands[0]) == DFmode)
   && !(ANY_FP_REG_P (operands[0]) || ANY_FP_REG_P (operands[1])))
    : -1 },
#line 10715 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8HImode, operands)",
    __builtin_constant_p 
#line 10715 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8HImode, operands))
    ? (int) 
#line 10715 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8HImode, operands))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V16SImode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (AND, V4SImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (AND, V4SImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (AND, V4SImode, operands)))
    : -1 },
#line 17058 "../.././gcc/config/i386/i386.md"
  { "(TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())\n\
   && peep2_reg_dead_p (4, operands[0])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[2])\n\
   && (QImode != QImode\n\
       || immediate_operand (operands[2], QImode)\n\
       || q_regs_operand (operands[2], QImode))\n\
   && ix86_match_ccmode (peep2_next_insn (3),\n\
			 (GET_CODE (operands[3]) == PLUS\n\
			  || GET_CODE (operands[3]) == MINUS)\n\
			 ? CCGOCmode : CCNOmode)",
    __builtin_constant_p 
#line 17058 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && peep2_reg_dead_p (4, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && (QImode != QImode
       || immediate_operand (operands[2], QImode)
       || q_regs_operand (operands[2], QImode))
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    ? (int) 
#line 17058 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && peep2_reg_dead_p (4, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && (QImode != QImode
       || immediate_operand (operands[2], QImode)
       || q_regs_operand (operands[2], QImode))
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && (SSE_FLOAT_MODE_P (DFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2264 "../.././gcc/config/i386/sse.md"
(SSE_FLOAT_MODE_P (DFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2264 "../.././gcc/config/i386/sse.md"
(SSE_FLOAT_MODE_P (DFmode)))
    : -1 },
#line 1613 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && TARGET_CMOVE\n\
   && !(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 1613 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE
   && !(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 1613 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE
   && !(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))
    : -1 },
#line 9214 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ASHIFT, HImode, operands)",
    __builtin_constant_p 
#line 9214 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, HImode, operands))
    ? (int) 
#line 9214 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, HImode, operands))
    : -1 },
#line 4370 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && TARGET_SSE2 && TARGET_SSE_MATH\n\
   && optimize_function_for_speed_p (cfun)",
    __builtin_constant_p 
#line 4370 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_SSE_MATH
   && optimize_function_for_speed_p (cfun))
    ? (int) 
#line 4370 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_SSE_MATH
   && optimize_function_for_speed_p (cfun))
    : -1 },
#line 4965 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT\n\
   && ((TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)\n\
	&& TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)\n\
       || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))",
    __builtin_constant_p 
#line 4965 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && ((TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
	&& TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
       || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)))
    ? (int) 
#line 4965 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && ((TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
	&& TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
       || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)))
    : -1 },
  { "(!TARGET_X32) && (Pmode == SImode)",
    __builtin_constant_p (
#line 12869 "../.././gcc/config/i386/i386.md"
(!TARGET_X32) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 12869 "../.././gcc/config/i386/i386.md"
(!TARGET_X32) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 14837 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH\n\
       && SImode != HImode \n\
       && ((SImode != DImode) || TARGET_64BIT)\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && SImode != HImode 
       && ((SImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && SImode != HImode 
       && ((SImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
  { "(TARGET_SSSE3 && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && 1) && 
#line 301 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && 1) && 
#line 301 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F\n\
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[5]) - 2)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[6]) - 3)\n\
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)\n\
       && INTVAL (operands[7]) == (INTVAL (operands[9]) - 2)\n\
       && INTVAL (operands[7]) == (INTVAL (operands[10]) - 3)\n\
       && INTVAL (operands[11]) == (INTVAL (operands[12]) - 1)\n\
       && INTVAL (operands[11]) == (INTVAL (operands[13]) - 2)\n\
       && INTVAL (operands[11]) == (INTVAL (operands[14]) - 3)\n\
       && INTVAL (operands[15]) == (INTVAL (operands[16]) - 1)\n\
       && INTVAL (operands[15]) == (INTVAL (operands[17]) - 2)\n\
       && INTVAL (operands[15]) == (INTVAL (operands[18]) - 3)))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9877 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[3]) == (INTVAL (operands[5]) - 2)
       && INTVAL (operands[3]) == (INTVAL (operands[6]) - 3)
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)
       && INTVAL (operands[7]) == (INTVAL (operands[9]) - 2)
       && INTVAL (operands[7]) == (INTVAL (operands[10]) - 3)
       && INTVAL (operands[11]) == (INTVAL (operands[12]) - 1)
       && INTVAL (operands[11]) == (INTVAL (operands[13]) - 2)
       && INTVAL (operands[11]) == (INTVAL (operands[14]) - 3)
       && INTVAL (operands[15]) == (INTVAL (operands[16]) - 1)
       && INTVAL (operands[15]) == (INTVAL (operands[17]) - 2)
       && INTVAL (operands[15]) == (INTVAL (operands[18]) - 3))))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9877 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[3]) == (INTVAL (operands[5]) - 2)
       && INTVAL (operands[3]) == (INTVAL (operands[6]) - 3)
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)
       && INTVAL (operands[7]) == (INTVAL (operands[9]) - 2)
       && INTVAL (operands[7]) == (INTVAL (operands[10]) - 3)
       && INTVAL (operands[11]) == (INTVAL (operands[12]) - 1)
       && INTVAL (operands[11]) == (INTVAL (operands[13]) - 2)
       && INTVAL (operands[11]) == (INTVAL (operands[14]) - 3)
       && INTVAL (operands[15]) == (INTVAL (operands[16]) - 1)
       && INTVAL (operands[15]) == (INTVAL (operands[17]) - 2)
       && INTVAL (operands[15]) == (INTVAL (operands[18]) - 3))))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V4DFmode)\n\
       == GET_MODE_NUNITS (V32QImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V32QImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V32QImode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)\n\
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 14837 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH\n\
       && SImode != HImode \n\
       && ((SImode != DImode) || TARGET_64BIT)\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && SImode != HImode 
       && ((SImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && SImode != HImode 
       && ((SImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
#line 9826 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ASHIFTRT, QImode, operands)",
    __builtin_constant_p 
#line 9826 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, QImode, operands))
    ? (int) 
#line 9826 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, QImode, operands))
    : -1 },
#line 12751 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_cmodel == CM_LARGE_PIC && !TARGET_PECOFF\n\
   && GET_CODE (operands[2]) == CONST\n\
   && GET_CODE (XEXP (operands[2], 0)) == UNSPEC\n\
   && XINT (XEXP (operands[2], 0), 1) == UNSPEC_PLTOFF",
    __builtin_constant_p 
#line 12751 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_cmodel == CM_LARGE_PIC && !TARGET_PECOFF
   && GET_CODE (operands[2]) == CONST
   && GET_CODE (XEXP (operands[2], 0)) == UNSPEC
   && XINT (XEXP (operands[2], 0), 1) == UNSPEC_PLTOFF)
    ? (int) 
#line 12751 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_cmodel == CM_LARGE_PIC && !TARGET_PECOFF
   && GET_CODE (operands[2]) == CONST
   && GET_CODE (XEXP (operands[2], 0)) == UNSPEC
   && XINT (XEXP (operands[2], 0), 1) == UNSPEC_PLTOFF)
    : -1 },
#line 659 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (register_operand (operands[0], V8HImode)\n\
       || register_operand (operands[1], V8HImode))",
    __builtin_constant_p 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8HImode)
       || register_operand (operands[1], V8HImode)))
    ? (int) 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8HImode)
       || register_operand (operands[1], V8HImode)))
    : -1 },
#line 8087 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (IOR, QImode, operands)",
    __builtin_constant_p 
#line 8087 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (IOR, QImode, operands))
    ? (int) 
#line 8087 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (IOR, QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DImode)\n\
       == GET_MODE_NUNITS (V32HImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V32HImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V32HImode)))
    : -1 },
#line 14158 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && (V16SImode != V8DImode || TARGET_64BIT)",
    __builtin_constant_p 
#line 14158 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SImode != V8DImode || TARGET_64BIT))
    ? (int) 
#line 14158 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SImode != V8DImode || TARGET_64BIT))
    : -1 },
#line 8073 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (IOR, HImode, operands)",
    __builtin_constant_p 
#line 8073 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (IOR, HImode, operands))
    ? (int) 
#line 8073 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (IOR, HImode, operands))
    : -1 },
#line 8929 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSSE3 || TARGET_AVX || TARGET_XOP",
    __builtin_constant_p 
#line 8929 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 || TARGET_AVX || TARGET_XOP)
    ? (int) 
#line 8929 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 || TARGET_AVX || TARGET_XOP)
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DFmode)\n\
       == GET_MODE_NUNITS (V64QImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V64QImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V64QImode)))
    : -1 },
#line 4504 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && !TARGET_FISTTP\n\
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || HImode != DImode))\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
   && can_create_pseudo_p ())
    ? (int) 
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
   && can_create_pseudo_p ())
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 2625 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && ix86_binary_operator_ok (IOR, TFmode, operands)",
    __builtin_constant_p 
#line 2625 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && ix86_binary_operator_ok (IOR, TFmode, operands))
    ? (int) 
#line 2625 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && ix86_binary_operator_ok (IOR, TFmode, operands))
    : -1 },
#line 13607 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && flag_finite_math_only",
    __builtin_constant_p 
#line 13607 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_finite_math_only)
    ? (int) 
#line 13607 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_finite_math_only)
    : -1 },
#line 8861 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_unary_operator_ok (NOT, HImode, operands)",
    __builtin_constant_p 
#line 8861 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, HImode, operands))
    ? (int) 
#line 8861 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, HImode, operands))
    : -1 },
#line 84 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 84 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 84 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    : -1 },
  { "(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH\n\
   && !flag_trapping_math) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 15224 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
   && !flag_trapping_math) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 15224 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
   && !flag_trapping_math) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 12957 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512ER",
    __builtin_constant_p 
#line 12957 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER)
    ? (int) 
#line 12957 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER)
    : -1 },
#line 5436 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (PLUS, SImode, operands)",
    __builtin_constant_p 
#line 5436 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, SImode, operands))
    ? (int) 
#line 5436 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, SImode, operands))
    : -1 },
#line 7695 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V8HImode, operands)",
    __builtin_constant_p 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V8HImode, operands))
    ? (int) 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V8HImode, operands))
    : -1 },
  { "(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V8HImode)\n\
       == GET_MODE_NUNITS (V8HImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V8HImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V8HImode)))
    : -1 },
#line 8138 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (IOR, HImode, operands)",
    __builtin_constant_p 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, HImode, operands))
    ? (int) 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, HImode, operands))
    : -1 },
#line 16149 "../.././gcc/config/i386/i386.md"
  { "TARGET_CMOVE && !(MEM_P (operands[2]) && MEM_P (operands[3]))",
    __builtin_constant_p 
#line 16149 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && !(MEM_P (operands[2]) && MEM_P (operands[3])))
    ? (int) 
#line 16149 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && !(MEM_P (operands[2]) && MEM_P (operands[3])))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (IOR, V8HImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (IOR, V8HImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (IOR, V8HImode, operands)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)\n\
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V4DImode)\n\
       == GET_MODE_NUNITS (V8SFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V8SFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V8SFmode)))
    : -1 },
#line 139 "../.././gcc/config/i386/sync.md"
  { "TARGET_64BIT || (TARGET_CMPXCHG8B && (TARGET_80387 || TARGET_SSE))",
    __builtin_constant_p 
#line 139 "../.././gcc/config/i386/sync.md"
(TARGET_64BIT || (TARGET_CMPXCHG8B && (TARGET_80387 || TARGET_SSE)))
    ? (int) 
#line 139 "../.././gcc/config/i386/sync.md"
(TARGET_64BIT || (TARGET_CMPXCHG8B && (TARGET_80387 || TARGET_SSE)))
    : -1 },
#line 11287 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSSE3 && ix86_binary_operator_ok (MULT, V8HImode, operands)",
    __builtin_constant_p 
#line 11287 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && ix86_binary_operator_ok (MULT, V8HImode, operands))
    ? (int) 
#line 11287 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && ix86_binary_operator_ok (MULT, V8HImode, operands))
    : -1 },
  { "(ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (IOR, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == SImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17383 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17383 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && 1 && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)\n\
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 685 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (US_PLUS, V8QImode, operands)",
    __builtin_constant_p 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (US_PLUS, V8QImode, operands))
    ? (int) 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (US_PLUS, V8QImode, operands))
    : -1 },
#line 7695 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V16QImode, operands)",
    __builtin_constant_p 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V16QImode, operands))
    ? (int) 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V16QImode, operands))
    : -1 },
#line 659 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (register_operand (operands[0], V4SFmode)\n\
       || register_operand (operands[1], V4SFmode))",
    __builtin_constant_p 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V4SFmode)
       || register_operand (operands[1], V4SFmode)))
    ? (int) 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V4SFmode)
       || register_operand (operands[1], V4SFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 191 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 6851 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 191 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 6851 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(TARGET_SSSE3 && ix86_binary_operator_ok (MULT, V16HImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 11287 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && ix86_binary_operator_ok (MULT, V16HImode, operands)) && 
#line 250 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 11287 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && ix86_binary_operator_ok (MULT, V16HImode, operands)) && 
#line 250 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 13192 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (DFmode, HImode)\n\
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
   && (TARGET_USE_HIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 13192 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, HImode)
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 13192 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, HImode)
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
  { "(TARGET_AVOID_VECTOR_DECODE\n\
   && SSE_FLOAT_MODE_P (DFmode)\n\
   && optimize_insn_for_speed_p ()) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 4416 "../.././gcc/config/i386/i386.md"
(TARGET_AVOID_VECTOR_DECODE
   && SSE_FLOAT_MODE_P (DFmode)
   && optimize_insn_for_speed_p ()) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 4416 "../.././gcc/config/i386/i386.md"
(TARGET_AVOID_VECTOR_DECODE
   && SSE_FLOAT_MODE_P (DFmode)
   && optimize_insn_for_speed_p ()) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 5710 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCGOCmode)\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 5710 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 5710 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 10466 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && !TARGET_USE_BT",
    __builtin_constant_p 
#line 10466 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_USE_BT)
    ? (int) 
#line 10466 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_USE_BT)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)\n\
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16SImode, operands)
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 2317 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (GET_MODE_NUNITS (V8HImode)\n\
       == GET_MODE_NUNITS (V4SFmode))",
    __builtin_constant_p 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V4SFmode)))
    ? (int) 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V4SFmode)))
    : -1 },
#line 13580 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 || TARGET_XOP",
    __builtin_constant_p 
#line 13580 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 || TARGET_XOP)
    ? (int) 
#line 13580 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 || TARGET_XOP)
    : -1 },
#line 659 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (register_operand (operands[0], V16QImode)\n\
       || register_operand (operands[1], V16QImode))",
    __builtin_constant_p 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V16QImode)
       || register_operand (operands[1], V16QImode)))
    ? (int) 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V16QImode)
       || register_operand (operands[1], V16QImode)))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V8DImode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)\n\
   && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)
   && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8SFmode, operands)
   && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
  { "(!(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15938 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15938 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && 1 && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)\n\
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
  { "(TARGET_ROUND) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 12112 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 12112 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (XOR, V16SFmode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V16SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V16SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V4SFmode)\n\
       == GET_MODE_NUNITS (V4SImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V4SImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V4SImode)))
    : -1 },
#line 9955 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && INTVAL (operands[2]) + 4 == INTVAL (operands[6])\n\
   && INTVAL (operands[3]) + 4 == INTVAL (operands[7])\n\
   && INTVAL (operands[4]) + 4 == INTVAL (operands[8])\n\
   && INTVAL (operands[5]) + 4 == INTVAL (operands[9])\n\
   && INTVAL (operands[2]) + 8 == INTVAL (operands[10])\n\
   && INTVAL (operands[3]) + 8 == INTVAL (operands[11])\n\
   && INTVAL (operands[4]) + 8 == INTVAL (operands[12])\n\
   && INTVAL (operands[5]) + 8 == INTVAL (operands[13])\n\
   && INTVAL (operands[2]) + 12 == INTVAL (operands[14])\n\
   && INTVAL (operands[3]) + 12 == INTVAL (operands[15])\n\
   && INTVAL (operands[4]) + 12 == INTVAL (operands[16])\n\
   && INTVAL (operands[5]) + 12 == INTVAL (operands[17])",
    __builtin_constant_p 
#line 9955 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && INTVAL (operands[2]) + 4 == INTVAL (operands[6])
   && INTVAL (operands[3]) + 4 == INTVAL (operands[7])
   && INTVAL (operands[4]) + 4 == INTVAL (operands[8])
   && INTVAL (operands[5]) + 4 == INTVAL (operands[9])
   && INTVAL (operands[2]) + 8 == INTVAL (operands[10])
   && INTVAL (operands[3]) + 8 == INTVAL (operands[11])
   && INTVAL (operands[4]) + 8 == INTVAL (operands[12])
   && INTVAL (operands[5]) + 8 == INTVAL (operands[13])
   && INTVAL (operands[2]) + 12 == INTVAL (operands[14])
   && INTVAL (operands[3]) + 12 == INTVAL (operands[15])
   && INTVAL (operands[4]) + 12 == INTVAL (operands[16])
   && INTVAL (operands[5]) + 12 == INTVAL (operands[17]))
    ? (int) 
#line 9955 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && INTVAL (operands[2]) + 4 == INTVAL (operands[6])
   && INTVAL (operands[3]) + 4 == INTVAL (operands[7])
   && INTVAL (operands[4]) + 4 == INTVAL (operands[8])
   && INTVAL (operands[5]) + 4 == INTVAL (operands[9])
   && INTVAL (operands[2]) + 8 == INTVAL (operands[10])
   && INTVAL (operands[3]) + 8 == INTVAL (operands[11])
   && INTVAL (operands[4]) + 8 == INTVAL (operands[12])
   && INTVAL (operands[5]) + 8 == INTVAL (operands[13])
   && INTVAL (operands[2]) + 12 == INTVAL (operands[14])
   && INTVAL (operands[3]) + 12 == INTVAL (operands[15])
   && INTVAL (operands[4]) + 12 == INTVAL (operands[16])
   && INTVAL (operands[5]) + 12 == INTVAL (operands[17]))
    : -1 },
#line 13150 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_ARITH (SFmode)\n\
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
   && !COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13150 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (SFmode)
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   && !COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13150 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (SFmode)
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   && !COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 7560 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (AND, DImode, operands)",
    __builtin_constant_p 
#line 7560 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (AND, DImode, operands))
    ? (int) 
#line 7560 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (AND, DImode, operands))
    : -1 },
#line 9655 "../.././gcc/config/i386/i386.md"
  { "INTVAL (operands[2]) == 31\n\
   && (TARGET_USE_CLTD || optimize_function_for_size_p (cfun))\n\
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9655 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[2]) == 31
   && (TARGET_USE_CLTD || optimize_function_for_size_p (cfun))
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    ? (int) 
#line 9655 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[2]) == 31
   && (TARGET_USE_CLTD || optimize_function_for_size_p (cfun))
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    : -1 },
  { "(TARGET_CMPXCHG) && (TARGET_64BIT || TARGET_CMPXCHG8B)",
    __builtin_constant_p (
#line 343 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG) && 
#line 330 "../.././gcc/config/i386/sync.md"
(TARGET_64BIT || TARGET_CMPXCHG8B))
    ? (int) (
#line 343 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG) && 
#line 330 "../.././gcc/config/i386/sync.md"
(TARGET_64BIT || TARGET_CMPXCHG8B))
    : -1 },
  { "(TARGET_SSE) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9072 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9072 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 15386 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 15386 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 15386 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)) && ( reload_completed)",
    __builtin_constant_p (
#line 3530 "../.././gcc/config/i386/i386.md"
(TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)) && 
#line 3532 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 3530 "../.././gcc/config/i386/i386.md"
(TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)) && 
#line 3532 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(TARGET_SSE3) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1169 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3) && 
#line 236 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1169 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3) && 
#line 236 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V32QImode, operands) && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V32QImode, operands) && 1) && 
#line 229 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V32QImode, operands) && 1) && 
#line 229 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 8528 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (SMIN, V4SImode, operands)",
    __builtin_constant_p 
#line 8528 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (SMIN, V4SImode, operands))
    ? (int) 
#line 8528 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (SMIN, V4SImode, operands))
    : -1 },
  { "(TARGET_AVX512PF) && (Pmode == SImode)",
    __builtin_constant_p (
#line 12883 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512PF) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 12883 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512PF) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 7707 "../.././gcc/config/i386/i386.md"
  { "TARGET_AVX512F && !TARGET_BMI && reload_completed",
    __builtin_constant_p 
#line 7707 "../.././gcc/config/i386/i386.md"
(TARGET_AVX512F && !TARGET_BMI && reload_completed)
    ? (int) 
#line 7707 "../.././gcc/config/i386/i386.md"
(TARGET_AVX512F && !TARGET_BMI && reload_completed)
    : -1 },
#line 16810 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && ((QImode == HImode\n\
       && TARGET_LCP_STALL)\n\
       || (!TARGET_USE_MOV0\n\
          && TARGET_SPLIT_LONG_MOVES\n\
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 16810 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((QImode == HImode
       && TARGET_LCP_STALL)
       || (!TARGET_USE_MOV0
          && TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 16810 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((QImode == HImode
       && TARGET_LCP_STALL)
       || (!TARGET_USE_MOV0
          && TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
#line 17591 "../.././gcc/config/i386/i386.md"
  { "TARGET_PREFETCH_SSE",
    __builtin_constant_p 
#line 17591 "../.././gcc/config/i386/i386.md"
(TARGET_PREFETCH_SSE)
    ? (int) 
#line 17591 "../.././gcc/config/i386/i386.md"
(TARGET_PREFETCH_SSE)
    : -1 },
#line 2577 "../.././gcc/config/i386/sse.md"
  { "SSE_FLOAT_MODE_P (DFmode)",
    __builtin_constant_p 
#line 2577 "../.././gcc/config/i386/sse.md"
(SSE_FLOAT_MODE_P (DFmode))
    ? (int) 
#line 2577 "../.././gcc/config/i386/sse.md"
(SSE_FLOAT_MODE_P (DFmode))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SImode)\n\
       == GET_MODE_NUNITS (V16SFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V16SFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V16SFmode)))
    : -1 },
  { "(TARGET_64BIT && TARGET_MMX) && ( reload_completed)",
    __builtin_constant_p (
#line 1327 "../.././gcc/config/i386/mmx.md"
(TARGET_64BIT && TARGET_MMX) && 
#line 1329 "../.././gcc/config/i386/mmx.md"
( reload_completed))
    ? (int) (
#line 1327 "../.././gcc/config/i386/mmx.md"
(TARGET_64BIT && TARGET_MMX) && 
#line 1329 "../.././gcc/config/i386/mmx.md"
( reload_completed))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)\n\
   && (16 == 64) && 1) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)
   && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)
   && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 11454 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && !SIBLING_CALL_P (insn)",
    __builtin_constant_p 
#line 11454 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && !SIBLING_CALL_P (insn))
    ? (int) 
#line 11454 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && !SIBLING_CALL_P (insn))
    : -1 },
  { "((TARGET_USE_BT || optimize_function_for_size_p (cfun))\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))\n\
      == GET_MODE_BITSIZE (SImode)-1) && ( 1)",
    __builtin_constant_p (
#line 10829 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1) && 
#line 10833 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 10829 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1) && 
#line 10833 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)\n\
   && ix86_binary_operator_ok (AND, V16HImode, operands)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (AND, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (AND, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_SSE\n\
   && (GET_MODE_NUNITS (V2DFmode)\n\
       == GET_MODE_NUNITS (V2DFmode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX2) && (TARGET_AVX)",
    __builtin_constant_p (
#line 13985 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 13985 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 6847 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387",
    __builtin_constant_p 
#line 6847 "../.././gcc/config/i386/i386.md"
(TARGET_80387)
    ? (int) 
#line 6847 "../.././gcc/config/i386/i386.md"
(TARGET_80387)
    : -1 },
  { "(reload_completed) && (Pmode == SImode)",
    __builtin_constant_p (
#line 3815 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 3815 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V32QImode, operands) && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V32QImode, operands) && 1) && 
#line 229 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V32QImode, operands) && 1) && 
#line 229 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 320 "../.././gcc/config/i386/mmx.md"
  { "TARGET_3DNOW && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V2SFmode, operands)",
    __builtin_constant_p 
#line 320 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2SFmode, operands))
    ? (int) 
#line 320 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2SFmode, operands))
    : -1 },
#line 1483 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (TARGET_USE_HIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 1483 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 1483 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
#line 8138 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (XOR, SImode, operands)",
    __builtin_constant_p 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, SImode, operands))
    ? (int) 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, SImode, operands))
    : -1 },
#line 8703 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && !TARGET_XOP\n\
   && ix86_binary_operator_ok (EQ, V8HImode, operands)",
    __builtin_constant_p 
#line 8703 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP
   && ix86_binary_operator_ok (EQ, V8HImode, operands))
    ? (int) 
#line 8703 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP
   && ix86_binary_operator_ok (EQ, V8HImode, operands))
    : -1 },
#line 8005 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
    && QI_REG_P (operands[0])\n\
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
    && !(~INTVAL (operands[2]) & ~(255 << 8))\n\
    && GET_MODE (operands[0]) != QImode",
    __builtin_constant_p 
#line 8005 "../.././gcc/config/i386/i386.md"
(reload_completed
    && QI_REG_P (operands[0])
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    && !(~INTVAL (operands[2]) & ~(255 << 8))
    && GET_MODE (operands[0]) != QImode)
    ? (int) 
#line 8005 "../.././gcc/config/i386/i386.md"
(reload_completed
    && QI_REG_P (operands[0])
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    && !(~INTVAL (operands[2]) & ~(255 << 8))
    && GET_MODE (operands[0]) != QImode)
    : -1 },
  { "(TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V2DFmode)\n\
       == GET_MODE_NUNITS (V16QImode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V16QImode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V16QImode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 1152 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_SSE || TARGET_3DNOW_A)\n\
   && ((unsigned) exact_log2 (INTVAL (operands[3]))\n\
       < GET_MODE_NUNITS (V4HImode))",
    __builtin_constant_p 
#line 1152 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V4HImode)))
    ? (int) 
#line 1152 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V4HImode)))
    : -1 },
#line 8654 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (EQ, V16HImode, operands)",
    __builtin_constant_p 
#line 8654 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (EQ, V16HImode, operands))
    ? (int) 
#line 8654 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (EQ, V16HImode, operands))
    : -1 },
#line 7886 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (AND, SImode, operands)",
    __builtin_constant_p 
#line 7886 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (AND, SImode, operands))
    ? (int) 
#line 7886 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (AND, SImode, operands))
    : -1 },
#line 7276 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 7276 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 7276 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 17198 "../.././gcc/config/i386/i386.md"
  { "!TARGET_OPT_AGU\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 17198 "../.././gcc/config/i386/i386.md"
(!TARGET_OPT_AGU
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 17198 "../.././gcc/config/i386/i386.md"
(!TARGET_OPT_AGU
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
  { "((TARGET_SSE) && (TARGET_SSE2)) && ( reload_completed)",
    __builtin_constant_p ((
#line 1260 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)) && 
#line 1262 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) ((
#line 1260 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)) && 
#line 1262 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (IOR, V2DFmode, operands)) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V2DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V2DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && ((((((((word_mode == DImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17305 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17305 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 14837 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH\n\
       && DImode != HImode \n\
       && ((DImode != DImode) || TARGET_64BIT)\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH
       && DImode != HImode 
       && ((DImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH
       && DImode != HImode 
       && ((DImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
#line 13058 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH\n\
   && COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13058 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13058 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V16SImode, operands) && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V16SImode, operands) && (64 == 64)) && 
#line 262 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V16SImode, operands) && (64 == 64)) && 
#line 262 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 5913 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (INTVAL (operands[2]) == (INTVAL (operands[3]) - 1)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)\n\
       && INTVAL (operands[4]) == (INTVAL (operands[5]) - 1))",
    __builtin_constant_p 
#line 5913 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[2]) == (INTVAL (operands[3]) - 1)
       && INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[4]) == (INTVAL (operands[5]) - 1)))
    ? (int) 
#line 5913 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[2]) == (INTVAL (operands[3]) - 1)
       && INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[4]) == (INTVAL (operands[5]) - 1)))
    : -1 },
#line 6087 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (MINUS, SImode, operands)",
    __builtin_constant_p 
#line 6087 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, SImode, operands))
    ? (int) 
#line 6087 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, SImode, operands))
    : -1 },
#line 4504 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && !TARGET_FISTTP\n\
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || DImode != DImode))\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
   && can_create_pseudo_p ())
    ? (int) 
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
   && can_create_pseudo_p ())
    : -1 },
  { "(TARGET_SSE && !flag_finite_math_only\n\
   && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_64BIT\n\
   && !(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15743 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15743 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
#line 1003 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (IOR, V2SImode, operands)",
    __builtin_constant_p 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (IOR, V2SImode, operands))
    ? (int) 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (IOR, V2SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && ix86_binary_operator_ok (MULT, V16SImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7921 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (MULT, V16SImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7921 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (MULT, V16SImode, operands)))
    : -1 },
#line 9729 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ASHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9729 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    ? (int) 
#line 9729 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    : -1 },
#line 685 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (US_MINUS, V8QImode, operands)",
    __builtin_constant_p 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (US_MINUS, V8QImode, operands))
    ? (int) 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (US_MINUS, V8QImode, operands))
    : -1 },
#line 13192 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (SFmode, SImode)\n\
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
   && (TARGET_USE_SIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 13192 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, SImode)
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 13192 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, SImode)
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
#line 2410 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && ix86_binary_operator_ok (XOR, V4SFmode, operands)",
    __builtin_constant_p 
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V4SFmode, operands))
    ? (int) 
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V4SFmode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V4SImode, operands) && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V4SImode, operands) && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V4SImode, operands) && (16 == 64)))
    : -1 },
#line 6836 "../.././gcc/config/i386/i386.md"
  { "(TARGET_80387 && X87_ENABLE_ARITH (DFmode))\n\
    || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 6836 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && X87_ENABLE_ARITH (DFmode))
    || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 6836 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && X87_ENABLE_ARITH (DFmode))
    || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
#line 8138 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (IOR, QImode, operands)",
    __builtin_constant_p 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, QImode, operands))
    ? (int) 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, QImode, operands))
    : -1 },
  { "(ix86_binary_operator_ok (MINUS, DImode, operands)\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 6164 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, DImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 6164 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, DImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 1456 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_SSE || TARGET_3DNOW)\n\
   && ix86_binary_operator_ok (PLUS, V8QImode, operands)",
    __builtin_constant_p 
#line 1456 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW)
   && ix86_binary_operator_ok (PLUS, V8QImode, operands))
    ? (int) 
#line 1456 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW)
   && ix86_binary_operator_ok (PLUS, V8QImode, operands))
    : -1 },
#line 1670 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && !symbolic_operand (operands[1], DImode)\n\
   && !x86_64_immediate_operand (operands[1], DImode) && 1",
    __builtin_constant_p 
#line 1670 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !symbolic_operand (operands[1], DImode)
   && !x86_64_immediate_operand (operands[1], DImode) && 1)
    ? (int) 
#line 1670 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !symbolic_operand (operands[1], DImode)
   && !x86_64_immediate_operand (operands[1], DImode) && 1)
    : -1 },
#line 2307 "../.././gcc/config/i386/i386.md"
  { "!(MEM_P (operands[0]) && MEM_P (operands[1])) && TARGET_AVX512F",
    __builtin_constant_p 
#line 2307 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1])) && TARGET_AVX512F)
    ? (int) 
#line 2307 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1])) && TARGET_AVX512F)
    : -1 },
#line 6087 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (MINUS, HImode, operands)",
    __builtin_constant_p 
#line 6087 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, HImode, operands))
    ? (int) 
#line 6087 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, HImode, operands))
    : -1 },
#line 2410 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && ix86_binary_operator_ok (IOR, V4SFmode, operands)",
    __builtin_constant_p 
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V4SFmode, operands))
    ? (int) 
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V4SFmode, operands))
    : -1 },
#line 16672 "../.././gcc/config/i386/i386.md"
  { "! TARGET_PARTIAL_REG_STALL && reload_completed\n\
   && ((GET_MODE (operands[0]) == HImode\n\
	&& ((optimize_function_for_speed_p (cfun) && !TARGET_FAST_PREFIX)\n\
            /* ??? next two lines just !satisfies_constraint_K (...) */\n\
	    || !CONST_INT_P (operands[2])\n\
	    || satisfies_constraint_K (operands[2])))\n\
       || (GET_MODE (operands[0]) == QImode\n\
	   && (TARGET_PROMOTE_QImode || optimize_function_for_size_p (cfun))))",
    __builtin_constant_p 
#line 16672 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL && reload_completed
   && ((GET_MODE (operands[0]) == HImode
	&& ((optimize_function_for_speed_p (cfun) && !TARGET_FAST_PREFIX)
            /* ??? next two lines just !satisfies_constraint_K (...) */
	    || !CONST_INT_P (operands[2])
	    || satisfies_constraint_K (operands[2])))
       || (GET_MODE (operands[0]) == QImode
	   && (TARGET_PROMOTE_QImode || optimize_function_for_size_p (cfun)))))
    ? (int) 
#line 16672 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL && reload_completed
   && ((GET_MODE (operands[0]) == HImode
	&& ((optimize_function_for_speed_p (cfun) && !TARGET_FAST_PREFIX)
            /* ??? next two lines just !satisfies_constraint_K (...) */
	    || !CONST_INT_P (operands[2])
	    || satisfies_constraint_K (operands[2])))
       || (GET_MODE (operands[0]) == QImode
	   && (TARGET_PROMOTE_QImode || optimize_function_for_size_p (cfun)))))
    : -1 },
  { "(ix86_match_ccmode (insn, CCmode)\n\
   && ix86_binary_operator_ok (MINUS, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 6183 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 6183 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 1613 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && TARGET_CMOVE\n\
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 1613 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 1613 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_CMOVE
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    : -1 },
#line 6279 "../.././gcc/config/i386/i386.md"
  { "TARGET_ADX && ix86_binary_operator_ok (PLUS, SImode, operands)",
    __builtin_constant_p 
#line 6279 "../.././gcc/config/i386/i386.md"
(TARGET_ADX && ix86_binary_operator_ok (PLUS, SImode, operands))
    ? (int) 
#line 6279 "../.././gcc/config/i386/i386.md"
(TARGET_ADX && ix86_binary_operator_ok (PLUS, SImode, operands))
    : -1 },
#line 8835 "../.././gcc/config/i386/i386.md"
  { "ix86_unary_operator_ok (NOT, QImode, operands)",
    __builtin_constant_p 
#line 8835 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NOT, QImode, operands))
    ? (int) 
#line 8835 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NOT, QImode, operands))
    : -1 },
#line 14672 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14672 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14672 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
#line 7621 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (AND, SImode, operands)",
    __builtin_constant_p 
#line 7621 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (AND, SImode, operands))
    ? (int) 
#line 7621 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (AND, SImode, operands))
    : -1 },
#line 10488 "../.././gcc/config/i386/sse.md"
  { "TARGET_64BIT && TARGET_SSE",
    __builtin_constant_p 
#line 10488 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE)
    ? (int) 
#line 10488 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE)
    : -1 },
#line 8861 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_unary_operator_ok (NOT, QImode, operands)",
    __builtin_constant_p 
#line 8861 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, QImode, operands))
    ? (int) 
#line 8861 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, QImode, operands))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (AND, V8DFmode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V8DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V8DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8SImode, operands) && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8SImode, operands) && 1) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8SImode, operands) && 1) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V4DImode)\n\
       == GET_MODE_NUNITS (V8SImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V8SImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V8SImode)))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16HImode, operands) && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16HImode, operands) && 1) && 
#line 230 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16HImode, operands) && 1) && 
#line 230 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 4082 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSE2 && TARGET_SSE_MATH",
    __builtin_constant_p 
#line 4082 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH)
    ? (int) 
#line 4082 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH)
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V2DImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V2DImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V2DImode, operands))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V4DImode)\n\
       == GET_MODE_NUNITS (V4DImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V4DImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V4DImode)))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V4SFmode)\n\
       == GET_MODE_NUNITS (V8HImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V8HImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SFmode)
       == GET_MODE_NUNITS (V8HImode)))
    : -1 },
#line 13319 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (TARGET_USE_HIMODE_FIOP || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 13319 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (TARGET_USE_HIMODE_FIOP || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 13319 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (TARGET_USE_HIMODE_FIOP || optimize_function_for_size_p (cfun)))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V4DImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 3286 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
   && (standard_80387_constant_p (operands[1]) == 8\n\
       || standard_80387_constant_p (operands[1]) == 9)",
    __builtin_constant_p 
#line 3286 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (standard_80387_constant_p (operands[1]) == 8
       || standard_80387_constant_p (operands[1]) == 9))
    ? (int) 
#line 3286 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (standard_80387_constant_p (operands[1]) == 8
       || standard_80387_constant_p (operands[1]) == 9))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 9729 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (LSHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9729 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, SImode, operands))
    ? (int) 
#line 9729 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, SImode, operands))
    : -1 },
#line 4453 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || DImode != DImode))\n\
	&& TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 4453 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
	&& TARGET_SSE_MATH))
    ? (int) 
#line 4453 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
	&& TARGET_SSE_MATH))
    : -1 },
#line 659 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (register_operand (operands[0], V2DImode)\n\
       || register_operand (operands[1], V2DImode))",
    __builtin_constant_p 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V2DImode)
       || register_operand (operands[1], V2DImode)))
    ? (int) 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V2DImode)
       || register_operand (operands[1], V2DImode)))
    : -1 },
#line 5528 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCZmode)\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 5528 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCZmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 5528 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCZmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == DImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17316 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17316 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 15224 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH\n\
   && !flag_trapping_math",
    __builtin_constant_p 
#line 15224 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
   && !flag_trapping_math)
    ? (int) 
#line 15224 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
   && !flag_trapping_math)
    : -1 },
#line 4877 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC\n\
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)",
    __builtin_constant_p 
#line 4877 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun))
    ? (int) 
#line 4877 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V8HImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V8HImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V8HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 186 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 186 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 6197 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_match_ccmode (insn, CCmode)\n\
   && ix86_binary_operator_ok (MINUS, SImode, operands)",
    __builtin_constant_p 
#line 6197 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, SImode, operands))
    ? (int) 
#line 6197 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)\n\
   && ix86_binary_operator_ok (XOR, V8SImode, operands)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (XOR, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (XOR, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 4347 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && TARGET_SSE2 && TARGET_SSE_MATH",
    __builtin_constant_p 
#line 4347 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_SSE_MATH)
    ? (int) 
#line 4347 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_SSE_MATH)
    : -1 },
#line 13769 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && flag_unsafe_math_optimizations\n\
   && standard_80387_constant_p (operands[3]) == 2",
    __builtin_constant_p 
#line 13769 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations
   && standard_80387_constant_p (operands[3]) == 2)
    ? (int) 
#line 13769 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations
   && standard_80387_constant_p (operands[3]) == 2)
    : -1 },
#line 7446 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
    && QI_REG_P (operands[2])\n\
    && GET_MODE (operands[2]) != QImode\n\
    && ((ix86_match_ccmode (insn, CCZmode)\n\
    	 && !(INTVAL (operands[3]) & ~(255 << 8)))\n\
	|| (ix86_match_ccmode (insn, CCNOmode)\n\
	    && !(INTVAL (operands[3]) & ~(127 << 8))))",
    __builtin_constant_p 
#line 7446 "../.././gcc/config/i386/i386.md"
(reload_completed
    && QI_REG_P (operands[2])
    && GET_MODE (operands[2]) != QImode
    && ((ix86_match_ccmode (insn, CCZmode)
    	 && !(INTVAL (operands[3]) & ~(255 << 8)))
	|| (ix86_match_ccmode (insn, CCNOmode)
	    && !(INTVAL (operands[3]) & ~(127 << 8)))))
    ? (int) 
#line 7446 "../.././gcc/config/i386/i386.md"
(reload_completed
    && QI_REG_P (operands[2])
    && GET_MODE (operands[2]) != QImode
    && ((ix86_match_ccmode (insn, CCZmode)
    	 && !(INTVAL (operands[3]) & ~(255 << 8)))
	|| (ix86_match_ccmode (insn, CCNOmode)
	    && !(INTVAL (operands[3]) & ~(127 << 8)))))
    : -1 },
#line 8714 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (reload_completed\n\
       || !(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))",
    __builtin_constant_p 
#line 8714 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (reload_completed
       || !(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)))
    ? (int) 
#line 8714 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (reload_completed
       || !(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)\n\
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)\n\
   && (16 == 64) && 1) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)
   && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)
   && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(SSE_FLOAT_MODE_P (SFmode)\n\
   && (!TARGET_FISTTP || TARGET_SSE_MATH)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 4396 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode)
   && (!TARGET_FISTTP || TARGET_SSE_MATH)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 4396 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode)
   && (!TARGET_FISTTP || TARGET_SSE_MATH)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 3555 "../.././gcc/config/i386/i386.md"
  { "!(TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun))",
    __builtin_constant_p 
#line 3555 "../.././gcc/config/i386/i386.md"
(!(TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)))
    ? (int) 
#line 3555 "../.././gcc/config/i386/i386.md"
(!(TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)))
    : -1 },
#line 13728 "../.././gcc/config/i386/i386.md"
  { "find_regno_note (insn, REG_UNUSED, REGNO (operands[0]))\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 13728 "../.././gcc/config/i386/i386.md"
(find_regno_note (insn, REG_UNUSED, REGNO (operands[0]))
   && can_create_pseudo_p ())
    ? (int) 
#line 13728 "../.././gcc/config/i386/i386.md"
(find_regno_note (insn, REG_UNUSED, REGNO (operands[0]))
   && can_create_pseudo_p ())
    : -1 },
#line 15145 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2",
    __builtin_constant_p 
#line 15145 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)
    ? (int) 
#line 15145 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)
    : -1 },
  { "(ix86_binary_operator_ok (XOR, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8062 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (XOR, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8062 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (XOR, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8797 "../.././gcc/config/i386/i386.md"
  { "((SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
    || (TARGET_SSE && (DFmode == TFmode)))\n\
   && reload_completed",
    __builtin_constant_p 
#line 8797 "../.././gcc/config/i386/i386.md"
(((SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
    || (TARGET_SSE && (DFmode == TFmode)))
   && reload_completed)
    ? (int) 
#line 8797 "../.././gcc/config/i386/i386.md"
(((SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
    || (TARGET_SSE && (DFmode == TFmode)))
   && reload_completed)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)\n\
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 1233 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCmode)",
    __builtin_constant_p 
#line 1233 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode))
    ? (int) 
#line 1233 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode))
    : -1 },
  { "(TARGET_AVX) && ( reload_completed && (V8SFmode != V4DFmode || !TARGET_AVX2))",
    __builtin_constant_p (
#line 14272 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 14274 "../.././gcc/config/i386/sse.md"
( reload_completed && (V8SFmode != V4DFmode || !TARGET_AVX2)))
    ? (int) (
#line 14272 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 14274 "../.././gcc/config/i386/sse.md"
( reload_completed && (V8SFmode != V4DFmode || !TARGET_AVX2)))
    : -1 },
#line 6215 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (MINUS, QImode, operands)",
    __builtin_constant_p 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, QImode, operands))
    ? (int) 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)\n\
   && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)
   && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)
   && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    : -1 },
#line 11431 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSSE3 && 1",
    __builtin_constant_p 
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && 1)
    ? (int) 
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && 1)
    : -1 },
#line 15224 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH\n\
   && !flag_trapping_math",
    __builtin_constant_p 
#line 15224 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
   && !flag_trapping_math)
    ? (int) 
#line 15224 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
   && !flag_trapping_math)
    : -1 },
  { "(TARGET_FMA4) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 3318 "../.././gcc/config/i386/sse.md"
(TARGET_FMA4) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 3318 "../.././gcc/config/i386/sse.md"
(TARGET_FMA4) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 9360 "../.././gcc/config/i386/i386.md"
  { "GET_MODE (operands[0]) == GET_MODE (operands[1])\n\
   && reload_completed\n\
   && true_regnum (operands[0]) != true_regnum (operands[1])",
    __builtin_constant_p 
#line 9360 "../.././gcc/config/i386/i386.md"
(GET_MODE (operands[0]) == GET_MODE (operands[1])
   && reload_completed
   && true_regnum (operands[0]) != true_regnum (operands[1]))
    ? (int) 
#line 9360 "../.././gcc/config/i386/i386.md"
(GET_MODE (operands[0]) == GET_MODE (operands[1])
   && reload_completed
   && true_regnum (operands[0]) != true_regnum (operands[1]))
    : -1 },
#line 4327 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && !(SSE_FLOAT_MODE_P (DFmode) && (!TARGET_FISTTP || TARGET_SSE_MATH))",
    __builtin_constant_p 
#line 4327 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !(SSE_FLOAT_MODE_P (DFmode) && (!TARGET_FISTTP || TARGET_SSE_MATH)))
    ? (int) 
#line 4327 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !(SSE_FLOAT_MODE_P (DFmode) && (!TARGET_FISTTP || TARGET_SSE_MATH)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (16 == 64)))
    : -1 },
#line 16768 "../.././gcc/config/i386/i386.md"
  { "! TARGET_PARTIAL_REG_STALL && reload_completed\n\
   && (GET_MODE (operands[0]) == HImode\n\
       || (GET_MODE (operands[0]) == QImode\n\
	   && (TARGET_PROMOTE_QImode\n\
	       || optimize_insn_for_size_p ())))",
    __builtin_constant_p 
#line 16768 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL && reload_completed
   && (GET_MODE (operands[0]) == HImode
       || (GET_MODE (operands[0]) == QImode
	   && (TARGET_PROMOTE_QImode
	       || optimize_insn_for_size_p ()))))
    ? (int) 
#line 16768 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL && reload_completed
   && (GET_MODE (operands[0]) == HImode
       || (GET_MODE (operands[0]) == QImode
	   && (TARGET_PROMOTE_QImode
	       || optimize_insn_for_size_p ()))))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)\n\
   && (32 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)
   && (32 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)
   && (32 == 64) && 1))
    : -1 },
  { "(TARGET_64BIT\n\
   && !(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15672 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15672 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "(TARGET_AVX) && ( reload_completed && (V4DFmode != V4DFmode || !TARGET_AVX2))",
    __builtin_constant_p (
#line 14272 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 14274 "../.././gcc/config/i386/sse.md"
( reload_completed && (V4DFmode != V4DFmode || !TARGET_AVX2)))
    ? (int) (
#line 14272 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 14274 "../.././gcc/config/i386/sse.md"
( reload_completed && (V4DFmode != V4DFmode || !TARGET_AVX2)))
    : -1 },
#line 7262 "../.././gcc/config/i386/i386.md"
  { "!(MEM_P (operands[0]) && MEM_P (operands[1]))\n\
    && ix86_match_ccmode (insn,\n\
 			 CONST_INT_P (operands[1])\n\
 			 && INTVAL (operands[1]) >= 0 ? CCNOmode : CCZmode)",
    __builtin_constant_p 
#line 7262 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1]))
    && ix86_match_ccmode (insn,
 			 CONST_INT_P (operands[1])
 			 && INTVAL (operands[1]) >= 0 ? CCNOmode : CCZmode))
    ? (int) 
#line 7262 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1]))
    && ix86_match_ccmode (insn,
 			 CONST_INT_P (operands[1])
 			 && INTVAL (operands[1]) >= 0 ? CCNOmode : CCZmode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (32 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (32 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
#line 4927 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT\n\
   && SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH",
    __builtin_constant_p 
#line 4927 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
    ? (int) 
#line 4927 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
    : -1 },
#line 1977 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE3",
    __builtin_constant_p 
#line 1977 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3)
    ? (int) 
#line 1977 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3)
    : -1 },
#line 1636 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && reload_completed\n\
   && !SSE_REG_P (operands[1])",
    __builtin_constant_p 
#line 1636 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && reload_completed
   && !SSE_REG_P (operands[1]))
    ? (int) 
#line 1636 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && reload_completed
   && !SSE_REG_P (operands[1]))
    : -1 },
  { "(TARGET_64BIT\n\
   && !(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15672 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15672 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
#line 15042 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && flag_unsafe_math_optimizations",
    __builtin_constant_p 
#line 15042 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations)
    ? (int) 
#line 15042 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations)
    : -1 },
#line 7695 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V8HImode, operands)",
    __builtin_constant_p 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V8HImode, operands))
    ? (int) 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V8HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4DImode, operands) && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4DImode, operands) && (32 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4DImode, operands) && (32 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16HImode, operands) && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16HImode, operands) && 1) && 
#line 230 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16HImode, operands) && 1) && 
#line 230 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 2113 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && GET_RTX_CLASS (GET_CODE (operands[3])) == RTX_COMM_COMPARE",
    __builtin_constant_p 
#line 2113 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && GET_RTX_CLASS (GET_CODE (operands[3])) == RTX_COMM_COMPARE)
    ? (int) 
#line 2113 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && GET_RTX_CLASS (GET_CODE (operands[3])) == RTX_COMM_COMPARE)
    : -1 },
  { "(TARGET_AVX512F) && (1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3005 "../.././gcc/config/i386/sse.md"
(1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3005 "../.././gcc/config/i386/sse.md"
(1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
#line 1807 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE3\n\
   && INTVAL (operands[3]) != INTVAL (operands[4])\n\
   && INTVAL (operands[5]) != INTVAL (operands[6])",
    __builtin_constant_p 
#line 1807 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3
   && INTVAL (operands[3]) != INTVAL (operands[4])
   && INTVAL (operands[5]) != INTVAL (operands[6]))
    ? (int) 
#line 1807 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3
   && INTVAL (operands[3]) != INTVAL (operands[4])
   && INTVAL (operands[5]) != INTVAL (operands[6]))
    : -1 },
#line 13923 "../.././gcc/config/i386/sse.md"
  { "TARGET_PCLMUL",
    __builtin_constant_p 
#line 13923 "../.././gcc/config/i386/sse.md"
(TARGET_PCLMUL)
    ? (int) 
#line 13923 "../.././gcc/config/i386/sse.md"
(TARGET_PCLMUL)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "((optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& (TARGET_SHIFT1\n\
	    || TARGET_DOUBLE_WITH_ADD)))\n\
   && ix86_match_ccmode (insn, CCGOCmode)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9505 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || TARGET_DOUBLE_WITH_ADD)))
   && ix86_match_ccmode (insn, CCGOCmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9505 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || TARGET_DOUBLE_WITH_ADD)))
   && ix86_match_ccmode (insn, CCGOCmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 10167 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (ROTATERT, SImode, operands)",
    __builtin_constant_p 
#line 10167 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (ROTATERT, SImode, operands))
    ? (int) 
#line 10167 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (ROTATERT, SImode, operands))
    : -1 },
#line 666 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_MMX || (TARGET_SSE2 && V8QImode == V1DImode))\n\
   && ix86_binary_operator_ok (PLUS, V8QImode, operands)",
    __builtin_constant_p 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V8QImode == V1DImode))
   && ix86_binary_operator_ok (PLUS, V8QImode, operands))
    ? (int) 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V8QImode == V1DImode))
   && ix86_binary_operator_ok (PLUS, V8QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 186 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 186 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)\n\
   && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)
   && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)
   && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_ADX && ix86_binary_operator_ok (PLUS, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 6279 "../.././gcc/config/i386/i386.md"
(TARGET_ADX && ix86_binary_operator_ok (PLUS, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 6279 "../.././gcc/config/i386/i386.md"
(TARGET_ADX && ix86_binary_operator_ok (PLUS, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8DImode, operands) && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8DImode, operands) && (64 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8DImode, operands) && (64 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 7672 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4SImode, operands) && 1",
    __builtin_constant_p 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4SImode, operands) && 1)
    ? (int) 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4SImode, operands) && 1)
    : -1 },
#line 9410 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& (TARGET_SHIFT1\n\
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFT, HImode, operands)",
    __builtin_constant_p 
#line 9410 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, HImode, operands))
    ? (int) 
#line 9410 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || (TARGET_DOUBLE_WITH_ADD && REG_P (operands[0])))))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFT, HImode, operands))
    : -1 },
#line 1306 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && 1 && 1",
    __builtin_constant_p 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && 1 && 1)
    ? (int) 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && 1 && 1)
    : -1 },
#line 7672 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16QImode, operands) && 1",
    __builtin_constant_p 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16QImode, operands) && 1)
    ? (int) 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16QImode, operands) && 1)
    : -1 },
#line 8329 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
    && QI_REG_P (operands[0])\n\
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
    && !(INTVAL (operands[2]) & ~(255 << 8))\n\
    && GET_MODE (operands[0]) != QImode",
    __builtin_constant_p 
#line 8329 "../.././gcc/config/i386/i386.md"
(reload_completed
    && QI_REG_P (operands[0])
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    && !(INTVAL (operands[2]) & ~(255 << 8))
    && GET_MODE (operands[0]) != QImode)
    ? (int) 
#line 8329 "../.././gcc/config/i386/i386.md"
(reload_completed
    && QI_REG_P (operands[0])
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    && !(INTVAL (operands[2]) & ~(255 << 8))
    && GET_MODE (operands[0]) != QImode)
    : -1 },
#line 659 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (register_operand (operands[0], V4SImode)\n\
       || register_operand (operands[1], V4SImode))",
    __builtin_constant_p 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V4SImode)
       || register_operand (operands[1], V4SImode)))
    ? (int) 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V4SImode)
       || register_operand (operands[1], V4SImode)))
    : -1 },
#line 12028 "../.././gcc/config/i386/i386.md"
  { "TARGET_LZCNT",
    __builtin_constant_p 
#line 12028 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT)
    ? (int) 
#line 12028 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT)
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 6563 "../.././gcc/config/i386/i386.md"
  { "!(MEM_P (operands[1]) && MEM_P (operands[2]))\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])",
    __builtin_constant_p 
#line 6563 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2]))
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    ? (int) 
#line 6563 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2]))
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    : -1 },
#line 96 "../.././gcc/config/i386/sync.md"
  { "!(TARGET_64BIT || TARGET_SSE2)",
    __builtin_constant_p 
#line 96 "../.././gcc/config/i386/sync.md"
(!(TARGET_64BIT || TARGET_SSE2))
    ? (int) 
#line 96 "../.././gcc/config/i386/sync.md"
(!(TARGET_64BIT || TARGET_SSE2))
    : -1 },
  { "(TARGET_FMA) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 3236 "../.././gcc/config/i386/sse.md"
(TARGET_FMA) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 3236 "../.././gcc/config/i386/sse.md"
(TARGET_FMA) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 7367 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ((TARGET_64BIT && GET_MODE (operands[0]) == DImode)\n\
       || GET_MODE (operands[0]) == SImode\n\
       || GET_MODE (operands[0]) == HImode\n\
       || GET_MODE (operands[0]) == QImode)\n\
   /* Ensure that resulting mask is zero or sign extended operand.  */\n\
   && INTVAL (operands[2]) >= 0\n\
   && ((INTVAL (operands[1]) > 0\n\
	&& INTVAL (operands[1]) + INTVAL (operands[2]) <= 32)\n\
       || (SImode == DImode\n\
	   && INTVAL (operands[1]) > 32\n\
	   && INTVAL (operands[1]) + INTVAL (operands[2]) == 64))",
    __builtin_constant_p 
#line 7367 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ((TARGET_64BIT && GET_MODE (operands[0]) == DImode)
       || GET_MODE (operands[0]) == SImode
       || GET_MODE (operands[0]) == HImode
       || GET_MODE (operands[0]) == QImode)
   /* Ensure that resulting mask is zero or sign extended operand.  */
   && INTVAL (operands[2]) >= 0
   && ((INTVAL (operands[1]) > 0
	&& INTVAL (operands[1]) + INTVAL (operands[2]) <= 32)
       || (SImode == DImode
	   && INTVAL (operands[1]) > 32
	   && INTVAL (operands[1]) + INTVAL (operands[2]) == 64)))
    ? (int) 
#line 7367 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ((TARGET_64BIT && GET_MODE (operands[0]) == DImode)
       || GET_MODE (operands[0]) == SImode
       || GET_MODE (operands[0]) == HImode
       || GET_MODE (operands[0]) == QImode)
   /* Ensure that resulting mask is zero or sign extended operand.  */
   && INTVAL (operands[2]) >= 0
   && ((INTVAL (operands[1]) > 0
	&& INTVAL (operands[1]) + INTVAL (operands[2]) <= 32)
       || (SImode == DImode
	   && INTVAL (operands[1]) > 32
	   && INTVAL (operands[1]) + INTVAL (operands[2]) == 64)))
    : -1 },
#line 16615 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && flag_pic",
    __builtin_constant_p 
#line 16615 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && flag_pic)
    ? (int) 
#line 16615 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && flag_pic)
    : -1 },
#line 4235 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && reload_completed",
    __builtin_constant_p 
#line 4235 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && reload_completed)
    ? (int) 
#line 4235 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && reload_completed)
    : -1 },
  { "(ix86_binary_operator_ok (LSHIFTRT, DImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))\n\
      == GET_MODE_BITSIZE (DImode)-1) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9563 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9563 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 2625 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && ix86_binary_operator_ok (AND, TFmode, operands)",
    __builtin_constant_p 
#line 2625 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && ix86_binary_operator_ok (AND, TFmode, operands))
    ? (int) 
#line 2625 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && ix86_binary_operator_ok (AND, TFmode, operands))
    : -1 },
#line 9826 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ASHIFTRT, HImode, operands)",
    __builtin_constant_p 
#line 9826 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, HImode, operands))
    ? (int) 
#line 9826 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, HImode, operands))
    : -1 },
  { "((TARGET_BMI || TARGET_GENERIC)\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 11939 "../.././gcc/config/i386/i386.md"
((TARGET_BMI || TARGET_GENERIC)
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 11939 "../.././gcc/config/i386/i386.md"
((TARGET_BMI || TARGET_GENERIC)
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX) && ( reload_completed)",
    __builtin_constant_p (
#line 14797 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 14799 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) (
#line 14797 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 14799 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
#line 1565 "../.././gcc/config/i386/i386.md"
  { "TARGET_MIX_SSE_I387\n\
   && SSE_FLOAT_MODE_P (DFmode)",
    __builtin_constant_p 
#line 1565 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387
   && SSE_FLOAT_MODE_P (DFmode))
    ? (int) 
#line 1565 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387
   && SSE_FLOAT_MODE_P (DFmode))
    : -1 },
#line 6637 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && TARGET_BMI2\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 6637 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_BMI2
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 6637 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_BMI2
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V16QImode)\n\
       == GET_MODE_NUNITS (V8HImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V8HImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V8HImode)))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V16QImode)\n\
       == GET_MODE_NUNITS (V16QImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V16QImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V16QImode)))
    : -1 },
  { "(TARGET_SSE) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 9072 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 9072 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(reload_completed) && (TARGET_HIMODE_MATH)",
    __builtin_constant_p (
#line 7109 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 914 "../.././gcc/config/i386/i386.md"
(TARGET_HIMODE_MATH))
    ? (int) (
#line 7109 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 914 "../.././gcc/config/i386/i386.md"
(TARGET_HIMODE_MATH))
    : -1 },
  { "(TARGET_LZCNT\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && ( reload_completed)",
    __builtin_constant_p (
#line 12035 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 12038 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 12035 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 12038 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_FROM_VEC) && ( reload_completed)",
    __builtin_constant_p (
#line 10405 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_FROM_VEC) && 
#line 10407 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) (
#line 10405 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_FROM_VEC) && 
#line 10407 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (32 == 64)\n\
   && avx_vpermilp_parallel (operands[2], V8SFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64)
   && avx_vpermilp_parallel (operands[2], V8SFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64)
   && avx_vpermilp_parallel (operands[2], V8SFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_SSE2 && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 13635 "../.././gcc/config/i386/sse.md"
  { "TARGET_XOP && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13635 "../.././gcc/config/i386/sse.md"
(TARGET_XOP && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13635 "../.././gcc/config/i386/sse.md"
(TARGET_XOP && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 5859 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (PLUS, SImode, operands)\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])",
    __builtin_constant_p 
#line 5859 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, SImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    ? (int) 
#line 5859 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, SImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    : -1 },
#line 9945 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)",
    __builtin_constant_p 
#line 9945 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode))
    ? (int) 
#line 9945 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode))
    : -1 },
#line 16436 "../.././gcc/config/i386/i386.md"
  { "REGNO (operands[0]) != REGNO (operands[1])",
    __builtin_constant_p 
#line 16436 "../.././gcc/config/i386/i386.md"
(REGNO (operands[0]) != REGNO (operands[1]))
    ? (int) 
#line 16436 "../.././gcc/config/i386/i386.md"
(REGNO (operands[0]) != REGNO (operands[1]))
    : -1 },
  { "(TARGET_BSWAP) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 12456 "../.././gcc/config/i386/i386.md"
(TARGET_BSWAP) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 12456 "../.././gcc/config/i386/i386.md"
(TARGET_BSWAP) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8087 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (XOR, QImode, operands)",
    __builtin_constant_p 
#line 8087 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (XOR, QImode, operands))
    ? (int) 
#line 8087 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (XOR, QImode, operands))
    : -1 },
#line 4672 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
       || TARGET_MIX_SSE_I387)",
    __builtin_constant_p 
#line 4672 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387))
    ? (int) 
#line 4672 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE)",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3533 "../.././gcc/config/i386/sse.md"
(TARGET_SSE))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3533 "../.././gcc/config/i386/sse.md"
(TARGET_SSE))
    : -1 },
  { "(TARGET_AVX512F) && ((1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 2776 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 2776 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)\n\
   && ix86_binary_operator_ok (IOR, V16HImode, operands)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (IOR, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (IOR, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_USE_FANCY_MATH_387\n\
   && can_create_pseudo_p ()) && ( 1)",
    __builtin_constant_p (
#line 15257 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && can_create_pseudo_p ()) && 
#line 15260 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 15257 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && can_create_pseudo_p ()) && 
#line 15260 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "(TARGET_64BIT && TARGET_FSGSBASE) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 18133 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_FSGSBASE) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 18133 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_FSGSBASE) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8676 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && ix86_binary_operator_ok (EQ, V8DImode, operands)",
    __builtin_constant_p 
#line 8676 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (EQ, V8DImode, operands))
    ? (int) 
#line 8676 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (EQ, V8DImode, operands))
    : -1 },
#line 4945 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT\n\
   && TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC",
    __builtin_constant_p 
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
    ? (int) 
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
    : -1 },
  { "(ix86_binary_operator_ok (ROTATE, DImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))\n\
      == GET_MODE_BITSIZE (DImode)-1) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10018 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10018 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(!(MEM_P (operands[1]) && MEM_P (operands[2]))\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 6563 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2]))
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3])) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 6563 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2]))
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3])) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 6656 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE3 && INTVAL (operands[2]) + 2 == INTVAL (operands[3])",
    __builtin_constant_p 
#line 6656 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3 && INTVAL (operands[2]) + 2 == INTVAL (operands[3]))
    ? (int) 
#line 6656 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3 && INTVAL (operands[2]) + 2 == INTVAL (operands[3]))
    : -1 },
#line 1003 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (AND, V2SImode, operands)",
    __builtin_constant_p 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (AND, V2SImode, operands))
    ? (int) 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (AND, V2SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)\n\
   && (32 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)
   && (32 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)
   && (32 == 64) && 1))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DImode)\n\
       == GET_MODE_NUNITS (V64QImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V64QImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V64QImode)))
    : -1 },
#line 11095 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && !TARGET_CMOVE\n\
   && (TARGET_USE_SIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 11095 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !TARGET_CMOVE
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 11095 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !TARGET_CMOVE
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)
   && 1 && 1)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "((((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)\n\
      && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun)))\n\
    || GET_MODE (operands[0]) == SImode\n\
    || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[1])\n\
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[2])) - 1 < 3\n\
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[3])\n\
       < ((unsigned HOST_WIDE_INT) 1 << INTVAL (operands[2])))) && ( reload_completed)",
    __builtin_constant_p (
#line 5978 "../.././gcc/config/i386/i386.md"
((((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
      && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun)))
    || GET_MODE (operands[0]) == SImode
    || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[2])) - 1 < 3
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[3])
       < ((unsigned HOST_WIDE_INT) 1 << INTVAL (operands[2])))) && 
#line 5987 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 5978 "../.././gcc/config/i386/i386.md"
((((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
      && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun)))
    || GET_MODE (operands[0]) == SImode
    || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[2])) - 1 < 3
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[3])
       < ((unsigned HOST_WIDE_INT) 1 << INTVAL (operands[2])))) && 
#line 5987 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V4DFmode)\n\
       == GET_MODE_NUNITS (V4DImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V4DImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V4DImode)))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V2TImode)\n\
       || register_operand (operands[1], V2TImode))) && (TARGET_AVX)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V2TImode)
       || register_operand (operands[1], V2TImode))) && 
#line 149 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V2TImode)
       || register_operand (operands[1], V2TImode))) && 
#line 149 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 7695 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V8HImode, operands)",
    __builtin_constant_p 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V8HImode, operands))
    ? (int) 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V8HImode, operands))
    : -1 },
#line 1295 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 1295 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 1295 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 17220 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && !TARGET_OPT_AGU\n\
   && REGNO (operands[0]) == REGNO (operands[2])\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 17220 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_OPT_AGU
   && REGNO (operands[0]) == REGNO (operands[2])
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 17220 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_OPT_AGU
   && REGNO (operands[0]) == REGNO (operands[2])
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
  { "((optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFTRT, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 7871 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (AND, HImode, operands)",
    __builtin_constant_p 
#line 7871 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (AND, HImode, operands))
    ? (int) 
#line 7871 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (AND, HImode, operands))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V32QImode)\n\
       || register_operand (operands[1], V32QImode))) && (TARGET_AVX)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V32QImode)
       || register_operand (operands[1], V32QImode))) && 
#line 145 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V32QImode)
       || register_operand (operands[1], V32QImode))) && 
#line 145 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 8236 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (IOR, SImode, operands)",
    __builtin_constant_p 
#line 8236 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, SImode, operands))
    ? (int) 
#line 8236 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
  { "(!(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15938 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15938 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(TARGET_AVX512F) && ((1 && (SFmode == V16SFmode || SFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (SFmode == V16SFmode || SFmode == V8DFmode)) && 
#line 2771 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (SFmode == V16SFmode || SFmode == V8DFmode)) && 
#line 2771 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
#line 14837 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH\n\
       && HImode != HImode \n\
       && ((HImode != DImode) || TARGET_64BIT)\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH
       && HImode != HImode 
       && ((HImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH
       && HImode != HImode 
       && ((HImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
#line 14672 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14672 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14672 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
#line 8676 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && ix86_binary_operator_ok (EQ, V16SImode, operands)",
    __builtin_constant_p 
#line 8676 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (EQ, V16SImode, operands))
    ? (int) 
#line 8676 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (EQ, V16SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)\n\
   && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)
   && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)
   && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V8HImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V8HImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V8HImode, operands))
    : -1 },
#line 3068 "../.././gcc/config/i386/sse.md"
  { "TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F",
    __builtin_constant_p 
#line 3068 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F)
    ? (int) 
#line 3068 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F)
    : -1 },
  { "(TARGET_64BIT\n\
   && !(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15480 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15480 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 5978 "../.././gcc/config/i386/i386.md"
  { "(((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)\n\
      && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun)))\n\
    || GET_MODE (operands[0]) == SImode\n\
    || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[1])\n\
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[2])) - 1 < 3\n\
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[3])\n\
       < ((unsigned HOST_WIDE_INT) 1 << INTVAL (operands[2])))",
    __builtin_constant_p 
#line 5978 "../.././gcc/config/i386/i386.md"
((((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
      && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun)))
    || GET_MODE (operands[0]) == SImode
    || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[2])) - 1 < 3
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[3])
       < ((unsigned HOST_WIDE_INT) 1 << INTVAL (operands[2]))))
    ? (int) 
#line 5978 "../.././gcc/config/i386/i386.md"
((((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
      && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun)))
    || GET_MODE (operands[0]) == SImode
    || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[2])) - 1 < 3
   && ((unsigned HOST_WIDE_INT) INTVAL (operands[3])
       < ((unsigned HOST_WIDE_INT) 1 << INTVAL (operands[2]))))
    : -1 },
  { "(ix86_binary_operator_ok (LSHIFTRT, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9729 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9729 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V8SImode)\n\
       || register_operand (operands[1], V8SImode))) && (TARGET_AVX)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8SImode)
       || register_operand (operands[1], V8SImode))) && 
#line 147 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8SImode)
       || register_operand (operands[1], V8SImode))) && 
#line 147 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 10272 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
  && (TARGET_USE_XCHGB || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 10272 "../.././gcc/config/i386/i386.md"
(reload_completed
  && (TARGET_USE_XCHGB || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 10272 "../.././gcc/config/i386/i386.md"
(reload_completed
  && (TARGET_USE_XCHGB || optimize_function_for_size_p (cfun)))
    : -1 },
  { "(SSE_FLOAT_MODE_P (DFmode)\n\
   && (!TARGET_FISTTP || TARGET_SSE_MATH)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 4396 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode)
   && (!TARGET_FISTTP || TARGET_SSE_MATH)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 4396 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode)
   && (!TARGET_FISTTP || TARGET_SSE_MATH)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && !TARGET_FISTTP\n\
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || SImode != DImode))\n\
   && can_create_pseudo_p ()) && ( 1)",
    __builtin_constant_p (
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
   && can_create_pseudo_p ()) && 
#line 4510 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
   && can_create_pseudo_p ()) && 
#line 4510 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4SImode, operands) && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4SImode, operands) && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4SImode, operands) && (16 == 64)))
    : -1 },
#line 1003 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (XOR, V8QImode, operands)",
    __builtin_constant_p 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (XOR, V8QImode, operands))
    ? (int) 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (XOR, V8QImode, operands))
    : -1 },
  { "((TARGET_USE_BT || optimize_function_for_size_p (cfun))\n\
   && (INTVAL (operands[3]) & 0x1f) == 0x1f) && ( 1)",
    __builtin_constant_p (
#line 10900 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & 0x1f) == 0x1f) && 
#line 10903 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 10900 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & 0x1f) == 0x1f) && 
#line 10903 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)\n\
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V8SImode)\n\
       == GET_MODE_NUNITS (V32QImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V32QImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V32QImode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)\n\
   && (32 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)
   && (32 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8SImode, operands)
   && (32 == 64) && 1))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V16SImode)\n\
       || register_operand (operands[1], V16SImode))) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V16SImode)
       || register_operand (operands[1], V16SImode))) && 
#line 147 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V16SImode)
       || register_operand (operands[1], V16SImode))) && 
#line 147 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 8564 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && !(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 8564 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 8564 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))
    : -1 },
#line 5381 "../.././gcc/config/i386/i386.md"
  { "reload_completed && ix86_lea_for_add_ok (insn, operands)",
    __builtin_constant_p 
#line 5381 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_lea_for_add_ok (insn, operands))
    ? (int) 
#line 5381 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_lea_for_add_ok (insn, operands))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)\n\
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)\n\
   && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)
   && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)
   && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 4982 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && TARGET_SSE_MATH",
    __builtin_constant_p 
#line 4982 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_SSE_MATH)
    ? (int) 
#line 4982 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_SSE_MATH)
    : -1 },
#line 4467 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	&& (TARGET_64BIT || SImode != DImode))\n\
	&& TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 4467 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	&& (TARGET_64BIT || SImode != DImode))
	&& TARGET_SSE_MATH))
    ? (int) 
#line 4467 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	&& (TARGET_64BIT || SImode != DImode))
	&& TARGET_SSE_MATH))
    : -1 },
#line 7847 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (AND, QImode, operands)\n\
   && ix86_match_ccmode (insn,\n\
			 CONST_INT_P (operands[2])\n\
			 && INTVAL (operands[2]) >= 0 ? CCNOmode : CCZmode)",
    __builtin_constant_p 
#line 7847 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (AND, QImode, operands)
   && ix86_match_ccmode (insn,
			 CONST_INT_P (operands[2])
			 && INTVAL (operands[2]) >= 0 ? CCNOmode : CCZmode))
    ? (int) 
#line 7847 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (AND, QImode, operands)
   && ix86_match_ccmode (insn,
			 CONST_INT_P (operands[2])
			 && INTVAL (operands[2]) >= 0 ? CCNOmode : CCZmode))
    : -1 },
#line 4917 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC\n\
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)\n\
   && reload_completed",
    __builtin_constant_p 
#line 4917 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)
   && reload_completed)
    ? (int) 
#line 4917 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)
   && reload_completed)
    : -1 },
#line 6472 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_vec_interleave_v2df_operator_ok (operands, 1)",
    __builtin_constant_p 
#line 6472 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_vec_interleave_v2df_operator_ok (operands, 1))
    ? (int) 
#line 6472 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_vec_interleave_v2df_operator_ok (operands, 1))
    : -1 },
#line 8654 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (EQ, V4DImode, operands)",
    __builtin_constant_p 
#line 8654 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (EQ, V4DImode, operands))
    ? (int) 
#line 8654 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (EQ, V4DImode, operands))
    : -1 },
  { "(ix86_match_ccmode (insn, CCNOmode)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8874 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8874 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 13058 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH\n\
   && COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13058 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13058 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(ix86_unary_operator_ok (NEG, DImode, operands)) && (!TARGET_64BIT)",
    __builtin_constant_p (
#line 8427 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, DImode, operands)) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    ? (int) (
#line 8427 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, DImode, operands)) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    : -1 },
#line 4216 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && flag_unsafe_math_optimizations",
    __builtin_constant_p 
#line 4216 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && flag_unsafe_math_optimizations)
    ? (int) 
#line 4216 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && flag_unsafe_math_optimizations)
    : -1 },
  { "(TARGET_MACHO && !TARGET_64BIT && flag_pic) && ( reload_completed)",
    __builtin_constant_p (
#line 16636 "../.././gcc/config/i386/i386.md"
(TARGET_MACHO && !TARGET_64BIT && flag_pic) && 
#line 16638 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 16636 "../.././gcc/config/i386/i386.md"
(TARGET_MACHO && !TARGET_64BIT && flag_pic) && 
#line 16638 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "((optimize && flag_peephole2) ? epilogue_completed : reload_completed) && (!TARGET_64BIT)",
    __builtin_constant_p (
#line 9579 "../.././gcc/config/i386/i386.md"
((optimize && flag_peephole2) ? epilogue_completed : reload_completed) && 
#line 918 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    ? (int) (
#line 9579 "../.././gcc/config/i386/i386.md"
((optimize && flag_peephole2) ? epilogue_completed : reload_completed) && 
#line 918 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    : -1 },
  { "(!(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15526 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15526 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
#line 6639 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE3 && reload_completed",
    __builtin_constant_p 
#line 6639 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3 && reload_completed)
    ? (int) 
#line 6639 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3 && reload_completed)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (64 == 64)\n\
   && avx_vpermilp_parallel (operands[2], V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64)
   && avx_vpermilp_parallel (operands[2], V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64)
   && avx_vpermilp_parallel (operands[2], V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_64BIT\n\
   && !(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15743 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15743 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 8703 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && !TARGET_XOP\n\
   && ix86_binary_operator_ok (EQ, V4SImode, operands)",
    __builtin_constant_p 
#line 8703 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP
   && ix86_binary_operator_ok (EQ, V4SImode, operands))
    ? (int) 
#line 8703 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP
   && ix86_binary_operator_ok (EQ, V4SImode, operands))
    : -1 },
#line 7243 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 7243 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 7243 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
  { "(!TARGET_X32) && (Pmode == DImode)",
    __builtin_constant_p (
#line 12869 "../.././gcc/config/i386/i386.md"
(!TARGET_X32) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 12869 "../.././gcc/config/i386/i386.md"
(!TARGET_X32) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(reload_completed && ix86_avoid_lea_for_add (insn, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 5370 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_avoid_lea_for_add (insn, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 5370 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_avoid_lea_for_add (insn, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_SSE4_1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 11611 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 272 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 11611 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 272 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 8236 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (XOR, SImode, operands)",
    __builtin_constant_p 
#line 8236 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, SImode, operands))
    ? (int) 
#line 8236 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, SImode, operands))
    : -1 },
#line 12885 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && TARGET_SUN_TLS",
    __builtin_constant_p 
#line 12885 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_SUN_TLS)
    ? (int) 
#line 12885 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_SUN_TLS)
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (IOR, V4DFmode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V4DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V4DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 5575 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_match_ccmode (insn, CCZmode)\n\
   && ix86_binary_operator_ok (PLUS, SImode, operands)",
    __builtin_constant_p 
#line 5575 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCZmode)
   && ix86_binary_operator_ok (PLUS, SImode, operands))
    ? (int) 
#line 5575 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCZmode)
   && ix86_binary_operator_ok (PLUS, SImode, operands))
    : -1 },
  { "(TARGET_AVX2) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15236 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15236 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
#line 11021 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && !TARGET_CMOVE",
    __builtin_constant_p 
#line 11021 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !TARGET_CMOVE)
    ? (int) 
#line 11021 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !TARGET_CMOVE)
    : -1 },
  { "(TARGET_SSE4_1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 11594 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 11594 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (TARGET_AVX)",
    __builtin_constant_p (
#line 3068 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 3068 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (64 == 64)\n\
   && ix86_binary_operator_ok (AND, V16SImode, operands)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (AND, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (AND, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)\n\
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    : -1 },
#line 7631 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (AND, HImode, operands)",
    __builtin_constant_p 
#line 7631 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (AND, HImode, operands))
    ? (int) 
#line 7631 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (AND, HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8DImode, operands) && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8DImode, operands) && (64 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8DImode, operands) && (64 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1 && 1) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1 && 1) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 15302 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && ix86_libc_has_function (function_c99_misc)\n\
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 15302 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && ix86_libc_has_function (function_c99_misc)
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 15302 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && ix86_libc_has_function (function_c99_misc)
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)
   && 1 && 1)
    : -1 },
#line 16282 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && reload_completed",
    __builtin_constant_p 
#line 16282 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && reload_completed)
    ? (int) 
#line 16282 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && reload_completed)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)\n\
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16SImode, operands)
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "((TARGET_SSE) && (TARGET_AVX512F)) && ( reload_completed)",
    __builtin_constant_p ((
#line 1260 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)) && 
#line 1262 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) ((
#line 1260 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)) && 
#line 1262 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
  { "(reload_completed\n\
   && true_regnum (operands[0]) != true_regnum (operands[1])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 7732 "../.././gcc/config/i386/i386.md"
(reload_completed
   && true_regnum (operands[0]) != true_regnum (operands[1])) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 7732 "../.././gcc/config/i386/i386.md"
(reload_completed
   && true_regnum (operands[0]) != true_regnum (operands[1])) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE\n\
   && (TARGET_USE_SIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))) && ( reload_completed)",
    __builtin_constant_p (
#line 1499 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun))) && 
#line 1503 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 1499 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && TARGET_SAHF && !TARGET_CMOVE
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun))) && 
#line 1503 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || HImode != DImode))\n\
	&& TARGET_SSE_MATH)\n\
   && can_create_pseudo_p ()) && ( 1)",
    __builtin_constant_p (
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ()) && 
#line 4432 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ()) && 
#line 4432 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
#line 13282 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && !COMMUTATIVE_ARITH_P (operands[3])",
    __builtin_constant_p 
#line 13282 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !COMMUTATIVE_ARITH_P (operands[3]))
    ? (int) 
#line 13282 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !COMMUTATIVE_ARITH_P (operands[3]))
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)\n\
   && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)
   && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8SFmode, operands)
   && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)\n\
   && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)
   && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)
   && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 465 "../.././gcc/config/i386/sync.md"
  { "TARGET_XADD",
    __builtin_constant_p 
#line 465 "../.././gcc/config/i386/sync.md"
(TARGET_XADD)
    ? (int) 
#line 465 "../.././gcc/config/i386/sync.md"
(TARGET_XADD)
    : -1 },
#line 16732 "../.././gcc/config/i386/i386.md"
  { "! TARGET_PARTIAL_REG_STALL && reload_completed\n\
   && ! TARGET_FAST_PREFIX\n\
   && optimize_insn_for_speed_p ()\n\
   /* Ensure that the operand will remain sign-extended immediate.  */\n\
   && ix86_match_ccmode (insn, INTVAL (operands[3]) >= 0 ? CCNOmode : CCZmode)",
    __builtin_constant_p 
#line 16732 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL && reload_completed
   && ! TARGET_FAST_PREFIX
   && optimize_insn_for_speed_p ()
   /* Ensure that the operand will remain sign-extended immediate.  */
   && ix86_match_ccmode (insn, INTVAL (operands[3]) >= 0 ? CCNOmode : CCZmode))
    ? (int) 
#line 16732 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL && reload_completed
   && ! TARGET_FAST_PREFIX
   && optimize_insn_for_speed_p ()
   /* Ensure that the operand will remain sign-extended immediate.  */
   && ix86_match_ccmode (insn, INTVAL (operands[3]) >= 0 ? CCNOmode : CCZmode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (XOR, V8HImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (XOR, V8HImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (XOR, V8HImode, operands)))
    : -1 },
#line 486 "../.././gcc/config/i386/sync.md"
  { "peep2_reg_dead_p (3, operands[0])\n\
   && (unsigned HOST_WIDE_INT) INTVAL (operands[2])\n\
      == -(unsigned HOST_WIDE_INT) INTVAL (operands[3])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])",
    __builtin_constant_p 
#line 486 "../.././gcc/config/i386/sync.md"
(peep2_reg_dead_p (3, operands[0])
   && (unsigned HOST_WIDE_INT) INTVAL (operands[2])
      == -(unsigned HOST_WIDE_INT) INTVAL (operands[3])
   && !reg_overlap_mentioned_p (operands[0], operands[1]))
    ? (int) 
#line 486 "../.././gcc/config/i386/sync.md"
(peep2_reg_dead_p (3, operands[0])
   && (unsigned HOST_WIDE_INT) INTVAL (operands[2])
      == -(unsigned HOST_WIDE_INT) INTVAL (operands[3])
   && !reg_overlap_mentioned_p (operands[0], operands[1]))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)\n\
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)
   && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 11873 "../.././gcc/config/i386/i386.md"
  { "!TARGET_CMOVE",
    __builtin_constant_p 
#line 11873 "../.././gcc/config/i386/i386.md"
(!TARGET_CMOVE)
    ? (int) 
#line 11873 "../.././gcc/config/i386/i386.md"
(!TARGET_CMOVE)
    : -1 },
#line 18133 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && TARGET_FSGSBASE",
    __builtin_constant_p 
#line 18133 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_FSGSBASE)
    ? (int) 
#line 18133 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_FSGSBASE)
    : -1 },
  { "(!TARGET_OPT_AGU\n\
   && peep2_regno_dead_p (0, FLAGS_REG)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 17198 "../.././gcc/config/i386/i386.md"
(!TARGET_OPT_AGU
   && peep2_regno_dead_p (0, FLAGS_REG)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 17198 "../.././gcc/config/i386/i386.md"
(!TARGET_OPT_AGU
   && peep2_regno_dead_p (0, FLAGS_REG)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8028 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
    && ANY_QI_REG_P (operands[0])\n\
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
    && !(~INTVAL (operands[2]) & ~255)\n\
    && !(INTVAL (operands[2]) & 128)\n\
    && GET_MODE (operands[0]) != QImode",
    __builtin_constant_p 
#line 8028 "../.././gcc/config/i386/i386.md"
(reload_completed
    && ANY_QI_REG_P (operands[0])
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    && !(~INTVAL (operands[2]) & ~255)
    && !(INTVAL (operands[2]) & 128)
    && GET_MODE (operands[0]) != QImode)
    ? (int) 
#line 8028 "../.././gcc/config/i386/i386.md"
(reload_completed
    && ANY_QI_REG_P (operands[0])
    && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    && !(~INTVAL (operands[2]) & ~255)
    && !(INTVAL (operands[2]) & 128)
    && GET_MODE (operands[0]) != QImode)
    : -1 },
#line 7307 "../.././gcc/config/i386/sse.md"
  { "!TARGET_SSE2 && TARGET_SSE\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 7307 "../.././gcc/config/i386/sse.md"
(!TARGET_SSE2 && TARGET_SSE
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 7307 "../.././gcc/config/i386/sse.md"
(!TARGET_SSE2 && TARGET_SSE
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V4DImode)\n\
       == GET_MODE_NUNITS (V32QImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V32QImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DImode)
       == GET_MODE_NUNITS (V32QImode)))
    : -1 },
#line 8528 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (SMIN, V16QImode, operands)",
    __builtin_constant_p 
#line 8528 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (SMIN, V16QImode, operands))
    ? (int) 
#line 8528 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (SMIN, V16QImode, operands))
    : -1 },
  { "(!TARGET_CMOVE) && ( reload_completed)",
    __builtin_constant_p (
#line 11873 "../.././gcc/config/i386/i386.md"
(!TARGET_CMOVE) && 
#line 11875 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 11873 "../.././gcc/config/i386/i386.md"
(!TARGET_CMOVE) && 
#line 11875 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 17858 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && TARGET_FXSR",
    __builtin_constant_p 
#line 17858 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_FXSR)
    ? (int) 
#line 17858 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_FXSR)
    : -1 },
#line 10405 "../.././gcc/config/i386/sse.md"
  { "TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_FROM_VEC",
    __builtin_constant_p 
#line 10405 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_FROM_VEC)
    ? (int) 
#line 10405 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_FROM_VEC)
    : -1 },
  { "(TARGET_SSE) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 5765 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 160 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 5765 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 160 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX && 1\n\
   && avx_vpermilp_parallel (operands[2], V4DFmode)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V4DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V4DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 2317 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (GET_MODE_NUNITS (V16QImode)\n\
       == GET_MODE_NUNITS (V4SFmode))",
    __builtin_constant_p 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V4SFmode)))
    ? (int) 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V4SFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)\n\
   && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)
   && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)
   && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_SSE2) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 11443 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 302 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 11443 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 302 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == SImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17316 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17316 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V32HImode)\n\
       || register_operand (operands[1], V32HImode))) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V32HImode)
       || register_operand (operands[1], V32HImode))) && 
#line 146 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V32HImode)
       || register_operand (operands[1], V32HImode))) && 
#line 146 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 3902 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSE2 && TARGET_MIX_SSE_I387",
    __builtin_constant_p 
#line 3902 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_MIX_SSE_I387)
    ? (int) 
#line 3902 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_MIX_SSE_I387)
    : -1 },
  { "((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)\n\
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[1])\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[3])) && ( reload_completed)",
    __builtin_constant_p (
#line 5943 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && GET_MODE (operands[0]) == GET_MODE (operands[3])) && 
#line 5948 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 5943 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && GET_MODE (operands[0]) == GET_MODE (operands[3])) && 
#line 5948 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(TARGET_BMI2 && reload_completed\n\
  && true_regnum (operands[1]) == DX_REG) && (!TARGET_64BIT)",
    __builtin_constant_p (
#line 6679 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2 && reload_completed
  && true_regnum (operands[1]) == DX_REG) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    ? (int) (
#line 6679 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2 && reload_completed
  && true_regnum (operands[1]) == DX_REG) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V32HImode)\n\
       == GET_MODE_NUNITS (V64QImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V64QImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V64QImode)))
    : -1 },
#line 685 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (US_PLUS, V4HImode, operands)",
    __builtin_constant_p 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (US_PLUS, V4HImode, operands))
    ? (int) 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (US_PLUS, V4HImode, operands))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V32QImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)
   && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_BMI2 && reload_completed) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10147 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2 && reload_completed) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10147 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2 && reload_completed) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 6323 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (PLUS, SImode, operands)",
    __builtin_constant_p 
#line 6323 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (PLUS, SImode, operands))
    ? (int) 
#line 6323 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (PLUS, SImode, operands))
    : -1 },
  { "(TARGET_SSE4_1) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10358 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10358 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 4102 "../.././gcc/config/i386/i386.md"
  { "TARGET_MIX_SSE_I387",
    __builtin_constant_p 
#line 4102 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387)
    ? (int) 
#line 4102 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387)
    : -1 },
#line 5912 "../.././gcc/config/i386/i386.md"
  { "(GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)\n\
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[1])\n\
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])\n\
       || GET_MODE (operands[3]) == VOIDmode)",
    __builtin_constant_p 
#line 5912 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])
       || GET_MODE (operands[3]) == VOIDmode))
    ? (int) 
#line 5912 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])
       || GET_MODE (operands[3]) == VOIDmode))
    : -1 },
#line 85 "../.././gcc/config/i386/sync.md"
  { "TARGET_64BIT || TARGET_SSE2",
    __builtin_constant_p 
#line 85 "../.././gcc/config/i386/sync.md"
(TARGET_64BIT || TARGET_SSE2)
    ? (int) 
#line 85 "../.././gcc/config/i386/sync.md"
(TARGET_64BIT || TARGET_SSE2)
    : -1 },
  { "(TARGET_XOP) && (TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F)",
    __builtin_constant_p (
#line 13717 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 2741 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F))
    ? (int) (
#line 13717 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 2741 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V64QImode)\n\
       || register_operand (operands[1], V64QImode))) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V64QImode)
       || register_operand (operands[1], V64QImode))) && 
#line 145 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V64QImode)
       || register_operand (operands[1], V64QImode))) && 
#line 145 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX && 1\n\
   && avx_vpermilp_parallel (operands[2], V8SFmode)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V8SFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1
   && avx_vpermilp_parallel (operands[2], V8SFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 4303 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 || SSE_FLOAT_MODE_P (DFmode)",
    __builtin_constant_p 
#line 4303 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || SSE_FLOAT_MODE_P (DFmode))
    ? (int) 
#line 4303 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || SSE_FLOAT_MODE_P (DFmode))
    : -1 },
#line 17058 "../.././gcc/config/i386/i386.md"
  { "(TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())\n\
   && peep2_reg_dead_p (4, operands[0])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[2])\n\
   && (HImode != QImode\n\
       || immediate_operand (operands[2], QImode)\n\
       || q_regs_operand (operands[2], QImode))\n\
   && ix86_match_ccmode (peep2_next_insn (3),\n\
			 (GET_CODE (operands[3]) == PLUS\n\
			  || GET_CODE (operands[3]) == MINUS)\n\
			 ? CCGOCmode : CCNOmode)",
    __builtin_constant_p 
#line 17058 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && peep2_reg_dead_p (4, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && (HImode != QImode
       || immediate_operand (operands[2], QImode)
       || q_regs_operand (operands[2], QImode))
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    ? (int) 
#line 17058 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && peep2_reg_dead_p (4, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && (HImode != QImode
       || immediate_operand (operands[2], QImode)
       || q_regs_operand (operands[2], QImode))
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    : -1 },
#line 15048 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
       || TARGET_MIX_SSE_I387)\n\
   && flag_unsafe_math_optimizations",
    __builtin_constant_p 
#line 15048 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations)
    ? (int) 
#line 15048 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations)
    : -1 },
#line 7695 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V16QImode, operands)",
    __builtin_constant_p 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V16QImode, operands))
    ? (int) 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V16QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (V8DFmode == V16SFmode || V8DFmode == V8DFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    : -1 },
#line 16226 "../.././gcc/config/i386/i386.md"
  { "TARGET_CMOVE && TARGET_AVOID_MEM_OPND_FOR_CMOVE\n\
   && optimize_insn_for_speed_p ()",
    __builtin_constant_p 
#line 16226 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && optimize_insn_for_speed_p ())
    ? (int) 
#line 16226 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && optimize_insn_for_speed_p ())
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V16HImode)\n\
       == GET_MODE_NUNITS (V8SFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V8SFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V8SFmode)))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V16QImode)\n\
       == GET_MODE_NUNITS (V4SImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V4SImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V4SImode)))
    : -1 },
#line 4425 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || SImode != DImode))\n\
	&& TARGET_SSE_MATH)\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ())
    ? (int) 
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ())
    : -1 },
#line 18230 "../.././gcc/config/i386/i386.md"
  { "TARGET_RTM",
    __builtin_constant_p 
#line 18230 "../.././gcc/config/i386/i386.md"
(TARGET_RTM)
    ? (int) 
#line 18230 "../.././gcc/config/i386/i386.md"
(TARGET_RTM)
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V4DFmode)\n\
       == GET_MODE_NUNITS (V8SImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V8SImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V8SImode)))
    : -1 },
  { "(TARGET_SSE4_2) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 8454 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_2) && 
#line 269 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 8454 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_2) && 
#line 269 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)\n\
   && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)
   && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)
   && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 13037 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (SFmode) && TARGET_MIX_SSE_I387\n\
   && COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13037 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_MIX_SSE_I387
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13037 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_MIX_SSE_I387
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V8SImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8DImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 12513 "../.././gcc/config/i386/i386.md"
  { "! TARGET_POPCNT",
    __builtin_constant_p 
#line 12513 "../.././gcc/config/i386/i386.md"
(! TARGET_POPCNT)
    ? (int) 
#line 12513 "../.././gcc/config/i386/i386.md"
(! TARGET_POPCNT)
    : -1 },
#line 8528 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (SMAX, V4SImode, operands)",
    __builtin_constant_p 
#line 8528 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (SMAX, V4SImode, operands))
    ? (int) 
#line 8528 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (SMAX, V4SImode, operands))
    : -1 },
#line 7662 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (AND, QImode, operands)",
    __builtin_constant_p 
#line 7662 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (AND, QImode, operands))
    ? (int) 
#line 7662 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (AND, QImode, operands))
    : -1 },
  { "(TARGET_AVX && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 14378 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 14378 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 2950 "../.././gcc/config/i386/i386.md"
  { "!(MEM_P (operands[0]) && MEM_P (operands[1]))\n\
   && (!can_create_pseudo_p ()\n\
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)\n\
       || GET_CODE (operands[1]) != CONST_DOUBLE\n\
       || (optimize_function_for_size_p (cfun)\n\
	   && ((!(TARGET_SSE2 && TARGET_SSE_MATH)\n\
		&& standard_80387_constant_p (operands[1]) > 0)\n\
	       || (TARGET_SSE2 && TARGET_SSE_MATH\n\
		   && standard_sse_constant_p (operands[1])))\n\
	   && !memory_operand (operands[0], DFmode))\n\
       || ((TARGET_64BIT || !TARGET_MEMORY_MISMATCH_STALL)\n\
	   && memory_operand (operands[0], DFmode)))",
    __builtin_constant_p 
#line 2950 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1]))
   && (!can_create_pseudo_p ()
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)
       || GET_CODE (operands[1]) != CONST_DOUBLE
       || (optimize_function_for_size_p (cfun)
	   && ((!(TARGET_SSE2 && TARGET_SSE_MATH)
		&& standard_80387_constant_p (operands[1]) > 0)
	       || (TARGET_SSE2 && TARGET_SSE_MATH
		   && standard_sse_constant_p (operands[1])))
	   && !memory_operand (operands[0], DFmode))
       || ((TARGET_64BIT || !TARGET_MEMORY_MISMATCH_STALL)
	   && memory_operand (operands[0], DFmode))))
    ? (int) 
#line 2950 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1]))
   && (!can_create_pseudo_p ()
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)
       || GET_CODE (operands[1]) != CONST_DOUBLE
       || (optimize_function_for_size_p (cfun)
	   && ((!(TARGET_SSE2 && TARGET_SSE_MATH)
		&& standard_80387_constant_p (operands[1]) > 0)
	       || (TARGET_SSE2 && TARGET_SSE_MATH
		   && standard_sse_constant_p (operands[1])))
	   && !memory_operand (operands[0], DFmode))
       || ((TARGET_64BIT || !TARGET_MEMORY_MISMATCH_STALL)
	   && memory_operand (operands[0], DFmode))))
    : -1 },
  { "(!(TARGET_PUSH_MEMORY || optimize_insn_for_size_p ())\n\
   && !RTX_FRAME_RELATED_P (peep2_next_insn (0))) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 16788 "../.././gcc/config/i386/i386.md"
(!(TARGET_PUSH_MEMORY || optimize_insn_for_size_p ())
   && !RTX_FRAME_RELATED_P (peep2_next_insn (0))) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 16788 "../.././gcc/config/i386/i386.md"
(!(TARGET_PUSH_MEMORY || optimize_insn_for_size_p ())
   && !RTX_FRAME_RELATED_P (peep2_next_insn (0))) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 4917 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC\n\
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)\n\
   && reload_completed",
    __builtin_constant_p 
#line 4917 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)
   && reload_completed)
    ? (int) 
#line 4917 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)
   && reload_completed)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 6280 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 6280 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 6280 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 4945 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT\n\
   && TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC",
    __builtin_constant_p 
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
    ? (int) 
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && (16 == 64) && 1) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)\n\
   && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)
   && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4DFmode, operands)
   && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (64 == 64)\n\
   && ix86_binary_operator_ok (XOR, V16SImode, operands)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (XOR, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (XOR, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8DImode, operands) && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8DImode, operands) && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8DImode, operands) && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && (16 == 64) && 1) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V2DFmode, operands) && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 890 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT",
    __builtin_constant_p 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)
    ? (int) 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V4SImode)\n\
       == GET_MODE_NUNITS (V8HImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V8HImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V8HImode)))
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)\n\
   && 1 && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F && (V2DFmode == V4SFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3567 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V2DFmode == V4SFmode)) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3567 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V2DFmode == V4SFmode)) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 8654 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (EQ, V32QImode, operands)",
    __builtin_constant_p 
#line 8654 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (EQ, V32QImode, operands))
    ? (int) 
#line 8654 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (EQ, V32QImode, operands))
    : -1 },
#line 8822 "../.././gcc/config/i386/i386.md"
  { "ix86_unary_operator_ok (NOT, HImode, operands)",
    __builtin_constant_p 
#line 8822 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NOT, HImode, operands))
    ? (int) 
#line 8822 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NOT, HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)\n\
   && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)
   && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)
   && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V8HImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V8HImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V8HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)\n\
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)
   && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)
   && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && (16 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (16 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (16 == 64) && 1))
    : -1 },
#line 1951 "../.././gcc/config/i386/i386.md"
  { "(TARGET_64BIT || TARGET_SSE)\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 1951 "../.././gcc/config/i386/i386.md"
((TARGET_64BIT || TARGET_SSE)
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 1951 "../.././gcc/config/i386/i386.md"
((TARGET_64BIT || TARGET_SSE)
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 4945 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT\n\
   && TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC",
    __builtin_constant_p 
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
    ? (int) 
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
    : -1 },
#line 4757 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)",
    __builtin_constant_p 
#line 4757 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode))
    ? (int) 
#line 4757 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode))
    : -1 },
  { "(ix86_binary_operator_ok (ASHIFTRT, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9729 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9729 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8703 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && !TARGET_XOP\n\
   && ix86_binary_operator_ok (EQ, V16QImode, operands)",
    __builtin_constant_p 
#line 8703 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP
   && ix86_binary_operator_ok (EQ, V16QImode, operands))
    ? (int) 
#line 8703 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP
   && ix86_binary_operator_ok (EQ, V16QImode, operands))
    : -1 },
  { "(ix86_binary_operator_ok (PLUS, TImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 5061 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, TImode, operands)) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 5061 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, TImode, operands)) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 14832 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 14832 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 274 "../.././gcc/config/i386/mmx.md"
  { "TARGET_3DNOW && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 274 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 274 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
  { "(TARGET_CMOVE && !(MEM_P (operands[2]) && MEM_P (operands[3]))) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 16149 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && !(MEM_P (operands[2]) && MEM_P (operands[3]))) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 16149 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && !(MEM_P (operands[2]) && MEM_P (operands[3]))) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 10385 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && (TARGET_USE_BT || reload_completed)",
    __builtin_constant_p 
#line 10385 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && (TARGET_USE_BT || reload_completed))
    ? (int) 
#line 10385 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && (TARGET_USE_BT || reload_completed))
    : -1 },
#line 5966 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1]))\n\
  && reload_completed",
    __builtin_constant_p 
#line 5966 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1]))
  && reload_completed)
    ? (int) 
#line 5966 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1]))
  && reload_completed)
    : -1 },
#line 14895 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && 1",
    __builtin_constant_p 
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1)
    ? (int) 
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1)
    : -1 },
#line 8159 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V4SImode, operands) && 1",
    __builtin_constant_p 
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V4SImode, operands) && 1)
    ? (int) 
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V4SImode, operands) && 1)
    : -1 },
#line 3530 "../.././gcc/config/i386/i386.md"
  { "TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)",
    __builtin_constant_p 
#line 3530 "../.././gcc/config/i386/i386.md"
(TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun))
    ? (int) 
#line 3530 "../.././gcc/config/i386/i386.md"
(TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F\n\
   && (INTVAL (operands[2]) == (INTVAL (operands[3]) - 1)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)\n\
       && INTVAL (operands[4]) == (INTVAL (operands[5]) - 1)))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 5913 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[2]) == (INTVAL (operands[3]) - 1)
       && INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[4]) == (INTVAL (operands[5]) - 1))))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 5913 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[2]) == (INTVAL (operands[3]) - 1)
       && INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[4]) == (INTVAL (operands[5]) - 1))))
    : -1 },
  { "(ix86_unary_operator_ok (NEG, DImode, operands)\n\
   && mode_signbit_p (DImode, operands[2])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8526 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, DImode, operands)
   && mode_signbit_p (DImode, operands[2])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8526 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, DImode, operands)
   && mode_signbit_p (DImode, operands[2])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 16238 "../.././gcc/config/i386/i386.md"
  { "(TARGET_80387 && TARGET_CMOVE)\n\
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 16238 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && TARGET_CMOVE)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 16238 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && TARGET_CMOVE)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))
    : -1 },
#line 1317 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && reload_completed",
    __builtin_constant_p 
#line 1317 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && reload_completed)
    ? (int) 
#line 1317 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && reload_completed)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512CD)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 15525 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512CD))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 15525 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512CD))
    : -1 },
#line 17089 "../.././gcc/config/i386/i386.md"
  { "(TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())\n\
   && GET_CODE (operands[2]) != MINUS\n\
   && peep2_reg_dead_p (3, operands[0])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])\n\
   && ix86_match_ccmode (peep2_next_insn (2),\n\
			 GET_CODE (operands[2]) == PLUS\n\
			 ? CCGOCmode : CCNOmode)",
    __builtin_constant_p 
#line 17089 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && GET_CODE (operands[2]) != MINUS
   && peep2_reg_dead_p (3, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && ix86_match_ccmode (peep2_next_insn (2),
			 GET_CODE (operands[2]) == PLUS
			 ? CCGOCmode : CCNOmode))
    ? (int) 
#line 17089 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && GET_CODE (operands[2]) != MINUS
   && peep2_reg_dead_p (3, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && ix86_match_ccmode (peep2_next_insn (2),
			 GET_CODE (operands[2]) == PLUS
			 ? CCGOCmode : CCNOmode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (16 == 64) && 1) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(ix86_match_ccmode (insn, CCNOmode)\n\
   && ((TARGET_64BIT && GET_MODE (operands[0]) == DImode)\n\
       || GET_MODE (operands[0]) == SImode\n\
       || GET_MODE (operands[0]) == HImode\n\
       || GET_MODE (operands[0]) == QImode)\n\
   /* Ensure that resulting mask is zero or sign extended operand.  */\n\
   && INTVAL (operands[2]) >= 0\n\
   && ((INTVAL (operands[1]) > 0\n\
	&& INTVAL (operands[1]) + INTVAL (operands[2]) <= 32)\n\
       || (DImode == DImode\n\
	   && INTVAL (operands[1]) > 32\n\
	   && INTVAL (operands[1]) + INTVAL (operands[2]) == 64))) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 7367 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ((TARGET_64BIT && GET_MODE (operands[0]) == DImode)
       || GET_MODE (operands[0]) == SImode
       || GET_MODE (operands[0]) == HImode
       || GET_MODE (operands[0]) == QImode)
   /* Ensure that resulting mask is zero or sign extended operand.  */
   && INTVAL (operands[2]) >= 0
   && ((INTVAL (operands[1]) > 0
	&& INTVAL (operands[1]) + INTVAL (operands[2]) <= 32)
       || (DImode == DImode
	   && INTVAL (operands[1]) > 32
	   && INTVAL (operands[1]) + INTVAL (operands[2]) == 64))) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 7367 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ((TARGET_64BIT && GET_MODE (operands[0]) == DImode)
       || GET_MODE (operands[0]) == SImode
       || GET_MODE (operands[0]) == HImode
       || GET_MODE (operands[0]) == QImode)
   /* Ensure that resulting mask is zero or sign extended operand.  */
   && INTVAL (operands[2]) >= 0
   && ((INTVAL (operands[1]) > 0
	&& INTVAL (operands[1]) + INTVAL (operands[2]) <= 32)
       || (DImode == DImode
	   && INTVAL (operands[1]) > 32
	   && INTVAL (operands[1]) + INTVAL (operands[2]) == 64))) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX2 && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 16985 "../.././gcc/config/i386/i386.md"
  { "REGNO (operands[0]) != REGNO (operands[1])\n\
   && GENERAL_REGNO_P (REGNO (operands[0]))\n\
   && GENERAL_REGNO_P (REGNO (operands[1]))",
    __builtin_constant_p 
#line 16985 "../.././gcc/config/i386/i386.md"
(REGNO (operands[0]) != REGNO (operands[1])
   && GENERAL_REGNO_P (REGNO (operands[0]))
   && GENERAL_REGNO_P (REGNO (operands[1])))
    ? (int) 
#line 16985 "../.././gcc/config/i386/i386.md"
(REGNO (operands[0]) != REGNO (operands[1])
   && GENERAL_REGNO_P (REGNO (operands[0]))
   && GENERAL_REGNO_P (REGNO (operands[1])))
    : -1 },
#line 685 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (SS_PLUS, V4HImode, operands)",
    __builtin_constant_p 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (SS_PLUS, V4HImode, operands))
    ? (int) 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (SS_PLUS, V4HImode, operands))
    : -1 },
#line 8318 "../.././gcc/config/i386/i386.md"
  { "!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun)",
    __builtin_constant_p 
#line 8318 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    ? (int) 
#line 8318 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
    : -1 },
  { "(TARGET_64BIT\n\
   && !(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15561 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15561 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[CX_REG] || fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
#line 6946 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (INTVAL (operands[3]) == (INTVAL (operands[7]) - 4)\n\
       && INTVAL (operands[4]) == (INTVAL (operands[8]) - 4)\n\
       && INTVAL (operands[5]) == (INTVAL (operands[9]) - 4)\n\
       && INTVAL (operands[6]) == (INTVAL (operands[10]) - 4)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[11]) - 8)\n\
       && INTVAL (operands[4]) == (INTVAL (operands[12]) - 8)\n\
       && INTVAL (operands[5]) == (INTVAL (operands[13]) - 8)\n\
       && INTVAL (operands[6]) == (INTVAL (operands[14]) - 8)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[15]) - 12)\n\
       && INTVAL (operands[4]) == (INTVAL (operands[16]) - 12)\n\
       && INTVAL (operands[5]) == (INTVAL (operands[17]) - 12)\n\
       && INTVAL (operands[6]) == (INTVAL (operands[18]) - 12))",
    __builtin_constant_p 
#line 6946 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[7]) - 4)
       && INTVAL (operands[4]) == (INTVAL (operands[8]) - 4)
       && INTVAL (operands[5]) == (INTVAL (operands[9]) - 4)
       && INTVAL (operands[6]) == (INTVAL (operands[10]) - 4)
       && INTVAL (operands[3]) == (INTVAL (operands[11]) - 8)
       && INTVAL (operands[4]) == (INTVAL (operands[12]) - 8)
       && INTVAL (operands[5]) == (INTVAL (operands[13]) - 8)
       && INTVAL (operands[6]) == (INTVAL (operands[14]) - 8)
       && INTVAL (operands[3]) == (INTVAL (operands[15]) - 12)
       && INTVAL (operands[4]) == (INTVAL (operands[16]) - 12)
       && INTVAL (operands[5]) == (INTVAL (operands[17]) - 12)
       && INTVAL (operands[6]) == (INTVAL (operands[18]) - 12)))
    ? (int) 
#line 6946 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[7]) - 4)
       && INTVAL (operands[4]) == (INTVAL (operands[8]) - 4)
       && INTVAL (operands[5]) == (INTVAL (operands[9]) - 4)
       && INTVAL (operands[6]) == (INTVAL (operands[10]) - 4)
       && INTVAL (operands[3]) == (INTVAL (operands[11]) - 8)
       && INTVAL (operands[4]) == (INTVAL (operands[12]) - 8)
       && INTVAL (operands[5]) == (INTVAL (operands[13]) - 8)
       && INTVAL (operands[6]) == (INTVAL (operands[14]) - 8)
       && INTVAL (operands[3]) == (INTVAL (operands[15]) - 12)
       && INTVAL (operands[4]) == (INTVAL (operands[16]) - 12)
       && INTVAL (operands[5]) == (INTVAL (operands[17]) - 12)
       && INTVAL (operands[6]) == (INTVAL (operands[18]) - 12)))
    : -1 },
#line 12856 "../.././gcc/config/i386/i386.md"
  { "TARGET_X32",
    __builtin_constant_p 
#line 12856 "../.././gcc/config/i386/i386.md"
(TARGET_X32)
    ? (int) 
#line 12856 "../.././gcc/config/i386/i386.md"
(TARGET_X32)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)
   && 1 && 1)
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SImode)\n\
       == GET_MODE_NUNITS (V32HImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V32HImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V32HImode)))
    : -1 },
  { "(TARGET_LWP) && (Pmode == SImode)",
    __builtin_constant_p (
#line 18058 "../.././gcc/config/i386/i386.md"
(TARGET_LWP) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 18058 "../.././gcc/config/i386/i386.md"
(TARGET_LWP) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 4877 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC\n\
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)",
    __builtin_constant_p 
#line 4877 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun))
    ? (int) 
#line 4877 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun))
    : -1 },
#line 13319 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (TARGET_USE_SIMODE_FIOP || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 13319 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (TARGET_USE_SIMODE_FIOP || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 13319 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (TARGET_USE_SIMODE_FIOP || optimize_function_for_size_p (cfun)))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V2DImode)\n\
       == GET_MODE_NUNITS (V8HImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V8HImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DImode)
       == GET_MODE_NUNITS (V8HImode)))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V16QImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V16QImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V16QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    : -1 },
  { "((SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
   || (TARGET_SSE && (DFmode == TFmode))) && ( reload_completed)",
    __builtin_constant_p (
#line 8768 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (DFmode == TFmode))) && 
#line 8771 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 8768 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (DFmode == TFmode))) && 
#line 8771 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 14815 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX",
    __builtin_constant_p 
#line 14815 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)
    ? (int) 
#line 14815 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)
    : -1 },
  { "((SSE_FLOAT_MODE_P (TFmode) && TARGET_SSE_MATH)\n\
   || (TARGET_SSE && (TFmode == TFmode))) && ( reload_completed)",
    __builtin_constant_p (
#line 8768 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (TFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (TFmode == TFmode))) && 
#line 8771 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 8768 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (TFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (TFmode == TFmode))) && 
#line 8771 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 76 "../.././gcc/config/i386/sync.md"
  { "TARGET_SSE2",
    __builtin_constant_p 
#line 76 "../.././gcc/config/i386/sync.md"
(TARGET_SSE2)
    ? (int) 
#line 76 "../.././gcc/config/i386/sync.md"
(TARGET_SSE2)
    : -1 },
#line 6087 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (MINUS, QImode, operands)",
    __builtin_constant_p 
#line 6087 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, QImode, operands))
    ? (int) 
#line 6087 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)\n\
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && !flag_finite_math_only\n\
   && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DImode)\n\
       == GET_MODE_NUNITS (V16SFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V16SFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V16SFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)\n\
   && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)
   && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)
   && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(reload_completed && ix86_avoid_lea_for_addr (insn, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 5013 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_avoid_lea_for_addr (insn, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 5013 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_avoid_lea_for_addr (insn, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 14532 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && avx_vperm2f128_parallel (operands[3], V8SFmode)",
    __builtin_constant_p 
#line 14532 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && avx_vperm2f128_parallel (operands[3], V8SFmode))
    ? (int) 
#line 14532 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && avx_vperm2f128_parallel (operands[3], V8SFmode))
    : -1 },
  { "(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == SImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17383 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17383 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 10247 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_REG_STALL\n\
    || (operands[1] == const1_rtx\n\
	&& TARGET_SHIFT1))",
    __builtin_constant_p 
#line 10247 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_REG_STALL
    || (operands[1] == const1_rtx
	&& TARGET_SHIFT1)))
    ? (int) 
#line 10247 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_REG_STALL
    || (operands[1] == const1_rtx
	&& TARGET_SHIFT1)))
    : -1 },
#line 6788 "../.././gcc/config/i386/i386.md"
  { "!(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 6788 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 6788 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 14190 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX && !TARGET_AVX2 && reload_completed",
    __builtin_constant_p 
#line 14190 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && !TARGET_AVX2 && reload_completed)
    ? (int) 
#line 14190 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && !TARGET_AVX2 && reload_completed)
    : -1 },
  { "(TARGET_ROUND && !flag_trapping_math) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 12133 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND && !flag_trapping_math) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 12133 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND && !flag_trapping_math) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (IOR, V8DFmode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V8DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V8DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V32QImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V32QImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V32QImode, operands)
   && 1 && 1)
    : -1 },
  { "(TARGET_SSE && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 9803 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)\n\
       && INTVAL (operands[5]) == (INTVAL (operands[6]) - 1)\n\
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)\n\
       && INTVAL (operands[9]) == (INTVAL (operands[10]) - 1))",
    __builtin_constant_p 
#line 9803 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[5]) == (INTVAL (operands[6]) - 1)
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)
       && INTVAL (operands[9]) == (INTVAL (operands[10]) - 1)))
    ? (int) 
#line 9803 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[4]) - 1)
       && INTVAL (operands[5]) == (INTVAL (operands[6]) - 1)
       && INTVAL (operands[7]) == (INTVAL (operands[8]) - 1)
       && INTVAL (operands[9]) == (INTVAL (operands[10]) - 1)))
    : -1 },
#line 11903 "../.././gcc/config/i386/i386.md"
  { "TARGET_BMI && !TARGET_AVOID_FALSE_DEP_FOR_BMI",
    __builtin_constant_p 
#line 11903 "../.././gcc/config/i386/i386.md"
(TARGET_BMI && !TARGET_AVOID_FALSE_DEP_FOR_BMI)
    ? (int) 
#line 11903 "../.././gcc/config/i386/i386.md"
(TARGET_BMI && !TARGET_AVOID_FALSE_DEP_FOR_BMI)
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V64QImode)\n\
       == GET_MODE_NUNITS (V8DFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V8DFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V8DFmode)))
    : -1 },
#line 13251 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_ARITH (DFmode)\n\
   && !(TARGET_SSE2 && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 13251 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (DFmode)
   && !(TARGET_SSE2 && TARGET_SSE_MATH))
    ? (int) 
#line 13251 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (DFmode)
   && !(TARGET_SSE2 && TARGET_SSE_MATH))
    : -1 },
#line 7672 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8HImode, operands) && 1",
    __builtin_constant_p 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8HImode, operands) && 1)
    ? (int) 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V8HImode, operands) && 1)
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V32HImode)\n\
       == GET_MODE_NUNITS (V8DImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V8DImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V8DImode)))
    : -1 },
  { "(TARGET_CMPXCHG) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 392 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 392 "../.././gcc/config/i386/sync.md"
(TARGET_CMPXCHG) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 15069 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && flag_unsafe_math_optimizations\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 15069 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations
   && can_create_pseudo_p ())
    ? (int) 
#line 15069 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations
   && can_create_pseudo_p ())
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V4SImode)\n\
       == GET_MODE_NUNITS (V4SImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V4SImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V4SImode)))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V4DFmode)\n\
       || register_operand (operands[1], V4DFmode))) && (TARGET_AVX)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V4DFmode)
       || register_operand (operands[1], V4DFmode))) && 
#line 151 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V4DFmode)
       || register_operand (operands[1], V4DFmode))) && 
#line 151 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 9673 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && INTVAL (operands[2]) == 31\n\
   && (TARGET_USE_CLTD || optimize_function_for_size_p (cfun))\n\
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9673 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && INTVAL (operands[2]) == 31
   && (TARGET_USE_CLTD || optimize_function_for_size_p (cfun))
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    ? (int) 
#line 9673 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && INTVAL (operands[2]) == 31
   && (TARGET_USE_CLTD || optimize_function_for_size_p (cfun))
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SImode)\n\
       == GET_MODE_NUNITS (V8DImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V8DImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SImode)
       == GET_MODE_NUNITS (V8DImode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 8719 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && !TARGET_XOP ",
    __builtin_constant_p 
#line 8719 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP )
    ? (int) 
#line 8719 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_XOP )
    : -1 },
#line 8317 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && 1",
    __builtin_constant_p 
#line 8317 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1)
    ? (int) 
#line 8317 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1)
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16SImode, operands) && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16SImode, operands) && 1) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16SImode, operands) && 1) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 3236 "../.././gcc/config/i386/sse.md"
  { "TARGET_FMA",
    __builtin_constant_p 
#line 3236 "../.././gcc/config/i386/sse.md"
(TARGET_FMA)
    ? (int) 
#line 3236 "../.././gcc/config/i386/sse.md"
(TARGET_FMA)
    : -1 },
  { "(TARGET_USE_FANCY_MATH_387\n\
   && flag_unsafe_math_optimizations\n\
   && can_create_pseudo_p ()) && ( 1)",
    __builtin_constant_p (
#line 15069 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations
   && can_create_pseudo_p ()) && 
#line 15073 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 15069 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations
   && can_create_pseudo_p ()) && 
#line 15073 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (16 == 64)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14378 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (16 == 64)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14378 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (16 == 64)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(TARGET_SSSE3 || TARGET_AVX || TARGET_XOP) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 8929 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 || TARGET_AVX || TARGET_XOP) && 
#line 8920 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 8929 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 || TARGET_AVX || TARGET_XOP) && 
#line 8920 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (64 == 64)\n\
   && ix86_binary_operator_ok (XOR, V8DImode, operands)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (XOR, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (XOR, V8DImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)\n\
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)
   && (64 == 64) && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 10715 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16QImode, operands)",
    __builtin_constant_p 
#line 10715 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16QImode, operands))
    ? (int) 
#line 10715 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (64 == 64)\n\
   && ix86_binary_operator_ok (IOR, V16SImode, operands)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (IOR, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (64 == 64)
   && ix86_binary_operator_ok (IOR, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_POPCNT\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 12358 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 12358 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 12133 "../.././gcc/config/i386/i386.md"
  { "TARGET_BMI",
    __builtin_constant_p 
#line 12133 "../.././gcc/config/i386/i386.md"
(TARGET_BMI)
    ? (int) 
#line 12133 "../.././gcc/config/i386/i386.md"
(TARGET_BMI)
    : -1 },
#line 8797 "../.././gcc/config/i386/i386.md"
  { "((SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
    || (TARGET_SSE && (SFmode == TFmode)))\n\
   && reload_completed",
    __builtin_constant_p 
#line 8797 "../.././gcc/config/i386/i386.md"
(((SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
    || (TARGET_SSE && (SFmode == TFmode)))
   && reload_completed)
    ? (int) 
#line 8797 "../.././gcc/config/i386/i386.md"
(((SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
    || (TARGET_SSE && (SFmode == TFmode)))
   && reload_completed)
    : -1 },
#line 1592 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSE_MATH\n\
   && SSE_FLOAT_MODE_P (DFmode)",
    __builtin_constant_p 
#line 1592 "../.././gcc/config/i386/i386.md"
(TARGET_SSE_MATH
   && SSE_FLOAT_MODE_P (DFmode))
    ? (int) 
#line 1592 "../.././gcc/config/i386/i386.md"
(TARGET_SSE_MATH
   && SSE_FLOAT_MODE_P (DFmode))
    : -1 },
#line 17452 "../.././gcc/config/i386/i386.md"
  { "TARGET_SLOW_IMUL_IMM32_MEM && optimize_insn_for_speed_p ()\n\
   && !satisfies_constraint_K (operands[2])",
    __builtin_constant_p 
#line 17452 "../.././gcc/config/i386/i386.md"
(TARGET_SLOW_IMUL_IMM32_MEM && optimize_insn_for_speed_p ()
   && !satisfies_constraint_K (operands[2]))
    ? (int) 
#line 17452 "../.././gcc/config/i386/i386.md"
(TARGET_SLOW_IMUL_IMM32_MEM && optimize_insn_for_speed_p ()
   && !satisfies_constraint_K (operands[2]))
    : -1 },
#line 6853 "../.././gcc/config/i386/i386.md"
  { "(TARGET_80387 && X87_ENABLE_ARITH (DFmode))\n\
    || (TARGET_SSE2 && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 6853 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && X87_ENABLE_ARITH (DFmode))
    || (TARGET_SSE2 && TARGET_SSE_MATH))
    ? (int) 
#line 6853 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && X87_ENABLE_ARITH (DFmode))
    || (TARGET_SSE2 && TARGET_SSE_MATH))
    : -1 },
#line 8062 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (IOR, SImode, operands)",
    __builtin_constant_p 
#line 8062 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (IOR, SImode, operands))
    ? (int) 
#line 8062 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (IOR, SImode, operands))
    : -1 },
#line 1619 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && !flag_finite_math_only\n\
   && 1 && 1",
    __builtin_constant_p 
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && 1)
    ? (int) 
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && 1)
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V64QImode)\n\
       == GET_MODE_NUNITS (V16SImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V16SImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V16SImode)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (AND, V4DFmode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V4DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V4DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DImode)\n\
       == GET_MODE_NUNITS (V16SImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V16SImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V16SImode)))
    : -1 },
#line 7439 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && reload_completed",
    __builtin_constant_p 
#line 7439 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && reload_completed)
    ? (int) 
#line 7439 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && reload_completed)
    : -1 },
#line 4965 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT\n\
   && ((TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)\n\
	&& TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)\n\
       || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))",
    __builtin_constant_p 
#line 4965 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && ((TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
	&& TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
       || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)))
    ? (int) 
#line 4965 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && ((TARGET_80387 && X87_ENABLE_FLOAT (XFmode, DImode)
	&& TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
       || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V4SImode)\n\
       == GET_MODE_NUNITS (V16QImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V16QImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V16QImode)))
    : -1 },
#line 14837 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH\n\
       && HImode != HImode \n\
       && ((HImode != DImode) || TARGET_64BIT)\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && HImode != HImode 
       && ((HImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && HImode != HImode 
       && ((HImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
#line 8497 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_unary_operator_ok (NEG, SImode, operands)",
    __builtin_constant_p 
#line 8497 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_unary_operator_ok (NEG, SImode, operands))
    ? (int) 
#line 8497 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_unary_operator_ok (NEG, SImode, operands))
    : -1 },
#line 8066 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (MULT, V16HImode, operands)",
    __builtin_constant_p 
#line 8066 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (MULT, V16HImode, operands))
    ? (int) 
#line 8066 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (MULT, V16HImode, operands))
    : -1 },
#line 1875 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
   && operands[1] == constm1_rtx",
    __builtin_constant_p 
#line 1875 "../.././gcc/config/i386/i386.md"
(reload_completed
   && operands[1] == constm1_rtx)
    ? (int) 
#line 1875 "../.././gcc/config/i386/i386.md"
(reload_completed
   && operands[1] == constm1_rtx)
    : -1 },
#line 4044 "../.././gcc/config/i386/i386.md"
  { "TARGET_SPLIT_MEM_OPND_FOR_FP_CONVERTS\n\
   && optimize_insn_for_speed_p ()\n\
   && SSE_REG_P (operands[0])",
    __builtin_constant_p 
#line 4044 "../.././gcc/config/i386/i386.md"
(TARGET_SPLIT_MEM_OPND_FOR_FP_CONVERTS
   && optimize_insn_for_speed_p ()
   && SSE_REG_P (operands[0]))
    ? (int) 
#line 4044 "../.././gcc/config/i386/i386.md"
(TARGET_SPLIT_MEM_OPND_FOR_FP_CONVERTS
   && optimize_insn_for_speed_p ()
   && SSE_REG_P (operands[0]))
    : -1 },
#line 293 "../.././gcc/config/i386/mmx.md"
  { "TARGET_3DNOW && ix86_binary_operator_ok (MULT, V2SFmode, operands)",
    __builtin_constant_p 
#line 293 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && ix86_binary_operator_ok (MULT, V2SFmode, operands))
    ? (int) 
#line 293 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && ix86_binary_operator_ok (MULT, V2SFmode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    : -1 },
#line 8249 "../.././gcc/config/i386/i386.md"
  { "(!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && ix86_match_ccmode (insn, CCNOmode)\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 8249 "../.././gcc/config/i386/i386.md"
((!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 8249 "../.././gcc/config/i386/i386.md"
((!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 6215 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (PLUS, HImode, operands)",
    __builtin_constant_p 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, HImode, operands))
    ? (int) 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, HImode, operands))
    : -1 },
#line 10224 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ROTATERT, QImode, operands)",
    __builtin_constant_p 
#line 10224 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, QImode, operands))
    ? (int) 
#line 10224 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, QImode, operands))
    : -1 },
#line 12700 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && TARGET_GNU_TLS",
    __builtin_constant_p 
#line 12700 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_GNU_TLS)
    ? (int) 
#line 12700 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_GNU_TLS)
    : -1 },
  { "(TARGET_SSE3) && (Pmode == SImode)",
    __builtin_constant_p (
#line 10854 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 10854 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 1659 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && !symbolic_operand (operands[1], DImode)\n\
   && !x86_64_immediate_operand (operands[1], DImode)",
    __builtin_constant_p 
#line 1659 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !symbolic_operand (operands[1], DImode)
   && !x86_64_immediate_operand (operands[1], DImode))
    ? (int) 
#line 1659 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !symbolic_operand (operands[1], DImode)
   && !x86_64_immediate_operand (operands[1], DImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && (16 == 64) && 1) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && (16 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 15364 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 15364 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 15364 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SFmode)\n\
       == GET_MODE_NUNITS (V8DFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V8DFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
#line 10863 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_BT || optimize_function_for_size_p (cfun)",
    __builtin_constant_p 
#line 10863 "../.././gcc/config/i386/i386.md"
(TARGET_USE_BT || optimize_function_for_size_p (cfun))
    ? (int) 
#line 10863 "../.././gcc/config/i386/i386.md"
(TARGET_USE_BT || optimize_function_for_size_p (cfun))
    : -1 },
#line 10829 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_BT || optimize_function_for_size_p (cfun))\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))\n\
      == GET_MODE_BITSIZE (SImode)-1",
    __builtin_constant_p 
#line 10829 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    ? (int) 
#line 10829 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (XOR, V4DFmode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V4DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V4DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_SSE2\n\
   && ((unsigned) exact_log2 (INTVAL (operands[3]))\n\
       < GET_MODE_NUNITS (V2DImode))) && (TARGET_SSE4_1 && TARGET_64BIT)",
    __builtin_constant_p (
#line 9598 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V2DImode))) && 
#line 9584 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && TARGET_64BIT))
    ? (int) (
#line 9598 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V2DImode))) && 
#line 9584 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && TARGET_64BIT))
    : -1 },
#line 666 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_MMX || (TARGET_SSE2 && V1DImode == V1DImode))\n\
   && ix86_binary_operator_ok (MINUS, V1DImode, operands)",
    __builtin_constant_p 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V1DImode == V1DImode))
   && ix86_binary_operator_ok (MINUS, V1DImode, operands))
    ? (int) 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V1DImode == V1DImode))
   && ix86_binary_operator_ok (MINUS, V1DImode, operands))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V8DFmode)\n\
       || register_operand (operands[1], V8DFmode))) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8DFmode)
       || register_operand (operands[1], V8DFmode))) && 
#line 151 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V8DFmode)
       || register_operand (operands[1], V8DFmode))) && 
#line 151 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(!TARGET_64BIT && TARGET_XSAVE) && (TARGET_XSAVEOPT)",
    __builtin_constant_p (
#line 17884 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_XSAVE) && 
#line 17867 "../.././gcc/config/i386/i386.md"
(TARGET_XSAVEOPT))
    ? (int) (
#line 17884 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_XSAVE) && 
#line 17867 "../.././gcc/config/i386/i386.md"
(TARGET_XSAVEOPT))
    : -1 },
#line 2577 "../.././gcc/config/i386/sse.md"
  { "SSE_FLOAT_MODE_P (SFmode)",
    __builtin_constant_p 
#line 2577 "../.././gcc/config/i386/sse.md"
(SSE_FLOAT_MODE_P (SFmode))
    ? (int) 
#line 2577 "../.././gcc/config/i386/sse.md"
(SSE_FLOAT_MODE_P (SFmode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)\n\
   && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)
   && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V8DFmode, operands)
   && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 10018 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ROTATE, SImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))\n\
      == GET_MODE_BITSIZE (SImode)-1",
    __builtin_constant_p 
#line 10018 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    ? (int) 
#line 10018 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    : -1 },
#line 4554 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && !TARGET_FISTTP\n\
   && !(TARGET_64BIT && SSE_FLOAT_MODE_P (GET_MODE (operands[1])))",
    __builtin_constant_p 
#line 4554 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(TARGET_64BIT && SSE_FLOAT_MODE_P (GET_MODE (operands[1]))))
    ? (int) 
#line 4554 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(TARGET_64BIT && SSE_FLOAT_MODE_P (GET_MODE (operands[1]))))
    : -1 },
  { "(TARGET_AVX512F) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15427 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15427 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 4453 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || SImode != DImode))\n\
	&& TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 4453 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
	&& TARGET_SSE_MATH))
    ? (int) 
#line 4453 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || SImode != DImode))
	&& TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_AVX512F) && ((1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 2775 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 2775 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)
   && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)
   && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && ix86_binary_operator_ok (EQ, V16SImode, operands))",
    __builtin_constant_p (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8676 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (EQ, V16SImode, operands)))
    ? (int) (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8676 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (EQ, V16SImode, operands)))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)
   && 1 && 1)
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V4DImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)\n\
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 11112 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && !TARGET_CMOVE\n\
   && reload_completed",
    __builtin_constant_p 
#line 11112 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !TARGET_CMOVE
   && reload_completed)
    ? (int) 
#line 11112 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !TARGET_CMOVE
   && reload_completed)
    : -1 },
#line 7819 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT\n\
   && ix86_match_ccmode\n\
	(insn,\n\
	 /* If we are going to emit andl instead of andq, and the operands[2]\n\
	    constant might have the SImode sign bit set, make sure the sign\n\
	    flag isn't tested, because the instruction will set the sign flag\n\
	    based on bit 31 rather than bit 63.  If it isn't CONST_INT,\n\
	    conservatively assume it might have bit 31 set.  */\n\
	 (satisfies_constraint_Z (operands[2])\n\
	  && (!CONST_INT_P (operands[2])\n\
	      || val_signbit_known_set_p (SImode, INTVAL (operands[2]))))\n\
	 ? CCZmode : CCNOmode)\n\
   && ix86_binary_operator_ok (AND, DImode, operands)",
    __builtin_constant_p 
#line 7819 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && ix86_match_ccmode
	(insn,
	 /* If we are going to emit andl instead of andq, and the operands[2]
	    constant might have the SImode sign bit set, make sure the sign
	    flag isn't tested, because the instruction will set the sign flag
	    based on bit 31 rather than bit 63.  If it isn't CONST_INT,
	    conservatively assume it might have bit 31 set.  */
	 (satisfies_constraint_Z (operands[2])
	  && (!CONST_INT_P (operands[2])
	      || val_signbit_known_set_p (SImode, INTVAL (operands[2]))))
	 ? CCZmode : CCNOmode)
   && ix86_binary_operator_ok (AND, DImode, operands))
    ? (int) 
#line 7819 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && ix86_match_ccmode
	(insn,
	 /* If we are going to emit andl instead of andq, and the operands[2]
	    constant might have the SImode sign bit set, make sure the sign
	    flag isn't tested, because the instruction will set the sign flag
	    based on bit 31 rather than bit 63.  If it isn't CONST_INT,
	    conservatively assume it might have bit 31 set.  */
	 (satisfies_constraint_Z (operands[2])
	  && (!CONST_INT_P (operands[2])
	      || val_signbit_known_set_p (SImode, INTVAL (operands[2]))))
	 ? CCZmode : CCNOmode)
   && ix86_binary_operator_ok (AND, DImode, operands))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8SImode, operands) && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8SImode, operands) && 1) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8SImode, operands) && 1) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V16HImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 1063 "../.././gcc/config/i386/i386.md"
  { "word_mode == SImode",
    __builtin_constant_p 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)
    ? (int) 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)
    : -1 },
#line 685 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (SS_MINUS, V4HImode, operands)",
    __builtin_constant_p 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (SS_MINUS, V4HImode, operands))
    ? (int) 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (SS_MINUS, V4HImode, operands))
    : -1 },
  { "(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && ((((((((word_mode == DImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17371 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17371 "../.././gcc/config/i386/i386.md"
(INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
  { "(TARGET_SSE2) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10373 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10373 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 7958 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (MULT, V8SImode, operands)",
    __builtin_constant_p 
#line 7958 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (MULT, V8SImode, operands))
    ? (int) 
#line 7958 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (MULT, V8SImode, operands))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 13909 "../.././gcc/config/i386/sse.md"
  { "TARGET_AES",
    __builtin_constant_p 
#line 13909 "../.././gcc/config/i386/sse.md"
(TARGET_AES)
    ? (int) 
#line 13909 "../.././gcc/config/i386/sse.md"
(TARGET_AES)
    : -1 },
  { "(TARGET_AVX512PF) && (Pmode == DImode)",
    __builtin_constant_p (
#line 12883 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512PF) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 12883 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512PF) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
#line 8727 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1",
    __builtin_constant_p 
#line 8727 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1)
    ? (int) 
#line 8727 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1)
    : -1 },
  { "((TARGET_LZCNT\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && (TARGET_64BIT)) && ( reload_completed)",
    __builtin_constant_p ((
#line 12035 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 12038 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) ((
#line 12035 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 12038 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 8861 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_unary_operator_ok (NOT, SImode, operands)",
    __builtin_constant_p 
#line 8861 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, SImode, operands))
    ? (int) 
#line 8861 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, SImode, operands))
    : -1 },
#line 658 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX || (TARGET_SSE2 && V4HImode == V1DImode)",
    __builtin_constant_p 
#line 658 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX || (TARGET_SSE2 && V4HImode == V1DImode))
    ? (int) 
#line 658 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX || (TARGET_SSE2 && V4HImode == V1DImode))
    : -1 },
#line 2625 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && ix86_binary_operator_ok (XOR, TFmode, operands)",
    __builtin_constant_p 
#line 2625 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && ix86_binary_operator_ok (XOR, TFmode, operands))
    ? (int) 
#line 2625 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && ix86_binary_operator_ok (XOR, TFmode, operands))
    : -1 },
#line 14934 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && flag_unsafe_math_optimizations\n\
   && !optimize_insn_for_size_p ()",
    __builtin_constant_p 
#line 14934 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations
   && !optimize_insn_for_size_p ())
    ? (int) 
#line 14934 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && flag_unsafe_math_optimizations
   && !optimize_insn_for_size_p ())
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX && (16 == 64)\n\
   && avx_vpermilp_parallel (operands[2], V4SFmode))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (16 == 64)
   && avx_vpermilp_parallel (operands[2], V4SFmode)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (16 == 64)
   && avx_vpermilp_parallel (operands[2], V4SFmode)))
    : -1 },
#line 6723 "../.././gcc/config/i386/i386.md"
  { "TARGET_QIMODE_MATH\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 6723 "../.././gcc/config/i386/i386.md"
(TARGET_QIMODE_MATH
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 6723 "../.././gcc/config/i386/i386.md"
(TARGET_QIMODE_MATH
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(TARGET_SSE) && (TARGET_SSE4A)",
    __builtin_constant_p (
#line 1240 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 1230 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4A))
    ? (int) (
#line 1240 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 1230 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4A))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 3179 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && (64 == 64) && 1",
    __builtin_constant_p 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (64 == 64) && 1)
    ? (int) 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (64 == 64) && 1)
    : -1 },
  { "(reload_completed) && (Pmode == DImode)",
    __builtin_constant_p (
#line 3815 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 3815 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 11312 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSSE3 && ix86_binary_operator_ok (MULT, V4HImode, operands)",
    __builtin_constant_p 
#line 11312 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && ix86_binary_operator_ok (MULT, V4HImode, operands))
    ? (int) 
#line 11312 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && ix86_binary_operator_ok (MULT, V4HImode, operands))
    : -1 },
#line 10289 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE",
    __builtin_constant_p 
#line 10289 "../.././gcc/config/i386/sse.md"
(TARGET_SSE)
    ? (int) 
#line 10289 "../.././gcc/config/i386/sse.md"
(TARGET_SSE)
    : -1 },
#line 3988 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && TARGET_64BIT",
    __builtin_constant_p 
#line 3988 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && TARGET_64BIT)
    ? (int) 
#line 3988 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && TARGET_64BIT)
    : -1 },
#line 10466 "../.././gcc/config/i386/sse.md"
  { "TARGET_64BIT && TARGET_SSE4_1",
    __builtin_constant_p 
#line 10466 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE4_1)
    ? (int) 
#line 10466 "../.././gcc/config/i386/sse.md"
(TARGET_64BIT && TARGET_SSE4_1)
    : -1 },
#line 16164 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && TARGET_CMOVE\n\
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE\n\
   && (MEM_P (operands[2]) || MEM_P (operands[3]))\n\
   && can_create_pseudo_p ()\n\
   && optimize_insn_for_speed_p ()",
    __builtin_constant_p 
#line 16164 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && (MEM_P (operands[2]) || MEM_P (operands[3]))
   && can_create_pseudo_p ()
   && optimize_insn_for_speed_p ())
    ? (int) 
#line 16164 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && (MEM_P (operands[2]) || MEM_P (operands[3]))
   && can_create_pseudo_p ()
   && optimize_insn_for_speed_p ())
    : -1 },
  { "(X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && !TARGET_FISTTP\n\
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || HImode != DImode))\n\
   && can_create_pseudo_p ()) && ( 1)",
    __builtin_constant_p (
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
   && can_create_pseudo_p ()) && 
#line 4510 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 4504 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && !TARGET_FISTTP
   && !(SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
   && can_create_pseudo_p ()) && 
#line 4510 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && 1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (16 == 64) && 1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX512F) && ((1 && (DFmode == V16SFmode || DFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (DFmode == V16SFmode || DFmode == V8DFmode)) && 
#line 2772 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (DFmode == V16SFmode || DFmode == V8DFmode)) && 
#line 2772 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F)))
    : -1 },
  { "((SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
   || (TARGET_SSE && (SFmode == TFmode))) && ( reload_completed)",
    __builtin_constant_p (
#line 8768 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (SFmode == TFmode))) && 
#line 8771 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 8768 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (SFmode == TFmode))) && 
#line 8771 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 906 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_SSE || TARGET_3DNOW_A)\n\
   && ix86_binary_operator_ok (UMIN, V8QImode, operands)",
    __builtin_constant_p 
#line 906 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (UMIN, V8QImode, operands))
    ? (int) 
#line 906 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (UMIN, V8QImode, operands))
    : -1 },
#line 17118 "../.././gcc/config/i386/i386.md"
  { "(TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())\n\
   && REG_P (operands[0]) && REG_P (operands[4])\n\
   && REGNO (operands[0]) == REGNO (operands[4])\n\
   && peep2_reg_dead_p (4, operands[0])\n\
   && (HImode != QImode\n\
       || immediate_operand (operands[2], SImode)\n\
       || q_regs_operand (operands[2], SImode))\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[2])\n\
   && ix86_match_ccmode (peep2_next_insn (3),\n\
			 (GET_CODE (operands[3]) == PLUS\n\
			  || GET_CODE (operands[3]) == MINUS)\n\
			 ? CCGOCmode : CCNOmode)",
    __builtin_constant_p 
#line 17118 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && REG_P (operands[0]) && REG_P (operands[4])
   && REGNO (operands[0]) == REGNO (operands[4])
   && peep2_reg_dead_p (4, operands[0])
   && (HImode != QImode
       || immediate_operand (operands[2], SImode)
       || q_regs_operand (operands[2], SImode))
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    ? (int) 
#line 17118 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && REG_P (operands[0]) && REG_P (operands[4])
   && REGNO (operands[0]) == REGNO (operands[4])
   && peep2_reg_dead_p (4, operands[0])
   && (HImode != QImode
       || immediate_operand (operands[2], SImode)
       || q_regs_operand (operands[2], SImode))
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode))
    : -1 },
#line 2469 "../.././gcc/config/i386/i386.md"
  { "TARGET_LP64 && ix86_check_movabs (insn, 0)",
    __builtin_constant_p 
#line 2469 "../.././gcc/config/i386/i386.md"
(TARGET_LP64 && ix86_check_movabs (insn, 0))
    ? (int) 
#line 2469 "../.././gcc/config/i386/i386.md"
(TARGET_LP64 && ix86_check_movabs (insn, 0))
    : -1 },
#line 838 "../.././gcc/config/i386/mmx.md"
  { "TARGET_3DNOW && ix86_binary_operator_ok (MULT, V4HImode, operands)",
    __builtin_constant_p 
#line 838 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && ix86_binary_operator_ok (MULT, V4HImode, operands))
    ? (int) 
#line 838 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && ix86_binary_operator_ok (MULT, V4HImode, operands))
    : -1 },
#line 804 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (MULT, V4HImode, operands)",
    __builtin_constant_p 
#line 804 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (MULT, V4HImode, operands))
    ? (int) 
#line 804 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (MULT, V4HImode, operands))
    : -1 },
#line 9505 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& (TARGET_SHIFT1\n\
	    || TARGET_DOUBLE_WITH_ADD)))\n\
   && ix86_match_ccmode (insn, CCGOCmode)",
    __builtin_constant_p 
#line 9505 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || TARGET_DOUBLE_WITH_ADD)))
   && ix86_match_ccmode (insn, CCGOCmode))
    ? (int) 
#line 9505 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& (TARGET_SHIFT1
	    || TARGET_DOUBLE_WITH_ADD)))
   && ix86_match_ccmode (insn, CCGOCmode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && 1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 5671 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCGCmode)",
    __builtin_constant_p 
#line 5671 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGCmode))
    ? (int) 
#line 5671 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGCmode))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 4793 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSE2 && TARGET_SSE_MATH\n\
   && TARGET_SSE_PARTIAL_REG_DEPENDENCY\n\
   && optimize_function_for_speed_p (cfun)\n\
   && reload_completed && SSE_REG_P (operands[0])",
    __builtin_constant_p 
#line 4793 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH
   && TARGET_SSE_PARTIAL_REG_DEPENDENCY
   && optimize_function_for_speed_p (cfun)
   && reload_completed && SSE_REG_P (operands[0]))
    ? (int) 
#line 4793 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH
   && TARGET_SSE_PARTIAL_REG_DEPENDENCY
   && optimize_function_for_speed_p (cfun)
   && reload_completed && SSE_REG_P (operands[0]))
    : -1 },
#line 10344 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && !TARGET_SSE4_1",
    __builtin_constant_p 
#line 10344 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_SSE4_1)
    ? (int) 
#line 10344 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !TARGET_SSE4_1)
    : -1 },
  { "(TARGET_SSE2) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 13647 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 247 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 13647 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 247 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 2745 "../.././gcc/config/i386/sse.md"
  { "TARGET_FMA || TARGET_FMA4",
    __builtin_constant_p 
#line 2745 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)
    ? (int) 
#line 2745 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && 1)",
    __builtin_constant_p (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1))
    ? (int) (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && 1 && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 8138 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (IOR, SImode, operands)",
    __builtin_constant_p 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, SImode, operands))
    ? (int) 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (IOR, SImode, operands))
    : -1 },
#line 13343 "../.././gcc/config/i386/sse.md"
  { "TARGET_XOP && !(MEM_P (operands[2]) && MEM_P (operands[3]))",
    __builtin_constant_p 
#line 13343 "../.././gcc/config/i386/sse.md"
(TARGET_XOP && !(MEM_P (operands[2]) && MEM_P (operands[3])))
    ? (int) 
#line 13343 "../.././gcc/config/i386/sse.md"
(TARGET_XOP && !(MEM_P (operands[2]) && MEM_P (operands[3])))
    : -1 },
#line 13463 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387 && X87_ENABLE_ARITH (SFmode))\n\
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 13463 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387 && X87_ENABLE_ARITH (SFmode))
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 13463 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387 && X87_ENABLE_ARITH (SFmode))
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    : -1 },
#line 9882 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFTRT, HImode, operands)",
    __builtin_constant_p 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, HImode, operands))
    ? (int) 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)\n\
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (MULT, V16HImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7759 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MULT, V16HImode, operands)) && 
#line 250 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7759 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MULT, V16HImode, operands)) && 
#line 250 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 8624 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (UMIN, V16QImode, operands)",
    __builtin_constant_p 
#line 8624 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (UMIN, V16QImode, operands))
    ? (int) 
#line 8624 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (UMIN, V16QImode, operands))
    : -1 },
#line 7695 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V16QImode, operands)",
    __builtin_constant_p 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V16QImode, operands))
    ? (int) 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V16QImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8SImode, operands) && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8SImode, operands) && (32 == 64)) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8SImode, operands) && (32 == 64)) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V8SFmode)\n\
       == GET_MODE_NUNITS (V32QImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V32QImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V32QImode)))
    : -1 },
  { "(TARGET_SSE && !flag_finite_math_only\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 13740 "../.././gcc/config/i386/i386.md"
  { "find_regno_note (insn, REG_UNUSED, REGNO (operands[1]))\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 13740 "../.././gcc/config/i386/i386.md"
(find_regno_note (insn, REG_UNUSED, REGNO (operands[1]))
   && can_create_pseudo_p ())
    ? (int) 
#line 13740 "../.././gcc/config/i386/i386.md"
(find_regno_note (insn, REG_UNUSED, REGNO (operands[1]))
   && can_create_pseudo_p ())
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V16HImode)\n\
       == GET_MODE_NUNITS (V32QImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V32QImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V32QImode)))
    : -1 },
#line 16924 "../.././gcc/config/i386/i386.md"
  { "! TARGET_PARTIAL_REG_STALL\n\
   && ix86_match_ccmode (insn, CCNOmode)\n\
   && true_regnum (operands[2]) != AX_REG\n\
   && peep2_reg_dead_p (1, operands[2])",
    __builtin_constant_p 
#line 16924 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL
   && ix86_match_ccmode (insn, CCNOmode)
   && true_regnum (operands[2]) != AX_REG
   && peep2_reg_dead_p (1, operands[2]))
    ? (int) 
#line 16924 "../.././gcc/config/i386/i386.md"
(! TARGET_PARTIAL_REG_STALL
   && ix86_match_ccmode (insn, CCNOmode)
   && true_regnum (operands[2]) != AX_REG
   && peep2_reg_dead_p (1, operands[2]))
    : -1 },
#line 8263 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 8263 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 8263 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(!TARGET_64BIT && TARGET_CMOVE\n\
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE\n\
   && (MEM_P (operands[2]) || MEM_P (operands[3]))\n\
   && can_create_pseudo_p ()\n\
   && optimize_insn_for_speed_p ()) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 16164 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && (MEM_P (operands[2]) || MEM_P (operands[3]))
   && can_create_pseudo_p ()
   && optimize_insn_for_speed_p ()) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 16164 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && (MEM_P (operands[2]) || MEM_P (operands[3]))
   && can_create_pseudo_p ()
   && optimize_insn_for_speed_p ()) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512ER)",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 12957 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 12957 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER))
    : -1 },
  { "(TARGET_SSE2\n\
   && ((unsigned) exact_log2 (INTVAL (operands[3]))\n\
       < GET_MODE_NUNITS (V4SImode))) && (TARGET_SSE4_1)",
    __builtin_constant_p (
#line 9598 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V4SImode))) && 
#line 9583 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1))
    ? (int) (
#line 9598 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V4SImode))) && 
#line 9583 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1))
    : -1 },
#line 16859 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && ((TARGET_NOT_UNPAIRABLE\n\
	&& (!MEM_P (operands[0])\n\
	    || !memory_displacement_operand (operands[0], QImode)))\n\
       || (TARGET_NOT_VECTORMODE\n\
	   && long_memory_operand (operands[0], QImode)))\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 16859 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((TARGET_NOT_UNPAIRABLE
	&& (!MEM_P (operands[0])
	    || !memory_displacement_operand (operands[0], QImode)))
       || (TARGET_NOT_VECTORMODE
	   && long_memory_operand (operands[0], QImode)))
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 16859 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((TARGET_NOT_UNPAIRABLE
	&& (!MEM_P (operands[0])
	    || !memory_displacement_operand (operands[0], QImode)))
       || (TARGET_NOT_VECTORMODE
	   && long_memory_operand (operands[0], QImode)))
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
  { "((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && 1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && 1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && (16 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && (16 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && (16 == 64) && 1))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
#line 17848 "../.././gcc/config/i386/i386.md"
  { "TARGET_FXSR",
    __builtin_constant_p 
#line 17848 "../.././gcc/config/i386/i386.md"
(TARGET_FXSR)
    ? (int) 
#line 17848 "../.././gcc/config/i386/i386.md"
(TARGET_FXSR)
    : -1 },
#line 8757 "../.././gcc/config/i386/i386.md"
  { "(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
   || (TARGET_SSE && (SFmode == TFmode))",
    __builtin_constant_p 
#line 8757 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (SFmode == TFmode)))
    ? (int) 
#line 8757 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (SFmode == TFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)
   && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V16SFmode, operands)
   && (64 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_AVX2 && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (64 == 64) && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 14158 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && (V8DImode != V8DImode || TARGET_64BIT)",
    __builtin_constant_p 
#line 14158 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DImode != V8DImode || TARGET_64BIT))
    ? (int) 
#line 14158 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DImode != V8DImode || TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && ( reload_completed)",
    __builtin_constant_p (
#line 6280 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 6282 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) (
#line 6280 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 6282 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
  { "((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && ((((((((word_mode == SImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17305 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17305 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
#line 2283 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SFmode)\n\
       == GET_MODE_NUNITS (V16SFmode))",
    __builtin_constant_p 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V16SFmode)))
    ? (int) 
#line 2283 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V16SFmode)))
    : -1 },
#line 8537 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 8537 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 8537 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V32HImode)\n\
       == GET_MODE_NUNITS (V32HImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V32HImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V32HImode)
       == GET_MODE_NUNITS (V32HImode)))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DFmode)\n\
       == GET_MODE_NUNITS (V8DImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V8DImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DFmode)
       == GET_MODE_NUNITS (V8DImode)))
    : -1 },
  { "(!TARGET_PARTIAL_REG_STALL\n\
   || DImode == SImode\n\
   || optimize_function_for_size_p (cfun)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 17422 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   || DImode == SImode
   || optimize_function_for_size_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 17422 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   || DImode == SImode
   || optimize_function_for_size_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 17568 "../.././gcc/config/i386/i386.md"
  { "TARGET_PREFETCH_SSE || TARGET_PRFCHW || TARGET_PREFETCHWT1",
    __builtin_constant_p 
#line 17568 "../.././gcc/config/i386/i386.md"
(TARGET_PREFETCH_SSE || TARGET_PRFCHW || TARGET_PREFETCHWT1)
    ? (int) 
#line 17568 "../.././gcc/config/i386/i386.md"
(TARGET_PREFETCH_SSE || TARGET_PRFCHW || TARGET_PREFETCHWT1)
    : -1 },
  { "(ix86_binary_operator_ok (PLUS, DImode, operands)\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 5859 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, DImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 5859 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, DImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3])) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 5013 "../.././gcc/config/i386/i386.md"
  { "reload_completed && ix86_avoid_lea_for_addr (insn, operands)",
    __builtin_constant_p 
#line 5013 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_avoid_lea_for_addr (insn, operands))
    ? (int) 
#line 5013 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_avoid_lea_for_addr (insn, operands))
    : -1 },
#line 14943 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH\n\
       && !flag_trapping_math)",
    __builtin_constant_p 
#line 14943 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && !flag_trapping_math))
    ? (int) 
#line 14943 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && !flag_trapping_math))
    : -1 },
#line 14837 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH\n\
       && SImode != HImode \n\
       && ((SImode != DImode) || TARGET_64BIT)\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH
       && SImode != HImode 
       && ((SImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH
       && SImode != HImode 
       && ((SImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V8SImode)\n\
       == GET_MODE_NUNITS (V8SFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V8SFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V8SImode)
       == GET_MODE_NUNITS (V8SFmode)))
    : -1 },
#line 9913 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT\n\
   && (optimize_function_for_size_p (cfun)\n\
       || !TARGET_PARTIAL_FLAG_REG_STALL\n\
       || (operands[2] == const1_rtx\n\
	   && TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9913 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && (optimize_function_for_size_p (cfun)
       || !TARGET_PARTIAL_FLAG_REG_STALL
       || (operands[2] == const1_rtx
	   && TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    ? (int) 
#line 9913 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && (optimize_function_for_size_p (cfun)
       || !TARGET_PARTIAL_FLAG_REG_STALL
       || (operands[2] == const1_rtx
	   && TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    : -1 },
#line 13192 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (SFmode, HImode)\n\
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
   && (TARGET_USE_HIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 13192 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, HImode)
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 13192 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (SFmode, HImode)
   && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_ROUND) && (TARGET_AVX)",
    __builtin_constant_p (
#line 12070 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 12070 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 4965 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT\n\
   && ((TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)\n\
	&& TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)\n\
       || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))",
    __builtin_constant_p 
#line 4965 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && ((TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
	&& TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
       || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)))
    ? (int) 
#line 4965 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && ((TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
	&& TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
       || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)))
    : -1 },
#line 8543 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (SMAX, V8HImode, operands)",
    __builtin_constant_p 
#line 8543 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SMAX, V8HImode, operands))
    ? (int) 
#line 8543 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SMAX, V8HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 7871 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (AND, QImode, operands)",
    __builtin_constant_p 
#line 7871 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (AND, QImode, operands))
    ? (int) 
#line 7871 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (AND, QImode, operands))
    : -1 },
  { "(TARGET_USE_8BIT_IDIV\n\
   && TARGET_QIMODE_MATH\n\
   && can_create_pseudo_p ()\n\
   && !optimize_insn_for_size_p ()) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 7071 "../.././gcc/config/i386/i386.md"
(TARGET_USE_8BIT_IDIV
   && TARGET_QIMODE_MATH
   && can_create_pseudo_p ()
   && !optimize_insn_for_size_p ()) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 7071 "../.././gcc/config/i386/i386.md"
(TARGET_USE_8BIT_IDIV
   && TARGET_QIMODE_MATH
   && can_create_pseudo_p ()
   && !optimize_insn_for_size_p ()) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16SImode, operands) && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16SImode, operands) && (64 == 64)) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16SImode, operands) && (64 == 64)) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8564 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 8564 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 8564 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    : -1 },
#line 6072 "../.././gcc/config/i386/i386.md"
  { "(! TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 6072 "../.././gcc/config/i386/i386.md"
((! TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 6072 "../.././gcc/config/i386/i386.md"
((! TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 5484 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (PLUS, SImode, operands)",
    __builtin_constant_p 
#line 5484 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, SImode, operands))
    ? (int) 
#line 5484 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, SImode, operands))
    : -1 },
  { "(!(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15712 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15712 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "(TARGET_CMOVE) && (!TARGET_64BIT)",
    __builtin_constant_p (
#line 9596 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    ? (int) (
#line 9596 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    : -1 },
#line 11758 "../.././gcc/config/i386/i386.md"
  { "epilogue_completed",
    __builtin_constant_p 
#line 11758 "../.././gcc/config/i386/i386.md"
(epilogue_completed)
    ? (int) 
#line 11758 "../.././gcc/config/i386/i386.md"
(epilogue_completed)
    : -1 },
  { "(ix86_binary_operator_ok (ROTATE, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10101 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10101 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 12441 "../.././gcc/config/i386/i386.md"
  { "TARGET_MOVBE\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 12441 "../.././gcc/config/i386/i386.md"
(TARGET_MOVBE
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 12441 "../.././gcc/config/i386/i386.md"
(TARGET_MOVBE
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 4917 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC\n\
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)\n\
   && reload_completed",
    __builtin_constant_p 
#line 4917 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)
   && reload_completed)
    ? (int) 
#line 4917 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC
   && !TARGET_64BIT && optimize_function_for_speed_p (cfun)
   && reload_completed)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1])))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 6002 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1]))))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 6002 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && !(MEM_P (operands[0]) && MEM_P (operands[1]))))
    : -1 },
#line 1252 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && !TARGET_SSE",
    __builtin_constant_p 
#line 1252 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && !TARGET_SSE)
    ? (int) 
#line 1252 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && !TARGET_SSE)
    : -1 },
#line 15302 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && ix86_libc_has_function (function_c99_misc)\n\
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 15302 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && ix86_libc_has_function (function_c99_misc)
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 15302 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && ix86_libc_has_function (function_c99_misc)
   && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    : -1 },
#line 7672 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V2DImode, operands) && 1",
    __builtin_constant_p 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V2DImode, operands) && 1)
    ? (int) 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V2DImode, operands) && 1)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)\n\
   && ix86_binary_operator_ok (XOR, V4DImode, operands)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (XOR, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (XOR, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 12494 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_2\n\
   && can_create_pseudo_p ()",
    __builtin_constant_p 
#line 12494 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_2
   && can_create_pseudo_p ())
    ? (int) 
#line 12494 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_2
   && can_create_pseudo_p ())
    : -1 },
  { "(!(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15979 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15979 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[AX_REG] || fixed_regs[CX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64) && 1) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64) && 1) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 7695 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V8HImode, operands)",
    __builtin_constant_p 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V8HImode, operands))
    ? (int) 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_MINUS, V8HImode, operands))
    : -1 },
#line 9259 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ASHIFT, QImode, operands)",
    __builtin_constant_p 
#line 9259 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, QImode, operands))
    ? (int) 
#line 9259 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, QImode, operands))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SFmode)\n\
       == GET_MODE_NUNITS (V64QImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V64QImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V64QImode)))
    : -1 },
  { "(TARGET_SSSE3 || TARGET_AVX || TARGET_XOP) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8929 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 || TARGET_AVX || TARGET_XOP) && 
#line 8922 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8929 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 || TARGET_AVX || TARGET_XOP) && 
#line 8922 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)\n\
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V16HImode, operands)
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    : -1 },
#line 252 "../.././gcc/config/i386/mmx.md"
  { "TARGET_3DNOW && ix86_binary_operator_ok (PLUS, V2SFmode, operands)",
    __builtin_constant_p 
#line 252 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && ix86_binary_operator_ok (PLUS, V2SFmode, operands))
    ? (int) 
#line 252 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && ix86_binary_operator_ok (PLUS, V2SFmode, operands))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)
   && 1 && 1)
    : -1 },
#line 7921 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && ix86_binary_operator_ok (MULT, V16SImode, operands)",
    __builtin_constant_p 
#line 7921 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (MULT, V16SImode, operands))
    ? (int) 
#line 7921 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && ix86_binary_operator_ok (MULT, V16SImode, operands))
    : -1 },
#line 1849 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE3\n\
   && INTVAL (operands[2]) != INTVAL (operands[3])",
    __builtin_constant_p 
#line 1849 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3
   && INTVAL (operands[2]) != INTVAL (operands[3]))
    ? (int) 
#line 1849 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3
   && INTVAL (operands[2]) != INTVAL (operands[3]))
    : -1 },
#line 1003 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (IOR, V4HImode, operands)",
    __builtin_constant_p 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (IOR, V4HImode, operands))
    ? (int) 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (IOR, V4HImode, operands))
    : -1 },
#line 460 "../.././gcc/config/i386/mmx.md"
  { "TARGET_3DNOW && ix86_binary_operator_ok (EQ, V2SFmode, operands)",
    __builtin_constant_p 
#line 460 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && ix86_binary_operator_ok (EQ, V2SFmode, operands))
    ? (int) 
#line 460 "../.././gcc/config/i386/mmx.md"
(TARGET_3DNOW && ix86_binary_operator_ok (EQ, V2SFmode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 5436 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (PLUS, QImode, operands)",
    __builtin_constant_p 
#line 5436 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, QImode, operands))
    ? (int) 
#line 5436 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, QImode, operands))
    : -1 },
#line 16859 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && ((TARGET_NOT_UNPAIRABLE\n\
	&& (!MEM_P (operands[0])\n\
	    || !memory_displacement_operand (operands[0], SImode)))\n\
       || (TARGET_NOT_VECTORMODE\n\
	   && long_memory_operand (operands[0], SImode)))\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 16859 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((TARGET_NOT_UNPAIRABLE
	&& (!MEM_P (operands[0])
	    || !memory_displacement_operand (operands[0], SImode)))
       || (TARGET_NOT_VECTORMODE
	   && long_memory_operand (operands[0], SImode)))
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 16859 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((TARGET_NOT_UNPAIRABLE
	&& (!MEM_P (operands[0])
	    || !memory_displacement_operand (operands[0], SImode)))
       || (TARGET_NOT_VECTORMODE
	   && long_memory_operand (operands[0], SImode)))
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16SImode, operands) && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16SImode, operands) && (64 == 64)) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16SImode, operands) && (64 == 64)) && 
#line 231 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64) && 1))
    : -1 },
#line 13463 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387 && X87_ENABLE_ARITH (DFmode))\n\
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 13463 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387 && X87_ENABLE_ARITH (DFmode))
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 13463 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387 && X87_ENABLE_ARITH (DFmode))
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    : -1 },
#line 7732 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
   && true_regnum (operands[0]) != true_regnum (operands[1])",
    __builtin_constant_p 
#line 7732 "../.././gcc/config/i386/i386.md"
(reload_completed
   && true_regnum (operands[0]) != true_regnum (operands[1]))
    ? (int) 
#line 7732 "../.././gcc/config/i386/i386.md"
(reload_completed
   && true_regnum (operands[0]) != true_regnum (operands[1]))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (16 == 64)\n\
   && avx_vpermilp_parallel (operands[2], V2DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (16 == 64)
   && avx_vpermilp_parallel (operands[2], V2DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (16 == 64)
   && avx_vpermilp_parallel (operands[2], V2DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)\n\
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V8SImode, operands)
   && (32 == 64) && (V8SImode == V16SFmode || V8SImode == V8DFmode))))
    : -1 },
#line 1551 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX",
    __builtin_constant_p 
#line 1551 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX)
    ? (int) 
#line 1551 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX)
    : -1 },
  { "(TARGET_AVX512F) && (((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 2815 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
   && (GET_MODE (operands[0]) == TFmode\n\
       || GET_MODE (operands[0]) == XFmode\n\
       || GET_MODE (operands[0]) == DFmode)\n\
   && !ANY_FP_REG_P (operands[1])",
    __builtin_constant_p 
#line 2815 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (GET_MODE (operands[0]) == TFmode
       || GET_MODE (operands[0]) == XFmode
       || GET_MODE (operands[0]) == DFmode)
   && !ANY_FP_REG_P (operands[1]))
    ? (int) 
#line 2815 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (GET_MODE (operands[0]) == TFmode
       || GET_MODE (operands[0]) == XFmode
       || GET_MODE (operands[0]) == DFmode)
   && !ANY_FP_REG_P (operands[1]))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)\n\
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V16HImode, operands)
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE2 && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
#line 14832 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH",
    __builtin_constant_p 
#line 14832 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
    ? (int) 
#line 14832 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
    : -1 },
  { "(((TARGET_USE_BT || optimize_function_for_size_p (cfun))\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))\n\
      == GET_MODE_BITSIZE (DImode)-1) && (TARGET_64BIT)) && ( 1)",
    __builtin_constant_p ((
#line 10829 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 10833 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) ((
#line 10829 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 10833 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
#line 3248 "../.././gcc/config/i386/i386.md"
  { "reload_completed\n\
   && (GET_MODE (operands[0]) == TFmode\n\
       || GET_MODE (operands[0]) == XFmode\n\
       || GET_MODE (operands[0]) == DFmode\n\
       || GET_MODE (operands[0]) == SFmode)\n\
   && (operands[2] = find_constant_src (insn))",
    __builtin_constant_p 
#line 3248 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (GET_MODE (operands[0]) == TFmode
       || GET_MODE (operands[0]) == XFmode
       || GET_MODE (operands[0]) == DFmode
       || GET_MODE (operands[0]) == SFmode)
   && (operands[2] = find_constant_src (insn)))
    ? (int) 
#line 3248 "../.././gcc/config/i386/i386.md"
(reload_completed
   && (GET_MODE (operands[0]) == TFmode
       || GET_MODE (operands[0]) == XFmode
       || GET_MODE (operands[0]) == DFmode
       || GET_MODE (operands[0]) == SFmode)
   && (operands[2] = find_constant_src (insn)))
    : -1 },
  { "(TARGET_SSE2 && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 1602 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)
   && 1 && 1)
    ? (int) 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)
   && 1 && 1)
    : -1 },
  { "((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())\n\
   && peep2_reg_dead_p (4, operands[0])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[2])\n\
   && (DImode != QImode\n\
       || immediate_operand (operands[2], QImode)\n\
       || q_regs_operand (operands[2], QImode))\n\
   && ix86_match_ccmode (peep2_next_insn (3),\n\
			 (GET_CODE (operands[3]) == PLUS\n\
			  || GET_CODE (operands[3]) == MINUS)\n\
			 ? CCGOCmode : CCNOmode)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 17058 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && peep2_reg_dead_p (4, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && (DImode != QImode
       || immediate_operand (operands[2], QImode)
       || q_regs_operand (operands[2], QImode))
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 17058 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && peep2_reg_dead_p (4, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && !reg_overlap_mentioned_p (operands[0], operands[2])
   && (DImode != QImode
       || immediate_operand (operands[2], QImode)
       || q_regs_operand (operands[2], QImode))
   && ix86_match_ccmode (peep2_next_insn (3),
			 (GET_CODE (operands[3]) == PLUS
			  || GET_CODE (operands[3]) == MINUS)
			 ? CCGOCmode : CCNOmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V8SFmode)\n\
       == GET_MODE_NUNITS (V8SFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V8SFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V8SFmode)
       == GET_MODE_NUNITS (V8SFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16HImode, operands) && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16HImode, operands) && (32 == 64)) && 
#line 230 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16HImode, operands) && (32 == 64)) && 
#line 230 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)\n\
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)
   && (32 == 64) && (V16HImode == V16SFmode || V16HImode == V8DFmode))))
    : -1 },
#line 1058 "../.././gcc/config/i386/i386.md"
  { "Pmode == SImode",
    __builtin_constant_p 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)
    ? (int) 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)
    : -1 },
  { "(TARGET_BMI2 && reload_completed\n\
  && true_regnum (operands[1]) == DX_REG) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 6679 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2 && reload_completed
  && true_regnum (operands[1]) == DX_REG) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 6679 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2 && reload_completed
  && true_regnum (operands[1]) == DX_REG) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 16353 "../.././gcc/config/i386/i386.md"
  { "(DFmode != DFmode || TARGET_64BIT)\n\
   && TARGET_80387 && TARGET_CMOVE\n\
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE\n\
   && optimize_insn_for_speed_p ()",
    __builtin_constant_p 
#line 16353 "../.././gcc/config/i386/i386.md"
((DFmode != DFmode || TARGET_64BIT)
   && TARGET_80387 && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && optimize_insn_for_speed_p ())
    ? (int) 
#line 16353 "../.././gcc/config/i386/i386.md"
((DFmode != DFmode || TARGET_64BIT)
   && TARGET_80387 && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && optimize_insn_for_speed_p ())
    : -1 },
  { "(TARGET_SSE2) && (TARGET_AVX)",
    __builtin_constant_p (
#line 4837 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 4837 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX512ER))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 12943 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 12943 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER)))
    : -1 },
#line 16184 "../.././gcc/config/i386/i386.md"
  { "TARGET_CMOVE && !TARGET_PARTIAL_REG_STALL",
    __builtin_constant_p 
#line 16184 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && !TARGET_PARTIAL_REG_STALL)
    ? (int) 
#line 16184 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE && !TARGET_PARTIAL_REG_STALL)
    : -1 },
#line 9563 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ASHIFTRT, SImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))\n\
      == GET_MODE_BITSIZE (SImode)-1",
    __builtin_constant_p 
#line 9563 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    ? (int) 
#line 9563 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFTRT, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    : -1 },
  { "(TARGET_SSE2 && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 3610 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && (V4SFmode == V4SFmode || TARGET_AVX2)",
    __builtin_constant_p 
#line 3610 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (V4SFmode == V4SFmode || TARGET_AVX2))
    ? (int) 
#line 3610 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (V4SFmode == V4SFmode || TARGET_AVX2))
    : -1 },
  { "(!TARGET_64BIT\n\
   && TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)\n\
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && ( reload_completed)",
    __builtin_constant_p (
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && 
#line 4949 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 4945 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT
   && TARGET_80387 && X87_ENABLE_FLOAT (SFmode, DImode)
   && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && 
#line 4949 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 8714 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (reload_completed\n\
       || !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))",
    __builtin_constant_p 
#line 8714 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (reload_completed
       || !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)))
    ? (int) 
#line 8714 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (reload_completed
       || !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)))
    : -1 },
#line 13130 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH\n\
   && !COMMUTATIVE_ARITH_P (operands[3])",
    __builtin_constant_p 
#line 13130 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
   && !COMMUTATIVE_ARITH_P (operands[3]))
    ? (int) 
#line 13130 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
   && !COMMUTATIVE_ARITH_P (operands[3]))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && (32 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14060 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14060 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64) && 1) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64) && 1) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(optimize_insn_for_speed_p ()\n\
   && (!TARGET_PARTIAL_REG_STALL || DImode == SImode)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 17436 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && (!TARGET_PARTIAL_REG_STALL || DImode == SImode)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 17436 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && (!TARGET_PARTIAL_REG_STALL || DImode == SImode)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 10018 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ROTATERT, SImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))\n\
      == GET_MODE_BITSIZE (SImode)-1",
    __builtin_constant_p 
#line 10018 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    ? (int) 
#line 10018 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V16HImode)\n\
       == GET_MODE_NUNITS (V8SImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V8SImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V8SImode)))
    : -1 },
#line 658 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX || (TARGET_SSE2 && V2SImode == V1DImode)",
    __builtin_constant_p 
#line 658 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX || (TARGET_SSE2 && V2SImode == V1DImode))
    ? (int) 
#line 658 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX || (TARGET_SSE2 && V2SImode == V1DImode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)\n\
   && (16 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)
   && (16 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4SFmode, operands)
   && (16 == 64) && 1))
    : -1 },
#line 11095 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && !TARGET_CMOVE\n\
   && (TARGET_USE_HIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 11095 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !TARGET_CMOVE
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 11095 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !TARGET_CMOVE
   && (TARGET_USE_HIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
#line 8433 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && 1 && 1",
    __builtin_constant_p 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && 1)
    ? (int) 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && 1)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)\n\
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMIN, V8SImode, operands)
   && 1 && (V8SImode == V16SFmode || V8SImode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4SFmode, operands) && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    : -1 },
  { "(TARGET_SLOW_IMUL_IMM32_MEM && optimize_insn_for_speed_p ()\n\
   && !satisfies_constraint_K (operands[2])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 17452 "../.././gcc/config/i386/i386.md"
(TARGET_SLOW_IMUL_IMM32_MEM && optimize_insn_for_speed_p ()
   && !satisfies_constraint_K (operands[2])) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 17452 "../.././gcc/config/i386/i386.md"
(TARGET_SLOW_IMUL_IMM32_MEM && optimize_insn_for_speed_p ()
   && !satisfies_constraint_K (operands[2])) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 16636 "../.././gcc/config/i386/i386.md"
  { "TARGET_MACHO && !TARGET_64BIT && flag_pic",
    __builtin_constant_p 
#line 16636 "../.././gcc/config/i386/i386.md"
(TARGET_MACHO && !TARGET_64BIT && flag_pic)
    ? (int) 
#line 16636 "../.././gcc/config/i386/i386.md"
(TARGET_MACHO && !TARGET_64BIT && flag_pic)
    : -1 },
#line 9019 "../.././gcc/config/i386/i386.md"
  { "TARGET_CMOVE",
    __builtin_constant_p 
#line 9019 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE)
    ? (int) 
#line 9019 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE)
    : -1 },
#line 3440 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && reload_completed\n\
   && !(MMX_REG_P (operands[0]) || SSE_REG_P (operands[0]))\n\
   && true_regnum (operands[0]) == true_regnum (operands[1])",
    __builtin_constant_p 
#line 3440 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && reload_completed
   && !(MMX_REG_P (operands[0]) || SSE_REG_P (operands[0]))
   && true_regnum (operands[0]) == true_regnum (operands[1]))
    ? (int) 
#line 3440 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && reload_completed
   && !(MMX_REG_P (operands[0]) || SSE_REG_P (operands[0]))
   && true_regnum (operands[0]) == true_regnum (operands[1]))
    : -1 },
#line 1003 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (XOR, V2SImode, operands)",
    __builtin_constant_p 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (XOR, V2SImode, operands))
    ? (int) 
#line 1003 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (XOR, V2SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (32 == 64)\n\
   && avx_vpermilp_parallel (operands[2], V4DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64)
   && avx_vpermilp_parallel (operands[2], V4DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64)
   && avx_vpermilp_parallel (operands[2], V4DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 15338 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387",
    __builtin_constant_p 
#line 15338 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387)
    ? (int) 
#line 15338 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387)
    : -1 },
#line 4453 "../.././gcc/config/i386/i386.md"
  { "X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || HImode != DImode))\n\
	&& TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 4453 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
	&& TARGET_SSE_MATH))
    ? (int) 
#line 4453 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || HImode != DImode))
	&& TARGET_SSE_MATH))
    : -1 },
#line 210 "../.././gcc/config/i386/mmx.md"
  { "!TARGET_64BIT && reload_completed\n\
   && !(MMX_REG_P (operands[0]) || SSE_REG_P (operands[0]))\n\
   && !(MMX_REG_P (operands[1]) || SSE_REG_P (operands[1]))",
    __builtin_constant_p 
#line 210 "../.././gcc/config/i386/mmx.md"
(!TARGET_64BIT && reload_completed
   && !(MMX_REG_P (operands[0]) || SSE_REG_P (operands[0]))
   && !(MMX_REG_P (operands[1]) || SSE_REG_P (operands[1])))
    ? (int) 
#line 210 "../.././gcc/config/i386/mmx.md"
(!TARGET_64BIT && reload_completed
   && !(MMX_REG_P (operands[0]) || SSE_REG_P (operands[0]))
   && !(MMX_REG_P (operands[1]) || SSE_REG_P (operands[1])))
    : -1 },
  { "(!TARGET_PARTIAL_REG_STALL\n\
   && (!TARGET_ZERO_EXTEND_WITH_AND || optimize_function_for_size_p (cfun))) && ( reload_completed)",
    __builtin_constant_p (
#line 10544 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   && (!TARGET_ZERO_EXTEND_WITH_AND || optimize_function_for_size_p (cfun))) && 
#line 10547 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 10544 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   && (!TARGET_ZERO_EXTEND_WITH_AND || optimize_function_for_size_p (cfun))) && 
#line 10547 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(!(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && (Pmode == SImode)",
    __builtin_constant_p (
#line 15526 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 15526 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "(optimize_insn_for_size_p ()\n\
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == DImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17393 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_size_p ()
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17393 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_size_p ()
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 15204 "../.././gcc/config/i386/i386.md"
  { "reload_completed",
    __builtin_constant_p 
#line 15204 "../.././gcc/config/i386/i386.md"
(reload_completed)
    ? (int) 
#line 15204 "../.././gcc/config/i386/i386.md"
(reload_completed)
    : -1 },
  { "(TARGET_AVX512F && TARGET_64BIT) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 3581 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && TARGET_64BIT) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 3581 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && TARGET_64BIT) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_SSE\n\
   && (GET_MODE_NUNITS (V4SImode)\n\
       == GET_MODE_NUNITS (V2DFmode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_ROUND && !flag_trapping_math) && (TARGET_AVX)",
    __builtin_constant_p (
#line 12176 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND && !flag_trapping_math) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 12176 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND && !flag_trapping_math) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 6183 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCmode)\n\
   && ix86_binary_operator_ok (MINUS, HImode, operands)",
    __builtin_constant_p 
#line 6183 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, HImode, operands))
    ? (int) 
#line 6183 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, HImode, operands))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V2DImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V2DImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V2DImode, operands))
    : -1 },
#line 11456 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSSE3",
    __builtin_constant_p 
#line 11456 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3)
    ? (int) 
#line 11456 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3)
    : -1 },
  { "(TARGET_SSE3) && (Pmode == DImode)",
    __builtin_constant_p (
#line 10854 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 10854 "../.././gcc/config/i386/sse.md"
(TARGET_SSE3) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
#line 17208 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && !TARGET_OPT_AGU\n\
   && REGNO (operands[0]) == REGNO (operands[1])\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 17208 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_OPT_AGU
   && REGNO (operands[0]) == REGNO (operands[1])
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 17208 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && !TARGET_OPT_AGU
   && REGNO (operands[0]) == REGNO (operands[1])
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V16QImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V16QImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V16QImode, operands))
    : -1 },
  { "(TARGET_SSE && reload_completed) && (Pmode == DImode)",
    __builtin_constant_p (
#line 2691 "../.././gcc/config/i386/i386.md"
(TARGET_SSE && reload_completed) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 2691 "../.././gcc/config/i386/i386.md"
(TARGET_SSE && reload_completed) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (IOR, V4SImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (IOR, V4SImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (IOR, V4SImode, operands)))
    : -1 },
#line 9882 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (LSHIFTRT, HImode, operands)",
    __builtin_constant_p 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, HImode, operands))
    ? (int) 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, HImode, operands))
    : -1 },
#line 14837 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH\n\
       && DImode != HImode \n\
       && ((DImode != DImode) || TARGET_64BIT)\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && DImode != HImode 
       && ((DImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
       && DImode != HImode 
       && ((DImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX512F && (V8DImode == V16SFmode || V8DImode == V8DFmode)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DImode == V16SFmode || V8DImode == V8DFmode))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V8DImode == V16SFmode || V8DImode == V8DFmode))))
    : -1 },
  { "(SIBLING_CALL_P (insn)) && (word_mode == DImode)",
    __builtin_constant_p (
#line 11419 "../.././gcc/config/i386/i386.md"
(SIBLING_CALL_P (insn)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode))
    ? (int) (
#line 11419 "../.././gcc/config/i386/i386.md"
(SIBLING_CALL_P (insn)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode))
    : -1 },
#line 869 "../.././gcc/config/i386/mmx.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (MULT, V2SImode, operands)",
    __builtin_constant_p 
#line 869 "../.././gcc/config/i386/mmx.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MULT, V2SImode, operands))
    ? (int) 
#line 869 "../.././gcc/config/i386/mmx.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MULT, V2SImode, operands))
    : -1 },
  { "(1 && 1) && (TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F)",
    __builtin_constant_p (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && 1) && 
#line 2772 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F))
    ? (int) (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && 1) && 
#line 2772 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)\n\
   && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)
   && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V2DFmode, operands)
   && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 9084 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ASHIFT, SImode, operands)",
    __builtin_constant_p 
#line 9084 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, SImode, operands))
    ? (int) 
#line 9084 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, SImode, operands))
    : -1 },
#line 18143 "../.././gcc/config/i386/i386.md"
  { "TARGET_RDRND",
    __builtin_constant_p 
#line 18143 "../.././gcc/config/i386/i386.md"
(TARGET_RDRND)
    ? (int) 
#line 18143 "../.././gcc/config/i386/i386.md"
(TARGET_RDRND)
    : -1 },
  { "(TARGET_PREFETCHWT1) && (Pmode == SImode)",
    __builtin_constant_p (
#line 17628 "../.././gcc/config/i386/i386.md"
(TARGET_PREFETCHWT1) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 17628 "../.././gcc/config/i386/i386.md"
(TARGET_PREFETCHWT1) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 3634 "../.././gcc/config/i386/i386.md"
  { "REGNO (operands[1]) != REGNO (operands[2])\n\
   && peep2_reg_dead_p (2, operands[1])\n\
   && peep2_reg_dead_p (4, operands[2])\n\
   && !reg_mentioned_p (operands[2], operands[3])",
    __builtin_constant_p 
#line 3634 "../.././gcc/config/i386/i386.md"
(REGNO (operands[1]) != REGNO (operands[2])
   && peep2_reg_dead_p (2, operands[1])
   && peep2_reg_dead_p (4, operands[2])
   && !reg_mentioned_p (operands[2], operands[3]))
    ? (int) 
#line 3634 "../.././gcc/config/i386/i386.md"
(REGNO (operands[1]) != REGNO (operands[2])
   && peep2_reg_dead_p (2, operands[1])
   && peep2_reg_dead_p (4, operands[2])
   && !reg_mentioned_p (operands[2], operands[3]))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (IOR, V16QImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (IOR, V16QImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (IOR, V16QImode, operands)))
    : -1 },
#line 8479 "../.././gcc/config/i386/i386.md"
  { "ix86_unary_operator_ok (NEG, SImode, operands)",
    __builtin_constant_p 
#line 8479 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, SImode, operands))
    ? (int) 
#line 8479 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V8SImode, operands) && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V8SImode, operands) && (32 == 64)) && 
#line 262 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V8SImode, operands) && (32 == 64)) && 
#line 262 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
  { "(TARGET_SSE4_2\n\
   && can_create_pseudo_p ()) && ( 1)",
    __builtin_constant_p (
#line 12494 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_2
   && can_create_pseudo_p ()) && 
#line 12497 "../.././gcc/config/i386/sse.md"
( 1))
    ? (int) (
#line 12494 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_2
   && can_create_pseudo_p ()) && 
#line 12497 "../.././gcc/config/i386/sse.md"
( 1))
    : -1 },
#line 13075 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_ARITH (DFmode)\n\
   && COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13075 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (DFmode)
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13075 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (DFmode)
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16HImode, operands) && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16HImode, operands) && (32 == 64)) && 
#line 230 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16HImode, operands) && (32 == 64)) && 
#line 230 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && (32 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 10544 "../.././gcc/config/i386/i386.md"
  { "!TARGET_PARTIAL_REG_STALL\n\
   && (!TARGET_ZERO_EXTEND_WITH_AND || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 10544 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   && (!TARGET_ZERO_EXTEND_WITH_AND || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 10544 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   && (!TARGET_ZERO_EXTEND_WITH_AND || optimize_function_for_size_p (cfun)))
    : -1 },
  { "(ix86_target_stack_probe ()) && (Pmode == DImode)",
    __builtin_constant_p (
#line 16535 "../.././gcc/config/i386/i386.md"
(ix86_target_stack_probe ()) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 16535 "../.././gcc/config/i386/i386.md"
(ix86_target_stack_probe ()) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)\n\
   && (32 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)
   && (32 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)
   && (32 == 64) && 1))
    : -1 },
#line 8194 "../.././gcc/config/i386/i386.md"
  { "TARGET_AVX512F && ix86_match_ccmode (insn, CCCmode)",
    __builtin_constant_p 
#line 8194 "../.././gcc/config/i386/i386.md"
(TARGET_AVX512F && ix86_match_ccmode (insn, CCCmode))
    ? (int) 
#line 8194 "../.././gcc/config/i386/i386.md"
(TARGET_AVX512F && ix86_match_ccmode (insn, CCCmode))
    : -1 },
#line 8984 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && 1",
    __builtin_constant_p 
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1)
    ? (int) 
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)\n\
   && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)
   && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)
   && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V2DImode, operands) && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V2DImode, operands) && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V2DImode, operands) && (16 == 64)))
    : -1 },
  { "(TARGET_SSSE3 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && 1) && 
#line 302 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && 1) && 
#line 302 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 7672 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V2DImode, operands) && 1",
    __builtin_constant_p 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V2DImode, operands) && 1)
    ? (int) 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V2DImode, operands) && 1)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)\n\
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8DImode, operands)
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 1068 "../.././gcc/config/i386/i386.md"
  { "ptr_mode == SImode",
    __builtin_constant_p 
#line 1068 "../.././gcc/config/i386/i386.md"
(ptr_mode == SImode)
    ? (int) 
#line 1068 "../.././gcc/config/i386/i386.md"
(ptr_mode == SImode)
    : -1 },
#line 6836 "../.././gcc/config/i386/i386.md"
  { "(TARGET_80387 && X87_ENABLE_ARITH (SFmode))\n\
    || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 6836 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && X87_ENABLE_ARITH (SFmode))
    || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 6836 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && X87_ENABLE_ARITH (SFmode))
    || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))
    : -1 },
#line 3125 "../.././gcc/config/i386/i386.md"
  { "!(MEM_P (operands[0]) && MEM_P (operands[1]))\n\
   && (!can_create_pseudo_p ()\n\
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)\n\
       || GET_CODE (operands[1]) != CONST_DOUBLE\n\
       || (optimize_function_for_size_p (cfun)\n\
	   && ((!TARGET_SSE_MATH\n\
		&& standard_80387_constant_p (operands[1]) > 0)\n\
	       || (TARGET_SSE_MATH\n\
		   && standard_sse_constant_p (operands[1]))))\n\
       || memory_operand (operands[0], SFmode))",
    __builtin_constant_p 
#line 3125 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1]))
   && (!can_create_pseudo_p ()
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)
       || GET_CODE (operands[1]) != CONST_DOUBLE
       || (optimize_function_for_size_p (cfun)
	   && ((!TARGET_SSE_MATH
		&& standard_80387_constant_p (operands[1]) > 0)
	       || (TARGET_SSE_MATH
		   && standard_sse_constant_p (operands[1]))))
       || memory_operand (operands[0], SFmode)))
    ? (int) 
#line 3125 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1]))
   && (!can_create_pseudo_p ()
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)
       || GET_CODE (operands[1]) != CONST_DOUBLE
       || (optimize_function_for_size_p (cfun)
	   && ((!TARGET_SSE_MATH
		&& standard_80387_constant_p (operands[1]) > 0)
	       || (TARGET_SSE_MATH
		   && standard_sse_constant_p (operands[1]))))
       || memory_operand (operands[0], SFmode)))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V8SImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V8SImode, operands)) && 
#line 225 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 9061 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ASHIFT, SImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))\n\
      == GET_MODE_BITSIZE (SImode)-1",
    __builtin_constant_p 
#line 9061 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    ? (int) 
#line 9061 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    : -1 },
#line 6164 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (MINUS, HImode, operands)\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])",
    __builtin_constant_p 
#line 6164 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, HImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    ? (int) 
#line 6164 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, HImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    : -1 },
#line 666 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_MMX || (TARGET_SSE2 && V4HImode == V1DImode))\n\
   && ix86_binary_operator_ok (PLUS, V4HImode, operands)",
    __builtin_constant_p 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V4HImode == V1DImode))
   && ix86_binary_operator_ok (PLUS, V4HImode, operands))
    ? (int) 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V4HImode == V1DImode))
   && ix86_binary_operator_ok (PLUS, V4HImode, operands))
    : -1 },
#line 8814 "../.././gcc/config/i386/i386.md"
  { "ix86_unary_operator_ok (NOT, SImode, operands)",
    __builtin_constant_p 
#line 8814 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NOT, SImode, operands))
    ? (int) 
#line 8814 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NOT, SImode, operands))
    : -1 },
#line 17681 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSP_TLS_GUARD",
    __builtin_constant_p 
#line 17681 "../.././gcc/config/i386/i386.md"
(TARGET_SSP_TLS_GUARD)
    ? (int) 
#line 17681 "../.././gcc/config/i386/i386.md"
(TARGET_SSP_TLS_GUARD)
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (XOR, V2DFmode, operands)) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V2DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V2DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && (32 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (32 == 64) && 1))
    : -1 },
#line 666 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_MMX || (TARGET_SSE2 && V8QImode == V1DImode))\n\
   && ix86_binary_operator_ok (MINUS, V8QImode, operands)",
    __builtin_constant_p 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V8QImode == V1DImode))
   && ix86_binary_operator_ok (MINUS, V8QImode, operands))
    ? (int) 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V8QImode == V1DImode))
   && ix86_binary_operator_ok (MINUS, V8QImode, operands))
    : -1 },
#line 8138 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_binary_operator_ok (XOR, HImode, operands)",
    __builtin_constant_p 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, HImode, operands))
    ? (int) 
#line 8138 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_binary_operator_ok (XOR, HImode, operands))
    : -1 },
#line 8114 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (IOR, SImode, operands)",
    __builtin_constant_p 
#line 8114 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (IOR, SImode, operands))
    ? (int) 
#line 8114 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (IOR, SImode, operands))
    : -1 },
  { "(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 14832 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 14832 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)\n\
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16SImode, operands)
   && 1 && (V16SImode == V16SFmode || V16SImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V16QImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V16QImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V16QImode, operands))
    : -1 },
#line 9882 "../.././gcc/config/i386/i386.md"
  { "(optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (ASHIFTRT, QImode, operands)",
    __builtin_constant_p 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, QImode, operands))
    ? (int) 
#line 9882 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (ASHIFTRT, QImode, operands))
    : -1 },
#line 11440 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT",
    __builtin_constant_p 
#line 11440 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT)
    ? (int) 
#line 11440 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT)
    : -1 },
#line 2317 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (GET_MODE_NUNITS (V4SImode)\n\
       == GET_MODE_NUNITS (V4SFmode))",
    __builtin_constant_p 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V4SFmode)))
    ? (int) 
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V4SImode)
       == GET_MODE_NUNITS (V4SFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8DFmode, operands) && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8124 "../.././gcc/config/i386/i386.md"
  { "(!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 8124 "../.././gcc/config/i386/i386.md"
((!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 8124 "../.././gcc/config/i386/i386.md"
((!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 4672 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)\n\
       || TARGET_MIX_SSE_I387)",
    __builtin_constant_p 
#line 4672 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387))
    ? (int) 
#line 4672 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (!(SSE_FLOAT_MODE_P (XFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 4319 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 206 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 685 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (US_MINUS, V4HImode, operands)",
    __builtin_constant_p 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (US_MINUS, V4HImode, operands))
    ? (int) 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (US_MINUS, V4HImode, operands))
    : -1 },
#line 3898 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && TARGET_64BIT",
    __builtin_constant_p 
#line 3898 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && TARGET_64BIT)
    ? (int) 
#line 3898 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && TARGET_64BIT)
    : -1 },
  { "(TARGET_SSE4A) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 11487 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4A) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 11487 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4A) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(!TARGET_64BIT && TARGET_SSE2 && TARGET_SSE_MATH\n\
   && optimize_function_for_speed_p (cfun)) && ( reload_completed)",
    __builtin_constant_p (
#line 4370 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_SSE_MATH
   && optimize_function_for_speed_p (cfun)) && 
#line 4373 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 4370 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_SSE_MATH
   && optimize_function_for_speed_p (cfun)) && 
#line 4373 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
#line 2910 "../.././gcc/config/i386/i386.md"
  { "!(MEM_P (operands[0]) && MEM_P (operands[1]))\n\
   && (!can_create_pseudo_p ()\n\
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)\n\
       || GET_CODE (operands[1]) != CONST_DOUBLE\n\
       || (optimize_function_for_size_p (cfun)\n\
	   && standard_80387_constant_p (operands[1]) > 0\n\
	   && !memory_operand (operands[0], XFmode))\n\
       || (!TARGET_MEMORY_MISMATCH_STALL\n\
	   && memory_operand (operands[0], XFmode)))",
    __builtin_constant_p 
#line 2910 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1]))
   && (!can_create_pseudo_p ()
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)
       || GET_CODE (operands[1]) != CONST_DOUBLE
       || (optimize_function_for_size_p (cfun)
	   && standard_80387_constant_p (operands[1]) > 0
	   && !memory_operand (operands[0], XFmode))
       || (!TARGET_MEMORY_MISMATCH_STALL
	   && memory_operand (operands[0], XFmode))))
    ? (int) 
#line 2910 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1]))
   && (!can_create_pseudo_p ()
       || (ix86_cmodel == CM_MEDIUM || ix86_cmodel == CM_LARGE)
       || GET_CODE (operands[1]) != CONST_DOUBLE
       || (optimize_function_for_size_p (cfun)
	   && standard_80387_constant_p (operands[1]) > 0
	   && !memory_operand (operands[0], XFmode))
       || (!TARGET_MEMORY_MISMATCH_STALL
	   && memory_operand (operands[0], XFmode))))
    : -1 },
#line 57 "../.././gcc/config/i386/sync.md"
  { "TARGET_SSE || TARGET_3DNOW_A",
    __builtin_constant_p 
#line 57 "../.././gcc/config/i386/sync.md"
(TARGET_SSE || TARGET_3DNOW_A)
    ? (int) 
#line 57 "../.././gcc/config/i386/sync.md"
(TARGET_SSE || TARGET_3DNOW_A)
    : -1 },
  { "((optimize_function_for_size_p (cfun)\n\
    || !TARGET_PARTIAL_FLAG_REG_STALL\n\
    || (operands[2] == const1_rtx\n\
	&& TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9945 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9945 "../.././gcc/config/i386/i386.md"
((optimize_function_for_size_p (cfun)
    || !TARGET_PARTIAL_FLAG_REG_STALL
    || (operands[2] == const1_rtx
	&& TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 510 "../.././gcc/config/i386/sync.md"
  { "(unsigned HOST_WIDE_INT) INTVAL (operands[1])\n\
   == -(unsigned HOST_WIDE_INT) INTVAL (operands[2])",
    __builtin_constant_p 
#line 510 "../.././gcc/config/i386/sync.md"
((unsigned HOST_WIDE_INT) INTVAL (operands[1])
   == -(unsigned HOST_WIDE_INT) INTVAL (operands[2]))
    ? (int) 
#line 510 "../.././gcc/config/i386/sync.md"
((unsigned HOST_WIDE_INT) INTVAL (operands[1])
   == -(unsigned HOST_WIDE_INT) INTVAL (operands[2]))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V16SFmode, operands) && (64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
  { "((TARGET_SINGLE_POP || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && ((((((((word_mode == DImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == DImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17330 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17330 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 8725 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && (!TARGET_SSE_MATH || TARGET_MIX_SSE_I387)",
    __builtin_constant_p 
#line 8725 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && (!TARGET_SSE_MATH || TARGET_MIX_SSE_I387))
    ? (int) 
#line 8725 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && (!TARGET_SSE_MATH || TARGET_MIX_SSE_I387))
    : -1 },
  { "(ix86_match_ccmode (insn, CCNOmode)\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8263 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8263 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(ix86_binary_operator_ok (ROTATERT, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10101 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10101 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8062 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (XOR, SImode, operands)",
    __builtin_constant_p 
#line 8062 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (XOR, SImode, operands))
    ? (int) 
#line 8062 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (XOR, SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8HImode, operands) && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8HImode, operands) && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8HImode, operands) && (16 == 64)))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V16SFmode)\n\
       == GET_MODE_NUNITS (V8DImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V8DImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V16SFmode)
       == GET_MODE_NUNITS (V8DImode)))
    : -1 },
  { "(ix86_match_ccmode (insn, CCZmode)\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 5528 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCZmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 5528 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCZmode)
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16SImode, operands)
   && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 7695 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V16QImode, operands)",
    __builtin_constant_p 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V16QImode, operands))
    ? (int) 
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V16QImode, operands))
    : -1 },
#line 16799 "../.././gcc/config/i386/i386.md"
  { "!(TARGET_PUSH_MEMORY || optimize_insn_for_size_p ())\n\
   && !RTX_FRAME_RELATED_P (peep2_next_insn (0))",
    __builtin_constant_p 
#line 16799 "../.././gcc/config/i386/i386.md"
(!(TARGET_PUSH_MEMORY || optimize_insn_for_size_p ())
   && !RTX_FRAME_RELATED_P (peep2_next_insn (0)))
    ? (int) 
#line 16799 "../.././gcc/config/i386/i386.md"
(!(TARGET_PUSH_MEMORY || optimize_insn_for_size_p ())
   && !RTX_FRAME_RELATED_P (peep2_next_insn (0)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && (64 == 64)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (64 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8984 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (64 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 8526 "../.././gcc/config/i386/i386.md"
  { "ix86_unary_operator_ok (NEG, QImode, operands)\n\
   && mode_signbit_p (QImode, operands[2])",
    __builtin_constant_p 
#line 8526 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, QImode, operands)
   && mode_signbit_p (QImode, operands[2]))
    ? (int) 
#line 8526 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, QImode, operands)
   && mode_signbit_p (QImode, operands[2]))
    : -1 },
  { "(TARGET_POPCNT\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && ( reload_completed)",
    __builtin_constant_p (
#line 12358 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 12361 "../.././gcc/config/i386/i386.md"
( reload_completed))
    ? (int) (
#line 12358 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 12361 "../.././gcc/config/i386/i386.md"
( reload_completed))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSSE3 && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && (32 == 64)) && 
#line 301 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 11431 "../.././gcc/config/i386/sse.md"
(TARGET_SSSE3 && (32 == 64)) && 
#line 301 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
  { "(TARGET_AVX) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 11986 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 11986 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 8875 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2\n\
   && (GET_MODE_NUNITS (V16HImode)\n\
       == GET_MODE_NUNITS (V4DImode))",
    __builtin_constant_p 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V4DImode)))
    ? (int) 
#line 8875 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2
   && (GET_MODE_NUNITS (V16HImode)
       == GET_MODE_NUNITS (V4DImode)))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V16SImode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 11275 "../.././gcc/config/i386/i386.md"
  { "(peep2_reg_dead_p (3, operands[1])\n\
    || operands_match_p (operands[1], operands[3]))\n\
   && ! reg_overlap_mentioned_p (operands[3], operands[0])",
    __builtin_constant_p 
#line 11275 "../.././gcc/config/i386/i386.md"
((peep2_reg_dead_p (3, operands[1])
    || operands_match_p (operands[1], operands[3]))
   && ! reg_overlap_mentioned_p (operands[3], operands[0]))
    ? (int) 
#line 11275 "../.././gcc/config/i386/i386.md"
((peep2_reg_dead_p (3, operands[1])
    || operands_match_p (operands[1], operands[3]))
   && ! reg_overlap_mentioned_p (operands[3], operands[0]))
    : -1 },
#line 12456 "../.././gcc/config/i386/i386.md"
  { "TARGET_BSWAP",
    __builtin_constant_p 
#line 12456 "../.././gcc/config/i386/i386.md"
(TARGET_BSWAP)
    ? (int) 
#line 12456 "../.././gcc/config/i386/i386.md"
(TARGET_BSWAP)
    : -1 },
#line 15467 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F",
    __builtin_constant_p 
#line 15467 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)
    ? (int) 
#line 15467 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)
    : -1 },
#line 666 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_MMX || (TARGET_SSE2 && V2SImode == V1DImode))\n\
   && ix86_binary_operator_ok (PLUS, V2SImode, operands)",
    __builtin_constant_p 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V2SImode == V1DImode))
   && ix86_binary_operator_ok (PLUS, V2SImode, operands))
    ? (int) 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V2SImode == V1DImode))
   && ix86_binary_operator_ok (PLUS, V2SImode, operands))
    : -1 },
#line 8526 "../.././gcc/config/i386/i386.md"
  { "ix86_unary_operator_ok (NEG, HImode, operands)\n\
   && mode_signbit_p (HImode, operands[2])",
    __builtin_constant_p 
#line 8526 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, HImode, operands)
   && mode_signbit_p (HImode, operands[2]))
    ? (int) 
#line 8526 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, HImode, operands)
   && mode_signbit_p (HImode, operands[2]))
    : -1 },
  { "(TARGET_SSE2 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1041 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1) && 
#line 239 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1041 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1) && 
#line 239 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 5408 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT\n\
   && reload_completed && ix86_avoid_lea_for_add (insn, operands)",
    __builtin_constant_p 
#line 5408 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && reload_completed && ix86_avoid_lea_for_add (insn, operands))
    ? (int) 
#line 5408 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && reload_completed && ix86_avoid_lea_for_add (insn, operands))
    : -1 },
  { "((TARGET_SSE) && (TARGET_AVX)) && ( reload_completed)",
    __builtin_constant_p ((
#line 1260 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)) && 
#line 1262 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) ((
#line 1260 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)) && 
#line 1262 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)\n\
   && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && (TARGET_AVX512F)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)
   && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V8DFmode, operands)
   && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (XOR, V2DImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (XOR, V2DImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (XOR, V2DImode, operands)))
    : -1 },
#line 658 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX || (TARGET_SSE2 && V8QImode == V1DImode)",
    __builtin_constant_p 
#line 658 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX || (TARGET_SSE2 && V8QImode == V1DImode))
    ? (int) 
#line 658 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX || (TARGET_SSE2 && V8QImode == V1DImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE2 && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
#line 16826 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && ((SImode == HImode\n\
       && TARGET_LCP_STALL)\n\
       || (TARGET_SPLIT_LONG_MOVES\n\
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))",
    __builtin_constant_p 
#line 16826 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((SImode == HImode
       && TARGET_LCP_STALL)
       || (TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn)))
    ? (int) 
#line 16826 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((SImode == HImode
       && TARGET_LCP_STALL)
       || (TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn)))
    : -1 },
  { "(TARGET_LZCNT\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 12035 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 12035 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 6183 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCmode)\n\
   && ix86_binary_operator_ok (MINUS, QImode, operands)",
    __builtin_constant_p 
#line 6183 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, QImode, operands))
    ? (int) 
#line 6183 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, QImode, operands))
    : -1 },
#line 1438 "../.././gcc/config/i386/mmx.md"
  { "TARGET_SSE || TARGET_3DNOW",
    __builtin_constant_p 
#line 1438 "../.././gcc/config/i386/mmx.md"
(TARGET_SSE || TARGET_3DNOW)
    ? (int) 
#line 1438 "../.././gcc/config/i386/mmx.md"
(TARGET_SSE || TARGET_3DNOW)
    : -1 },
#line 1159 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCGOCmode)",
    __builtin_constant_p 
#line 1159 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode))
    ? (int) 
#line 1159 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode))
    : -1 },
  { "(ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (MINUS, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 6087 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 6087 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (MINUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)\n\
   && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)
   && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)
   && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 3549 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && TARGET_64BIT",
    __builtin_constant_p 
#line 3549 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && TARGET_64BIT)
    ? (int) 
#line 3549 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && TARGET_64BIT)
    : -1 },
#line 8797 "../.././gcc/config/i386/i386.md"
  { "((SSE_FLOAT_MODE_P (TFmode) && TARGET_SSE_MATH)\n\
    || (TARGET_SSE && (TFmode == TFmode)))\n\
   && reload_completed",
    __builtin_constant_p 
#line 8797 "../.././gcc/config/i386/i386.md"
(((SSE_FLOAT_MODE_P (TFmode) && TARGET_SSE_MATH)
    || (TARGET_SSE && (TFmode == TFmode)))
   && reload_completed)
    ? (int) 
#line 8797 "../.././gcc/config/i386/i386.md"
(((SSE_FLOAT_MODE_P (TFmode) && TARGET_SSE_MATH)
    || (TARGET_SSE && (TFmode == TFmode)))
   && reload_completed)
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)\n\
   && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V4DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 17484 "../.././gcc/config/i386/i386.md"
  { "TARGET_SLOW_IMUL_IMM8 && optimize_insn_for_speed_p ()\n\
   && satisfies_constraint_K (operands[2])",
    __builtin_constant_p 
#line 17484 "../.././gcc/config/i386/i386.md"
(TARGET_SLOW_IMUL_IMM8 && optimize_insn_for_speed_p ()
   && satisfies_constraint_K (operands[2]))
    ? (int) 
#line 17484 "../.././gcc/config/i386/i386.md"
(TARGET_SLOW_IMUL_IMM8 && optimize_insn_for_speed_p ()
   && satisfies_constraint_K (operands[2]))
    : -1 },
#line 16316 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && TARGET_80387 && TARGET_CMOVE\n\
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE\n\
   && (MEM_P (operands[2]) || MEM_P (operands[3]))\n\
   && can_create_pseudo_p ()\n\
   && optimize_insn_for_speed_p ()",
    __builtin_constant_p 
#line 16316 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_80387 && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && (MEM_P (operands[2]) || MEM_P (operands[3]))
   && can_create_pseudo_p ()
   && optimize_insn_for_speed_p ())
    ? (int) 
#line 16316 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && TARGET_80387 && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && (MEM_P (operands[2]) || MEM_P (operands[3]))
   && can_create_pseudo_p ()
   && optimize_insn_for_speed_p ())
    : -1 },
#line 6860 "../.././gcc/config/i386/i386.md"
  { "(TARGET_80387 && X87_ENABLE_ARITH (SFmode))\n\
    || TARGET_SSE_MATH",
    __builtin_constant_p 
#line 6860 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && X87_ENABLE_ARITH (SFmode))
    || TARGET_SSE_MATH)
    ? (int) 
#line 6860 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && X87_ENABLE_ARITH (SFmode))
    || TARGET_SSE_MATH)
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V8DImode)\n\
       == GET_MODE_NUNITS (V8DImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V8DImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V8DImode)
       == GET_MODE_NUNITS (V8DImode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)\n\
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V16HImode, operands)
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    : -1 },
#line 10101 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ROTATERT, SImode, operands)",
    __builtin_constant_p 
#line 10101 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, SImode, operands))
    ? (int) 
#line 10101 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, SImode, operands))
    : -1 },
  { "(!SIBLING_CALL_P (insn)) && (word_mode == DImode)",
    __builtin_constant_p (
#line 11411 "../.././gcc/config/i386/i386.md"
(!SIBLING_CALL_P (insn)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode))
    ? (int) (
#line 11411 "../.././gcc/config/i386/i386.md"
(!SIBLING_CALL_P (insn)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    : -1 },
#line 8714 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (reload_completed\n\
       || !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH))",
    __builtin_constant_p 
#line 8714 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (reload_completed
       || !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)))
    ? (int) 
#line 8714 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (reload_completed
       || !(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)))
    : -1 },
#line 8757 "../.././gcc/config/i386/i386.md"
  { "(SSE_FLOAT_MODE_P (TFmode) && TARGET_SSE_MATH)\n\
   || (TARGET_SSE && (TFmode == TFmode))",
    __builtin_constant_p 
#line 8757 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (TFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (TFmode == TFmode)))
    ? (int) 
#line 8757 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (TFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (TFmode == TFmode)))
    : -1 },
#line 905 "../.././gcc/config/i386/i386.md"
  { "TARGET_HIMODE_MATH",
    __builtin_constant_p 
#line 905 "../.././gcc/config/i386/i386.md"
(TARGET_HIMODE_MATH)
    ? (int) 
#line 905 "../.././gcc/config/i386/i386.md"
(TARGET_HIMODE_MATH)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
  { "(TARGET_AVX512F) && ((64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3005 "../.././gcc/config/i386/sse.md"
((64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3005 "../.././gcc/config/i386/sse.md"
((64 == 64) && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    : -1 },
#line 4266 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 || (TARGET_64BIT && SSE_FLOAT_MODE_P (SFmode))",
    __builtin_constant_p 
#line 4266 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (TARGET_64BIT && SSE_FLOAT_MODE_P (SFmode)))
    ? (int) 
#line 4266 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (TARGET_64BIT && SSE_FLOAT_MODE_P (SFmode)))
    : -1 },
#line 8909 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_2",
    __builtin_constant_p 
#line 8909 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_2)
    ? (int) 
#line 8909 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_2)
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)\n\
   && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)
   && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMAX, V16SFmode, operands)
   && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V16SFmode)\n\
       || register_operand (operands[1], V16SFmode))) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V16SFmode)
       || register_operand (operands[1], V16SFmode))) && 
#line 150 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V16SFmode)
       || register_operand (operands[1], V16SFmode))) && 
#line 150 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 9563 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (LSHIFTRT, SImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))\n\
      == GET_MODE_BITSIZE (SImode)-1",
    __builtin_constant_p 
#line 9563 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    ? (int) 
#line 9563 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, SImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (SImode)-1))
      == GET_MODE_BITSIZE (SImode)-1)
    : -1 },
  { "(TARGET_ROUND && !flag_trapping_math) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 12176 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND && !flag_trapping_math) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 12176 "../.././gcc/config/i386/sse.md"
(TARGET_ROUND && !flag_trapping_math) && 
#line 199 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (XOR, V8SFmode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V8SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (XOR, V8SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 3449 "../.././gcc/config/i386/i386.md"
  { "!TARGET_64BIT && reload_completed\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))\n\
   && !(MMX_REG_P (operands[0]) || SSE_REG_P (operands[0]))",
    __builtin_constant_p 
#line 3449 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && reload_completed
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))
   && !(MMX_REG_P (operands[0]) || SSE_REG_P (operands[0])))
    ? (int) 
#line 3449 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT && reload_completed
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))
   && !(MMX_REG_P (operands[0]) || SSE_REG_P (operands[0])))
    : -1 },
#line 8114 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (XOR, SImode, operands)",
    __builtin_constant_p 
#line 8114 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (XOR, SImode, operands))
    ? (int) 
#line 8114 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (XOR, SImode, operands))
    : -1 },
#line 4327 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && !(SSE_FLOAT_MODE_P (XFmode) && (!TARGET_FISTTP || TARGET_SSE_MATH))",
    __builtin_constant_p 
#line 4327 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !(SSE_FLOAT_MODE_P (XFmode) && (!TARGET_FISTTP || TARGET_SSE_MATH)))
    ? (int) 
#line 4327 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && !(SSE_FLOAT_MODE_P (XFmode) && (!TARGET_FISTTP || TARGET_SSE_MATH)))
    : -1 },
  { "(TARGET_SSE && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 928 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE && reload_completed) && (Pmode == SImode)",
    __builtin_constant_p (
#line 2691 "../.././gcc/config/i386/i386.md"
(TARGET_SSE && reload_completed) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 2691 "../.././gcc/config/i386/i386.md"
(TARGET_SSE && reload_completed) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
#line 7874 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_binary_operator_ok (MULT, V4SImode, operands)",
    __builtin_constant_p 
#line 7874 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MULT, V4SImode, operands))
    ? (int) 
#line 7874 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MULT, V4SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16QImode, operands) && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16QImode, operands) && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V16QImode, operands) && (16 == 64)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1638 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1638 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_BMI2) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 12168 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 12168 "../.././gcc/config/i386/i386.md"
(TARGET_BMI2) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V8SFmode, operands) && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 11255 "../.././gcc/config/i386/i386.md"
  { "REGNO (operands[1]) == REGNO (operands[3])\n\
   && ! reg_overlap_mentioned_p (operands[3], operands[0])",
    __builtin_constant_p 
#line 11255 "../.././gcc/config/i386/i386.md"
(REGNO (operands[1]) == REGNO (operands[3])
   && ! reg_overlap_mentioned_p (operands[3], operands[0]))
    ? (int) 
#line 11255 "../.././gcc/config/i386/i386.md"
(REGNO (operands[1]) == REGNO (operands[3])
   && ! reg_overlap_mentioned_p (operands[3], operands[0]))
    : -1 },
  { "((TARGET_USE_BT || optimize_function_for_size_p (cfun)) && (TARGET_64BIT)) && ( 1)",
    __builtin_constant_p ((
#line 10795 "../.././gcc/config/i386/i386.md"
(TARGET_USE_BT || optimize_function_for_size_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 10797 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) ((
#line 10795 "../.././gcc/config/i386/i386.md"
(TARGET_USE_BT || optimize_function_for_size_p (cfun)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT)) && 
#line 10797 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
#line 11557 "../.././gcc/config/i386/i386.md"
  { "ix86_can_use_return_insn_p ()",
    __builtin_constant_p 
#line 11557 "../.././gcc/config/i386/i386.md"
(ix86_can_use_return_insn_p ())
    ? (int) 
#line 11557 "../.././gcc/config/i386/i386.md"
(ix86_can_use_return_insn_p ())
    : -1 },
  { "(optimize_insn_for_size_p ()\n\
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == SImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17393 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_size_p ()
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17393 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_size_p ()
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
  { "((TARGET_USE_BT || optimize_function_for_size_p (cfun))\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))\n\
      == GET_MODE_BITSIZE (DImode)-1) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10829 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10829 "../.././gcc/config/i386/i386.md"
((TARGET_USE_BT || optimize_function_for_size_p (cfun))
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 14997 "../.././gcc/config/i386/sse.md"
  { "TARGET_F16C",
    __builtin_constant_p 
#line 14997 "../.././gcc/config/i386/sse.md"
(TARGET_F16C)
    ? (int) 
#line 14997 "../.././gcc/config/i386/sse.md"
(TARGET_F16C)
    : -1 },
#line 7405 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 7405 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 7405 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
#line 14837 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH\n\
       && HImode != HImode \n\
       && ((HImode != DImode) || TARGET_64BIT)\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && HImode != HImode 
       && ((HImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && HImode != HImode 
       && ((HImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V16HImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (SS_PLUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 15606 "../.././gcc/config/i386/sse.md"
  { "TARGET_SHA",
    __builtin_constant_p 
#line 15606 "../.././gcc/config/i386/sse.md"
(TARGET_SHA)
    ? (int) 
#line 15606 "../.././gcc/config/i386/sse.md"
(TARGET_SHA)
    : -1 },
#line 12351 "../.././gcc/config/i386/i386.md"
  { "TARGET_POPCNT",
    __builtin_constant_p 
#line 12351 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT)
    ? (int) 
#line 12351 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT)
    : -1 },
#line 17465 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT\n\
   && TARGET_SLOW_IMUL_IMM32_MEM && optimize_insn_for_speed_p ()\n\
   && !satisfies_constraint_K (operands[2])",
    __builtin_constant_p 
#line 17465 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && TARGET_SLOW_IMUL_IMM32_MEM && optimize_insn_for_speed_p ()
   && !satisfies_constraint_K (operands[2]))
    ? (int) 
#line 17465 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && TARGET_SLOW_IMUL_IMM32_MEM && optimize_insn_for_speed_p ()
   && !satisfies_constraint_K (operands[2]))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (XOR, V4SImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (XOR, V4SImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (XOR, V4SImode, operands)))
    : -1 },
#line 6215 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (PLUS, SImode, operands)",
    __builtin_constant_p 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, SImode, operands))
    ? (int) 
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, SImode, operands))
    : -1 },
#line 1685 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ((optimize > 0 && flag_peephole2)\n\
		    ? epilogue_completed : reload_completed)\n\
   && !symbolic_operand (operands[1], DImode)\n\
   && !x86_64_immediate_operand (operands[1], DImode)",
    __builtin_constant_p 
#line 1685 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ((optimize > 0 && flag_peephole2)
		    ? epilogue_completed : reload_completed)
   && !symbolic_operand (operands[1], DImode)
   && !x86_64_immediate_operand (operands[1], DImode))
    ? (int) 
#line 1685 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ((optimize > 0 && flag_peephole2)
		    ? epilogue_completed : reload_completed)
   && !symbolic_operand (operands[1], DImode)
   && !x86_64_immediate_operand (operands[1], DImode))
    : -1 },
  { "(TARGET_AVX512F) && ((1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 2774 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
(1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 2774 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    : -1 },
#line 1306 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && 1 && 1",
    __builtin_constant_p 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && 1 && 1)
    ? (int) 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4SFmode, operands) && 1 && 1)
    : -1 },
  { "(TARGET_AVX2) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 14023 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2) && 
#line 428 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 14023 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2) && 
#line 428 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 13075 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_ARITH (SFmode)\n\
   && COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13075 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (SFmode)
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13075 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_ARITH (SFmode)
   && COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (64 == 64)\n\
   && avx_vpermilp_parallel (operands[2], V16SFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64)
   && avx_vpermilp_parallel (operands[2], V16SFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14359 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (64 == 64)
   && avx_vpermilp_parallel (operands[2], V16SFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 10501 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && !(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 10501 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 10501 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
  { "(ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (PLUS, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 5436 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 5436 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (PLUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 14790 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 332 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 14790 "../.././gcc/config/i386/sse.md"
(TARGET_AVX) && 
#line 332 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 6183 "../.././gcc/config/i386/i386.md"
  { "ix86_match_ccmode (insn, CCmode)\n\
   && ix86_binary_operator_ok (MINUS, SImode, operands)",
    __builtin_constant_p 
#line 6183 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, SImode, operands))
    ? (int) 
#line 6183 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCmode)
   && ix86_binary_operator_ok (MINUS, SImode, operands))
    : -1 },
  { "(TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)\n\
   && 1 && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V2DFmode, operands)
   && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX512F) && (((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(!TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && ( reload_completed)",
    __builtin_constant_p (
#line 865 "../.././gcc/config/i386/sse.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && 
#line 867 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) (
#line 865 "../.././gcc/config/i386/sse.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC) && 
#line 867 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
  { "(TARGET_AVX512F) && (1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3005 "../.././gcc/config/i386/sse.md"
(1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3005 "../.././gcc/config/i386/sse.md"
(1 && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512ER) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 12957 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 12957 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512ER) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_PREFETCHWT1) && (Pmode == DImode)",
    __builtin_constant_p (
#line 17628 "../.././gcc/config/i386/i386.md"
(TARGET_PREFETCHWT1) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 17628 "../.././gcc/config/i386/i386.md"
(TARGET_PREFETCHWT1) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(TARGET_MOVBE\n\
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 12441 "../.././gcc/config/i386/i386.md"
(TARGET_MOVBE
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 12441 "../.././gcc/config/i386/i386.md"
(TARGET_MOVBE
   && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (XOR, V16SImode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (XOR, V16SImode, operands)) && 
#line 222 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 685 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (SS_MINUS, V8QImode, operands)",
    __builtin_constant_p 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (SS_MINUS, V8QImode, operands))
    ? (int) 
#line 685 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (SS_MINUS, V8QImode, operands))
    : -1 },
  { "(TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V2DFmode)\n\
       == GET_MODE_NUNITS (V8HImode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V8HImode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V2DFmode)
       == GET_MODE_NUNITS (V8HImode))) && 
#line 164 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 8892 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2\n\
   && (GET_MODE_NUNITS (V8HImode)\n\
       == GET_MODE_NUNITS (V4SImode))",
    __builtin_constant_p 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V4SImode)))
    ? (int) 
#line 8892 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && (GET_MODE_NUNITS (V8HImode)
       == GET_MODE_NUNITS (V4SImode)))
    : -1 },
#line 3160 "../.././gcc/config/i386/sse.md"
  { "(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && 1",
    __builtin_constant_p 
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && 1)
    ? (int) 
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && 1)
    : -1 },
  { "(TARGET_SSE || TARGET_3DNOW_A) && (Pmode == SImode)",
    __builtin_constant_p (
#line 1543 "../.././gcc/config/i386/mmx.md"
(TARGET_SSE || TARGET_3DNOW_A) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 1543 "../.././gcc/config/i386/mmx.md"
(TARGET_SSE || TARGET_3DNOW_A) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F\n\
   && (INTVAL (operands[3]) == (INTVAL (operands[7]) - 4)\n\
       && INTVAL (operands[4]) == (INTVAL (operands[8]) - 4)\n\
       && INTVAL (operands[5]) == (INTVAL (operands[9]) - 4)\n\
       && INTVAL (operands[6]) == (INTVAL (operands[10]) - 4)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[11]) - 8)\n\
       && INTVAL (operands[4]) == (INTVAL (operands[12]) - 8)\n\
       && INTVAL (operands[5]) == (INTVAL (operands[13]) - 8)\n\
       && INTVAL (operands[6]) == (INTVAL (operands[14]) - 8)\n\
       && INTVAL (operands[3]) == (INTVAL (operands[15]) - 12)\n\
       && INTVAL (operands[4]) == (INTVAL (operands[16]) - 12)\n\
       && INTVAL (operands[5]) == (INTVAL (operands[17]) - 12)\n\
       && INTVAL (operands[6]) == (INTVAL (operands[18]) - 12)))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 6946 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[7]) - 4)
       && INTVAL (operands[4]) == (INTVAL (operands[8]) - 4)
       && INTVAL (operands[5]) == (INTVAL (operands[9]) - 4)
       && INTVAL (operands[6]) == (INTVAL (operands[10]) - 4)
       && INTVAL (operands[3]) == (INTVAL (operands[11]) - 8)
       && INTVAL (operands[4]) == (INTVAL (operands[12]) - 8)
       && INTVAL (operands[5]) == (INTVAL (operands[13]) - 8)
       && INTVAL (operands[6]) == (INTVAL (operands[14]) - 8)
       && INTVAL (operands[3]) == (INTVAL (operands[15]) - 12)
       && INTVAL (operands[4]) == (INTVAL (operands[16]) - 12)
       && INTVAL (operands[5]) == (INTVAL (operands[17]) - 12)
       && INTVAL (operands[6]) == (INTVAL (operands[18]) - 12))))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 6946 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (INTVAL (operands[3]) == (INTVAL (operands[7]) - 4)
       && INTVAL (operands[4]) == (INTVAL (operands[8]) - 4)
       && INTVAL (operands[5]) == (INTVAL (operands[9]) - 4)
       && INTVAL (operands[6]) == (INTVAL (operands[10]) - 4)
       && INTVAL (operands[3]) == (INTVAL (operands[11]) - 8)
       && INTVAL (operands[4]) == (INTVAL (operands[12]) - 8)
       && INTVAL (operands[5]) == (INTVAL (operands[13]) - 8)
       && INTVAL (operands[6]) == (INTVAL (operands[14]) - 8)
       && INTVAL (operands[3]) == (INTVAL (operands[15]) - 12)
       && INTVAL (operands[4]) == (INTVAL (operands[16]) - 12)
       && INTVAL (operands[5]) == (INTVAL (operands[17]) - 12)
       && INTVAL (operands[6]) == (INTVAL (operands[18]) - 12))))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16QImode, operands) && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16QImode, operands) && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16QImode, operands) && (16 == 64)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F && TARGET_64BIT) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3581 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && TARGET_64BIT) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3581 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && TARGET_64BIT) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
#line 8564 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 8564 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 8564 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && !(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8433 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (64 == 64) && 1) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4DFmode, operands) && 1 && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(ix86_unary_operator_ok (NOT, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8814 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NOT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8814 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NOT, DImode, operands)) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 14532 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && avx_vperm2f128_parallel (operands[3], V4DFmode)",
    __builtin_constant_p 
#line 14532 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && avx_vperm2f128_parallel (operands[3], V4DFmode))
    ? (int) 
#line 14532 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && avx_vperm2f128_parallel (operands[3], V4DFmode))
    : -1 },
#line 887 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_SSE || TARGET_3DNOW_A)\n\
   && ix86_binary_operator_ok (SMAX, V4HImode, operands)",
    __builtin_constant_p 
#line 887 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (SMAX, V4HImode, operands))
    ? (int) 
#line 887 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (SMAX, V4HImode, operands))
    : -1 },
#line 8688 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (EQ, V2DImode, operands)",
    __builtin_constant_p 
#line 8688 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (EQ, V2DImode, operands))
    ? (int) 
#line 8688 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (EQ, V2DImode, operands))
    : -1 },
#line 3179 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F && 1 && 1",
    __builtin_constant_p 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1 && 1)
    ? (int) 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1 && 1)
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V16HImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_PLUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 16353 "../.././gcc/config/i386/i386.md"
  { "(SFmode != DFmode || TARGET_64BIT)\n\
   && TARGET_80387 && TARGET_CMOVE\n\
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE\n\
   && optimize_insn_for_speed_p ()",
    __builtin_constant_p 
#line 16353 "../.././gcc/config/i386/i386.md"
((SFmode != DFmode || TARGET_64BIT)
   && TARGET_80387 && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && optimize_insn_for_speed_p ())
    ? (int) 
#line 16353 "../.././gcc/config/i386/i386.md"
((SFmode != DFmode || TARGET_64BIT)
   && TARGET_80387 && TARGET_CMOVE
   && TARGET_AVOID_MEM_OPND_FOR_CMOVE
   && optimize_insn_for_speed_p ())
    : -1 },
#line 4845 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSE2 && TARGET_SSE_MATH\n\
   && TARGET_SSE_PARTIAL_REG_DEPENDENCY\n\
   && optimize_function_for_speed_p (cfun)\n\
   && SSE_REG_P (operands[0])\n\
   && (!SSE_REG_P (operands[1])\n\
       || REGNO (operands[0]) != REGNO (operands[1]))",
    __builtin_constant_p 
#line 4845 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH
   && TARGET_SSE_PARTIAL_REG_DEPENDENCY
   && optimize_function_for_speed_p (cfun)
   && SSE_REG_P (operands[0])
   && (!SSE_REG_P (operands[1])
       || REGNO (operands[0]) != REGNO (operands[1])))
    ? (int) 
#line 4845 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH
   && TARGET_SSE_PARTIAL_REG_DEPENDENCY
   && optimize_function_for_speed_p (cfun)
   && SSE_REG_P (operands[0])
   && (!SSE_REG_P (operands[1])
       || REGNO (operands[0]) != REGNO (operands[1])))
    : -1 },
#line 1483 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && (TARGET_USE_SIMODE_FIOP\n\
       || optimize_function_for_size_p (cfun))",
    __builtin_constant_p 
#line 1483 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    ? (int) 
#line 1483 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && (TARGET_USE_SIMODE_FIOP
       || optimize_function_for_size_p (cfun)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX && (32 == 64)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 14335 "../.././gcc/config/i386/sse.md"
(TARGET_AVX && (32 == 64)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && !flag_finite_math_only\n\
   && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1619 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !flag_finite_math_only
   && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V16HImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (V16SImode == V16SFmode || V16SImode == V8DFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SImode == V16SFmode || V16SImode == V8DFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SImode == V16SFmode || V16SImode == V8DFmode)))
    : -1 },
#line 12667 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_cmodel == CM_LARGE_PIC && !TARGET_PECOFF\n\
   && GET_CODE (operands[3]) == CONST\n\
   && GET_CODE (XEXP (operands[3], 0)) == UNSPEC\n\
   && XINT (XEXP (operands[3], 0), 1) == UNSPEC_PLTOFF",
    __builtin_constant_p 
#line 12667 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_cmodel == CM_LARGE_PIC && !TARGET_PECOFF
   && GET_CODE (operands[3]) == CONST
   && GET_CODE (XEXP (operands[3], 0)) == UNSPEC
   && XINT (XEXP (operands[3], 0), 1) == UNSPEC_PLTOFF)
    ? (int) 
#line 12667 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_cmodel == CM_LARGE_PIC && !TARGET_PECOFF
   && GET_CODE (operands[3]) == CONST
   && GET_CODE (XEXP (operands[3], 0)) == UNSPEC
   && XINT (XEXP (operands[3], 0), 1) == UNSPEC_PLTOFF)
    : -1 },
  { "(TARGET_AVX2 && 1) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1) && 
#line 326 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && 1) && 
#line 326 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 16238 "../.././gcc/config/i386/i386.md"
  { "(TARGET_80387 && TARGET_CMOVE)\n\
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 16238 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && TARGET_CMOVE)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 16238 "../.././gcc/config/i386/i386.md"
((TARGET_80387 && TARGET_CMOVE)
   || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && 1 && 1) && (TARGET_AVX)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_AVX512F && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 3567 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 3567 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && 1) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)\n\
   && ix86_binary_operator_ok (XOR, V16HImode, operands)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (XOR, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (XOR, V16HImode, operands)) && 
#line 224 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
#line 9913 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT\n\
   && (optimize_function_for_size_p (cfun)\n\
       || !TARGET_PARTIAL_FLAG_REG_STALL\n\
       || (operands[2] == const1_rtx\n\
	   && TARGET_SHIFT1))\n\
   && ix86_match_ccmode (insn, CCGOCmode)\n\
   && ix86_binary_operator_ok (LSHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9913 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && (optimize_function_for_size_p (cfun)
       || !TARGET_PARTIAL_FLAG_REG_STALL
       || (operands[2] == const1_rtx
	   && TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, SImode, operands))
    ? (int) 
#line 9913 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && (optimize_function_for_size_p (cfun)
       || !TARGET_PARTIAL_FLAG_REG_STALL
       || (operands[2] == const1_rtx
	   && TARGET_SHIFT1))
   && ix86_match_ccmode (insn, CCGOCmode)
   && ix86_binary_operator_ok (LSHIFTRT, SImode, operands))
    : -1 },
#line 16810 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && ((HImode == HImode\n\
       && TARGET_LCP_STALL)\n\
       || (!TARGET_USE_MOV0\n\
          && TARGET_SPLIT_LONG_MOVES\n\
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 16810 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((HImode == HImode
       && TARGET_LCP_STALL)
       || (!TARGET_USE_MOV0
          && TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 16810 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((HImode == HImode
       && TARGET_LCP_STALL)
       || (!TARGET_USE_MOV0
          && TARGET_SPLIT_LONG_MOVES
          && get_attr_length (insn) >= ix86_cur_cost ()->large_insn))
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
  { "(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH\n\
   && !flag_trapping_math) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 15224 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
   && !flag_trapping_math) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 15224 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH
   && !flag_trapping_math) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16HImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 10715 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 10715 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V16HImode, operands)) && 
#line 292 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
#line 10224 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ROTATE, QImode, operands)",
    __builtin_constant_p 
#line 10224 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, QImode, operands))
    ? (int) 
#line 10224 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, QImode, operands))
    : -1 },
  { "(TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (AND, V32QImode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V32QImode, operands)) && 
#line 223 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (AND, V32QImode, operands)) && 
#line 223 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8DImode, operands) && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8DImode, operands) && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V8DImode, operands) && 1) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (IOR, V16SFmode, operands)) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V16SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V16SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 13090 "../.././gcc/config/i386/i386.md"
  { "SSE_FLOAT_MODE_P (DFmode) && TARGET_MIX_SSE_I387\n\
   && !COMMUTATIVE_ARITH_P (operands[3])\n\
   && !(MEM_P (operands[1]) && MEM_P (operands[2]))",
    __builtin_constant_p 
#line 13090 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_MIX_SSE_I387
   && !COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    ? (int) 
#line 13090 "../.././gcc/config/i386/i386.md"
(SSE_FLOAT_MODE_P (DFmode) && TARGET_MIX_SSE_I387
   && !COMMUTATIVE_ARITH_P (operands[3])
   && !(MEM_P (operands[1]) && MEM_P (operands[2])))
    : -1 },
  { "(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 3068 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 3068 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 666 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_MMX || (TARGET_SSE2 && V4HImode == V1DImode))\n\
   && ix86_binary_operator_ok (MINUS, V4HImode, operands)",
    __builtin_constant_p 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V4HImode == V1DImode))
   && ix86_binary_operator_ok (MINUS, V4HImode, operands))
    ? (int) 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V4HImode == V1DImode))
   && ix86_binary_operator_ok (MINUS, V4HImode, operands))
    : -1 },
#line 14837 "../.././gcc/config/i386/i386.md"
  { "(TARGET_USE_FANCY_MATH_387\n\
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
	|| TARGET_MIX_SSE_I387)\n\
    && flag_unsafe_math_optimizations)\n\
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH\n\
       && DImode != HImode \n\
       && ((DImode != DImode) || TARGET_64BIT)\n\
       && !flag_trapping_math && !flag_rounding_math)",
    __builtin_constant_p 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && DImode != HImode 
       && ((DImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    ? (int) 
#line 14837 "../.././gcc/config/i386/i386.md"
((TARGET_USE_FANCY_MATH_387
    && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
	|| TARGET_MIX_SSE_I387)
    && flag_unsafe_math_optimizations)
   || (SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH
       && DImode != HImode 
       && ((DImode != DImode) || TARGET_64BIT)
       && !flag_trapping_math && !flag_rounding_math))
    : -1 },
  { "(TARGET_AVX512F) && (SSE_FLOAT_MODE_P (SFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2264 "../.././gcc/config/i386/sse.md"
(SSE_FLOAT_MODE_P (SFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2264 "../.././gcc/config/i386/sse.md"
(SSE_FLOAT_MODE_P (SFmode)))
    : -1 },
#line 8858 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX512F\n\
   && (GET_MODE_NUNITS (V64QImode)\n\
       == GET_MODE_NUNITS (V64QImode))",
    __builtin_constant_p 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V64QImode)))
    ? (int) 
#line 8858 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F
   && (GET_MODE_NUNITS (V64QImode)
       == GET_MODE_NUNITS (V64QImode)))
    : -1 },
  { "(reload_completed && ix86_lea_for_add_ok (insn, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 5381 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_lea_for_add_ok (insn, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 5381 "../.././gcc/config/i386/i386.md"
(reload_completed && ix86_lea_for_add_ok (insn, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_MMX && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && ( reload_completed)",
    __builtin_constant_p (
#line 1277 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 1279 "../.././gcc/config/i386/mmx.md"
( reload_completed))
    ? (int) (
#line 1277 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && !(MEM_P (operands[0]) && MEM_P (operands[1]))) && 
#line 1279 "../.././gcc/config/i386/mmx.md"
( reload_completed))
    : -1 },
#line 15215 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && (!TARGET_SSE_MATH || TARGET_MIX_SSE_I387)\n\
   && flag_unsafe_math_optimizations",
    __builtin_constant_p 
#line 15215 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!TARGET_SSE_MATH || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations)
    ? (int) 
#line 15215 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!TARGET_SSE_MATH || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations)
    : -1 },
#line 5880 "../.././gcc/config/i386/i386.md"
  { "(GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)\n\
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[1])\n\
   && GET_MODE (operands[0]) == GET_MODE (operands[2])\n\
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])\n\
       || GET_MODE (operands[3]) == VOIDmode)",
    __builtin_constant_p 
#line 5880 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && GET_MODE (operands[0]) == GET_MODE (operands[2])
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])
       || GET_MODE (operands[3]) == VOIDmode))
    ? (int) 
#line 5880 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode || GET_MODE (operands[0]) == HImode)
   && (!TARGET_PARTIAL_REG_STALL || optimize_function_for_size_p (cfun))
   && GET_MODE (operands[0]) == GET_MODE (operands[1])
   && GET_MODE (operands[0]) == GET_MODE (operands[2])
   && (GET_MODE (operands[0]) == GET_MODE (operands[3])
       || GET_MODE (operands[3]) == VOIDmode))
    : -1 },
#line 659 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE\n\
   && (register_operand (operands[0], V2DFmode)\n\
       || register_operand (operands[1], V2DFmode))",
    __builtin_constant_p 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V2DFmode)
       || register_operand (operands[1], V2DFmode)))
    ? (int) 
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V2DFmode)
       || register_operand (operands[1], V2DFmode)))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V16HImode)\n\
       || register_operand (operands[1], V16HImode))) && (TARGET_AVX)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V16HImode)
       || register_operand (operands[1], V16HImode))) && 
#line 146 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V16HImode)
       || register_operand (operands[1], V16HImode))) && 
#line 146 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 8757 "../.././gcc/config/i386/i386.md"
  { "(SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)\n\
   || (TARGET_SSE && (DFmode == TFmode))",
    __builtin_constant_p 
#line 8757 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (DFmode == TFmode)))
    ? (int) 
#line 8757 "../.././gcc/config/i386/i386.md"
((SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)
   || (TARGET_SSE && (DFmode == TFmode)))
    : -1 },
#line 10592 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && !TARGET_SSE4_1",
    __builtin_constant_p 
#line 10592 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !TARGET_SSE4_1)
    ? (int) 
#line 10592 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && !TARGET_SSE4_1)
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V32QImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V32QImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V32QImode, operands)
   && 1 && 1)
    : -1 },
#line 2394 "../.././gcc/config/i386/i386.md"
  { "!(MEM_P (operands[0]) && MEM_P (operands[1]))",
    __builtin_constant_p 
#line 2394 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1])))
    ? (int) 
#line 2394 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[0]) && MEM_P (operands[1])))
    : -1 },
#line 16859 "../.././gcc/config/i386/i386.md"
  { "optimize_insn_for_speed_p ()\n\
   && ((TARGET_NOT_UNPAIRABLE\n\
	&& (!MEM_P (operands[0])\n\
	    || !memory_displacement_operand (operands[0], HImode)))\n\
       || (TARGET_NOT_VECTORMODE\n\
	   && long_memory_operand (operands[0], HImode)))\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 16859 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((TARGET_NOT_UNPAIRABLE
	&& (!MEM_P (operands[0])
	    || !memory_displacement_operand (operands[0], HImode)))
       || (TARGET_NOT_VECTORMODE
	   && long_memory_operand (operands[0], HImode)))
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 16859 "../.././gcc/config/i386/i386.md"
(optimize_insn_for_speed_p ()
   && ((TARGET_NOT_UNPAIRABLE
	&& (!MEM_P (operands[0])
	    || !memory_displacement_operand (operands[0], HImode)))
       || (TARGET_NOT_VECTORMODE
	   && long_memory_operand (operands[0], HImode)))
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (IOR, V8SFmode, operands)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V8SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (IOR, V8SFmode, operands)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
#line 9826 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (LSHIFTRT, QImode, operands)",
    __builtin_constant_p 
#line 9826 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, QImode, operands))
    ? (int) 
#line 9826 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (LSHIFTRT, QImode, operands))
    : -1 },
#line 8479 "../.././gcc/config/i386/i386.md"
  { "ix86_unary_operator_ok (NEG, QImode, operands)",
    __builtin_constant_p 
#line 8479 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, QImode, operands))
    ? (int) 
#line 8479 "../.././gcc/config/i386/i386.md"
(ix86_unary_operator_ok (NEG, QImode, operands))
    : -1 },
#line 8850 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_unary_operator_ok (NOT, SImode, operands)",
    __builtin_constant_p 
#line 8850 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_unary_operator_ok (NOT, SImode, operands))
    ? (int) 
#line 8850 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_unary_operator_ok (NOT, SImode, operands))
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V32QImode)\n\
       == GET_MODE_NUNITS (V8SFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V8SFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V32QImode)
       == GET_MODE_NUNITS (V8SFmode)))
    : -1 },
#line 9782 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_binary_operator_ok (ASHIFTRT, SImode, operands)",
    __builtin_constant_p 
#line 9782 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    ? (int) 
#line 9782 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_binary_operator_ok (ASHIFTRT, SImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3005 "../.././gcc/config/i386/sse.md"
((64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3005 "../.././gcc/config/i386/sse.md"
((64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    : -1 },
  { "(TARGET_CMOVE) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9596 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9596 "../.././gcc/config/i386/i386.md"
(TARGET_CMOVE) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8546 "../.././gcc/config/i386/i386.md"
  { "TARGET_MIX_SSE_I387 && SSE_FLOAT_MODE_P (SFmode)",
    __builtin_constant_p 
#line 8546 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387 && SSE_FLOAT_MODE_P (SFmode))
    ? (int) 
#line 8546 "../.././gcc/config/i386/i386.md"
(TARGET_MIX_SSE_I387 && SSE_FLOAT_MODE_P (SFmode))
    : -1 },
#line 15048 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)\n\
       || TARGET_MIX_SSE_I387)\n\
   && flag_unsafe_math_optimizations",
    __builtin_constant_p 
#line 15048 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations)
    ? (int) 
#line 15048 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && (!(SSE_FLOAT_MODE_P (SFmode) && TARGET_SSE_MATH)
       || TARGET_MIX_SSE_I387)
   && flag_unsafe_math_optimizations)
    : -1 },
#line 11573 "../.././gcc/config/i386/i386.md"
  { "!TARGET_SEH",
    __builtin_constant_p 
#line 11573 "../.././gcc/config/i386/i386.md"
(!TARGET_SEH)
    ? (int) 
#line 11573 "../.././gcc/config/i386/i386.md"
(!TARGET_SEH)
    : -1 },
  { "(ix86_binary_operator_ok (MINUS, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 17037 "../.././gcc/config/i386/i386.md"
  { "!(TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())\n\
   /* Do not split stack checking probes.  */\n\
   && GET_CODE (operands[3]) != IOR && operands[1] != const0_rtx",
    __builtin_constant_p 
#line 17037 "../.././gcc/config/i386/i386.md"
(!(TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   /* Do not split stack checking probes.  */
   && GET_CODE (operands[3]) != IOR && operands[1] != const0_rtx)
    ? (int) 
#line 17037 "../.././gcc/config/i386/i386.md"
(!(TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   /* Do not split stack checking probes.  */
   && GET_CODE (operands[3]) != IOR && operands[1] != const0_rtx)
    : -1 },
#line 2300 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (GET_MODE_NUNITS (V4DFmode)\n\
       == GET_MODE_NUNITS (V8SFmode))",
    __builtin_constant_p 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V8SFmode)))
    ? (int) 
#line 2300 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (GET_MODE_NUNITS (V4DFmode)
       == GET_MODE_NUNITS (V8SFmode)))
    : -1 },
  { "(reload_completed) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10066 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10066 "../.././gcc/config/i386/i386.md"
(reload_completed) && 
#line 941 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (64 == 64) && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (64 == 64) && 1) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_SLOW_IMUL_IMM8 && optimize_insn_for_speed_p ()\n\
   && satisfies_constraint_K (operands[2])) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 17484 "../.././gcc/config/i386/i386.md"
(TARGET_SLOW_IMUL_IMM8 && optimize_insn_for_speed_p ()
   && satisfies_constraint_K (operands[2])) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 17484 "../.././gcc/config/i386/i386.md"
(TARGET_SLOW_IMUL_IMM8 && optimize_insn_for_speed_p ()
   && satisfies_constraint_K (operands[2])) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 3591 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && 1 && 1",
    __builtin_constant_p 
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && 1)
    ? (int) 
#line 3591 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && 1 && 1)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V4SFmode, operands) && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (V16SImode != V8DImode || TARGET_64BIT))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14158 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SImode != V8DImode || TARGET_64BIT)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14158 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SImode != V8DImode || TARGET_64BIT)))
    : -1 },
#line 7990 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V4SImode, operands)",
    __builtin_constant_p 
#line 7990 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V4SImode, operands))
    ? (int) 
#line 7990 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V4SImode, operands))
    : -1 },
#line 4757 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 && X87_ENABLE_FLOAT (DFmode, SImode)",
    __builtin_constant_p 
#line 4757 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, SImode))
    ? (int) 
#line 4757 "../.././gcc/config/i386/i386.md"
(TARGET_80387 && X87_ENABLE_FLOAT (DFmode, SImode))
    : -1 },
  { "(TARGET_SSE\n\
   && (GET_MODE_NUNITS (V16QImode)\n\
       == GET_MODE_NUNITS (V2DFmode))) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2317 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (GET_MODE_NUNITS (V16QImode)
       == GET_MODE_NUNITS (V2DFmode))) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 13268 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387\n\
   && COMMUTATIVE_ARITH_P (operands[3])",
    __builtin_constant_p 
#line 13268 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && COMMUTATIVE_ARITH_P (operands[3]))
    ? (int) 
#line 13268 "../.././gcc/config/i386/i386.md"
(TARGET_80387
   && COMMUTATIVE_ARITH_P (operands[3]))
    : -1 },
#line 17171 "../.././gcc/config/i386/i386.md"
  { "(optimize_insn_for_size_p () || TARGET_MOVE_M1_VIA_OR)\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 17171 "../.././gcc/config/i386/i386.md"
((optimize_insn_for_size_p () || TARGET_MOVE_M1_VIA_OR)
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 17171 "../.././gcc/config/i386/i386.md"
((optimize_insn_for_size_p () || TARGET_MOVE_M1_VIA_OR)
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (V16SFmode == V16SFmode || V16SFmode == V8DFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SFmode == V16SFmode || V16SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_SSE\n\
   && GET_RTX_CLASS (GET_CODE (operands[3])) == RTX_COMM_COMPARE) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2113 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && GET_RTX_CLASS (GET_CODE (operands[3])) == RTX_COMM_COMPARE) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2113 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && GET_RTX_CLASS (GET_CODE (operands[3])) == RTX_COMM_COMPARE) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())\n\
   && GET_CODE (operands[2]) != MINUS\n\
   && peep2_reg_dead_p (3, operands[0])\n\
   && !reg_overlap_mentioned_p (operands[0], operands[1])\n\
   && ix86_match_ccmode (peep2_next_insn (2),\n\
			 GET_CODE (operands[2]) == PLUS\n\
			 ? CCGOCmode : CCNOmode)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 17089 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && GET_CODE (operands[2]) != MINUS
   && peep2_reg_dead_p (3, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && ix86_match_ccmode (peep2_next_insn (2),
			 GET_CODE (operands[2]) == PLUS
			 ? CCGOCmode : CCNOmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 17089 "../.././gcc/config/i386/i386.md"
((TARGET_READ_MODIFY_WRITE || optimize_insn_for_size_p ())
   && GET_CODE (operands[2]) != MINUS
   && peep2_reg_dead_p (3, operands[0])
   && !reg_overlap_mentioned_p (operands[0], operands[1])
   && ix86_match_ccmode (peep2_next_insn (2),
			 GET_CODE (operands[2]) == PLUS
			 ? CCGOCmode : CCNOmode)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_SSE && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64) && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))))
    : -1 },
#line 3005 "../.././gcc/config/i386/sse.md"
  { "(64 == 64) && 1",
    __builtin_constant_p 
#line 3005 "../.././gcc/config/i386/sse.md"
((64 == 64) && 1)
    ? (int) 
#line 3005 "../.././gcc/config/i386/sse.md"
((64 == 64) && 1)
    : -1 },
#line 4988 "../.././gcc/config/i386/i386.md"
  { "(TARGET_64BIT || TARGET_KEEPS_VECTOR_ALIGNED_STACK)\n\
   && TARGET_SSE2 && TARGET_SSE_MATH",
    __builtin_constant_p 
#line 4988 "../.././gcc/config/i386/i386.md"
((TARGET_64BIT || TARGET_KEEPS_VECTOR_ALIGNED_STACK)
   && TARGET_SSE2 && TARGET_SSE_MATH)
    ? (int) 
#line 4988 "../.././gcc/config/i386/i386.md"
((TARGET_64BIT || TARGET_KEEPS_VECTOR_ALIGNED_STACK)
   && TARGET_SSE2 && TARGET_SSE_MATH)
    : -1 },
#line 12971 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && TARGET_GNU2_TLS",
    __builtin_constant_p 
#line 12971 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_GNU2_TLS)
    ? (int) 
#line 12971 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_GNU2_TLS)
    : -1 },
  { "(TARGET_XOP) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 13730 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 13730 "../.././gcc/config/i386/sse.md"
(TARGET_XOP) && 
#line 210 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(TARGET_SSE) && (TARGET_SSE2 && TARGET_64BIT)",
    __builtin_constant_p (
#line 1240 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 1229 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && TARGET_64BIT))
    ? (int) (
#line 1240 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 1229 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && TARGET_64BIT))
    : -1 },
#line 1538 "../.././gcc/config/i386/i386.md"
  { "TARGET_SAHF",
    __builtin_constant_p 
#line 1538 "../.././gcc/config/i386/i386.md"
(TARGET_SAHF)
    ? (int) 
#line 1538 "../.././gcc/config/i386/i386.md"
(TARGET_SAHF)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1590 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1 && (V4SFmode == V16SFmode || V4SFmode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
  { "(TARGET_SSE\n\
   && (register_operand (operands[0], V4DImode)\n\
       || register_operand (operands[1], V4DImode))) && (TARGET_AVX)",
    __builtin_constant_p (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V4DImode)
       || register_operand (operands[1], V4DImode))) && 
#line 148 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 659 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && (register_operand (operands[0], V4DImode)
       || register_operand (operands[1], V4DImode))) && 
#line 148 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(ix86_binary_operator_ok (ASHIFT, DImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))\n\
      == GET_MODE_BITSIZE (DImode)-1) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 9061 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 9061 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ASHIFT, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 17948 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && TARGET_XSAVE",
    __builtin_constant_p 
#line 17948 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_XSAVE)
    ? (int) 
#line 17948 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_XSAVE)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && flag_finite_math_only\n\
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)\n\
   && (16 == 64) && 1)",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)
   && (16 == 64) && 1))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 1602 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && flag_finite_math_only
   && ix86_binary_operator_ok (SMIN, V4SFmode, operands)
   && (16 == 64) && 1))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V4DFmode, operands) && 1 && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_SSE2 && (V8SFmode == V4SFmode || TARGET_AVX2)) && (TARGET_AVX)",
    __builtin_constant_p (
#line 3610 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (V8SFmode == V4SFmode || TARGET_AVX2)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 3610 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && (V8SFmode == V4SFmode || TARGET_AVX2)) && 
#line 191 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(ix86_binary_operator_ok (MINUS, SImode, operands)) && (!TARGET_64BIT)",
    __builtin_constant_p (
#line 6029 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, SImode, operands)) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    ? (int) (
#line 6029 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, SImode, operands)) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    : -1 },
  { "(TARGET_SSE\n\
   && GET_RTX_CLASS (GET_CODE (operands[3])) == RTX_COMM_COMPARE) && (TARGET_AVX)",
    __builtin_constant_p (
#line 2113 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && GET_RTX_CLASS (GET_CODE (operands[3])) == RTX_COMM_COMPARE) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    ? (int) (
#line 2113 "../.././gcc/config/i386/sse.md"
(TARGET_SSE
   && GET_RTX_CLASS (GET_CODE (operands[3])) == RTX_COMM_COMPARE) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))
    : -1 },
  { "(!(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15712 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15712 "../.././gcc/config/i386/i386.md"
(!(fixed_regs[AX_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "(TARGET_AVX512F) && (((32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_FMA || TARGET_FMA4))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
((32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 2775 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 2986 "../.././gcc/config/i386/sse.md"
((32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 2775 "../.././gcc/config/i386/sse.md"
(TARGET_FMA || TARGET_FMA4)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && 1 && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V16SFmode, operands) && 1 && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
  { "(TARGET_SSE) && ( reload_completed)",
    __builtin_constant_p (
#line 5833 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 5835 "../.././gcc/config/i386/sse.md"
( reload_completed))
    ? (int) (
#line 5833 "../.././gcc/config/i386/sse.md"
(TARGET_SSE) && 
#line 5835 "../.././gcc/config/i386/sse.md"
( reload_completed))
    : -1 },
  { "(TARGET_BMI && !TARGET_AVOID_FALSE_DEP_FOR_BMI) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 11903 "../.././gcc/config/i386/i386.md"
(TARGET_BMI && !TARGET_AVOID_FALSE_DEP_FOR_BMI) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 11903 "../.././gcc/config/i386/i386.md"
(TARGET_BMI && !TARGET_AVOID_FALSE_DEP_FOR_BMI) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 887 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_SSE || TARGET_3DNOW_A)\n\
   && ix86_binary_operator_ok (SMIN, V4HImode, operands)",
    __builtin_constant_p 
#line 887 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (SMIN, V4HImode, operands))
    ? (int) 
#line 887 "../.././gcc/config/i386/mmx.md"
((TARGET_SSE || TARGET_3DNOW_A)
   && ix86_binary_operator_ok (SMIN, V4HImode, operands))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && (TARGET_AVX512F && (V16SImode == V16SFmode || V16SImode == V8DFmode)))",
    __builtin_constant_p (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SImode == V16SFmode || V16SImode == V8DFmode))))
    ? (int) (
#line 156 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 78 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 2168 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (V16SImode == V16SFmode || V16SImode == V8DFmode))))
    : -1 },
  { "(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V32QImode, operands)) && (TARGET_AVX2)",
    __builtin_constant_p (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    ? (int) (
#line 7695 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (US_MINUS, V32QImode, operands)) && 
#line 291 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE && (16 == 64)\n\
   && ix86_binary_operator_ok (AND, V8HImode, operands))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (AND, V8HImode, operands)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (16 == 64)
   && ix86_binary_operator_ok (AND, V8HImode, operands)))
    : -1 },
  { "(TARGET_64BIT) && (Pmode == DImode)",
    __builtin_constant_p (
#line 12772 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 12772 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == SImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17345 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17345 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
#line 18153 "../.././gcc/config/i386/i386.md"
  { "TARGET_RDSEED",
    __builtin_constant_p 
#line 18153 "../.././gcc/config/i386/i386.md"
(TARGET_RDSEED)
    ? (int) 
#line 18153 "../.././gcc/config/i386/i386.md"
(TARGET_RDSEED)
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && (32 == 64)\n\
   && ix86_binary_operator_ok (AND, V4DImode, operands)) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (AND, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && (32 == 64)
   && ix86_binary_operator_ok (AND, V4DImode, operands)) && 
#line 226 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V2DFmode, operands) && 1 && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2)))
    : -1 },
  { "(ix86_binary_operator_ok (ROTATERT, DImode, operands)\n\
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))\n\
      == GET_MODE_BITSIZE (DImode)-1) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 10018 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 10018 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATERT, DImode, operands)
   && (INTVAL (operands[3]) & (GET_MODE_BITSIZE (DImode)-1))
      == GET_MODE_BITSIZE (DImode)-1) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && (64 == 64) && 1) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8DFmode, operands) && (64 == 64) && 1) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
  { "(TARGET_64BIT\n\
   && !(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && (Pmode == DImode)",
    __builtin_constant_p (
#line 15480 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    ? (int) (
#line 15480 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT
   && !(fixed_regs[SI_REG] || fixed_regs[DI_REG])) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode))
    : -1 },
  { "((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == DImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == DImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17345 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17345 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == 2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },
#line 12035 "../.././gcc/config/i386/i386.md"
  { "TARGET_LZCNT\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)",
    __builtin_constant_p 
#line 12035 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun))
    ? (int) 
#line 12035 "../.././gcc/config/i386/i386.md"
(TARGET_LZCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun))
    : -1 },
#line 6164 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (MINUS, SImode, operands)\n\
   && CONST_INT_P (operands[2])\n\
   && INTVAL (operands[2]) == INTVAL (operands[3])",
    __builtin_constant_p 
#line 6164 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, SImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    ? (int) 
#line 6164 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (MINUS, SImode, operands)
   && CONST_INT_P (operands[2])
   && INTVAL (operands[2]) == INTVAL (operands[3]))
    : -1 },
#line 959 "../.././gcc/config/i386/mmx.md"
  { "TARGET_MMX && ix86_binary_operator_ok (EQ, V4HImode, operands)",
    __builtin_constant_p 
#line 959 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (EQ, V4HImode, operands))
    ? (int) 
#line 959 "../.././gcc/config/i386/mmx.md"
(TARGET_MMX && ix86_binary_operator_ok (EQ, V4HImode, operands))
    : -1 },
#line 865 "../.././gcc/config/i386/sse.md"
  { "!TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC",
    __builtin_constant_p 
#line 865 "../.././gcc/config/i386/sse.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
    ? (int) 
#line 865 "../.././gcc/config/i386/sse.md"
(!TARGET_64BIT && TARGET_SSE2 && TARGET_INTER_UNIT_MOVES_TO_VEC)
    : -1 },
  { "(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V16SImode, operands) && 1) && (TARGET_AVX512F)",
    __builtin_constant_p (
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V16SImode, operands) && 1) && 
#line 262 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    ? (int) (
#line 8159 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (MULT, V16SImode, operands) && 1) && 
#line 262 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F))
    : -1 },
#line 4769 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSE2 && TARGET_SSE_MATH\n\
   && TARGET_USE_VECTOR_CONVERTS && optimize_function_for_speed_p (cfun)\n\
   && reload_completed && SSE_REG_P (operands[0])\n\
   && (MEM_P (operands[1]) || TARGET_INTER_UNIT_MOVES_TO_VEC)",
    __builtin_constant_p 
#line 4769 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH
   && TARGET_USE_VECTOR_CONVERTS && optimize_function_for_speed_p (cfun)
   && reload_completed && SSE_REG_P (operands[0])
   && (MEM_P (operands[1]) || TARGET_INTER_UNIT_MOVES_TO_VEC))
    ? (int) 
#line 4769 "../.././gcc/config/i386/i386.md"
(TARGET_SSE2 && TARGET_SSE_MATH
   && TARGET_USE_VECTOR_CONVERTS && optimize_function_for_speed_p (cfun)
   && reload_completed && SSE_REG_P (operands[0])
   && (MEM_P (operands[1]) || TARGET_INTER_UNIT_MOVES_TO_VEC))
    : -1 },
  { "(!(MEM_P (operands[1]) && MEM_P (operands[2]))) && (!TARGET_64BIT)",
    __builtin_constant_p (
#line 6703 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    ? (int) (
#line 6703 "../.././gcc/config/i386/i386.md"
(!(MEM_P (operands[1]) && MEM_P (operands[2]))) && 
#line 940 "../.././gcc/config/i386/i386.md"
(!TARGET_64BIT))
    : -1 },
  { "(TARGET_64BIT) && (Pmode == SImode)",
    __builtin_constant_p (
#line 12772 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 12772 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && 1) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && 1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 3160 "../.././gcc/config/i386/sse.md"
((TARGET_FMA || TARGET_FMA4 || TARGET_AVX512F) && 1 && 1) && 
#line 187 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
#line 666 "../.././gcc/config/i386/mmx.md"
  { "(TARGET_MMX || (TARGET_SSE2 && V1DImode == V1DImode))\n\
   && ix86_binary_operator_ok (PLUS, V1DImode, operands)",
    __builtin_constant_p 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V1DImode == V1DImode))
   && ix86_binary_operator_ok (PLUS, V1DImode, operands))
    ? (int) 
#line 666 "../.././gcc/config/i386/mmx.md"
((TARGET_MMX || (TARGET_SSE2 && V1DImode == V1DImode))
   && ix86_binary_operator_ok (PLUS, V1DImode, operands))
    : -1 },
#line 10224 "../.././gcc/config/i386/i386.md"
  { "ix86_binary_operator_ok (ROTATE, HImode, operands)",
    __builtin_constant_p 
#line 10224 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, HImode, operands))
    ? (int) 
#line 10224 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (ROTATE, HImode, operands))
    : -1 },
#line 6616 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE2 && ix86_vec_interleave_v2df_operator_ok (operands, 0)",
    __builtin_constant_p 
#line 6616 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_vec_interleave_v2df_operator_ok (operands, 0))
    ? (int) 
#line 6616 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_vec_interleave_v2df_operator_ok (operands, 0))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX512F && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 3179 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F && (64 == 64) && (V8DFmode == V16SFmode || V8DFmode == V8DFmode)))
    : -1 },
  { "(TARGET_SSE2) && (Pmode == SImode)",
    __builtin_constant_p (
#line 10790 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    ? (int) (
#line 10790 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 14895 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && (16 == 64)))
    : -1 },
  { "(TARGET_SSE && ix86_binary_operator_ok (AND, V2DFmode, operands)) && (TARGET_SSE2)",
    __builtin_constant_p (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V2DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    ? (int) (
#line 2410 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (AND, V2DFmode, operands)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))
    : -1 },
  { "(reload_completed\n\
   && operands[1] == constm1_rtx) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 1875 "../.././gcc/config/i386/i386.md"
(reload_completed
   && operands[1] == constm1_rtx) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 1875 "../.././gcc/config/i386/i386.md"
(reload_completed
   && operands[1] == constm1_rtx) && 
#line 896 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && (TARGET_SSE2)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1345 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MULT, V2DFmode, operands) && (16 == 64) && (V2DFmode == V16SFmode || V2DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2))))
    : -1 },
#line 10528 "../.././gcc/config/i386/i386.md"
  { "!TARGET_PARTIAL_REG_STALL\n\
   && TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun)",
    __builtin_constant_p 
#line 10528 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   && TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun))
    ? (int) 
#line 10528 "../.././gcc/config/i386/i386.md"
(!TARGET_PARTIAL_REG_STALL
   && TARGET_ZERO_EXTEND_WITH_AND && optimize_function_for_speed_p (cfun))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (PLUS, V8SFmode, operands) && (32 == 64) && (V8SFmode == V16SFmode || V8SFmode == V8DFmode)) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
#line 15276 "../.././gcc/config/i386/i386.md"
  { "TARGET_USE_FANCY_MATH_387\n\
   && ix86_libc_has_function (function_c99_misc)",
    __builtin_constant_p 
#line 15276 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && ix86_libc_has_function (function_c99_misc))
    ? (int) 
#line 15276 "../.././gcc/config/i386/i386.md"
(TARGET_USE_FANCY_MATH_387
   && ix86_libc_has_function (function_c99_misc))
    : -1 },
  { "(TARGET_SSE2\n\
   && ((unsigned) exact_log2 (INTVAL (operands[3]))\n\
       < GET_MODE_NUNITS (V16QImode))) && (TARGET_SSE4_1)",
    __builtin_constant_p (
#line 9598 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V16QImode))) && 
#line 9582 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1))
    ? (int) (
#line 9598 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2
   && ((unsigned) exact_log2 (INTVAL (operands[3]))
       < GET_MODE_NUNITS (V16QImode))) && 
#line 9582 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4DImode, operands) && (32 == 64)) && (TARGET_AVX2))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4DImode, operands) && (32 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (MINUS, V4DImode, operands) && (32 == 64)) && 
#line 232 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2)))
    : -1 },
  { "(TARGET_RDRND) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 18143 "../.././gcc/config/i386/i386.md"
(TARGET_RDRND) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 18143 "../.././gcc/config/i386/i386.md"
(TARGET_RDRND) && 
#line 893 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_unary_operator_ok (NOT, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 8861 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 8861 "../.././gcc/config/i386/i386.md"
(ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)\n\
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && (TARGET_AVX512F))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMAX, V8DImode, operands)
   && 1 && (V8DImode == V16SFmode || V8DImode == V8DFmode)) && 
#line 417 "../.././gcc/config/i386/sse.md"
(TARGET_AVX512F)))
    : -1 },
#line 7775 "../.././gcc/config/i386/i386.md"
  { "(TARGET_FAST_PREFIX && !TARGET_PARTIAL_REG_STALL)\n\
    || optimize_function_for_size_p (cfun)",
    __builtin_constant_p 
#line 7775 "../.././gcc/config/i386/i386.md"
((TARGET_FAST_PREFIX && !TARGET_PARTIAL_REG_STALL)
    || optimize_function_for_size_p (cfun))
    ? (int) 
#line 7775 "../.././gcc/config/i386/i386.md"
((TARGET_FAST_PREFIX && !TARGET_PARTIAL_REG_STALL)
    || optimize_function_for_size_p (cfun))
    : -1 },
#line 1592 "../.././gcc/config/i386/i386.md"
  { "TARGET_SSE_MATH\n\
   && SSE_FLOAT_MODE_P (SFmode)",
    __builtin_constant_p 
#line 1592 "../.././gcc/config/i386/i386.md"
(TARGET_SSE_MATH
   && SSE_FLOAT_MODE_P (SFmode))
    ? (int) 
#line 1592 "../.././gcc/config/i386/i386.md"
(TARGET_SSE_MATH
   && SSE_FLOAT_MODE_P (SFmode))
    : -1 },
#line 12358 "../.././gcc/config/i386/i386.md"
  { "TARGET_POPCNT\n\
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun)",
    __builtin_constant_p 
#line 12358 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun))
    ? (int) 
#line 12358 "../.././gcc/config/i386/i386.md"
(TARGET_POPCNT
   && TARGET_AVOID_FALSE_DEP_FOR_BMI && optimize_function_for_speed_p (cfun))
    : -1 },
#line 17160 "../.././gcc/config/i386/i386.md"
  { "(GET_MODE (operands[0]) == QImode\n\
    || GET_MODE (operands[0]) == HImode)\n\
   && (! TARGET_USE_MOV0 || optimize_insn_for_size_p ())\n\
   && peep2_regno_dead_p (0, FLAGS_REG)",
    __builtin_constant_p 
#line 17160 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode
    || GET_MODE (operands[0]) == HImode)
   && (! TARGET_USE_MOV0 || optimize_insn_for_size_p ())
   && peep2_regno_dead_p (0, FLAGS_REG))
    ? (int) 
#line 17160 "../.././gcc/config/i386/i386.md"
((GET_MODE (operands[0]) == QImode
    || GET_MODE (operands[0]) == HImode)
   && (! TARGET_USE_MOV0 || optimize_insn_for_size_p ())
   && peep2_regno_dead_p (0, FLAGS_REG))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)\n\
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (SMIN, V16HImode, operands)
   && 1 && (V16HImode == V16SFmode || V16HImode == V8DFmode)))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && (TARGET_AVX)))",
    __builtin_constant_p (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    ? (int) (
#line 127 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V4DFmode, operands) && (32 == 64) && (V4DFmode == V16SFmode || V4DFmode == V8DFmode)) && 
#line 182 "../.././gcc/config/i386/sse.md"
(TARGET_AVX))))
    : -1 },
  { "(ix86_binary_operator_ok (PLUS, DImode, operands)) && (TARGET_64BIT)",
    __builtin_constant_p (
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    ? (int) (
#line 6215 "../.././gcc/config/i386/i386.md"
(ix86_binary_operator_ok (PLUS, DImode, operands)) && 
#line 890 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT))
    : -1 },
#line 8609 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE4_1 && ix86_binary_operator_ok (UMIN, V4SImode, operands)",
    __builtin_constant_p 
#line 8609 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (UMIN, V4SImode, operands))
    ? (int) 
#line 8609 "../.././gcc/config/i386/sse.md"
(TARGET_SSE4_1 && ix86_binary_operator_ok (UMIN, V4SImode, operands))
    : -1 },
#line 10156 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && TARGET_BMI2",
    __builtin_constant_p 
#line 10156 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_BMI2)
    ? (int) 
#line 10156 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && TARGET_BMI2)
    : -1 },
#line 8166 "../.././gcc/config/i386/i386.md"
  { "TARGET_AVX512F && reload_completed",
    __builtin_constant_p 
#line 8166 "../.././gcc/config/i386/i386.md"
(TARGET_AVX512F && reload_completed)
    ? (int) 
#line 8166 "../.././gcc/config/i386/i386.md"
(TARGET_AVX512F && reload_completed)
    : -1 },
#line 8888 "../.././gcc/config/i386/i386.md"
  { "TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)\n\
   && ix86_unary_operator_ok (NOT, SImode, operands)",
    __builtin_constant_p 
#line 8888 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, SImode, operands))
    ? (int) 
#line 8888 "../.././gcc/config/i386/i386.md"
(TARGET_64BIT && ix86_match_ccmode (insn, CCNOmode)
   && ix86_unary_operator_ok (NOT, SImode, operands))
    : -1 },
#line 8441 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)\n\
   && 1 && 1",
    __builtin_constant_p 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)
   && 1 && 1)
    ? (int) 
#line 8441 "../.././gcc/config/i386/sse.md"
(TARGET_AVX2 && ix86_binary_operator_ok (UMAX, V8SImode, operands)
   && 1 && 1)
    : -1 },
  { "((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && (((((((((((word_mode == SImode) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode)) && (word_mode == SImode)) && (Pmode == SImode)) && (Pmode == SImode))",
    __builtin_constant_p (
#line 17316 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    ? (int) (
#line 17316 "../.././gcc/config/i386/i386.md"
((TARGET_DOUBLE_PUSH || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == -2*GET_MODE_SIZE (word_mode)) && ((((((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == SImode)))
    : -1 },
#line 5344 "../.././gcc/config/i386/sse.md"
  { "TARGET_AVX\n\
   && (INTVAL (operands[3]) == (INTVAL (operands[7]) - 4)\n\
       && INTVAL (operands[4]) == (INTVAL (operands[8]) - 4)\n\
       && INTVAL (operands[5]) == (INTVAL (operands[9]) - 4)\n\
       && INTVAL (operands[6]) == (INTVAL (operands[10]) - 4))",
    __builtin_constant_p 
#line 5344 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (INTVAL (operands[3]) == (INTVAL (operands[7]) - 4)
       && INTVAL (operands[4]) == (INTVAL (operands[8]) - 4)
       && INTVAL (operands[5]) == (INTVAL (operands[9]) - 4)
       && INTVAL (operands[6]) == (INTVAL (operands[10]) - 4)))
    ? (int) 
#line 5344 "../.././gcc/config/i386/sse.md"
(TARGET_AVX
   && (INTVAL (operands[3]) == (INTVAL (operands[7]) - 4)
       && INTVAL (operands[4]) == (INTVAL (operands[8]) - 4)
       && INTVAL (operands[5]) == (INTVAL (operands[9]) - 4)
       && INTVAL (operands[6]) == (INTVAL (operands[10]) - 4)))
    : -1 },
  { "(IN_RANGE (INTVAL (operands[2]), 1, 3)\n\
   /* Validate MODE for lea.  */\n\
   && ((!TARGET_PARTIAL_REG_STALL\n\
	&& (GET_MODE (operands[0]) == QImode\n\
	    || GET_MODE (operands[0]) == HImode))\n\
       || GET_MODE (operands[0]) == SImode\n\
       || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))\n\
   && (rtx_equal_p (operands[0], operands[3])\n\
       || peep2_reg_dead_p (2, operands[0]))\n\
   /* We reorder load and the shift.  */\n\
   && !reg_overlap_mentioned_p (operands[0], operands[4])) && (word_mode == SImode)",
    __builtin_constant_p (
#line 17516 "../.././gcc/config/i386/i386.md"
(IN_RANGE (INTVAL (operands[2]), 1, 3)
   /* Validate MODE for lea.  */
   && ((!TARGET_PARTIAL_REG_STALL
	&& (GET_MODE (operands[0]) == QImode
	    || GET_MODE (operands[0]) == HImode))
       || GET_MODE (operands[0]) == SImode
       || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))
   && (rtx_equal_p (operands[0], operands[3])
       || peep2_reg_dead_p (2, operands[0]))
   /* We reorder load and the shift.  */
   && !reg_overlap_mentioned_p (operands[0], operands[4])) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode))
    ? (int) (
#line 17516 "../.././gcc/config/i386/i386.md"
(IN_RANGE (INTVAL (operands[2]), 1, 3)
   /* Validate MODE for lea.  */
   && ((!TARGET_PARTIAL_REG_STALL
	&& (GET_MODE (operands[0]) == QImode
	    || GET_MODE (operands[0]) == HImode))
       || GET_MODE (operands[0]) == SImode
       || (TARGET_64BIT && GET_MODE (operands[0]) == DImode))
   && (rtx_equal_p (operands[0], operands[3])
       || peep2_reg_dead_p (2, operands[0]))
   /* We reorder load and the shift.  */
   && !reg_overlap_mentioned_p (operands[0], operands[4])) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode))
    : -1 },
  { "(TARGET_AVX512F) && ((TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && (32 == 64) && 1) && (TARGET_AVX))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && (
#line 1306 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (MINUS, V8SFmode, operands) && (32 == 64) && 1) && 
#line 181 "../.././gcc/config/i386/sse.md"
(TARGET_AVX)))
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4SImode, operands) && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4SImode, operands) && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V4SImode, operands) && (16 == 64)))
    : -1 },
#line 8537 "../.././gcc/config/i386/i386.md"
  { "TARGET_80387 || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH)",
    __builtin_constant_p 
#line 8537 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    ? (int) 
#line 8537 "../.././gcc/config/i386/i386.md"
(TARGET_80387 || (SSE_FLOAT_MODE_P (DFmode) && TARGET_SSE_MATH))
    : -1 },
#line 5012 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && ix86_binary_operator_ok (UNKNOWN, V4SFmode, operands)",
    __builtin_constant_p 
#line 5012 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (UNKNOWN, V4SFmode, operands))
    ? (int) 
#line 5012 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && ix86_binary_operator_ok (UNKNOWN, V4SFmode, operands))
    : -1 },
#line 1523 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE_MATH",
    __builtin_constant_p 
#line 1523 "../.././gcc/config/i386/sse.md"
(TARGET_SSE_MATH)
    ? (int) 
#line 1523 "../.././gcc/config/i386/sse.md"
(TARGET_SSE_MATH)
    : -1 },
  { "(TARGET_AVX512F) && (TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V2DImode, operands) && (16 == 64))",
    __builtin_constant_p (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V2DImode, operands) && (16 == 64)))
    ? (int) (
#line 64 "../.././gcc/config/i386/subst.md"
(TARGET_AVX512F) && 
#line 7672 "../.././gcc/config/i386/sse.md"
(TARGET_SSE2 && ix86_binary_operator_ok (PLUS, V2DImode, operands) && (16 == 64)))
    : -1 },
#line 9083 "../.././gcc/config/i386/sse.md"
  { "TARGET_SSE && 1\n\
   && ix86_binary_operator_ok (IOR, V4SImode, operands)",
    __builtin_constant_p 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V4SImode, operands))
    ? (int) 
#line 9083 "../.././gcc/config/i386/sse.md"
(TARGET_SSE && 1
   && ix86_binary_operator_ok (IOR, V4SImode, operands))
    : -1 },
  { "(X87_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
   && TARGET_FISTTP\n\
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))\n\
	 && (TARGET_64BIT || DImode != DImode))\n\
	&& TARGET_SSE_MATH)\n\
   && can_create_pseudo_p ()) && ( 1)",
    __builtin_constant_p (
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ()) && 
#line 4432 "../.././gcc/config/i386/i386.md"
( 1))
    ? (int) (
#line 4425 "../.././gcc/config/i386/i386.md"
(X87_FLOAT_MODE_P (GET_MODE (operands[1]))
   && TARGET_FISTTP
   && !((SSE_FLOAT_MODE_P (GET_MODE (operands[1]))
	 && (TARGET_64BIT || DImode != DImode))
	&& TARGET_SSE_MATH)
   && can_create_pseudo_p ()) && 
#line 4432 "../.././gcc/config/i386/i386.md"
( 1))
    : -1 },
  { "((TARGET_SINGLE_POP || optimize_insn_for_size_p ())\n\
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && ((((((((word_mode == SImode) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (Pmode == DImode)) && (word_mode == SImode)) && (Pmode == DImode)) && (Pmode == DImode))",
    __builtin_constant_p (
#line 17330 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    ? (int) (
#line 17330 "../.././gcc/config/i386/i386.md"
((TARGET_SINGLE_POP || optimize_insn_for_size_p ())
   && INTVAL (operands[0]) == GET_MODE_SIZE (word_mode)) && (((((((
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1063 "../.././gcc/config/i386/i386.md"
(word_mode == SImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)) && 
#line 1058 "../.././gcc/config/i386/i386.md"
(Pmode == DImode)))
    : -1 },

};
#endif /* gcc >= 3.0.1 */

int
main(void)
{
  unsigned int i;
  const char *p;
  puts ("(define_conditions [");
#if GCC_VERSION >= 3001
  for (i = 0; i < ARRAY_SIZE (insn_conditions); i++)
    {
      printf ("  (%d \"", insn_conditions[i].value);
      for (p = insn_conditions[i].expr; *p; p++)
        {
          switch (*p)
	     {
	     case '\\':
	     case '\"': putchar ('\\'); break;
	     default: break;
	     }
          putchar (*p);
        }
      puts ("\")");
    }
#endif /* gcc >= 3.0.1 */
  puts ("])");
  fflush (stdout);
return ferror (stdout) != 0 ? FATAL_EXIT_CODE : SUCCESS_EXIT_CODE;
}
