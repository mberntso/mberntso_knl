/* Generated automatically by the program `genextract'
   from the machine description file `md'.  */

#include "config.h"
#include "system.h"
#include "coretypes.h"
#include "tm.h"
#include "rtl.h"
#include "insn-config.h"
#include "recog.h"
#include "diagnostic-core.h"

/* This variable is used as the "location" of any missing operand
   whose numbers are skipped by a given pattern.  */
static rtx junk ATTRIBUTE_UNUSED;

void
insn_extract (rtx insn)
{
  rtx *ro = recog_data.operand;
  rtx **ro_loc = recog_data.operand_loc;
  rtx pat = PATTERN (insn);
  int i ATTRIBUTE_UNUSED; /* only for peepholes */

#ifdef ENABLE_CHECKING
  memset (ro, 0xab, sizeof (*ro) * MAX_RECOG_OPERANDS);
  memset (ro_loc, 0xab, sizeof (*ro_loc) * MAX_RECOG_OPERANDS);
#endif

  switch (INSN_CODE (insn))
    {
    default:
      /* Control reaches here if insn_extract has been called with an
         unrecognizable insn (code -1), or an insn whose INSN_CODE
         corresponds to a DEFINE_EXPAND in the machine description;
         either way, a bug.  */
      if (INSN_CODE (insn) < 0)
        fatal_insn ("unrecognizable insn:", insn);
      else
        fatal_insn ("insn with invalid code number:", insn);

    case 3168:  /* atomic_xordi */
    case 3167:  /* atomic_ordi */
    case 3166:  /* atomic_anddi */
    case 3165:  /* atomic_xorsi */
    case 3164:  /* atomic_orsi */
    case 3163:  /* atomic_andsi */
    case 3162:  /* atomic_xorhi */
    case 3161:  /* atomic_orhi */
    case 3160:  /* atomic_andhi */
    case 3159:  /* atomic_xorqi */
    case 3158:  /* atomic_orqi */
    case 3157:  /* atomic_andqi */
    case 3156:  /* atomic_subdi */
    case 3155:  /* atomic_subsi */
    case 3154:  /* atomic_subhi */
    case 3153:  /* atomic_subqi */
    case 3152:  /* atomic_adddi */
    case 3151:  /* atomic_addsi */
    case 3150:  /* atomic_addhi */
    case 3149:  /* atomic_addqi */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0), 1));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0), 0);
      recog_data.dup_num[0] = 0;
      break;

    case 3148:  /* atomic_exchangedi */
    case 3147:  /* atomic_exchangesi */
    case 3146:  /* atomic_exchangehi */
    case 3145:  /* atomic_exchangeqi */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 3144:  /* *atomic_fetch_add_cmpdi */
    case 3143:  /* *atomic_fetch_add_cmpsi */
    case 3142:  /* *atomic_fetch_add_cmphi */
    case 3141:  /* *atomic_fetch_add_cmpqi */
      ro[0] = *(ro_loc[0] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0);
      recog_data.dup_num[0] = 0;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[1] = 0;
      break;

    case 3140:  /* atomic_fetch_adddi */
    case 3139:  /* atomic_fetch_addsi */
    case 3138:  /* atomic_fetch_addhi */
    case 3137:  /* atomic_fetch_addqi */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[1] = 1;
      break;

    case 3136:  /* atomic_compare_and_swapti_doubleword */
    case 3135:  /* atomic_compare_and_swapdi_doubleword */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 4));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 5));
      ro[8] = *(ro_loc[8] = &XEXP (XVECEXP (pat, 0, 4), 0));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 2), 0);
      recog_data.dup_num[0] = 2;
      break;

    case 3134:  /* atomic_compare_and_swapdi_1 */
    case 3133:  /* atomic_compare_and_swapsi_1 */
    case 3132:  /* atomic_compare_and_swaphi_1 */
    case 3131:  /* atomic_compare_and_swapqi_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 3122:  /* mfence_nosse */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0);
      recog_data.dup_num[0] = 0;
      break;

    case 3087:  /* avx512f_compressstorev8df_mask */
    case 3086:  /* avx512f_compressstorev8di_mask */
    case 3085:  /* avx512f_compressstorev16sf_mask */
    case 3084:  /* avx512f_compressstorev16si_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 2));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (pat, 1), 0, 1);
      recog_data.dup_num[0] = 0;
      break;

    case 3079:  /* *avx512f_scatterdiv8df */
    case 3078:  /* *avx512f_scatterdiv8df */
    case 3077:  /* *avx512f_scatterdiv8di */
    case 3076:  /* *avx512f_scatterdiv8di */
    case 3075:  /* *avx512f_scatterdiv16sf */
    case 3074:  /* *avx512f_scatterdiv16sf */
    case 3073:  /* *avx512f_scatterdiv16si */
    case 3072:  /* *avx512f_scatterdiv16si */
    case 3071:  /* *avx512f_scattersiv8df */
    case 3070:  /* *avx512f_scattersiv8df */
    case 3069:  /* *avx512f_scattersiv8di */
    case 3068:  /* *avx512f_scattersiv8di */
    case 3067:  /* *avx512f_scattersiv16sf */
    case 3066:  /* *avx512f_scattersiv16sf */
    case 3065:  /* *avx512f_scattersiv16si */
    case 3064:  /* *avx512f_scattersiv16si */
      ro[0] = *(ro_loc[0] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0), 0, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0), 0, 2));
      ro[5] = *(ro_loc[5] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      break;

    case 3063:  /* *avx512f_gatherdiv8df_2 */
    case 3062:  /* *avx512f_gatherdiv8df_2 */
    case 3061:  /* *avx512f_gatherdiv8di_2 */
    case 3060:  /* *avx512f_gatherdiv8di_2 */
    case 3059:  /* *avx512f_gatherdiv16sf_2 */
    case 3058:  /* *avx512f_gatherdiv16sf_2 */
    case 3057:  /* *avx512f_gatherdiv16si_2 */
    case 3056:  /* *avx512f_gatherdiv16si_2 */
    case 3047:  /* *avx512f_gathersiv8df_2 */
    case 3046:  /* *avx512f_gathersiv8df_2 */
    case 3045:  /* *avx512f_gathersiv8di_2 */
    case 3044:  /* *avx512f_gathersiv8di_2 */
    case 3043:  /* *avx512f_gathersiv16sf_2 */
    case 3042:  /* *avx512f_gathersiv16sf_2 */
    case 3041:  /* *avx512f_gathersiv16si_2 */
    case 3040:  /* *avx512f_gathersiv16si_2 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2), 0), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2), 0), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      break;

    case 3055:  /* *avx512f_gatherdiv8df */
    case 3054:  /* *avx512f_gatherdiv8df */
    case 3053:  /* *avx512f_gatherdiv8di */
    case 3052:  /* *avx512f_gatherdiv8di */
    case 3051:  /* *avx512f_gatherdiv16sf */
    case 3050:  /* *avx512f_gatherdiv16sf */
    case 3049:  /* *avx512f_gatherdiv16si */
    case 3048:  /* *avx512f_gatherdiv16si */
    case 3039:  /* *avx512f_gathersiv8df */
    case 3038:  /* *avx512f_gathersiv8df */
    case 3037:  /* *avx512f_gathersiv8di */
    case 3036:  /* *avx512f_gathersiv8di */
    case 3035:  /* *avx512f_gathersiv16sf */
    case 3034:  /* *avx512f_gathersiv16sf */
    case 3033:  /* *avx512f_gathersiv16si */
    case 3032:  /* *avx512f_gathersiv16si */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2), 0), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2), 0), 0, 0));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2), 0), 0, 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      break;

    case 3031:  /* *avx2_gatherdiv8sf_4 */
    case 3030:  /* *avx2_gatherdiv8sf_4 */
    case 3029:  /* *avx2_gatherdiv8si_4 */
    case 3028:  /* *avx2_gatherdiv8si_4 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1), 0), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1), 0), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1), 0), 0, 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      break;

    case 3027:  /* *avx2_gatherdiv8sf_3 */
    case 3026:  /* *avx2_gatherdiv8sf_3 */
    case 3025:  /* *avx2_gatherdiv8si_3 */
    case 3024:  /* *avx2_gatherdiv8si_3 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1), 0), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1), 0), 0, 1));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1), 0), 0, 2));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      break;

    case 3023:  /* *avx2_gatherdiv8sf_2 */
    case 3022:  /* *avx2_gatherdiv8sf_2 */
    case 3021:  /* *avx2_gatherdiv8si_2 */
    case 3020:  /* *avx2_gatherdiv8si_2 */
    case 3019:  /* *avx2_gatherdiv4sf_2 */
    case 3018:  /* *avx2_gatherdiv4sf_2 */
    case 3017:  /* *avx2_gatherdiv4si_2 */
    case 3016:  /* *avx2_gatherdiv4si_2 */
    case 3015:  /* *avx2_gatherdiv4df_2 */
    case 3014:  /* *avx2_gatherdiv4df_2 */
    case 3013:  /* *avx2_gatherdiv4di_2 */
    case 3012:  /* *avx2_gatherdiv4di_2 */
    case 3011:  /* *avx2_gatherdiv2df_2 */
    case 3010:  /* *avx2_gatherdiv2df_2 */
    case 3009:  /* *avx2_gatherdiv2di_2 */
    case 3008:  /* *avx2_gatherdiv2di_2 */
    case 2991:  /* *avx2_gathersiv8sf_2 */
    case 2990:  /* *avx2_gathersiv8sf_2 */
    case 2989:  /* *avx2_gathersiv8si_2 */
    case 2988:  /* *avx2_gathersiv8si_2 */
    case 2987:  /* *avx2_gathersiv4sf_2 */
    case 2986:  /* *avx2_gathersiv4sf_2 */
    case 2985:  /* *avx2_gathersiv4si_2 */
    case 2984:  /* *avx2_gathersiv4si_2 */
    case 2983:  /* *avx2_gathersiv4df_2 */
    case 2982:  /* *avx2_gathersiv4df_2 */
    case 2981:  /* *avx2_gathersiv4di_2 */
    case 2980:  /* *avx2_gathersiv4di_2 */
    case 2979:  /* *avx2_gathersiv2df_2 */
    case 2978:  /* *avx2_gathersiv2df_2 */
    case 2977:  /* *avx2_gathersiv2di_2 */
    case 2976:  /* *avx2_gathersiv2di_2 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1), 0), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1), 0), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1), 0), 0, 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      break;

    case 3007:  /* *avx2_gatherdiv8sf */
    case 3006:  /* *avx2_gatherdiv8sf */
    case 3005:  /* *avx2_gatherdiv8si */
    case 3004:  /* *avx2_gatherdiv8si */
    case 3003:  /* *avx2_gatherdiv4sf */
    case 3002:  /* *avx2_gatherdiv4sf */
    case 3001:  /* *avx2_gatherdiv4si */
    case 3000:  /* *avx2_gatherdiv4si */
    case 2999:  /* *avx2_gatherdiv4df */
    case 2998:  /* *avx2_gatherdiv4df */
    case 2997:  /* *avx2_gatherdiv4di */
    case 2996:  /* *avx2_gatherdiv4di */
    case 2995:  /* *avx2_gatherdiv2df */
    case 2994:  /* *avx2_gatherdiv2df */
    case 2993:  /* *avx2_gatherdiv2di */
    case 2992:  /* *avx2_gatherdiv2di */
    case 2975:  /* *avx2_gathersiv8sf */
    case 2974:  /* *avx2_gathersiv8sf */
    case 2973:  /* *avx2_gathersiv8si */
    case 2972:  /* *avx2_gathersiv8si */
    case 2971:  /* *avx2_gathersiv4sf */
    case 2970:  /* *avx2_gathersiv4sf */
    case 2969:  /* *avx2_gathersiv4si */
    case 2968:  /* *avx2_gathersiv4si */
    case 2967:  /* *avx2_gathersiv4df */
    case 2966:  /* *avx2_gathersiv4df */
    case 2965:  /* *avx2_gathersiv4di */
    case 2964:  /* *avx2_gathersiv4di */
    case 2963:  /* *avx2_gathersiv2df */
    case 2962:  /* *avx2_gathersiv2df */
    case 2961:  /* *avx2_gathersiv2di */
    case 2960:  /* *avx2_gathersiv2di */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1), 0), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1), 0), 0, 1));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1), 0), 0, 2));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      break;

    case 2910:  /* avx2_maskstoreq256 */
    case 2909:  /* avx2_maskstored256 */
    case 2908:  /* avx2_maskstoreq */
    case 2907:  /* avx2_maskstored */
    case 2906:  /* avx_maskstorepd256 */
    case 2905:  /* avx_maskstoreps256 */
    case 2904:  /* avx_maskstorepd */
    case 2903:  /* avx_maskstoreps */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 1));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (pat, 1), 0, 2);
      recog_data.dup_num[0] = 0;
      break;

    case 2902:  /* avx2_maskloadq256 */
    case 2901:  /* avx2_maskloadd256 */
    case 2900:  /* avx2_maskloadq */
    case 2899:  /* avx2_maskloadd */
    case 2898:  /* avx_maskloadpd256 */
    case 2897:  /* avx_maskloadps256 */
    case 2896:  /* avx_maskloadpd */
    case 2895:  /* avx_maskloadps */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 1));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 0));
      break;

    case 2880:  /* *avx_vperm2f128v4df_nozero */
    case 2879:  /* *avx_vperm2f128v8sf_nozero */
    case 2878:  /* *avx_vperm2f128v8si_nozero */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      break;

    case 2874:  /* avx512f_vpermt2varv8df3_mask */
    case 2873:  /* avx512f_vpermt2varv8di3_mask */
    case 2872:  /* avx512f_vpermt2varv16sf3_mask */
    case 2871:  /* avx512f_vpermt2varv16si3_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 2;
      break;

    case 2862:  /* avx512f_vpermi2varv8df3_mask */
    case 2861:  /* avx512f_vpermi2varv8di3_mask */
    case 2860:  /* avx512f_vpermi2varv16sf3_mask */
    case 2859:  /* avx512f_vpermi2varv16si3_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 0;
      break;

    case 2840:  /* *avx512f_vpermilpv8df_mask */
    case 2836:  /* *avx512f_vpermilpv16sf_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2842:  /* *avx_vpermilpv2df */
    case 2841:  /* *avx_vpermilpv4df */
    case 2839:  /* *avx512f_vpermilpv8df */
    case 2838:  /* *avx_vpermilpv4sf */
    case 2837:  /* *avx_vpermilpv8sf */
    case 2835:  /* *avx512f_vpermilpv16sf */
    case 2834:  /* *avx_vperm_broadcast_v4df */
    case 2833:  /* *avx_vperm_broadcast_v8sf */
    case 2832:  /* *avx_vperm_broadcast_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      break;

    case 2829:  /* avx_vbroadcastf128_v4df */
    case 2828:  /* avx_vbroadcastf128_v8sf */
    case 2827:  /* avx_vbroadcastf128_v4di */
    case 2826:  /* avx_vbroadcastf128_v8si */
    case 2825:  /* avx_vbroadcastf128_v16hi */
    case 2824:  /* avx_vbroadcastf128_v32qi */
    case 2823:  /* avx2_vbroadcasti128_v4di */
    case 2822:  /* avx2_vbroadcasti128_v8si */
    case 2821:  /* avx2_vbroadcasti128_v16hi */
    case 2820:  /* avx2_vbroadcasti128_v32qi */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 2738:  /* xop_maskcmp_uns2v2di3 */
    case 2737:  /* xop_maskcmp_uns2v4si3 */
    case 2736:  /* xop_maskcmp_uns2v8hi3 */
    case 2735:  /* xop_maskcmp_uns2v16qi3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (XEXP (pat, 1), 0, 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (pat, 1), 0, 0), 1));
      break;

    case 2734:  /* xop_maskcmp_unsv2di3 */
    case 2733:  /* xop_maskcmp_unsv4si3 */
    case 2732:  /* xop_maskcmp_unsv8hi3 */
    case 2731:  /* xop_maskcmp_unsv16qi3 */
    case 2730:  /* xop_maskcmpv2di3 */
    case 2729:  /* xop_maskcmpv4si3 */
    case 2728:  /* xop_maskcmpv8hi3 */
    case 2727:  /* xop_maskcmpv16qi3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (pat, 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 2716:  /* xop_shlv2di3 */
    case 2715:  /* xop_shlv4si3 */
    case 2714:  /* xop_shlv8hi3 */
    case 2713:  /* xop_shlv16qi3 */
    case 2712:  /* xop_shav2di3 */
    case 2711:  /* xop_shav4si3 */
    case 2710:  /* xop_shav8hi3 */
    case 2709:  /* xop_shav16qi3 */
    case 2708:  /* xop_vrotlv2di3 */
    case 2707:  /* xop_vrotlv4si3 */
    case 2706:  /* xop_vrotlv8hi3 */
    case 2705:  /* xop_vrotlv16qi3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 1), 1);
      recog_data.dup_num[0] = 2;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 2), 0);
      recog_data.dup_num[1] = 1;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 2), 1), 0);
      recog_data.dup_num[2] = 2;
      break;

    case 2696:  /* xop_pperm_pack_v8hi_v16qi */
    case 2695:  /* xop_pperm_pack_v4si_v8hi */
    case 2694:  /* xop_pperm_pack_v2di_v4si */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 2683:  /* xop_phaddubq */
    case 2682:  /* xop_phaddbq */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0), 0);
      recog_data.dup_num[1] = 1;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 0), 0);
      recog_data.dup_num[2] = 1;
      recog_data.dup_loc[3] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 0), 0);
      recog_data.dup_num[3] = 1;
      recog_data.dup_loc[4] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 0), 0);
      recog_data.dup_num[4] = 1;
      recog_data.dup_loc[5] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 0), 0);
      recog_data.dup_num[5] = 1;
      recog_data.dup_loc[6] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 0), 0);
      recog_data.dup_num[6] = 1;
      break;

    case 2687:  /* xop_phadduwq */
    case 2686:  /* xop_phaddwq */
    case 2681:  /* xop_phaddubd */
    case 2680:  /* xop_phaddbd */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 0);
      recog_data.dup_num[1] = 1;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 0);
      recog_data.dup_num[2] = 1;
      break;

    case 2692:  /* xop_phsubdq */
    case 2691:  /* xop_phsubwd */
    case 2690:  /* xop_phsubbw */
    case 2689:  /* xop_phaddudq */
    case 2688:  /* xop_phadddq */
    case 2685:  /* xop_phadduwd */
    case 2684:  /* xop_phaddwd */
    case 2679:  /* xop_phaddubw */
    case 2678:  /* xop_phaddbw */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 2677:  /* xop_pcmov_v2df */
    case 2676:  /* xop_pcmov_v4df256 */
    case 2675:  /* xop_pcmov_v8df512 */
    case 2674:  /* xop_pcmov_v4sf */
    case 2673:  /* xop_pcmov_v8sf256 */
    case 2672:  /* xop_pcmov_v16sf512 */
    case 2671:  /* xop_pcmov_v2di */
    case 2670:  /* xop_pcmov_v4di256 */
    case 2669:  /* xop_pcmov_v8di512 */
    case 2668:  /* xop_pcmov_v4si */
    case 2667:  /* xop_pcmov_v8si256 */
    case 2666:  /* xop_pcmov_v16si512 */
    case 2665:  /* xop_pcmov_v8hi */
    case 2664:  /* xop_pcmov_v16hi256 */
    case 2663:  /* xop_pcmov_v16qi */
    case 2662:  /* xop_pcmov_v32qi256 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 2));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 0));
      break;

    case 2661:  /* xop_pmadcsswd */
    case 2660:  /* xop_pmadcswd */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 0), 0);
      recog_data.dup_num[1] = 2;
      break;

    case 2659:  /* xop_pmacsswd */
    case 2658:  /* xop_pmacswd */
    case 2657:  /* xop_pmacssdqh */
    case 2656:  /* xop_pmacsdqh */
    case 2655:  /* xop_pmacssdql */
    case 2654:  /* xop_pmacsdql */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 2649:  /* avx512er_vmrsqrt28v2df_round */
    case 2647:  /* avx512er_vmrsqrt28v4sf_round */
    case 2637:  /* avx512er_vmrcp28v2df_round */
    case 2635:  /* avx512er_vmrcp28v4sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 2617:  /* *avx512pf_scatterpfv8didf */
    case 2616:  /* *avx512pf_scatterpfv8didf */
    case 2615:  /* *avx512pf_scatterpfv8sidf */
    case 2614:  /* *avx512pf_scatterpfv8sidf */
    case 2609:  /* *avx512pf_scatterpfv8disf */
    case 2608:  /* *avx512pf_scatterpfv8disf */
    case 2607:  /* *avx512pf_scatterpfv16sisf */
    case 2606:  /* *avx512pf_scatterpfv16sisf */
    case 2601:  /* *avx512pf_gatherpfv8didf */
    case 2600:  /* *avx512pf_gatherpfv8didf */
    case 2599:  /* *avx512pf_gatherpfv8sidf */
    case 2598:  /* *avx512pf_gatherpfv8sidf */
    case 2593:  /* *avx512pf_gatherpfv8disf */
    case 2592:  /* *avx512pf_gatherpfv8disf */
    case 2591:  /* *avx512pf_gatherpfv16sisf */
    case 2590:  /* *avx512pf_gatherpfv16sisf */
      ro[0] = *(ro_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 0), 0, 1));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 0), 0, 2));
      ro[3] = *(ro_loc[3] = &XVECEXP (pat, 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (pat, 0, 1));
      break;

    case 2613:  /* *avx512pf_scatterpfv8didf_mask */
    case 2612:  /* *avx512pf_scatterpfv8didf_mask */
    case 2611:  /* *avx512pf_scatterpfv8sidf_mask */
    case 2610:  /* *avx512pf_scatterpfv8sidf_mask */
    case 2605:  /* *avx512pf_scatterpfv8disf_mask */
    case 2604:  /* *avx512pf_scatterpfv8disf_mask */
    case 2603:  /* *avx512pf_scatterpfv16sisf_mask */
    case 2602:  /* *avx512pf_scatterpfv16sisf_mask */
    case 2597:  /* *avx512pf_gatherpfv8didf_mask */
    case 2596:  /* *avx512pf_gatherpfv8didf_mask */
    case 2595:  /* *avx512pf_gatherpfv8sidf_mask */
    case 2594:  /* *avx512pf_gatherpfv8sidf_mask */
    case 2589:  /* *avx512pf_gatherpfv8disf_mask */
    case 2588:  /* *avx512pf_gatherpfv8disf_mask */
    case 2587:  /* *avx512pf_gatherpfv16sisf_mask */
    case 2586:  /* *avx512pf_gatherpfv16sisf_mask */
      ro[0] = *(ro_loc[0] = &XVECEXP (pat, 0, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 0), 0, 1));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 0), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (pat, 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (pat, 0, 1));
      break;

    case 2585:  /* sse4_2_pcmpistr_cconly */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      break;

    case 2584:  /* sse4_2_pcmpistrm */
    case 2583:  /* sse4_2_pcmpistri */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 2);
      recog_data.dup_num[0] = 3;
      recog_data.dup_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 1);
      recog_data.dup_num[1] = 2;
      recog_data.dup_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[2] = 1;
      break;

    case 2582:  /* *sse4_2_pcmpistr_unaligned */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 2);
      recog_data.dup_num[0] = 4;
      recog_data.dup_loc[1] = &XVECEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 1), 0, 0);
      recog_data.dup_num[1] = 3;
      recog_data.dup_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 0);
      recog_data.dup_num[2] = 2;
      recog_data.dup_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 2);
      recog_data.dup_num[3] = 4;
      recog_data.dup_loc[4] = &XVECEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 1), 0, 0);
      recog_data.dup_num[4] = 3;
      recog_data.dup_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[5] = 2;
      break;

    case 2581:  /* sse4_2_pcmpistr */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 2);
      recog_data.dup_num[0] = 4;
      recog_data.dup_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 1);
      recog_data.dup_num[1] = 3;
      recog_data.dup_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 0);
      recog_data.dup_num[2] = 2;
      recog_data.dup_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 2);
      recog_data.dup_num[3] = 4;
      recog_data.dup_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 1);
      recog_data.dup_num[4] = 3;
      recog_data.dup_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[5] = 2;
      break;

    case 2580:  /* sse4_2_pcmpestr_cconly */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 4));
      break;

    case 2579:  /* sse4_2_pcmpestrm */
    case 2578:  /* sse4_2_pcmpestri */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 4));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 4);
      recog_data.dup_num[0] = 5;
      recog_data.dup_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 3);
      recog_data.dup_num[1] = 4;
      recog_data.dup_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 2);
      recog_data.dup_num[2] = 3;
      recog_data.dup_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 1);
      recog_data.dup_num[3] = 2;
      recog_data.dup_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[4] = 1;
      break;

    case 2577:  /* *sse4_2_pcmpestr_unaligned */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2), 0, 0));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 4));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 4);
      recog_data.dup_num[0] = 6;
      recog_data.dup_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 3);
      recog_data.dup_num[1] = 5;
      recog_data.dup_loc[2] = &XVECEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 2), 0, 0);
      recog_data.dup_num[2] = 4;
      recog_data.dup_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 1);
      recog_data.dup_num[3] = 3;
      recog_data.dup_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 0);
      recog_data.dup_num[4] = 2;
      recog_data.dup_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 4);
      recog_data.dup_num[5] = 6;
      recog_data.dup_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 3);
      recog_data.dup_num[6] = 5;
      recog_data.dup_loc[7] = &XVECEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 2), 0, 0);
      recog_data.dup_num[7] = 4;
      recog_data.dup_loc[8] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 1);
      recog_data.dup_num[8] = 3;
      recog_data.dup_loc[9] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[9] = 2;
      break;

    case 2576:  /* sse4_2_pcmpestr */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 4));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 4);
      recog_data.dup_num[0] = 6;
      recog_data.dup_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 3);
      recog_data.dup_num[1] = 5;
      recog_data.dup_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 2);
      recog_data.dup_num[2] = 4;
      recog_data.dup_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 1);
      recog_data.dup_num[3] = 3;
      recog_data.dup_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 0);
      recog_data.dup_num[4] = 2;
      recog_data.dup_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 4);
      recog_data.dup_num[5] = 6;
      recog_data.dup_loc[6] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 3);
      recog_data.dup_num[6] = 5;
      recog_data.dup_loc[7] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 2);
      recog_data.dup_num[7] = 4;
      recog_data.dup_loc[8] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 1);
      recog_data.dup_num[8] = 3;
      recog_data.dup_loc[9] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[9] = 2;
      break;

    case 2575:  /* sse4_1_roundsd */
    case 2574:  /* sse4_1_roundss */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 1));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      break;

    case 2569:  /* sse4_1_ptest */
    case 2568:  /* avx_ptest256 */
    case 2567:  /* avx_vtestpd */
    case 2566:  /* avx_vtestpd256 */
    case 2565:  /* avx_vtestps */
    case 2564:  /* avx_vtestps256 */
      ro[0] = *(ro_loc[0] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 1));
      break;

    case 2799:  /* avx512f_vec_dupv8df_mask */
    case 2797:  /* avx512f_vec_dupv8di_mask */
    case 2795:  /* avx512f_vec_dupv16sf_mask */
    case 2793:  /* avx512f_vec_dupv16si_mask */
    case 2543:  /* avx512f_zero_extendv8qiv8di2_mask */
    case 2541:  /* avx512f_sign_extendv8qiv8di2_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2457:  /* *ssse3_pmulhrswv4hi3 */
    case 2456:  /* *ssse3_pmulhrswv8hi3 */
    case 2455:  /* *avx2_pmulhrswv16hi3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1));
      break;

    case 2447:  /* avx2_phsubdv8si3 */
    case 2446:  /* avx2_phadddv8si3 */
    case 2441:  /* ssse3_phsubswv8hi3 */
    case 2440:  /* ssse3_phsubwv8hi3 */
    case 2439:  /* ssse3_phaddswv8hi3 */
    case 2438:  /* ssse3_phaddwv8hi3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0), 0);
      recog_data.dup_num[1] = 1;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 1), 0);
      recog_data.dup_num[2] = 1;
      recog_data.dup_loc[3] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0), 0);
      recog_data.dup_num[3] = 1;
      recog_data.dup_loc[4] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 1), 0);
      recog_data.dup_num[4] = 1;
      recog_data.dup_loc[5] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 0), 0);
      recog_data.dup_num[5] = 1;
      recog_data.dup_loc[6] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 1), 0);
      recog_data.dup_num[6] = 1;
      recog_data.dup_loc[7] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 1), 0);
      recog_data.dup_num[7] = 2;
      recog_data.dup_loc[8] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 0), 0);
      recog_data.dup_num[8] = 2;
      recog_data.dup_loc[9] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 1), 0);
      recog_data.dup_num[9] = 2;
      recog_data.dup_loc[10] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 0), 0);
      recog_data.dup_num[10] = 2;
      recog_data.dup_loc[11] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 1), 0);
      recog_data.dup_num[11] = 2;
      recog_data.dup_loc[12] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 0), 0);
      recog_data.dup_num[12] = 2;
      recog_data.dup_loc[13] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 1), 0);
      recog_data.dup_num[13] = 2;
      break;

    case 2437:  /* avx2_phsubswv16hi3 */
    case 2436:  /* avx2_phsubwv16hi3 */
    case 2435:  /* avx2_phaddswv16hi3 */
    case 2434:  /* avx2_phaddwv16hi3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 1), 0), 0);
      recog_data.dup_num[1] = 1;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 1), 1), 0);
      recog_data.dup_num[2] = 1;
      recog_data.dup_loc[3] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0), 0), 0);
      recog_data.dup_num[3] = 1;
      recog_data.dup_loc[4] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0), 1), 0);
      recog_data.dup_num[4] = 1;
      recog_data.dup_loc[5] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 1), 0), 0);
      recog_data.dup_num[5] = 1;
      recog_data.dup_loc[6] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 1), 1), 0);
      recog_data.dup_num[6] = 1;
      recog_data.dup_loc[7] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0), 0), 0);
      recog_data.dup_num[7] = 1;
      recog_data.dup_loc[8] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0), 1), 0);
      recog_data.dup_num[8] = 1;
      recog_data.dup_loc[9] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 1), 0), 0);
      recog_data.dup_num[9] = 1;
      recog_data.dup_loc[10] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 1), 1), 0);
      recog_data.dup_num[10] = 1;
      recog_data.dup_loc[11] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 0), 0), 0);
      recog_data.dup_num[11] = 1;
      recog_data.dup_loc[12] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 0), 1), 0);
      recog_data.dup_num[12] = 1;
      recog_data.dup_loc[13] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 1), 0), 0);
      recog_data.dup_num[13] = 1;
      recog_data.dup_loc[14] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 1), 1), 0);
      recog_data.dup_num[14] = 1;
      recog_data.dup_loc[15] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 0), 1), 0);
      recog_data.dup_num[15] = 2;
      recog_data.dup_loc[16] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 1), 0), 0);
      recog_data.dup_num[16] = 2;
      recog_data.dup_loc[17] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 1), 1), 0);
      recog_data.dup_num[17] = 2;
      recog_data.dup_loc[18] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 0), 0), 0);
      recog_data.dup_num[18] = 2;
      recog_data.dup_loc[19] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 0), 1), 0);
      recog_data.dup_num[19] = 2;
      recog_data.dup_loc[20] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 1), 0), 0);
      recog_data.dup_num[20] = 2;
      recog_data.dup_loc[21] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 1), 1), 0);
      recog_data.dup_num[21] = 2;
      recog_data.dup_loc[22] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 0), 0), 0);
      recog_data.dup_num[22] = 2;
      recog_data.dup_loc[23] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 0), 1), 0);
      recog_data.dup_num[23] = 2;
      recog_data.dup_loc[24] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 1), 0), 0);
      recog_data.dup_num[24] = 2;
      recog_data.dup_loc[25] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 1), 1), 0);
      recog_data.dup_num[25] = 2;
      recog_data.dup_loc[26] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 0), 0), 0);
      recog_data.dup_num[26] = 2;
      recog_data.dup_loc[27] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 0), 1), 0);
      recog_data.dup_num[27] = 2;
      recog_data.dup_loc[28] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 1), 0), 0);
      recog_data.dup_num[28] = 2;
      recog_data.dup_loc[29] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 1), 1), 0);
      recog_data.dup_num[29] = 2;
      break;

    case 2417:  /* *sse2_uavgv8hi3 */
    case 2416:  /* *avx2_uavgv16hi3 */
    case 2415:  /* *sse2_uavgv16qi3 */
    case 2414:  /* *avx2_uavgv32qi3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1));
      break;

    case 2388:  /* sse2_pshufhw_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 4));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 5));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 6));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 7));
      break;

    case 2387:  /* avx2_pshufhw_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 4));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 5));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 6));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 7));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 12));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 13));
      ro[8] = *(ro_loc[8] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 14));
      ro[9] = *(ro_loc[9] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 15));
      break;

    case 2385:  /* avx2_pshuflw_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 8));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 9));
      ro[8] = *(ro_loc[8] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 10));
      ro[9] = *(ro_loc[9] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 11));
      break;

    case 2383:  /* avx2_pshufd_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 4));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 5));
      ro[8] = *(ro_loc[8] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 6));
      ro[9] = *(ro_loc[9] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 7));
      break;

    case 2382:  /* avx512f_pshufd_1_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 4));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 5));
      ro[8] = *(ro_loc[8] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 6));
      ro[9] = *(ro_loc[9] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 7));
      ro[10] = *(ro_loc[10] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 8));
      ro[11] = *(ro_loc[11] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 9));
      ro[12] = *(ro_loc[12] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 10));
      ro[13] = *(ro_loc[13] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 11));
      ro[14] = *(ro_loc[14] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 12));
      ro[15] = *(ro_loc[15] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 13));
      ro[16] = *(ro_loc[16] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 14));
      ro[17] = *(ro_loc[17] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 15));
      ro[18] = *(ro_loc[18] = &XEXP (XEXP (pat, 1), 1));
      ro[19] = *(ro_loc[19] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2381:  /* avx512f_pshufd_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 4));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 5));
      ro[8] = *(ro_loc[8] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 6));
      ro[9] = *(ro_loc[9] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 7));
      ro[10] = *(ro_loc[10] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 8));
      ro[11] = *(ro_loc[11] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 9));
      ro[12] = *(ro_loc[12] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 10));
      ro[13] = *(ro_loc[13] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 11));
      ro[14] = *(ro_loc[14] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 12));
      ro[15] = *(ro_loc[15] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 13));
      ro[16] = *(ro_loc[16] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 14));
      ro[17] = *(ro_loc[17] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 15));
      break;

    case 2372:  /* vec_set_hi_v8di_mask */
    case 2370:  /* vec_set_hi_v8df_mask */
    case 2368:  /* vec_set_lo_v8di_mask */
    case 2366:  /* vec_set_lo_v8df_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2364:  /* avx512f_vinserti32x4_1_mask */
    case 2362:  /* avx512f_vinsertf32x4_1_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2363:  /* *avx512f_vinserti32x4_1 */
    case 2361:  /* *avx512f_vinsertf32x4_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2282:  /* *andnotv8di3_mask */
    case 2280:  /* *andnotv16si3_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2955:  /* *vcvtps2ph */
    case 2334:  /* avx512f_testnmv8di3_mask */
    case 2332:  /* avx512f_testnmv16si3_mask */
    case 2330:  /* avx512f_testmv8di3_mask */
    case 2328:  /* avx512f_testmv16si3_mask */
    case 2275:  /* avx512f_gtv8di3_mask */
    case 2273:  /* avx512f_gtv16si3_mask */
    case 2262:  /* avx512f_eqv8di3_mask_1 */
    case 2260:  /* avx512f_eqv16si3_mask_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 2116:  /* *vec_widen_smult_even_v16si_mask */
    case 2112:  /* *vec_widen_umult_even_v16si_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2050:  /* avx512f_us_truncatev8div16qi2_mask_store */
    case 2049:  /* avx512f_truncatev8div16qi2_mask_store */
    case 2048:  /* avx512f_ss_truncatev8div16qi2_mask_store */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0);
      recog_data.dup_num[0] = 0;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 1), 0);
      recog_data.dup_num[1] = 0;
      break;

    case 2047:  /* avx512f_us_truncatev8div16qi2_mask */
    case 2046:  /* avx512f_truncatev8div16qi2_mask */
    case 2045:  /* avx512f_ss_truncatev8div16qi2_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      break;

    case 2044:  /* *avx512f_us_truncatev8div16qi2_store */
    case 2043:  /* *avx512f_truncatev8div16qi2_store */
    case 2042:  /* *avx512f_ss_truncatev8div16qi2_store */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 1), 0);
      recog_data.dup_num[0] = 0;
      break;

    case 2005:  /* sse2_shufpd_v2df */
    case 2004:  /* sse2_shufpd_v2di */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 1));
      break;

    case 2376:  /* avx512f_shuf_i64x2_1_mask */
    case 2374:  /* avx512f_shuf_f64x2_1_mask */
    case 1994:  /* avx512f_shufpd512_1_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 1));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 3));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 4));
      ro[8] = *(ro_loc[8] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 5));
      ro[9] = *(ro_loc[9] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 6));
      ro[10] = *(ro_loc[10] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 7));
      ro[11] = *(ro_loc[11] = &XEXP (XEXP (pat, 1), 1));
      ro[12] = *(ro_loc[12] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2380:  /* avx512f_shuf_i32x4_1_mask */
    case 2378:  /* avx512f_shuf_f32x4_1_mask */
    case 1992:  /* avx512f_shufps512_1_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 1));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 3));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 4));
      ro[8] = *(ro_loc[8] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 5));
      ro[9] = *(ro_loc[9] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 6));
      ro[10] = *(ro_loc[10] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 7));
      ro[11] = *(ro_loc[11] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 8));
      ro[12] = *(ro_loc[12] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 9));
      ro[13] = *(ro_loc[13] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 10));
      ro[14] = *(ro_loc[14] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 11));
      ro[15] = *(ro_loc[15] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 12));
      ro[16] = *(ro_loc[16] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 13));
      ro[17] = *(ro_loc[17] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 14));
      ro[18] = *(ro_loc[18] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 15));
      ro[19] = *(ro_loc[19] = &XEXP (XEXP (pat, 1), 1));
      ro[20] = *(ro_loc[20] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2379:  /* avx512f_shuf_i32x4_1 */
    case 2377:  /* avx512f_shuf_f32x4_1 */
    case 1991:  /* avx512f_shufps512_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 1));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 3));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 4));
      ro[8] = *(ro_loc[8] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 5));
      ro[9] = *(ro_loc[9] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 6));
      ro[10] = *(ro_loc[10] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 7));
      ro[11] = *(ro_loc[11] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 8));
      ro[12] = *(ro_loc[12] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 9));
      ro[13] = *(ro_loc[13] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 10));
      ro[14] = *(ro_loc[14] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 11));
      ro[15] = *(ro_loc[15] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 12));
      ro[16] = *(ro_loc[16] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 13));
      ro[17] = *(ro_loc[17] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 14));
      ro[18] = *(ro_loc[18] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 15));
      break;

    case 3103:  /* avx512f_getmantv2df_round */
    case 3101:  /* avx512f_getmantv4sf_round */
    case 1990:  /* avx512f_rndscalev2df_round */
    case 1988:  /* avx512f_rndscalev4sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1978:  /* avx512f_sfixupimmv2df_mask_round */
    case 1976:  /* avx512f_sfixupimmv4sf_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[1] = 1;
      break;

    case 1977:  /* avx512f_sfixupimmv2df_mask */
    case 1975:  /* avx512f_sfixupimmv4sf_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 0), 1);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[1] = 1;
      break;

    case 1974:  /* avx512f_sfixupimmv2df_maskz_1_round */
    case 1970:  /* avx512f_sfixupimmv4sf_maskz_1_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[6] = *(ro_loc[6] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[7] = *(ro_loc[7] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1973:  /* avx512f_sfixupimmv2df_maskz_1 */
    case 1969:  /* avx512f_sfixupimmv4sf_maskz_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 1));
      ro[6] = *(ro_loc[6] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 0), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1972:  /* avx512f_sfixupimmv2df_round */
    case 1968:  /* avx512f_sfixupimmv4sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1971:  /* avx512f_sfixupimmv2df */
    case 1967:  /* avx512f_sfixupimmv4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 3));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1966:  /* avx512f_fixupimmv8df_mask_round */
    case 1964:  /* avx512f_fixupimmv16sf_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1962:  /* avx512f_fixupimmv8df_maskz_1_round */
    case 1958:  /* avx512f_fixupimmv16sf_maskz_1_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[6] = *(ro_loc[6] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[7] = *(ro_loc[7] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1960:  /* avx512f_fixupimmv8df_round */
    case 1956:  /* avx512f_fixupimmv16sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1965:  /* avx512f_fixupimmv8df_mask */
    case 1963:  /* avx512f_fixupimmv16sf_mask */
    case 1938:  /* avx512f_vternlogv8di_mask */
    case 1937:  /* avx512f_vternlogv16si_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1961:  /* avx512f_fixupimmv8df_maskz_1 */
    case 1957:  /* avx512f_fixupimmv16sf_maskz_1 */
    case 1936:  /* avx512f_vternlogv8di_maskz_1 */
    case 1934:  /* avx512f_vternlogv16si_maskz_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 3));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 1));
      ro[6] = *(ro_loc[6] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2746:  /* xop_vpermil2v2df3 */
    case 2745:  /* xop_vpermil2v4df3 */
    case 2744:  /* xop_vpermil2v4sf3 */
    case 2743:  /* xop_vpermil2v8sf3 */
    case 2492:  /* sse4a_insertqi */
    case 1959:  /* avx512f_fixupimmv8df */
    case 1955:  /* avx512f_fixupimmv16sf */
    case 1935:  /* avx512f_vternlogv8di */
    case 1933:  /* avx512f_vternlogv16si */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (pat, 1), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (pat, 1), 0, 3));
      break;

    case 3099:  /* avx512f_getmantv8df_mask_round */
    case 3095:  /* avx512f_getmantv16sf_mask_round */
    case 1986:  /* avx512f_rndscalev8df_mask_round */
    case 1982:  /* avx512f_rndscalev16sf_mask_round */
    case 1932:  /* avx512f_scalefv8df_mask_round */
    case 1928:  /* avx512f_scalefv16sf_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 3098:  /* avx512f_getmantv8df_mask */
    case 3094:  /* avx512f_getmantv16sf_mask */
    case 2959:  /* avx512f_vcvtps2ph512_mask */
    case 2848:  /* avx512f_vpermilvarv8df3_mask */
    case 2844:  /* avx512f_vpermilvarv16sf3_mask */
    case 2779:  /* avx512f_permvarv8df_mask */
    case 2777:  /* avx512f_permvarv8di_mask */
    case 2775:  /* avx512f_permvarv16sf_mask */
    case 2773:  /* avx512f_permvarv16si_mask */
    case 1985:  /* avx512f_rndscalev8df_mask */
    case 1981:  /* avx512f_rndscalev16sf_mask */
    case 1931:  /* avx512f_scalefv8df_mask */
    case 1927:  /* avx512f_scalefv16sf_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 3097:  /* avx512f_getmantv8df_round */
    case 3093:  /* avx512f_getmantv16sf_round */
    case 1984:  /* avx512f_rndscalev8df_round */
    case 1980:  /* avx512f_rndscalev16sf_round */
    case 1930:  /* avx512f_scalefv8df_round */
    case 1926:  /* avx512f_scalefv16sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1950:  /* avx512f_sgetexpv2df_round */
    case 1948:  /* avx512f_sgetexpv4sf_round */
    case 1924:  /* avx512f_vmscalefv2df_round */
    case 1922:  /* avx512f_vmscalefv4sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1949:  /* avx512f_sgetexpv2df */
    case 1947:  /* avx512f_sgetexpv4sf */
    case 1923:  /* avx512f_vmscalefv2df */
    case 1921:  /* avx512f_vmscalefv4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 2785:  /* avx512f_permv8df_1_mask */
    case 2783:  /* avx512f_permv8di_1_mask */
    case 1880:  /* avx512f_vextracti32x4_1_mask */
    case 1878:  /* avx512f_vextractf32x4_1_mask */
    case 1876:  /* avx512f_vextracti32x4_1_maskm */
    case 1875:  /* avx512f_vextractf32x4_1_maskm */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 3));
      ro[6] = *(ro_loc[6] = &XEXP (XEXP (pat, 1), 1));
      ro[7] = *(ro_loc[7] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2405:  /* *vec_extractv4si_mem */
    case 2403:  /* *vec_extractv4si */
    case 2398:  /* *vec_extractv8hi_mem */
    case 2397:  /* *vec_extractv16qi_mem */
    case 2392:  /* *vec_extractv8hi_sse2 */
    case 2391:  /* *vec_extractv8hi */
    case 2390:  /* *vec_extractv16qi */
    case 1874:  /* *vec_extractv4sf_mem */
    case 1873:  /* *sse4_1_extractps */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      break;

    case 1871:  /* sse4_1_insertps */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 1));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (pat, 1), 0, 2));
      break;

    case 2389:  /* sse2_loadld */
    case 1869:  /* vec_setv4sf_0 */
    case 1868:  /* vec_setv4si_0 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      break;

    case 2012:  /* sse2_movsd */
    case 1860:  /* sse_movss */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 0));
      break;

    case 2893:  /* vec_set_lo_v32qi */
    case 2891:  /* vec_set_lo_v16hi */
    case 2888:  /* vec_set_lo_v8sf */
    case 2887:  /* vec_set_lo_v8si */
    case 2884:  /* vec_set_lo_v4df */
    case 2883:  /* vec_set_lo_v4di */
    case 2881:  /* avx2_vec_set_lo_v4di */
    case 2371:  /* vec_set_hi_v8di */
    case 2369:  /* vec_set_hi_v8df */
    case 2367:  /* vec_set_lo_v8di */
    case 2365:  /* vec_set_lo_v8df */
    case 2011:  /* sse2_loadlpd */
    case 1859:  /* sse_loadlps */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 0));
      break;

    case 1995:  /* avx_shufpd256_1 */
    case 1855:  /* sse_shufps_v4sf */
    case 1854:  /* sse_shufps_v4si */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 1));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 3));
      break;

    case 2375:  /* avx512f_shuf_i64x2_1 */
    case 2373:  /* avx512f_shuf_f64x2_1 */
    case 1993:  /* avx512f_shufpd512_1 */
    case 1853:  /* avx_shufps256_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 1));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 3));
      ro[7] = *(ro_loc[7] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 4));
      ro[8] = *(ro_loc[8] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 5));
      ro[9] = *(ro_loc[9] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 6));
      ro[10] = *(ro_loc[10] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 7));
      break;

    case 1852:  /* avx512f_movsldup512_mask */
    case 1848:  /* avx512f_movshdup512_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1851:  /* *avx512f_movsldup512 */
    case 1850:  /* sse3_movsldup */
    case 1849:  /* avx_movsldup256 */
    case 1847:  /* *avx512f_movshdup512 */
    case 1846:  /* sse3_movshdup */
    case 1845:  /* avx_movshdup256 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 0), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 2355:  /* avx512f_interleave_lowv16si_mask */
    case 2351:  /* avx512f_interleave_highv16si_mask */
    case 2002:  /* avx512f_interleave_lowv8di_mask */
    case 1998:  /* avx512f_interleave_highv8di_mask */
    case 1918:  /* *avx512f_unpcklpd512_mask */
    case 1914:  /* avx512f_unpckhpd512_mask */
    case 1842:  /* avx512f_unpcklps512_mask */
    case 1838:  /* avx512f_unpckhps512_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2954:  /* avx512f_vcvtph2ps512_mask_round */
    case 2645:  /* avx512er_rsqrt28v8df_mask_round */
    case 2641:  /* avx512er_rsqrt28v16sf_mask_round */
    case 2633:  /* avx512er_rcp28v8df_mask_round */
    case 2629:  /* avx512er_rcp28v16sf_mask_round */
    case 2625:  /* avx512er_exp2v8df_mask_round */
    case 2621:  /* avx512er_exp2v16sf_mask_round */
    case 1946:  /* avx512f_getexpv8df_mask_round */
    case 1942:  /* avx512f_getexpv16sf_mask_round */
    case 1805:  /* avx512f_ufix_notruncv8dfv8si_mask_round */
    case 1798:  /* avx512f_cvtpd2dq512_mask_round */
    case 1744:  /* avx512f_ufix_notruncv16sfv16si_mask_round */
    case 1740:  /* avx512f_fix_notruncv16sfv16si_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 2952:  /* *avx512f_vcvtph2ps512_round */
    case 2643:  /* *avx512er_rsqrt28v8df_round */
    case 2639:  /* *avx512er_rsqrt28v16sf_round */
    case 2631:  /* *avx512er_rcp28v8df_round */
    case 2627:  /* *avx512er_rcp28v16sf_round */
    case 2623:  /* avx512er_exp2v8df_round */
    case 2619:  /* avx512er_exp2v16sf_round */
    case 1944:  /* avx512f_getexpv8df_round */
    case 1940:  /* avx512f_getexpv16sf_round */
    case 1803:  /* avx512f_ufix_notruncv8dfv8si_round */
    case 1796:  /* *avx512f_cvtpd2dq512_round */
    case 1742:  /* *avx512f_ufix_notruncv16sfv16si_round */
    case 1738:  /* *avx512f_fix_notruncv16sfv16si_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1786:  /* sse2_cvttsd2siq_round */
    case 1784:  /* sse2_cvttsd2si_round */
    case 1776:  /* avx512f_vcvttsd2usiq_round */
    case 1774:  /* avx512f_vcvttsd2usi_round */
    case 1768:  /* avx512f_vcvttss2usiq_round */
    case 1766:  /* avx512f_vcvttss2usi_round */
    case 1717:  /* sse_cvttss2siq_round */
    case 1715:  /* sse_cvttss2si_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1781:  /* sse2_cvtsd2siq_round */
    case 1778:  /* sse2_cvtsd2si_round */
    case 1772:  /* avx512f_vcvtsd2usiq_round */
    case 1770:  /* avx512f_vcvtsd2usi_round */
    case 1764:  /* avx512f_vcvtss2usiq_round */
    case 1762:  /* avx512f_vcvtss2usi_round */
    case 1712:  /* sse_cvtss2siq_round */
    case 1709:  /* sse_cvtss2si_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1820:  /* sse2_cvtss2sd_round */
    case 1818:  /* sse2_cvtsd2ss_round */
    case 1760:  /* sse2_cvtsi2sdq_round */
    case 1724:  /* cvtusi2sd64_round */
    case 1722:  /* cvtusi2ss64_round */
    case 1719:  /* cvtusi2ss32_round */
    case 1707:  /* sse_cvtsi2ssq_round */
    case 1705:  /* sse_cvtsi2ss_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 2948:  /* vcvtph2ps */
    case 1702:  /* sse_cvtps2pi */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      break;

    case 1819:  /* sse2_cvtss2sd */
    case 1817:  /* sse2_cvtsd2ss */
    case 1759:  /* sse2_cvtsi2sdq */
    case 1758:  /* sse2_cvtsi2sd */
    case 1723:  /* cvtusi2sd64 */
    case 1721:  /* cvtusi2ss64 */
    case 1720:  /* cvtusi2sd32 */
    case 1718:  /* cvtusi2ss32 */
    case 1706:  /* sse_cvtsi2ssq */
    case 1704:  /* sse_cvtsi2ss */
    case 1701:  /* sse_cvtpi2ps */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      break;

    case 1700:  /* *fma4i_vmfnmsub_v2df */
    case 1699:  /* *fma4i_vmfnmsub_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 1698:  /* *fma4i_vmfnmadd_v2df */
    case 1697:  /* *fma4i_vmfnmadd_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 1696:  /* *fma4i_vmfmsub_v2df */
    case 1695:  /* *fma4i_vmfmsub_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 1694:  /* *fma4i_vmfmadd_v2df */
    case 1693:  /* *fma4i_vmfmadd_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 1692:  /* *fmai_fnmsub_v2df_round */
    case 1690:  /* *fmai_fnmsub_v4sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1691:  /* *fmai_fnmsub_v2df */
    case 1689:  /* *fmai_fnmsub_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1688:  /* *fmai_fnmadd_v2df_round */
    case 1686:  /* *fmai_fnmadd_v4sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1687:  /* *fmai_fnmadd_v2df */
    case 1685:  /* *fmai_fnmadd_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1684:  /* *fmai_fmsub_v2df */
    case 1682:  /* *fmai_fmsub_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1683:  /* *fmai_fmsub_v2df */
    case 1681:  /* *fmai_fmsub_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1680:  /* *fmai_fmadd_v2df */
    case 1678:  /* *fmai_fmadd_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1679:  /* *fmai_fmadd_v2df */
    case 1677:  /* *fmai_fmadd_v4sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1676:  /* avx512f_fmsubadd_v8df_mask3_round */
    case 1674:  /* avx512f_fmsubadd_v16sf_mask3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1675:  /* avx512f_fmsubadd_v8df_mask3 */
    case 1673:  /* avx512f_fmsubadd_v16sf_mask3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1672:  /* avx512f_fmsubadd_v8df_mask_round */
    case 1670:  /* avx512f_fmsubadd_v16sf_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1671:  /* avx512f_fmsubadd_v8df_mask */
    case 1669:  /* avx512f_fmsubadd_v16sf_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1668:  /* fma_fmsubadd_v8df_maskz_1_round */
    case 1664:  /* fma_fmsubadd_v16sf_maskz_1_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1667:  /* fma_fmsubadd_v8df_maskz_1 */
    case 1663:  /* fma_fmsubadd_v16sf_maskz_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 1666:  /* *fma_fmsubadd_v8df_round */
    case 1662:  /* *fma_fmsubadd_v16sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1665:  /* *fma_fmsubadd_v8df */
    case 1661:  /* *fma_fmsubadd_v16sf */
    case 1660:  /* *fma_fmsubadd_v2df */
    case 1659:  /* *fma_fmsubadd_v4df */
    case 1658:  /* *fma_fmsubadd_v4sf */
    case 1657:  /* *fma_fmsubadd_v8sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (pat, 1), 0, 2), 0));
      break;

    case 1656:  /* avx512f_fmaddsub_v8df_mask3_round */
    case 1654:  /* avx512f_fmaddsub_v16sf_mask3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1655:  /* avx512f_fmaddsub_v8df_mask3 */
    case 1653:  /* avx512f_fmaddsub_v16sf_mask3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1652:  /* avx512f_fmaddsub_v8df_mask_round */
    case 1650:  /* avx512f_fmaddsub_v16sf_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1651:  /* avx512f_fmaddsub_v8df_mask */
    case 1649:  /* avx512f_fmaddsub_v16sf_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1648:  /* fma_fmaddsub_v8df_maskz_1_round */
    case 1644:  /* fma_fmaddsub_v16sf_maskz_1_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 2870:  /* avx512f_vpermt2varv8df3_maskz_1 */
    case 2868:  /* avx512f_vpermt2varv8di3_maskz_1 */
    case 2866:  /* avx512f_vpermt2varv16sf3_maskz_1 */
    case 2864:  /* avx512f_vpermt2varv16si3_maskz_1 */
    case 2858:  /* avx512f_vpermi2varv8df3_maskz_1 */
    case 2856:  /* avx512f_vpermi2varv8di3_maskz_1 */
    case 2854:  /* avx512f_vpermi2varv16sf3_maskz_1 */
    case 2852:  /* avx512f_vpermi2varv16si3_maskz_1 */
    case 1954:  /* avx512f_alignv8di_mask */
    case 1952:  /* avx512f_alignv16si_mask */
    case 1647:  /* fma_fmaddsub_v8df_maskz_1 */
    case 1643:  /* fma_fmaddsub_v16sf_maskz_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 1636:  /* avx512f_fnmsub_v8df_mask3_round */
    case 1634:  /* avx512f_fnmsub_v16sf_mask3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1635:  /* avx512f_fnmsub_v8df_mask3 */
    case 1633:  /* avx512f_fnmsub_v16sf_mask3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1632:  /* avx512f_fnmsub_v8df_mask_round */
    case 1630:  /* avx512f_fnmsub_v16sf_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1631:  /* avx512f_fnmsub_v8df_mask */
    case 1629:  /* avx512f_fnmsub_v16sf_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1628:  /* fma_fnmsub_v8df_maskz_1_round */
    case 1624:  /* fma_fnmsub_v16sf_maskz_1_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1627:  /* fma_fnmsub_v8df_maskz_1 */
    case 1623:  /* fma_fnmsub_v16sf_maskz_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 1626:  /* *fma_fnmsub_v8df_round */
    case 1622:  /* *fma_fnmsub_v16sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2), 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1625:  /* *fma_fnmsub_v8df */
    case 1621:  /* *fma_fnmsub_v16sf */
    case 1620:  /* *fma_fnmsub_v4df */
    case 1619:  /* *fma_fnmsub_v8sf */
    case 1618:  /* *fma_fnmsub_v2df */
    case 1617:  /* *fma_fnmsub_v4sf */
    case 1616:  /* *fma_fnmsub_df */
    case 1615:  /* *fma_fnmsub_sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 2), 0));
      break;

    case 1614:  /* avx512f_fnmadd_v8df_mask3_round */
    case 1612:  /* avx512f_fnmadd_v16sf_mask3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1613:  /* avx512f_fnmadd_v8df_mask3 */
    case 1611:  /* avx512f_fnmadd_v16sf_mask3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1610:  /* avx512f_fnmadd_v8df_mask_round */
    case 1608:  /* avx512f_fnmadd_v16sf_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1609:  /* avx512f_fnmadd_v8df_mask */
    case 1607:  /* avx512f_fnmadd_v16sf_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1606:  /* fma_fnmadd_v8df_maskz_1_round */
    case 1602:  /* fma_fnmadd_v16sf_maskz_1_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1605:  /* fma_fnmadd_v8df_maskz_1 */
    case 1601:  /* fma_fnmadd_v16sf_maskz_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 1592:  /* avx512f_fmsub_v8df_mask3_round */
    case 1590:  /* avx512f_fmsub_v16sf_mask3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1591:  /* avx512f_fmsub_v8df_mask3 */
    case 1589:  /* avx512f_fmsub_v16sf_mask3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1588:  /* avx512f_fmsub_v8df_mask_round */
    case 1586:  /* avx512f_fmsub_v16sf_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1587:  /* avx512f_fmsub_v8df_mask */
    case 1585:  /* avx512f_fmsub_v16sf_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1584:  /* fma_fmsub_v8df_maskz_1_round */
    case 1580:  /* fma_fmsub_v16sf_maskz_1_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1583:  /* fma_fmsub_v8df_maskz_1 */
    case 1579:  /* fma_fmsub_v16sf_maskz_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 1582:  /* *fma_fmsub_v8df_round */
    case 1578:  /* *fma_fmsub_v16sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2), 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1581:  /* *fma_fmsub_v8df */
    case 1577:  /* *fma_fmsub_v16sf */
    case 1576:  /* *fma_fmsub_v4df */
    case 1575:  /* *fma_fmsub_v8sf */
    case 1574:  /* *fma_fmsub_v2df */
    case 1573:  /* *fma_fmsub_v4sf */
    case 1572:  /* *fma_fmsub_df */
    case 1571:  /* *fma_fmsub_sf */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 2), 0));
      break;

    case 1570:  /* avx512f_fmadd_v8df_mask3_round */
    case 1568:  /* avx512f_fmadd_v16sf_mask3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1569:  /* avx512f_fmadd_v8df_mask3 */
    case 1567:  /* avx512f_fmadd_v16sf_mask3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 3;
      break;

    case 1566:  /* avx512f_fmadd_v8df_mask_round */
    case 1564:  /* avx512f_fmadd_v16sf_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1565:  /* avx512f_fmadd_v8df_mask */
    case 1563:  /* avx512f_fmadd_v16sf_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1562:  /* fma_fmadd_v8df_maskz_1_round */
    case 1558:  /* fma_fmadd_v16sf_maskz_1_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[6] = *(ro_loc[6] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1561:  /* fma_fmadd_v8df_maskz_1 */
    case 1557:  /* fma_fmadd_v16sf_maskz_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 1560:  /* *fma_fmadd_v8df_round */
    case 1556:  /* *fma_fmadd_v16sf_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1508:  /* sse2_ucomi_round */
    case 1506:  /* sse_ucomi_round */
    case 1504:  /* sse2_comi_round */
    case 1502:  /* sse_comi_round */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1507:  /* sse2_ucomi */
    case 1505:  /* sse_ucomi */
    case 1503:  /* sse2_comi */
    case 1501:  /* sse_comi */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 1), 0));
      break;

    case 1494:  /* avx512f_vmcmpv2df3_mask_round */
    case 1492:  /* avx512f_vmcmpv4sf3_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1493:  /* avx512f_vmcmpv2df3_mask */
    case 1491:  /* avx512f_vmcmpv4sf3_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (pat, 1), 1), 0));
      break;

    case 1490:  /* avx512f_vmcmpv2df3_round */
    case 1488:  /* avx512f_vmcmpv4sf3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1489:  /* avx512f_vmcmpv2df3 */
    case 1487:  /* avx512f_vmcmpv4sf3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      break;

    case 1482:  /* avx512f_cmpv8df3_mask_round */
    case 1476:  /* avx512f_cmpv16sf3_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1646:  /* *fma_fmaddsub_v8df_round */
    case 1642:  /* *fma_fmaddsub_v16sf_round */
    case 1481:  /* avx512f_cmpv8df3_round */
    case 1475:  /* avx512f_cmpv16sf3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1486:  /* avx512f_ucmpv8di3_mask */
    case 1484:  /* avx512f_ucmpv16si3_mask */
    case 1480:  /* avx512f_cmpv8df3_mask */
    case 1478:  /* avx512f_cmpv8di3_mask */
    case 1474:  /* avx512f_cmpv16sf3_mask */
    case 1472:  /* avx512f_cmpv16si3_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 1470:  /* sse2_vmmaskcmpv2df3 */
    case 1469:  /* sse_vmmaskcmpv4sf3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 3102:  /* avx512f_getmantv2df */
    case 3100:  /* avx512f_getmantv4sf */
    case 1989:  /* avx512f_rndscalev2df */
    case 1987:  /* avx512f_rndscalev4sf */
    case 1460:  /* avx_vmcmpv2df3 */
    case 1459:  /* avx_vmcmpv4sf3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 2449:  /* ssse3_phsubdv4si3 */
    case 2448:  /* ssse3_phadddv4si3 */
    case 2445:  /* ssse3_phsubswv4hi3 */
    case 2444:  /* ssse3_phsubwv4hi3 */
    case 2443:  /* ssse3_phaddswv4hi3 */
    case 2442:  /* ssse3_phaddwv4hi3 */
    case 1454:  /* sse3_hsubv4sf3 */
    case 1453:  /* sse3_haddv4sf3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0);
      recog_data.dup_num[1] = 1;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 0);
      recog_data.dup_num[2] = 1;
      recog_data.dup_loc[3] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 0);
      recog_data.dup_num[3] = 2;
      recog_data.dup_loc[4] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 0);
      recog_data.dup_num[4] = 2;
      recog_data.dup_loc[5] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 0);
      recog_data.dup_num[5] = 2;
      break;

    case 1452:  /* avx_hsubv8sf3 */
    case 1451:  /* avx_haddv8sf3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0), 0);
      recog_data.dup_num[1] = 1;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 1), 0);
      recog_data.dup_num[2] = 1;
      recog_data.dup_loc[3] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 1), 0);
      recog_data.dup_num[3] = 2;
      recog_data.dup_loc[4] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 0), 0);
      recog_data.dup_num[4] = 2;
      recog_data.dup_loc[5] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 1), 0);
      recog_data.dup_num[5] = 2;
      recog_data.dup_loc[6] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 0), 0);
      recog_data.dup_num[6] = 1;
      recog_data.dup_loc[7] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 1), 0);
      recog_data.dup_num[7] = 1;
      recog_data.dup_loc[8] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 0), 0);
      recog_data.dup_num[8] = 1;
      recog_data.dup_loc[9] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 1), 0);
      recog_data.dup_num[9] = 1;
      recog_data.dup_loc[10] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 0), 0);
      recog_data.dup_num[10] = 2;
      recog_data.dup_loc[11] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 1), 0);
      recog_data.dup_num[11] = 2;
      recog_data.dup_loc[12] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 0), 0);
      recog_data.dup_num[12] = 2;
      recog_data.dup_loc[13] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 1), 0);
      recog_data.dup_num[13] = 2;
      break;

    case 1450:  /* *sse3_hsubv2df3_low */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 1449:  /* *sse3_haddv2df3_low */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 1447:  /* *sse3_haddv2df3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 0, 0));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 0, 0));
      ro[6] = *(ro_loc[6] = &XVECEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0);
      recog_data.dup_num[1] = 2;
      break;

    case 1446:  /* avx_hsubv4df3 */
    case 1445:  /* avx_haddv4df3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 1), 0);
      recog_data.dup_num[1] = 2;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 0);
      recog_data.dup_num[2] = 1;
      recog_data.dup_loc[3] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 1), 0);
      recog_data.dup_num[3] = 1;
      recog_data.dup_loc[4] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 0);
      recog_data.dup_num[4] = 2;
      recog_data.dup_loc[5] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 1), 0);
      recog_data.dup_num[5] = 2;
      break;

    case 1339:  /* sse2_vmsqrtv2df2_round */
    case 1337:  /* sse_vmsqrtv4sf2_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1830:  /* avx512f_cvtps2pd512_mask_round */
    case 1824:  /* avx512f_cvtpd2ps512_mask_round */
    case 1813:  /* ufix_truncv8dfv8si2_mask_round */
    case 1809:  /* fix_truncv8dfv8si2_mask_round */
    case 1752:  /* ufix_truncv16sfv16si2_mask_round */
    case 1748:  /* fix_truncv16sfv16si2_mask_round */
    case 1734:  /* ufloatv16siv16sf2_mask_round */
    case 1728:  /* floatv16siv16sf2_mask_round */
    case 1604:  /* *fma_fnmadd_v8df_round */
    case 1600:  /* *fma_fnmadd_v16sf_round */
    case 1333:  /* avx512f_sqrtv8df2_mask_round */
    case 1327:  /* avx512f_sqrtv16sf2_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 3107:  /* clzv8di2_mask */
    case 3105:  /* clzv16si2_mask */
    case 2819:  /* avx512f_vec_dup_memv8df_mask */
    case 2817:  /* avx512f_vec_dup_memv8di_mask */
    case 2815:  /* avx512f_vec_dup_memv16sf_mask */
    case 2813:  /* avx512f_vec_dup_memv16si_mask */
    case 2811:  /* avx512f_vec_dup_gprv8di_mask */
    case 2809:  /* avx512f_vec_dup_gprv16si_mask */
    case 2807:  /* avx512f_broadcastv8di_mask */
    case 2805:  /* avx512f_broadcastv8df_mask */
    case 2803:  /* avx512f_broadcastv16si_mask */
    case 2801:  /* avx512f_broadcastv16sf_mask */
    case 2559:  /* avx512f_zero_extendv8siv8di2_mask */
    case 2557:  /* avx512f_sign_extendv8siv8di2_mask */
    case 2551:  /* avx512f_zero_extendv8hiv8di2_mask */
    case 2549:  /* avx512f_sign_extendv8hiv8di2_mask */
    case 2535:  /* avx512f_zero_extendv16hiv16si2_mask */
    case 2533:  /* avx512f_sign_extendv16hiv16si2_mask */
    case 2527:  /* avx512f_zero_extendv16qiv16si2_mask */
    case 2525:  /* avx512f_sign_extendv16qiv16si2_mask */
    case 2482:  /* absv8di2_mask */
    case 2478:  /* absv16si2_mask */
    case 2038:  /* avx512f_us_truncatev8div8hi2_mask */
    case 2037:  /* avx512f_truncatev8div8hi2_mask */
    case 2036:  /* avx512f_ss_truncatev8div8hi2_mask */
    case 2035:  /* avx512f_us_truncatev8div8si2_mask */
    case 2034:  /* avx512f_truncatev8div8si2_mask */
    case 2033:  /* avx512f_ss_truncatev8div8si2_mask */
    case 2032:  /* avx512f_us_truncatev16siv16hi2_mask */
    case 2031:  /* avx512f_truncatev16siv16hi2_mask */
    case 2030:  /* avx512f_ss_truncatev16siv16hi2_mask */
    case 2029:  /* avx512f_us_truncatev16siv16qi2_mask */
    case 2028:  /* avx512f_truncatev16siv16qi2_mask */
    case 2027:  /* avx512f_ss_truncatev16siv16qi2_mask */
    case 1892:  /* vec_extract_hi_v8di_mask */
    case 1890:  /* vec_extract_hi_v8df_mask */
    case 1888:  /* vec_extract_hi_v8di_maskm */
    case 1887:  /* vec_extract_hi_v8df_maskm */
    case 1886:  /* vec_extract_lo_v8di_mask */
    case 1884:  /* vec_extract_lo_v8df_mask */
    case 1882:  /* vec_extract_lo_v8di_maskm */
    case 1881:  /* vec_extract_lo_v8df_maskm */
    case 1829:  /* avx512f_cvtps2pd512_mask */
    case 1823:  /* avx512f_cvtpd2ps512_mask */
    case 1812:  /* ufix_truncv8dfv8si2_mask */
    case 1808:  /* fix_truncv8dfv8si2_mask */
    case 1791:  /* ufloatv8siv8df_mask */
    case 1788:  /* floatv8siv8df2_mask */
    case 1751:  /* ufix_truncv16sfv16si2_mask */
    case 1747:  /* fix_truncv16sfv16si2_mask */
    case 1733:  /* ufloatv16siv16sf2_mask */
    case 1727:  /* floatv16siv16sf2_mask */
    case 1603:  /* *fma_fnmadd_v8df */
    case 1599:  /* *fma_fnmadd_v16sf */
    case 1598:  /* *fma_fnmadd_v4df */
    case 1597:  /* *fma_fnmadd_v8sf */
    case 1596:  /* *fma_fnmadd_v2df */
    case 1595:  /* *fma_fnmadd_v4sf */
    case 1594:  /* *fma_fnmadd_df */
    case 1593:  /* *fma_fnmadd_sf */
    case 1332:  /* avx512f_sqrtv8df2_mask */
    case 1326:  /* avx512f_sqrtv16sf2_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2726:  /* *xop_vmfrczv2df2 */
    case 2725:  /* *xop_vmfrczv4sf2 */
    case 2648:  /* avx512er_vmrsqrt28v2df */
    case 2646:  /* avx512er_vmrsqrt28v4sf */
    case 2636:  /* avx512er_vmrcp28v2df */
    case 2634:  /* avx512er_vmrcp28v4sf */
    case 1801:  /* *sse2_cvtpd2dq */
    case 1800:  /* *avx_cvtpd2dq256_2 */
    case 1348:  /* sse_vmrsqrtv4sf2 */
    case 1347:  /* rsqrt14v2df */
    case 1346:  /* rsqrt14v4sf */
    case 1323:  /* srcp14v2df */
    case 1322:  /* srcp14v4sf */
    case 1317:  /* sse_vmrcpv4sf2 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 1428:  /* sse2_vmsminv2df3_round */
    case 1426:  /* sse2_vmsmaxv2df3_round */
    case 1424:  /* sse_vmsminv4sf3_round */
    case 1422:  /* sse_vmsmaxv4sf3_round */
    case 1302:  /* sse2_vmdivv2df3_round */
    case 1300:  /* sse2_vmmulv2df3_round */
    case 1298:  /* sse_vmdivv4sf3_round */
    case 1296:  /* sse_vmmulv4sf3_round */
    case 1270:  /* sse2_vmsubv2df3_round */
    case 1268:  /* sse2_vmaddv2df3_round */
    case 1266:  /* sse_vmsubv4sf3_round */
    case 1264:  /* sse_vmaddv4sf3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 1427:  /* sse2_vmsminv2df3 */
    case 1425:  /* sse2_vmsmaxv2df3 */
    case 1423:  /* sse_vmsminv4sf3 */
    case 1421:  /* sse_vmsmaxv4sf3 */
    case 1301:  /* sse2_vmdivv2df3 */
    case 1299:  /* sse2_vmmulv2df3 */
    case 1297:  /* sse_vmdivv4sf3 */
    case 1295:  /* sse_vmmulv4sf3 */
    case 1269:  /* sse2_vmsubv2df3 */
    case 1267:  /* sse2_vmaddv2df3 */
    case 1265:  /* sse_vmsubv4sf3 */
    case 1263:  /* sse_vmaddv4sf3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 2242:  /* *avx2_uminv16si3_mask_round */
    case 2238:  /* *avx2_umaxv16si3_mask_round */
    case 2234:  /* *avx2_sminv16si3_mask_round */
    case 2230:  /* *avx2_smaxv16si3_mask_round */
    case 2226:  /* *avx2_uminv8di3_mask_round */
    case 2222:  /* *avx2_umaxv8di3_mask_round */
    case 2218:  /* *avx2_sminv8di3_mask_round */
    case 2214:  /* *avx2_smaxv8di3_mask_round */
    case 2210:  /* *avx2_uminv8si3_mask_round */
    case 2206:  /* *avx2_umaxv8si3_mask_round */
    case 2202:  /* *avx2_sminv8si3_mask_round */
    case 2198:  /* *avx2_smaxv8si3_mask_round */
    case 2194:  /* *avx2_uminv16hi3_mask_round */
    case 2190:  /* *avx2_umaxv16hi3_mask_round */
    case 2186:  /* *avx2_sminv16hi3_mask_round */
    case 2182:  /* *avx2_smaxv16hi3_mask_round */
    case 1416:  /* *sminv8df3_mask_round */
    case 1412:  /* *smaxv8df3_mask_round */
    case 1404:  /* *sminv16sf3_mask_round */
    case 1400:  /* *smaxv16sf3_mask_round */
    case 1396:  /* *sminv2df3_finite_mask_round */
    case 1392:  /* *smaxv2df3_finite_mask_round */
    case 1388:  /* *sminv4df3_finite_mask_round */
    case 1384:  /* *smaxv4df3_finite_mask_round */
    case 1380:  /* *sminv8df3_finite_mask_round */
    case 1376:  /* *smaxv8df3_finite_mask_round */
    case 1372:  /* *sminv4sf3_finite_mask_round */
    case 1368:  /* *smaxv4sf3_finite_mask_round */
    case 1364:  /* *sminv8sf3_finite_mask_round */
    case 1360:  /* *smaxv8sf3_finite_mask_round */
    case 1356:  /* *sminv16sf3_finite_mask_round */
    case 1352:  /* *smaxv16sf3_finite_mask_round */
    case 1312:  /* avx512f_divv8df3_mask_round */
    case 1306:  /* avx512f_divv16sf3_mask_round */
    case 1294:  /* *mulv2df3_mask_round */
    case 1290:  /* *mulv4df3_mask_round */
    case 1286:  /* *mulv8df3_mask_round */
    case 1282:  /* *mulv4sf3_mask_round */
    case 1278:  /* *mulv8sf3_mask_round */
    case 1274:  /* *mulv16sf3_mask_round */
    case 1262:  /* *subv2df3_mask_round */
    case 1258:  /* *addv2df3_mask_round */
    case 1254:  /* *subv4df3_mask_round */
    case 1250:  /* *addv4df3_mask_round */
    case 1246:  /* *subv8df3_mask_round */
    case 1242:  /* *addv8df3_mask_round */
    case 1238:  /* *subv4sf3_mask_round */
    case 1234:  /* *addv4sf3_mask_round */
    case 1230:  /* *subv8sf3_mask_round */
    case 1226:  /* *addv8sf3_mask_round */
    case 1222:  /* *subv16sf3_mask_round */
    case 1218:  /* *addv16sf3_mask_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 2931:  /* avx512f_lshrvv8di_mask */
    case 2929:  /* avx512f_ashlvv8di_mask */
    case 2923:  /* avx512f_lshrvv16si_mask */
    case 2921:  /* avx512f_ashlvv16si_mask */
    case 2919:  /* avx512f_ashrvv8di_mask */
    case 2915:  /* avx512f_ashrvv16si_mask */
    case 2302:  /* xorv8di3_mask */
    case 2300:  /* iorv8di3_mask */
    case 2298:  /* andv8di3_mask */
    case 2296:  /* xorv16si3_mask */
    case 2294:  /* iorv16si3_mask */
    case 2292:  /* andv16si3_mask */
    case 2241:  /* *avx2_uminv16si3_mask */
    case 2237:  /* *avx2_umaxv16si3_mask */
    case 2233:  /* *avx2_sminv16si3_mask */
    case 2229:  /* *avx2_smaxv16si3_mask */
    case 2225:  /* *avx2_uminv8di3_mask */
    case 2221:  /* *avx2_umaxv8di3_mask */
    case 2217:  /* *avx2_sminv8di3_mask */
    case 2213:  /* *avx2_smaxv8di3_mask */
    case 2209:  /* *avx2_uminv8si3_mask */
    case 2205:  /* *avx2_umaxv8si3_mask */
    case 2201:  /* *avx2_sminv8si3_mask */
    case 2197:  /* *avx2_smaxv8si3_mask */
    case 2193:  /* *avx2_uminv16hi3_mask */
    case 2189:  /* *avx2_umaxv16hi3_mask */
    case 2185:  /* *avx2_sminv16hi3_mask */
    case 2181:  /* *avx2_smaxv16hi3_mask */
    case 2174:  /* avx512f_rorv8di_mask */
    case 2172:  /* avx512f_rolv8di_mask */
    case 2170:  /* avx512f_rorv16si_mask */
    case 2168:  /* avx512f_rolv16si_mask */
    case 2166:  /* avx512f_rorvv8di_mask */
    case 2164:  /* avx512f_rolvv8di_mask */
    case 2162:  /* avx512f_rorvv16si_mask */
    case 2160:  /* avx512f_rolvv16si_mask */
    case 2154:  /* lshrv8di3_mask */
    case 2152:  /* ashlv8di3_mask */
    case 2150:  /* lshrv16si3_mask */
    case 2148:  /* ashlv16si3_mask */
    case 2134:  /* ashrv8di3_mask */
    case 2132:  /* ashrv16si3_mask */
    case 2126:  /* *sse4_1_mulv4si3_mask */
    case 2124:  /* *avx2_mulv8si3_mask */
    case 2122:  /* *avx512f_mulv16si3_mask */
    case 2088:  /* *subv2di3_mask */
    case 2086:  /* *addv2di3_mask */
    case 2084:  /* *subv4di3_mask */
    case 2082:  /* *addv4di3_mask */
    case 2080:  /* *subv8di3_mask */
    case 2078:  /* *addv8di3_mask */
    case 2076:  /* *subv4si3_mask */
    case 2074:  /* *addv4si3_mask */
    case 2072:  /* *subv8si3_mask */
    case 2070:  /* *addv8si3_mask */
    case 2068:  /* *subv16si3_mask */
    case 2066:  /* *addv16si3_mask */
    case 2064:  /* *subv8hi3_mask */
    case 2062:  /* *addv8hi3_mask */
    case 2060:  /* *subv16hi3_mask */
    case 2058:  /* *addv16hi3_mask */
    case 2056:  /* *subv16qi3_mask */
    case 2054:  /* *addv16qi3_mask */
    case 1415:  /* *sminv8df3_mask */
    case 1411:  /* *smaxv8df3_mask */
    case 1403:  /* *sminv16sf3_mask */
    case 1399:  /* *smaxv16sf3_mask */
    case 1395:  /* *sminv2df3_finite_mask */
    case 1391:  /* *smaxv2df3_finite_mask */
    case 1387:  /* *sminv4df3_finite_mask */
    case 1383:  /* *smaxv4df3_finite_mask */
    case 1379:  /* *sminv8df3_finite_mask */
    case 1375:  /* *smaxv8df3_finite_mask */
    case 1371:  /* *sminv4sf3_finite_mask */
    case 1367:  /* *smaxv4sf3_finite_mask */
    case 1363:  /* *sminv8sf3_finite_mask */
    case 1359:  /* *smaxv8sf3_finite_mask */
    case 1355:  /* *sminv16sf3_finite_mask */
    case 1351:  /* *smaxv16sf3_finite_mask */
    case 1311:  /* avx512f_divv8df3_mask */
    case 1305:  /* avx512f_divv16sf3_mask */
    case 1293:  /* *mulv2df3_mask */
    case 1289:  /* *mulv4df3_mask */
    case 1285:  /* *mulv8df3_mask */
    case 1281:  /* *mulv4sf3_mask */
    case 1277:  /* *mulv8sf3_mask */
    case 1273:  /* *mulv16sf3_mask */
    case 1261:  /* *subv2df3_mask */
    case 1257:  /* *addv2df3_mask */
    case 1253:  /* *subv4df3_mask */
    case 1249:  /* *addv4df3_mask */
    case 1245:  /* *subv8df3_mask */
    case 1241:  /* *addv8df3_mask */
    case 1237:  /* *subv4sf3_mask */
    case 1233:  /* *addv4sf3_mask */
    case 1229:  /* *subv8sf3_mask */
    case 1225:  /* *addv8sf3_mask */
    case 1221:  /* *subv16sf3_mask */
    case 1217:  /* *addv16sf3_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2240:  /* *avx2_uminv16si3_round */
    case 2236:  /* *avx2_umaxv16si3_round */
    case 2232:  /* *avx2_sminv16si3_round */
    case 2228:  /* *avx2_smaxv16si3_round */
    case 2224:  /* *avx2_uminv8di3_round */
    case 2220:  /* *avx2_umaxv8di3_round */
    case 2216:  /* *avx2_sminv8di3_round */
    case 2212:  /* *avx2_smaxv8di3_round */
    case 2208:  /* *avx2_uminv8si3_round */
    case 2204:  /* *avx2_umaxv8si3_round */
    case 2200:  /* *avx2_sminv8si3_round */
    case 2196:  /* *avx2_smaxv8si3_round */
    case 2192:  /* *avx2_uminv16hi3_round */
    case 2188:  /* *avx2_umaxv16hi3_round */
    case 2184:  /* *avx2_sminv16hi3_round */
    case 2180:  /* *avx2_smaxv16hi3_round */
    case 1414:  /* *sminv8df3_round */
    case 1410:  /* *smaxv8df3_round */
    case 1402:  /* *sminv16sf3_round */
    case 1398:  /* *smaxv16sf3_round */
    case 1394:  /* *sminv2df3_finite_round */
    case 1390:  /* *smaxv2df3_finite_round */
    case 1386:  /* *sminv4df3_finite_round */
    case 1382:  /* *smaxv4df3_finite_round */
    case 1378:  /* *sminv8df3_finite_round */
    case 1374:  /* *smaxv8df3_finite_round */
    case 1370:  /* *sminv4sf3_finite_round */
    case 1366:  /* *smaxv4sf3_finite_round */
    case 1362:  /* *sminv8sf3_finite_round */
    case 1358:  /* *smaxv8sf3_finite_round */
    case 1354:  /* *sminv16sf3_finite_round */
    case 1350:  /* *smaxv16sf3_finite_round */
    case 1310:  /* avx512f_divv8df3_round */
    case 1304:  /* avx512f_divv16sf3_round */
    case 1292:  /* *mulv2df3_round */
    case 1288:  /* *mulv4df3_round */
    case 1284:  /* *mulv8df3_round */
    case 1280:  /* *mulv4sf3_round */
    case 1276:  /* *mulv8sf3_round */
    case 1272:  /* *mulv16sf3_round */
    case 1260:  /* *subv2df3_round */
    case 1256:  /* *addv2df3_round */
    case 1252:  /* *subv4df3_round */
    case 1248:  /* *addv4df3_round */
    case 1244:  /* *subv8df3_round */
    case 1240:  /* *addv8df3_round */
    case 1236:  /* *subv4sf3_round */
    case 1232:  /* *addv4sf3_round */
    case 1228:  /* *subv8sf3_round */
    case 1224:  /* *addv8sf3_round */
    case 1220:  /* *subv16sf3_round */
    case 1216:  /* *addv16sf3_round */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 1195:  /* avx512f_storedquv8di_mask */
    case 1194:  /* avx512f_storedquv16si_mask */
    case 1183:  /* avx512f_storeupd512_mask */
    case 1182:  /* avx512f_storeups512_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 0;
      break;

    case 3111:  /* conflictv8di_mask */
    case 3109:  /* conflictv16si_mask */
    case 2953:  /* avx512f_vcvtph2ps512_mask */
    case 2644:  /* avx512er_rsqrt28v8df_mask */
    case 2640:  /* avx512er_rsqrt28v16sf_mask */
    case 2632:  /* avx512er_rcp28v8df_mask */
    case 2628:  /* avx512er_rcp28v16sf_mask */
    case 2624:  /* avx512er_exp2v8df_mask */
    case 2620:  /* avx512er_exp2v16sf_mask */
    case 1945:  /* avx512f_getexpv8df_mask */
    case 1941:  /* avx512f_getexpv16sf_mask */
    case 1804:  /* avx512f_ufix_notruncv8dfv8si_mask */
    case 1797:  /* avx512f_cvtpd2dq512_mask */
    case 1743:  /* avx512f_ufix_notruncv16sfv16si_mask */
    case 1739:  /* avx512f_fix_notruncv16sfv16si_mask */
    case 1345:  /* rsqrt14v8df_mask */
    case 1343:  /* rsqrt14v16sf_mask */
    case 1321:  /* rcp14v8df_mask */
    case 1319:  /* rcp14v16sf_mask */
    case 1189:  /* *avx512f_loaddquv8di_mask */
    case 1187:  /* *avx512f_loaddquv16si_mask */
    case 1173:  /* *avx512f_loadupd512_mask */
    case 1169:  /* *avx512f_loadups512_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (pat, 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 1165:  /* avx512f_storev8df_mask */
    case 1164:  /* avx512f_storev8di_mask */
    case 1163:  /* avx512f_storev16sf_mask */
    case 1162:  /* avx512f_storev16si_mask */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (pat, 1), 1);
      recog_data.dup_num[0] = 0;
      break;

    case 2518:  /* avx2_pblenddv4si */
    case 2517:  /* avx2_pblenddv8si */
    case 2516:  /* *avx2_pblendw */
    case 2515:  /* sse4_1_pblendw */
    case 2497:  /* sse4_1_blendpd */
    case 2496:  /* avx_blendpd256 */
    case 2495:  /* sse4_1_blendps */
    case 2494:  /* avx_blendps256 */
    case 1161:  /* avx512f_blendmv8df */
    case 1160:  /* avx512f_blendmv8di */
    case 1159:  /* avx512f_blendmv16sf */
    case 1158:  /* avx512f_blendmv16si */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2754:  /* *avx_vzeroall */
    case 1133:  /* *mmx_femms */
    case 1132:  /* *mmx_emms */
      ro[0] = *(ro_loc[0] = &PATTERN (insn));
      break;

    case 2427:  /* *sse2_maskmovdqu */
    case 2426:  /* *sse2_maskmovdqu */
    case 1131:  /* *mmx_maskmovq */
    case 1130:  /* *mmx_maskmovq */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (pat, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 1));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (XEXP (pat, 1), 0, 2), 0);
      recog_data.dup_num[0] = 0;
      break;

    case 2784:  /* avx512f_permv8df_1 */
    case 2782:  /* avx512f_permv8di_1 */
    case 2781:  /* avx2_permv4df_1 */
    case 2780:  /* avx2_permv4di_1 */
    case 2386:  /* sse2_pshuflw_1 */
    case 2384:  /* sse2_pshufd_1 */
    case 1879:  /* *avx512f_vextracti32x4_1 */
    case 1877:  /* *avx512f_vextractf32x4_1 */
    case 1118:  /* mmx_pshufw_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XEXP (pat, 1), 1), 0, 3));
      break;

    case 2406:  /* *vec_extractv4si_zext_mem */
    case 2404:  /* *vec_extractv4si_zext */
    case 2396:  /* *vec_extractv8hi_zext */
    case 2395:  /* *vec_extractv8hi_zext */
    case 2394:  /* *vec_extractv16qi_zext */
    case 2393:  /* *vec_extractv16qi_zext */
    case 1125:  /* *vec_extractv2si_zext_mem */
    case 1117:  /* mmx_pextrw */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0, 0));
      break;

    case 2360:  /* sse4_1_pinsrq */
    case 2359:  /* sse4_1_pinsrd */
    case 2358:  /* sse2_pinsrw */
    case 2357:  /* sse4_1_pinsrb */
    case 1870:  /* *vec_setv4sf_sse4_1 */
    case 1116:  /* *mmx_pinsrw */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 2118:  /* *sse4_1_mulv2siv2di3 */
    case 2117:  /* *vec_widen_smult_even_v8si */
    case 2115:  /* *vec_widen_smult_even_v16si */
    case 2114:  /* *vec_widen_umult_even_v4si */
    case 2113:  /* *vec_widen_umult_even_v8si */
    case 2111:  /* *vec_widen_umult_even_v16si */
    case 1076:  /* *sse2_umulv1siv1di3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0));
      break;

    case 1127:  /* *mmx_uavgv4hi3 */
    case 1126:  /* *mmx_uavgv8qi3 */
    case 1075:  /* *mmx_pmulhrwv4hi3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 1), 0));
      break;

    case 2454:  /* ssse3_pmaddubsw */
    case 2453:  /* ssse3_pmaddubsw128 */
    case 2452:  /* avx2_pmaddubsw256 */
    case 2120:  /* *sse2_pmaddwd */
    case 2119:  /* *avx2_pmaddwd */
    case 1074:  /* *mmx_pmaddwd */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0), 0);
      recog_data.dup_num[1] = 2;
      break;

    case 2110:  /* *umulv8hi3_highpart */
    case 2109:  /* *smulv8hi3_highpart */
    case 2108:  /* *umulv16hi3_highpart */
    case 2107:  /* *smulv16hi3_highpart */
    case 1073:  /* *mmx_umulv4hi3_highpart */
    case 1072:  /* *mmx_smulv4hi3_highpart */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1), 0));
      break;

    case 1048:  /* mmx_pi2fw */
    case 1047:  /* mmx_pf2iw */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      break;

    case 1444:  /* sse3_addsubv4sf3 */
    case 1443:  /* avx_addsubv8sf3 */
    case 1442:  /* sse3_addsubv2df3 */
    case 1441:  /* avx_addsubv4df3 */
    case 1042:  /* mmx_addsubv2sf3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 1), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 2451:  /* ssse3_phsubdv2si3 */
    case 2450:  /* ssse3_phadddv2si3 */
    case 1448:  /* sse3_hsubv2df3 */
    case 1041:  /* mmx_hsubv2sf3 */
    case 1040:  /* mmx_haddv2sf3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 1), 0);
      recog_data.dup_num[1] = 2;
      break;

    case 1018:  /* xbegin_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[0] = 0;
      break;

    case 1002:  /* *lwp_lwpinsdi3_1 */
    case 1001:  /* *lwp_lwpinssi3_1 */
      ro[0] = *(ro_loc[0] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 1));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 2));
      break;

    case 992:  /* fldenv */
      ro[0] = *(ro_loc[0] = &XVECEXP (XVECEXP (pat, 0, 0), 0, 0));
      break;

    case 2433:  /* sse3_monitor_di */
    case 2432:  /* sse3_monitor_si */
    case 1000:  /* *lwp_lwpvaldi3_1 */
    case 999:  /* *lwp_lwpvalsi3_1 */
    case 990:  /* xrstor64 */
    case 989:  /* xrstor_rex64 */
      ro[0] = *(ro_loc[0] = &XVECEXP (pat, 0, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (pat, 0, 1));
      ro[2] = *(ro_loc[2] = &XVECEXP (pat, 0, 2));
      break;

    case 2431:  /* sse3_mwait */
    case 988:  /* xrstor */
      ro[0] = *(ro_loc[0] = &XVECEXP (pat, 0, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (pat, 0, 1));
      break;

    case 977:  /* rdtscp_rex64 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 2), 0));
      break;

    case 976:  /* rdtscp */
    case 975:  /* rdtsc_rex64 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 956:  /* *prefetch_sse */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (pat, 2));
      break;

    case 952:  /* adjust_stack_and_probedi */
    case 951:  /* adjust_stack_and_probesi */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1));
      break;

    case 1559:  /* *fma_fmadd_v8df */
    case 1555:  /* *fma_fmadd_v16sf */
    case 1554:  /* *fma_fmadd_v4df */
    case 1553:  /* *fma_fmadd_v8sf */
    case 1552:  /* *fma_fmadd_v2df */
    case 1551:  /* *fma_fmadd_v4sf */
    case 1550:  /* *fma_fmadd_df */
    case 1549:  /* *fma_fmadd_sf */
    case 1157:  /* avx512f_loadv8df_mask */
    case 1156:  /* avx512f_loadv8di_mask */
    case 1155:  /* avx512f_loadv16sf_mask */
    case 1154:  /* avx512f_loadv16si_mask */
    case 936:  /* *xop_pcmov_df */
    case 935:  /* *xop_pcmov_sf */
    case 934:  /* *movsfcc_1_387 */
    case 933:  /* *movdfcc_1 */
    case 932:  /* *movxfcc_1 */
    case 931:  /* *movqicc_noc */
    case 930:  /* *movdicc_noc */
    case 929:  /* *movsicc_noc */
    case 928:  /* *movhicc_noc */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 2));
      break;

    case 921:  /* *strlenqi_1 */
    case 920:  /* *strlenqi_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      ro[5] = *(ro_loc[5] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0), 0));
      break;

    case 919:  /* *cmpstrnqi_1 */
    case 918:  /* *cmpstrnqi_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 3), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 4), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 5), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 0));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 1), 0));
      ro[6] = *(ro_loc[6] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      break;

    case 917:  /* *cmpstrnqi_nz_1 */
    case 916:  /* *cmpstrnqi_nz_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 3), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 4), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 5), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      ro[6] = *(ro_loc[6] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 915:  /* *rep_stosqi */
    case 914:  /* *rep_stosqi */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 3), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 4), 0);
      recog_data.dup_num[0] = 4;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 2), 0), 0);
      recog_data.dup_num[1] = 3;
      break;

    case 913:  /* *rep_stossi */
    case 912:  /* *rep_stossi */
    case 911:  /* *rep_stosdi_rex64 */
    case 910:  /* *rep_stosdi_rex64 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 3), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 4), 0);
      recog_data.dup_num[0] = 4;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 2), 0), 0);
      recog_data.dup_num[1] = 3;
      break;

    case 909:  /* *strsetqi_1 */
    case 908:  /* *strsetqi_1 */
    case 907:  /* *strsethi_1 */
    case 906:  /* *strsethi_1 */
    case 905:  /* *strsetsi_1 */
    case 904:  /* *strsetsi_1 */
    case 903:  /* *strsetdi_rex_1 */
    case 902:  /* *strsetdi_rex_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 901:  /* *rep_movqi */
    case 900:  /* *rep_movqi */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 4), 0);
      recog_data.dup_num[0] = 5;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 3), 0), 0);
      recog_data.dup_num[1] = 3;
      recog_data.dup_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 3), 1), 0);
      recog_data.dup_num[2] = 4;
      recog_data.dup_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 2), 1), 1);
      recog_data.dup_num[3] = 5;
      break;

    case 899:  /* *rep_movsi */
    case 898:  /* *rep_movsi */
    case 897:  /* *rep_movdi_rex64 */
    case 896:  /* *rep_movdi_rex64 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 2), 1), 1));
      ro[5] = *(ro_loc[5] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 4), 0);
      recog_data.dup_num[0] = 5;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 3), 0), 0);
      recog_data.dup_num[1] = 3;
      recog_data.dup_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 3), 1), 0);
      recog_data.dup_num[2] = 4;
      recog_data.dup_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0), 0);
      recog_data.dup_num[3] = 5;
      break;

    case 895:  /* *strmovqi_1 */
    case 894:  /* *strmovqi_1 */
    case 893:  /* *strmovhi_1 */
    case 892:  /* *strmovhi_1 */
    case 891:  /* *strmovsi_1 */
    case 890:  /* *strmovsi_1 */
    case 889:  /* *strmovdi_rex_1 */
    case 888:  /* *strmovdi_rex_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0);
      recog_data.dup_num[0] = 3;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[1] = 2;
      break;

    case 872:  /* fistdi2_ceil_with_temp */
    case 871:  /* fistdi2_floor_with_temp */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 3), 0));
      ro[5] = *(ro_loc[5] = &XEXP (XVECEXP (pat, 0, 4), 0));
      break;

    case 880:  /* fistsi2_ceil_with_temp */
    case 879:  /* fistsi2_floor_with_temp */
    case 878:  /* fisthi2_ceil_with_temp */
    case 877:  /* fisthi2_floor_with_temp */
    case 870:  /* fistdi2_ceil */
    case 869:  /* fistdi2_floor */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 3), 0));
      break;

    case 950:  /* allocate_stack_worker_probe_di */
    case 949:  /* allocate_stack_worker_probe_si */
    case 868:  /* *fistdi2_ceil_1 */
    case 867:  /* *fistdi2_floor_1 */
    case 866:  /* *fistsi2_ceil_1 */
    case 865:  /* *fistsi2_floor_1 */
    case 864:  /* *fisthi2_ceil_1 */
    case 863:  /* *fisthi2_floor_1 */
    case 861:  /* frndintxf2_mask_pm */
    case 857:  /* frndintxf2_trunc */
    case 856:  /* frndintxf2_ceil */
    case 855:  /* frndintxf2_floor */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      break;

    case 3128:  /* atomic_storedi_fpu */
    case 3123:  /* atomic_loaddi_fpu */
    case 876:  /* fistsi2_ceil */
    case 875:  /* fistsi2_floor */
    case 874:  /* fisthi2_ceil */
    case 873:  /* fisthi2_floor */
    case 862:  /* frndintxf2_mask_pm_i387 */
    case 860:  /* frndintxf2_trunc_i387 */
    case 859:  /* frndintxf2_ceil_i387 */
    case 858:  /* frndintxf2_floor_i387 */
    case 848:  /* fistdi2_with_temp */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 2), 0));
      break;

    case 963:  /* stack_tls_protect_set_di */
    case 962:  /* stack_tls_protect_set_si */
    case 961:  /* stack_protect_set_di */
    case 960:  /* stack_protect_set_si */
    case 854:  /* fistsi2_with_temp */
    case 853:  /* fisthi2_with_temp */
    case 847:  /* fistdi2 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 842:  /* fscalexf4_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 1);
      recog_data.dup_num[0] = 3;
      recog_data.dup_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[1] = 2;
      break;

    case 837:  /* fyl2xp1_extenddfxf3_i387 */
    case 836:  /* fyl2xp1_extendsfxf3_i387 */
    case 834:  /* fyl2x_extenddfxf3_i387 */
    case 833:  /* fyl2x_extendsfxf3_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 831:  /* fpatan_extenddfxf3_i387 */
    case 830:  /* fpatan_extendsfxf3_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 967:  /* stack_tls_protect_test_di */
    case 966:  /* stack_tls_protect_test_si */
    case 965:  /* stack_protect_test_di */
    case 964:  /* stack_protect_test_si */
    case 835:  /* fyl2xp1xf3_i387 */
    case 832:  /* fyl2xxf3_i387 */
    case 829:  /* *fpatanxf3_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 828:  /* fptan_extenddfxf4_i387 */
    case 827:  /* fptan_extendsfxf4_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 0), 1));
      break;

    case 826:  /* fptanxf4_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 0), 1));
      break;

    case 840:  /* fxtract_extenddfxf3_i387 */
    case 839:  /* fxtract_extendsfxf3_i387 */
    case 825:  /* sincos_extenddfxf3_i387 */
    case 824:  /* sincos_extendsfxf3_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0), 0));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0), 0);
      recog_data.dup_num[0] = 2;
      break;

    case 973:  /* rdpmc_rex64 */
    case 838:  /* fxtractxf3_i387 */
    case 823:  /* sincosxf3 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[0] = 2;
      break;

    case 816:  /* fprem1xf4_i387 */
    case 815:  /* fpremxf4_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 1);
      recog_data.dup_num[0] = 3;
      recog_data.dup_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 2), 1), 0, 0);
      recog_data.dup_num[1] = 2;
      recog_data.dup_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 1);
      recog_data.dup_num[2] = 3;
      recog_data.dup_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0, 0);
      recog_data.dup_num[3] = 2;
      break;

    case 806:  /* *fop_xf_6_i387 */
    case 805:  /* *fop_xf_6_i387 */
    case 794:  /* *fop_df_6_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (pat, 1));
      break;

    case 804:  /* *fop_xf_5_i387 */
    case 803:  /* *fop_xf_5_i387 */
    case 800:  /* *fop_xf_3_i387 */
    case 799:  /* *fop_xf_3_i387 */
    case 793:  /* *fop_df_5_i387 */
    case 791:  /* *fop_df_3_i387 */
    case 790:  /* *fop_sf_3_i387 */
    case 789:  /* *fop_df_3_i387 */
    case 788:  /* *fop_sf_3_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (pat, 1));
      break;

    case 802:  /* *fop_xf_4_i387 */
    case 801:  /* *fop_xf_4_i387 */
    case 798:  /* *fop_xf_2_i387 */
    case 797:  /* *fop_xf_2_i387 */
    case 792:  /* *fop_df_4_i387 */
    case 787:  /* *fop_df_2_i387 */
    case 786:  /* *fop_sf_2_i387 */
    case 785:  /* *fop_df_2_i387 */
    case 784:  /* *fop_sf_2_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (pat, 1));
      break;

    case 770:  /* *tls_dynamic_gnu2_combine_64 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      break;

    case 767:  /* *tls_dynamic_gnu2_combine_32 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 2));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      break;

    case 766:  /* *tls_dynamic_gnu2_call_32 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      break;

    case 765:  /* *tls_dynamic_gnu2_lea_32 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0, 0));
      break;

    case 762:  /* *add_tp_x32_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      break;

    case 764:  /* *add_tp_di */
    case 763:  /* *add_tp_si */
    case 761:  /* *add_tp_x32 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      break;

    case 756:  /* *tls_local_dynamic_32_once */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 0, 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[5] = *(ro_loc[5] = &XEXP (XVECEXP (pat, 0, 2), 0));
      break;

    case 755:  /* *tls_local_dynamic_base_64_largepic */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      break;

    case 752:  /* *tls_local_dynamic_base_32_gnu */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 2), 0));
      break;

    case 751:  /* *tls_global_dynamic_64_largepic */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      break;

    case 750:  /* *tls_global_dynamic_64_di */
    case 749:  /* *tls_global_dynamic_64_si */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      break;

    case 748:  /* *tls_global_dynamic_32_gnu */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[5] = *(ro_loc[5] = &XEXP (XVECEXP (pat, 0, 2), 0));
      break;

    case 747:  /* *parityhi2_cmp */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      break;

    case 746:  /* paritysi2_cmp */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      break;

    case 745:  /* paritydi2_cmp */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 3), 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      break;

    case 744:  /* bswaphi_lowpart */
    case 743:  /* *bswaphi_lowpart_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0);
      recog_data.dup_num[0] = 0;
      break;

    case 731:  /* *bsrhi */
    case 730:  /* bsr */
    case 729:  /* bsr_rex64 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      break;

    case 728:  /* *tbm_tzmsk_di */
    case 727:  /* *tbm_tzmsk_si */
    case 726:  /* *tbm_t1mskc_di */
    case 725:  /* *tbm_t1mskc_si */
    case 724:  /* *tbm_blsic_di */
    case 723:  /* *tbm_blsic_si */
    case 716:  /* *tbm_blcic_di */
    case 715:  /* *tbm_blcic_si */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 714:  /* *tbm_blci_di */
    case 713:  /* *tbm_blci_si */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 710:  /* tbm_bextri_di */
    case 709:  /* tbm_bextri_si */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2));
      break;

    case 3127:  /* atomic_storedi_1 */
    case 3126:  /* atomic_storesi_1 */
    case 3125:  /* atomic_storehi_1 */
    case 3124:  /* atomic_storeqi_1 */
    case 3117:  /* sha256msg2 */
    case 3116:  /* sha256msg1 */
    case 3114:  /* sha1nexte */
    case 3113:  /* sha1msg2 */
    case 3112:  /* sha1msg1 */
    case 3096:  /* avx512f_getmantv8df */
    case 3092:  /* avx512f_getmantv16sf */
    case 2958:  /* *avx512f_vcvtps2ph512 */
    case 2957:  /* vcvtps2ph256 */
    case 2956:  /* *vcvtps2ph_store */
    case 2850:  /* avx_vpermilvarv2df3 */
    case 2849:  /* avx_vpermilvarv4df3 */
    case 2847:  /* avx512f_vpermilvarv8df3 */
    case 2846:  /* avx_vpermilvarv4sf3 */
    case 2845:  /* avx_vpermilvarv8sf3 */
    case 2843:  /* avx512f_vpermilvarv16sf3 */
    case 2778:  /* avx512f_permvarv8df */
    case 2776:  /* avx512f_permvarv8di */
    case 2774:  /* avx512f_permvarv16sf */
    case 2772:  /* avx512f_permvarv16si */
    case 2771:  /* avx2_permvarv8sf */
    case 2770:  /* avx2_permvarv8si */
    case 2752:  /* aeskeygenassist */
    case 2750:  /* aesdeclast */
    case 2749:  /* aesdec */
    case 2748:  /* aesenclast */
    case 2747:  /* aesenc */
    case 2573:  /* sse4_1_roundpd */
    case 2572:  /* avx_roundpd256 */
    case 2571:  /* sse4_1_roundps */
    case 2570:  /* avx_roundps256 */
    case 2493:  /* sse4a_insertq */
    case 2491:  /* sse4a_extrq */
    case 2469:  /* ssse3_psignv2si3 */
    case 2468:  /* ssse3_psignv4hi3 */
    case 2467:  /* ssse3_psignv8qi3 */
    case 2466:  /* ssse3_psignv4si3 */
    case 2465:  /* avx2_psignv8si3 */
    case 2464:  /* ssse3_psignv8hi3 */
    case 2463:  /* avx2_psignv16hi3 */
    case 2462:  /* ssse3_psignv16qi3 */
    case 2461:  /* avx2_psignv32qi3 */
    case 2460:  /* ssse3_pshufbv8qi3 */
    case 2459:  /* ssse3_pshufbv16qi3 */
    case 2458:  /* avx2_pshufbv32qi3 */
    case 2419:  /* sse2_psadbw */
    case 2418:  /* avx2_psadbw */
    case 2333:  /* avx512f_testnmv8di3 */
    case 2331:  /* avx512f_testnmv16si3 */
    case 2329:  /* avx512f_testmv8di3 */
    case 2327:  /* avx512f_testmv16si3 */
    case 2274:  /* avx512f_gtv8di3 */
    case 2272:  /* avx512f_gtv16si3 */
    case 2261:  /* avx512f_eqv8di3_1 */
    case 2259:  /* avx512f_eqv16si3_1 */
    case 1983:  /* avx512f_rndscalev8df */
    case 1979:  /* avx512f_rndscalev16sf */
    case 1929:  /* avx512f_scalefv8df */
    case 1925:  /* avx512f_scalefv16sf */
    case 1440:  /* *ieee_smaxv2df3 */
    case 1439:  /* *ieee_smaxv4df3 */
    case 1438:  /* *ieee_smaxv8df3 */
    case 1437:  /* *ieee_smaxv4sf3 */
    case 1436:  /* *ieee_smaxv8sf3 */
    case 1435:  /* *ieee_smaxv16sf3 */
    case 1434:  /* *ieee_sminv2df3 */
    case 1433:  /* *ieee_sminv4df3 */
    case 1432:  /* *ieee_sminv8df3 */
    case 1431:  /* *ieee_sminv4sf3 */
    case 1430:  /* *ieee_sminv8sf3 */
    case 1429:  /* *ieee_sminv16sf3 */
    case 1128:  /* mmx_psadbw */
    case 1039:  /* mmx_rsqit1v2sf3 */
    case 1037:  /* mmx_rcpit2v2sf3 */
    case 1036:  /* mmx_rcpit1v2sf3 */
    case 987:  /* xsaveopt64 */
    case 986:  /* xsave64 */
    case 985:  /* xsaveopt_rex64 */
    case 984:  /* xsave_rex64 */
    case 971:  /* sse4_2_crc32di */
    case 970:  /* sse4_2_crc32si */
    case 969:  /* sse4_2_crc32hi */
    case 968:  /* sse4_2_crc32qi */
    case 944:  /* *ieee_smindf3 */
    case 943:  /* *ieee_smaxdf3 */
    case 942:  /* *ieee_sminsf3 */
    case 941:  /* *ieee_smaxsf3 */
    case 844:  /* sse4_1_rounddf2 */
    case 843:  /* sse4_1_roundsf2 */
    case 708:  /* bmi2_pext_di3 */
    case 707:  /* bmi2_pext_si3 */
    case 706:  /* bmi2_pdep_di3 */
    case 705:  /* bmi2_pdep_si3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 1));
      break;

    case 704:  /* bmi2_bzhi_di3 */
    case 703:  /* bmi2_bzhi_si3 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      break;

    case 722:  /* *tbm_blsfill_di */
    case 721:  /* *tbm_blsfill_si */
    case 720:  /* *tbm_blcs_di */
    case 719:  /* *tbm_blcs_si */
    case 718:  /* *tbm_blcmsk_di */
    case 717:  /* *tbm_blcmsk_si */
    case 712:  /* *tbm_blcfill_di */
    case 711:  /* *tbm_blcfill_si */
    case 702:  /* *bmi_blsr_di */
    case 701:  /* *bmi_blsr_si */
    case 700:  /* *bmi_blsmsk_di */
    case 699:  /* *bmi_blsmsk_si */
    case 698:  /* *bmi_blsi_di */
    case 697:  /* *bmi_blsi_si */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 954:  /* probe_stack_rangedi */
    case 953:  /* probe_stack_rangesi */
    case 769:  /* *tls_dynamic_gnu2_call_64 */
    case 696:  /* bmi_bextr_di */
    case 695:  /* bmi_bextr_si */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      break;

    case 1828:  /* avx512f_cvtps2pd512_round */
    case 1822:  /* *avx512f_cvtpd2ps512_round */
    case 1811:  /* ufix_truncv8dfv8si2_round */
    case 1807:  /* fix_truncv8dfv8si2_round */
    case 1750:  /* ufix_truncv16sfv16si2_round */
    case 1746:  /* fix_truncv16sfv16si2_round */
    case 1732:  /* ufloatv16siv16sf2_round */
    case 1726:  /* floatv16siv16sf2_round */
    case 1331:  /* avx512f_sqrtv8df2_round */
    case 1325:  /* avx512f_sqrtv16sf2_round */
    case 735:  /* *popcountdi2_falsedep */
    case 734:  /* *popcountsi2_falsedep */
    case 689:  /* *clzdi2_lzcnt_falsedep */
    case 688:  /* *clzsi2_lzcnt_falsedep */
    case 682:  /* *ctzdi2_falsedep */
    case 681:  /* *ctzsi2_falsedep */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XVECEXP (pat, 0, 1), 0, 0));
      break;

    case 678:  /* *bsfdi_1 */
    case 677:  /* *bsfsi_1 */
    case 676:  /* *tzcntdi_1 */
    case 675:  /* *tzcntsi_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 2489:  /* sse4a_vmmovntv2df */
    case 2488:  /* sse4a_vmmovntv4sf */
    case 1780:  /* sse2_cvtsd2siq */
    case 1777:  /* sse2_cvtsd2si */
    case 1771:  /* avx512f_vcvtsd2usiq */
    case 1769:  /* avx512f_vcvtsd2usi */
    case 1763:  /* avx512f_vcvtss2usiq */
    case 1761:  /* avx512f_vcvtss2usi */
    case 1711:  /* sse_cvtss2siq */
    case 1708:  /* sse_cvtss2si */
    case 822:  /* *cos_extenddfxf2_i387 */
    case 821:  /* *sin_extenddfxf2_i387 */
    case 820:  /* *cos_extendsfxf2_i387 */
    case 819:  /* *sin_extendsfxf2_i387 */
    case 669:  /* set_got_offset_rex64 */
    case 668:  /* set_rip_rex64 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (XEXP (pat, 1), 0, 0), 0));
      break;

    case 666:  /* set_got_labelled */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0), 0));
      break;

    case 1016:  /* rdseeddi_1 */
    case 1015:  /* rdseedsi_1 */
    case 1014:  /* rdseedhi_1 */
    case 1013:  /* rdranddi_1 */
    case 1012:  /* rdrandsi_1 */
    case 1011:  /* rdrandhi_1 */
    case 991:  /* fnstenv */
    case 665:  /* set_got */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      break;

    case 661:  /* simple_return_indirect_internal */
    case 660:  /* simple_return_pop_internal */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 2430:  /* sse2_clflush */
    case 2428:  /* sse_ldmxcsr */
    case 1020:  /* xabort */
    case 1010:  /* wrgsbasedi */
    case 1009:  /* wrfsbasedi */
    case 1008:  /* wrgsbasesi */
    case 1007:  /* wrfsbasesi */
    case 996:  /* *lwp_llwpcbdi1 */
    case 995:  /* *lwp_llwpcbsi1 */
    case 981:  /* fxrstor64 */
    case 980:  /* fxrstor */
    case 673:  /* split_stack_return */
    case 664:  /* pad */
    case 663:  /* nops */
    case 657:  /* prologue_use */
      ro[0] = *(ro_loc[0] = &XVECEXP (pat, 0, 0));
      break;

    case 3121:  /* mfence_sse2 */
    case 3120:  /* *sse_sfence */
    case 3119:  /* *sse2_lfence */
    case 1017:  /* *pause */
    case 656:  /* *memory_blockage */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      recog_data.dup_loc[0] = &XVECEXP (XEXP (pat, 1), 0, 0);
      recog_data.dup_num[0] = 0;
      break;

    case 2755:  /* avx_vzeroupper */
    case 1021:  /* xtest_1 */
    case 1019:  /* xend */
    case 994:  /* fnclex */
    case 955:  /* trap */
    case 887:  /* cld */
    case 672:  /* leave_rex64 */
    case 671:  /* leave */
    case 670:  /* eh_return_internal */
    case 662:  /* nop */
    case 659:  /* simple_return_internal_long */
    case 658:  /* simple_return_internal */
    case 655:  /* blockage */
      break;

    case 654:  /* *sibcall_value_pop */
    case 653:  /* *call_value_pop */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1));
      break;

    case 652:  /* *call_value_rex64_ms_sysv */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &PATTERN (insn));
      break;

    case 2894:  /* vec_set_hi_v32qi */
    case 2892:  /* vec_set_hi_v16hi */
    case 2890:  /* vec_set_hi_v8sf */
    case 2889:  /* vec_set_hi_v8si */
    case 2886:  /* vec_set_hi_v4df */
    case 2885:  /* vec_set_hi_v4di */
    case 2882:  /* avx2_vec_set_hi_v4di */
    case 2290:  /* *andnotv2di3 */
    case 2289:  /* *andnotv4di3 */
    case 2288:  /* *andnotv4si3 */
    case 2287:  /* *andnotv8si3 */
    case 2286:  /* *andnotv8hi3 */
    case 2285:  /* *andnotv16hi3 */
    case 2284:  /* *andnotv16qi3 */
    case 2283:  /* *andnotv32qi3 */
    case 2281:  /* *andnotv8di3 */
    case 2279:  /* *andnotv16si3 */
    case 2010:  /* sse2_loadhpd */
    case 1857:  /* sse_loadhps */
    case 1826:  /* *sse2_cvtpd2ps */
    case 1816:  /* *sse2_cvttpd2dq */
    case 1815:  /* *avx_cvttpd2dq256_2 */
    case 1535:  /* *andnottf3 */
    case 1534:  /* *andnotdf3 */
    case 1533:  /* *andnotsf3 */
    case 1514:  /* sse2_andnotv2df3 */
    case 1513:  /* avx_andnotv4df3 */
    case 1512:  /* avx512f_andnotv8df3 */
    case 1511:  /* sse_andnotv4sf3 */
    case 1510:  /* avx_andnotv8sf3 */
    case 1509:  /* avx512f_andnotv16sf3 */
    case 1338:  /* sse2_vmsqrtv2df2 */
    case 1336:  /* sse_vmsqrtv4sf2 */
    case 1097:  /* mmx_andnotv2si3 */
    case 1096:  /* mmx_andnotv4hi3 */
    case 1095:  /* mmx_andnotv8qi3 */
    case 651:  /* *sibcall_value */
    case 650:  /* *sibcall_value */
    case 649:  /* *call_value */
    case 648:  /* *call_value */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 647:  /* *sibcall_pop */
    case 646:  /* *call_pop */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 0), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1));
      break;

    case 643:  /* *call_rex64_ms_sysv */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 0), 1));
      ro[2] = *(ro_loc[2] = &PATTERN (insn));
      break;

    case 640:  /* *tablejump_1 */
    case 639:  /* *tablejump_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 1));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 0), 0));
      break;

    case 638:  /* *indirect_jump */
    case 637:  /* *indirect_jump */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 1));
      break;

    case 636:  /* jump */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (pat, 1), 0));
      break;

    case 635:  /* *jccxf_si_r_i387 */
    case 634:  /* *jccdf_si_r_i387 */
    case 633:  /* *jccsf_si_r_i387 */
    case 632:  /* *jccxf_hi_r_i387 */
    case 631:  /* *jccdf_hi_r_i387 */
    case 630:  /* *jccsf_hi_r_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2), 0));
      ro[5] = *(ro_loc[5] = &XEXP (XVECEXP (pat, 0, 3), 0));
      break;

    case 629:  /* *jccxf_si_i387 */
    case 628:  /* *jccdf_si_i387 */
    case 627:  /* *jccsf_si_i387 */
    case 626:  /* *jccxf_hi_i387 */
    case 625:  /* *jccdf_hi_i387 */
    case 624:  /* *jccsf_hi_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      ro[5] = *(ro_loc[5] = &XEXP (XVECEXP (pat, 0, 3), 0));
      break;

    case 623:  /* *jccuxf_r_i387 */
    case 622:  /* *jccudf_r_i387 */
    case 621:  /* *jccusf_r_i387 */
    case 617:  /* *jccdf_r_i387 */
    case 616:  /* *jccsf_r_i387 */
    case 613:  /* *jccxf_r_i387 */
    case 611:  /* *jccxf_0_r_i387 */
    case 610:  /* *jccdf_0_r_i387 */
    case 609:  /* *jccsf_0_r_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 3), 0));
      break;

    case 620:  /* *jccuxf_i387 */
    case 619:  /* *jccudf_i387 */
    case 618:  /* *jccusf_i387 */
    case 615:  /* *jccdf_i387 */
    case 614:  /* *jccsf_i387 */
    case 612:  /* *jccxf_i387 */
    case 608:  /* *jccxf_0_i387 */
    case 607:  /* *jccdf_0_i387 */
    case 606:  /* *jccsf_0_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 3), 0));
      break;

    case 605:  /* *jcc_btsi_mask_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 1), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 1), 0), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      break;

    case 604:  /* *jcc_btsi_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      break;

    case 603:  /* *jcc_btdi_mask */
    case 602:  /* *jcc_btsi_mask */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 2), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 2), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      break;

    case 601:  /* *jcc_btdi_1 */
    case 600:  /* *jcc_btsi_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 2));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      break;

    case 599:  /* *jcc_btdi */
    case 598:  /* *jcc_btsi */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 2), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      break;

    case 597:  /* *jcc_2 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 2), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      break;

    case 596:  /* *jcc_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      break;

    case 1500:  /* avx512f_maskcmpv2df3 */
    case 1499:  /* avx512f_maskcmpv4df3 */
    case 1498:  /* avx512f_maskcmpv8df3 */
    case 1497:  /* avx512f_maskcmpv4sf3 */
    case 1496:  /* avx512f_maskcmpv8sf3 */
    case 1495:  /* avx512f_maskcmpv16sf3 */
    case 1468:  /* sse2_maskcmpv2df3 */
    case 1467:  /* avx_maskcmpv4df3 */
    case 1466:  /* sse_maskcmpv4sf3 */
    case 1465:  /* avx_maskcmpv8sf3 */
    case 1464:  /* *sse2_maskcmpv2df3_comm */
    case 1463:  /* *avx_maskcmpv4df3_comm */
    case 1462:  /* *sse_maskcmpv4sf3_comm */
    case 1461:  /* *avx_maskcmpv8sf3_comm */
    case 796:  /* *fop_xf_1_i387 */
    case 795:  /* *fop_xf_comm_i387 */
    case 783:  /* *fop_df_1_i387 */
    case 782:  /* *fop_sf_1_i387 */
    case 781:  /* *fop_df_1_sse */
    case 780:  /* *fop_sf_1_sse */
    case 778:  /* *fop_df_1_mixed */
    case 777:  /* *fop_sf_1_mixed */
    case 776:  /* *fop_df_comm_i387 */
    case 775:  /* *fop_sf_comm_i387 */
    case 774:  /* *fop_df_comm_sse */
    case 773:  /* *fop_sf_comm_sse */
    case 772:  /* *fop_df_comm_mixed */
    case 771:  /* *fop_sf_comm_mixed */
    case 595:  /* setcc_df_sse */
    case 594:  /* setcc_sf_sse */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (pat, 1));
      break;

    case 588:  /* *btdi */
    case 587:  /* *btsi */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      break;

    case 586:  /* *btcq */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 2));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0);
      recog_data.dup_num[0] = 0;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 2);
      recog_data.dup_num[1] = 1;
      break;

    case 585:  /* *btrq */
    case 584:  /* *btsq */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 2));
      break;

    case 568:  /* ix86_rotrti3_doubleword */
    case 567:  /* ix86_rotrdi3_doubleword */
    case 566:  /* ix86_rotlti3_doubleword */
    case 565:  /* ix86_rotldi3_doubleword */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 2), 0));
      break;

    case 2356:  /* vec_interleave_lowv4si */
    case 2354:  /* *avx512f_interleave_lowv16si */
    case 2353:  /* avx2_interleave_lowv8si */
    case 2352:  /* vec_interleave_highv4si */
    case 2350:  /* *avx512f_interleave_highv16si */
    case 2349:  /* avx2_interleave_highv8si */
    case 2348:  /* vec_interleave_lowv8hi */
    case 2347:  /* avx2_interleave_lowv16hi */
    case 2346:  /* vec_interleave_highv8hi */
    case 2345:  /* avx2_interleave_highv16hi */
    case 2344:  /* vec_interleave_lowv16qi */
    case 2343:  /* avx2_interleave_lowv32qi */
    case 2342:  /* vec_interleave_highv16qi */
    case 2341:  /* avx2_interleave_highv32qi */
    case 2003:  /* vec_interleave_lowv2di */
    case 2001:  /* *avx512f_interleave_lowv8di */
    case 2000:  /* avx2_interleave_lowv4di */
    case 1999:  /* vec_interleave_highv2di */
    case 1997:  /* *avx512f_interleave_highv8di */
    case 1996:  /* avx2_interleave_highv4di */
    case 1920:  /* *vec_interleave_lowv2df */
    case 1919:  /* *avx_unpcklpd256 */
    case 1917:  /* *avx512f_unpcklpd512 */
    case 1916:  /* *vec_interleave_highv2df */
    case 1915:  /* avx_unpckhpd256 */
    case 1913:  /* *avx512f_unpckhpd512 */
    case 1844:  /* vec_interleave_lowv4sf */
    case 1843:  /* avx_unpcklps256 */
    case 1841:  /* *avx512f_unpcklps512 */
    case 1840:  /* vec_interleave_highv4sf */
    case 1839:  /* avx_unpckhps256 */
    case 1837:  /* *avx512f_unpckhps512 */
    case 1836:  /* sse_movlhps */
    case 1835:  /* sse_movhlps */
    case 1115:  /* mmx_punpckldq */
    case 1114:  /* mmx_punpckhdq */
    case 1113:  /* mmx_punpcklwd */
    case 1112:  /* mmx_punpckhwd */
    case 1111:  /* mmx_punpcklbw */
    case 1110:  /* mmx_punpckhbw */
    case 575:  /* *bmi2_rorxsi3_1_zext */
    case 534:  /* *bmi2_ashrsi3_1_zext */
    case 533:  /* *bmi2_lshrsi3_1_zext */
    case 498:  /* *bmi2_ashlsi3_1_zext */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      break;

    case 564:  /* *rotrdi3_mask */
    case 563:  /* *rotldi3_mask */
    case 562:  /* *rotrsi3_mask */
    case 561:  /* *rotlsi3_mask */
    case 515:  /* *ashrdi3_mask */
    case 514:  /* *lshrdi3_mask */
    case 513:  /* *ashrsi3_mask */
    case 512:  /* *lshrsi3_mask */
    case 493:  /* *ashldi3_mask */
    case 492:  /* *ashlsi3_mask */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 1));
      break;

    case 521:  /* x86_shrd */
    case 520:  /* x86_64_shrd */
    case 491:  /* x86_shld */
    case 490:  /* x86_64_shld */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0);
      recog_data.dup_num[0] = 0;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 1), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 487:  /* *one_cmplsi2_2_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 477:  /* copysigntf3_var */
    case 476:  /* copysigndf3_var */
    case 475:  /* copysignsf3_var */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      ro[4] = *(ro_loc[4] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 2));
      ro[5] = *(ro_loc[5] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 3));
      break;

    case 3118:  /* sha256rnds2 */
    case 3115:  /* sha1rnds4 */
    case 3091:  /* avx512f_expandv8df_mask */
    case 3090:  /* avx512f_expandv8di_mask */
    case 3089:  /* avx512f_expandv16sf_mask */
    case 3088:  /* avx512f_expandv16si_mask */
    case 3083:  /* avx512f_compressv8df_mask */
    case 3082:  /* avx512f_compressv8di_mask */
    case 3081:  /* avx512f_compressv16sf_mask */
    case 3080:  /* avx512f_compressv16si_mask */
    case 2877:  /* *avx_vperm2f128v4df_full */
    case 2876:  /* *avx_vperm2f128v8sf_full */
    case 2875:  /* *avx_vperm2f128v8si_full */
    case 2869:  /* avx512f_vpermt2varv8df3 */
    case 2867:  /* avx512f_vpermt2varv8di3 */
    case 2865:  /* avx512f_vpermt2varv16sf3 */
    case 2863:  /* avx512f_vpermt2varv16si3 */
    case 2857:  /* avx512f_vpermi2varv8df3 */
    case 2855:  /* avx512f_vpermi2varv8di3 */
    case 2853:  /* avx512f_vpermi2varv16sf3 */
    case 2851:  /* avx512f_vpermi2varv16si3 */
    case 2786:  /* avx2_permv2ti */
    case 2753:  /* pclmulqdq */
    case 2742:  /* xop_pcom_tfv2di3 */
    case 2741:  /* xop_pcom_tfv4si3 */
    case 2740:  /* xop_pcom_tfv8hi3 */
    case 2739:  /* xop_pcom_tfv16qi3 */
    case 2693:  /* xop_pperm */
    case 2514:  /* sse4_1_pblendvb */
    case 2513:  /* avx2_pblendvb */
    case 2510:  /* sse4_1_mpsadbw */
    case 2509:  /* avx2_mpsadbw */
    case 2505:  /* sse4_1_dppd */
    case 2504:  /* avx_dppd256 */
    case 2503:  /* sse4_1_dpps */
    case 2502:  /* avx_dpps256 */
    case 2501:  /* sse4_1_blendvpd */
    case 2500:  /* avx_blendvpd256 */
    case 2499:  /* sse4_1_blendvps */
    case 2498:  /* avx_blendvps256 */
    case 2490:  /* sse4a_extrqi */
    case 2472:  /* ssse3_palignrdi */
    case 2471:  /* ssse3_palignrti */
    case 2470:  /* avx2_palignrv2ti */
    case 1953:  /* *avx512f_alignv8di */
    case 1951:  /* *avx512f_alignv16si */
    case 1645:  /* *fma_fmaddsub_v8df */
    case 1641:  /* *fma_fmaddsub_v16sf */
    case 1640:  /* *fma_fmaddsub_v2df */
    case 1639:  /* *fma_fmaddsub_v4df */
    case 1638:  /* *fma_fmaddsub_v4sf */
    case 1637:  /* *fma_fmaddsub_v8sf */
    case 1485:  /* avx512f_ucmpv8di3 */
    case 1483:  /* avx512f_ucmpv16si3 */
    case 1479:  /* avx512f_cmpv8df3 */
    case 1477:  /* avx512f_cmpv8di3 */
    case 1473:  /* avx512f_cmpv16sf3 */
    case 1471:  /* avx512f_cmpv16si3 */
    case 1458:  /* avx_cmpv2df3 */
    case 1457:  /* avx_cmpv4df3 */
    case 1456:  /* avx_cmpv4sf3 */
    case 1455:  /* avx_cmpv8sf3 */
    case 474:  /* copysigntf3_const */
    case 473:  /* copysigndf3_const */
    case 472:  /* copysignsf3_const */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (pat, 1), 0, 1));
      ro[3] = *(ro_loc[3] = &XVECEXP (XEXP (pat, 1), 0, 2));
      break;

    case 1214:  /* *absnegv2df2 */
    case 1213:  /* *absnegv4df2 */
    case 1212:  /* *absnegv8df2 */
    case 1211:  /* *absnegv4sf2 */
    case 1210:  /* *absnegv8sf2 */
    case 1209:  /* *absnegv16sf2 */
    case 459:  /* *absnegtf2_sse */
    case 458:  /* *absnegxf2_i387 */
    case 457:  /* *absnegdf2_i387 */
    case 456:  /* *absnegsf2_i387 */
    case 455:  /* *absnegdf2_sse */
    case 454:  /* *absnegsf2_sse */
    case 453:  /* *absnegdf2_mixed */
    case 452:  /* *absnegsf2_mixed */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 0), 1));
      break;

    case 451:  /* *negvdi3 */
    case 450:  /* *negvsi3 */
    case 449:  /* *negvhi3 */
    case 448:  /* *negvqi3 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 447:  /* *negsi2_cmpz_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 486:  /* *one_cmpldi2_2 */
    case 485:  /* *one_cmplsi2_2 */
    case 484:  /* *one_cmplhi2_2 */
    case 483:  /* *one_cmplqi2_2 */
    case 446:  /* *negdi2_cmpz */
    case 445:  /* *negsi2_cmpz */
    case 444:  /* *neghi2_cmpz */
    case 443:  /* *negqi2_cmpz */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      break;

    case 442:  /* *negsi2_1_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      break;

    case 418:  /* *xorsi_2_zext_imm */
    case 417:  /* *iorsi_2_zext_imm */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 2512:  /* sse4_1_packusdw */
    case 2511:  /* avx2_packusdw */
    case 2340:  /* sse2_packuswb */
    case 2339:  /* avx2_packuswb */
    case 2338:  /* sse2_packssdw */
    case 2337:  /* avx2_packssdw */
    case 2336:  /* sse2_packsswb */
    case 2335:  /* avx2_packsswb */
    case 1109:  /* mmx_packuswb */
    case 1108:  /* mmx_packssdw */
    case 1107:  /* mmx_packsswb */
    case 414:  /* kunpckhi */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 1), 0));
      break;

    case 435:  /* *xorqi_cc_ext_1 */
    case 385:  /* *andqi_ext_0_cc */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 420:  /* *xorqi_2_slp */
    case 419:  /* *iorqi_2_slp */
    case 383:  /* *andqi_2_slp */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 0), 0);
      recog_data.dup_num[0] = 0;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[1] = 0;
      recog_data.dup_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[2] = 1;
      break;

    case 754:  /* *tls_local_dynamic_base_64_di */
    case 753:  /* *tls_local_dynamic_base_64_si */
    case 694:  /* *bmi_andn_di */
    case 693:  /* *bmi_andn_si */
    case 399:  /* *xorsi_1_zext_imm */
    case 398:  /* *iorsi_1_zext_imm */
    case 376:  /* kandnhi */
    case 375:  /* kandnqi */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      break;

    case 2947:  /* avx_vec_concatv8df */
    case 2946:  /* avx_vec_concatv16sf */
    case 2945:  /* avx_vec_concatv8di */
    case 2944:  /* avx_vec_concatv16si */
    case 2943:  /* avx_vec_concatv32hi */
    case 2942:  /* avx_vec_concatv64qi */
    case 2941:  /* avx_vec_concatv4df */
    case 2940:  /* avx_vec_concatv8sf */
    case 2939:  /* avx_vec_concatv4di */
    case 2938:  /* avx_vec_concatv8si */
    case 2937:  /* avx_vec_concatv16hi */
    case 2936:  /* avx_vec_concatv32qi */
    case 2935:  /* avx2_lshrvv2di */
    case 2934:  /* avx2_ashlvv2di */
    case 2933:  /* avx2_lshrvv4di */
    case 2932:  /* avx2_ashlvv4di */
    case 2930:  /* avx512f_lshrvv8di */
    case 2928:  /* avx512f_ashlvv8di */
    case 2927:  /* avx2_lshrvv4si */
    case 2926:  /* avx2_ashlvv4si */
    case 2925:  /* avx2_lshrvv8si */
    case 2924:  /* avx2_ashlvv8si */
    case 2922:  /* avx512f_lshrvv16si */
    case 2920:  /* avx512f_ashlvv16si */
    case 2918:  /* avx512f_ashrvv8di */
    case 2917:  /* avx2_ashrvv4si */
    case 2916:  /* avx2_ashrvv8si */
    case 2914:  /* avx512f_ashrvv16si */
    case 2704:  /* xop_rotrv2di3 */
    case 2703:  /* xop_rotrv4si3 */
    case 2702:  /* xop_rotrv8hi3 */
    case 2701:  /* xop_rotrv16qi3 */
    case 2700:  /* xop_rotlv2di3 */
    case 2699:  /* xop_rotlv4si3 */
    case 2698:  /* xop_rotlv8hi3 */
    case 2697:  /* xop_rotlv16qi3 */
    case 2413:  /* vec_concatv2di */
    case 2412:  /* *vec_concatv4si */
    case 2411:  /* *vec_concatv2si */
    case 2410:  /* *vec_concatv2si_sse4_1 */
    case 2326:  /* *xorv2di3 */
    case 2325:  /* *iorv2di3 */
    case 2324:  /* *andv2di3 */
    case 2323:  /* *xorv4di3 */
    case 2322:  /* *iorv4di3 */
    case 2321:  /* *andv4di3 */
    case 2320:  /* *xorv4si3 */
    case 2319:  /* *iorv4si3 */
    case 2318:  /* *andv4si3 */
    case 2317:  /* *xorv8si3 */
    case 2316:  /* *iorv8si3 */
    case 2315:  /* *andv8si3 */
    case 2314:  /* *xorv8hi3 */
    case 2313:  /* *iorv8hi3 */
    case 2312:  /* *andv8hi3 */
    case 2311:  /* *xorv16hi3 */
    case 2310:  /* *iorv16hi3 */
    case 2309:  /* *andv16hi3 */
    case 2308:  /* *xorv16qi3 */
    case 2307:  /* *iorv16qi3 */
    case 2306:  /* *andv16qi3 */
    case 2305:  /* *xorv32qi3 */
    case 2304:  /* *iorv32qi3 */
    case 2303:  /* *andv32qi3 */
    case 2301:  /* *xorv8di3 */
    case 2299:  /* *iorv8di3 */
    case 2297:  /* *andv8di3 */
    case 2295:  /* *xorv16si3 */
    case 2293:  /* *iorv16si3 */
    case 2291:  /* *andv16si3 */
    case 2278:  /* sse2_gtv4si3 */
    case 2277:  /* sse2_gtv8hi3 */
    case 2276:  /* sse2_gtv16qi3 */
    case 2271:  /* avx2_gtv4di3 */
    case 2270:  /* avx2_gtv8si3 */
    case 2269:  /* avx2_gtv16hi3 */
    case 2268:  /* avx2_gtv32qi3 */
    case 2267:  /* sse4_2_gtv2di3 */
    case 2266:  /* *sse2_eqv4si3 */
    case 2265:  /* *sse2_eqv8hi3 */
    case 2264:  /* *sse2_eqv16qi3 */
    case 2263:  /* *sse4_1_eqv2di3 */
    case 2258:  /* *avx2_eqv4di3 */
    case 2257:  /* *avx2_eqv8si3 */
    case 2256:  /* *avx2_eqv16hi3 */
    case 2255:  /* *avx2_eqv32qi3 */
    case 2254:  /* *uminv16qi3 */
    case 2253:  /* *umaxv16qi3 */
    case 2252:  /* *sse4_1_uminv4si3 */
    case 2251:  /* *sse4_1_umaxv4si3 */
    case 2250:  /* *sse4_1_uminv8hi3 */
    case 2249:  /* *sse4_1_umaxv8hi3 */
    case 2248:  /* *sminv8hi3 */
    case 2247:  /* *smaxv8hi3 */
    case 2246:  /* *sse4_1_sminv4si3 */
    case 2245:  /* *sse4_1_smaxv4si3 */
    case 2244:  /* *sse4_1_sminv16qi3 */
    case 2243:  /* *sse4_1_smaxv16qi3 */
    case 2239:  /* *avx2_uminv16si3 */
    case 2235:  /* *avx2_umaxv16si3 */
    case 2231:  /* *avx2_sminv16si3 */
    case 2227:  /* *avx2_smaxv16si3 */
    case 2223:  /* *avx2_uminv8di3 */
    case 2219:  /* *avx2_umaxv8di3 */
    case 2215:  /* *avx2_sminv8di3 */
    case 2211:  /* *avx2_smaxv8di3 */
    case 2207:  /* *avx2_uminv8si3 */
    case 2203:  /* *avx2_umaxv8si3 */
    case 2199:  /* *avx2_sminv8si3 */
    case 2195:  /* *avx2_smaxv8si3 */
    case 2191:  /* *avx2_uminv16hi3 */
    case 2187:  /* *avx2_umaxv16hi3 */
    case 2183:  /* *avx2_sminv16hi3 */
    case 2179:  /* *avx2_smaxv16hi3 */
    case 2178:  /* *avx2_uminv32qi3 */
    case 2177:  /* *avx2_umaxv32qi3 */
    case 2176:  /* *avx2_sminv32qi3 */
    case 2175:  /* *avx2_smaxv32qi3 */
    case 2173:  /* avx512f_rorv8di */
    case 2171:  /* avx512f_rolv8di */
    case 2169:  /* avx512f_rorv16si */
    case 2167:  /* avx512f_rolv16si */
    case 2165:  /* avx512f_rorvv8di */
    case 2163:  /* avx512f_rolvv8di */
    case 2161:  /* avx512f_rorvv16si */
    case 2159:  /* avx512f_rolvv16si */
    case 2158:  /* sse2_lshrv1ti3 */
    case 2157:  /* avx2_lshrv2ti3 */
    case 2156:  /* sse2_ashlv1ti3 */
    case 2155:  /* avx2_ashlv2ti3 */
    case 2153:  /* lshrv8di3 */
    case 2151:  /* ashlv8di3 */
    case 2149:  /* lshrv16si3 */
    case 2147:  /* ashlv16si3 */
    case 2146:  /* lshrv2di3 */
    case 2145:  /* ashlv2di3 */
    case 2144:  /* lshrv4di3 */
    case 2143:  /* ashlv4di3 */
    case 2142:  /* lshrv4si3 */
    case 2141:  /* ashlv4si3 */
    case 2140:  /* lshrv8si3 */
    case 2139:  /* ashlv8si3 */
    case 2138:  /* lshrv8hi3 */
    case 2137:  /* ashlv8hi3 */
    case 2136:  /* lshrv16hi3 */
    case 2135:  /* ashlv16hi3 */
    case 2133:  /* ashrv8di3 */
    case 2131:  /* ashrv16si3 */
    case 2130:  /* ashrv4si3 */
    case 2129:  /* ashrv8si3 */
    case 2128:  /* ashrv8hi3 */
    case 2127:  /* ashrv16hi3 */
    case 2125:  /* *sse4_1_mulv4si3 */
    case 2123:  /* *avx2_mulv8si3 */
    case 2121:  /* *avx512f_mulv16si3 */
    case 2106:  /* *mulv8hi3 */
    case 2105:  /* *mulv16hi3 */
    case 2104:  /* *sse2_ussubv8hi3 */
    case 2103:  /* *sse2_sssubv8hi3 */
    case 2102:  /* *sse2_usaddv8hi3 */
    case 2101:  /* *sse2_ssaddv8hi3 */
    case 2100:  /* *avx2_ussubv16hi3 */
    case 2099:  /* *avx2_sssubv16hi3 */
    case 2098:  /* *avx2_usaddv16hi3 */
    case 2097:  /* *avx2_ssaddv16hi3 */
    case 2096:  /* *sse2_ussubv16qi3 */
    case 2095:  /* *sse2_sssubv16qi3 */
    case 2094:  /* *sse2_usaddv16qi3 */
    case 2093:  /* *sse2_ssaddv16qi3 */
    case 2092:  /* *avx2_ussubv32qi3 */
    case 2091:  /* *avx2_sssubv32qi3 */
    case 2090:  /* *avx2_usaddv32qi3 */
    case 2089:  /* *avx2_ssaddv32qi3 */
    case 2087:  /* *subv2di3 */
    case 2085:  /* *addv2di3 */
    case 2083:  /* *subv4di3 */
    case 2081:  /* *addv4di3 */
    case 2079:  /* *subv8di3 */
    case 2077:  /* *addv8di3 */
    case 2075:  /* *subv4si3 */
    case 2073:  /* *addv4si3 */
    case 2071:  /* *subv8si3 */
    case 2069:  /* *addv8si3 */
    case 2067:  /* *subv16si3 */
    case 2065:  /* *addv16si3 */
    case 2063:  /* *subv8hi3 */
    case 2061:  /* *addv8hi3 */
    case 2059:  /* *subv16hi3 */
    case 2057:  /* *addv16hi3 */
    case 2055:  /* *subv16qi3 */
    case 2053:  /* *addv16qi3 */
    case 2052:  /* *subv32qi3 */
    case 2051:  /* *addv32qi3 */
    case 2014:  /* *vec_concatv2df */
    case 1867:  /* *vec_concatv4sf */
    case 1866:  /* *vec_concatv2sf_sse */
    case 1865:  /* *vec_concatv2sf_sse4_1 */
    case 1548:  /* avx512f_xorv8df */
    case 1547:  /* avx512f_andv8df */
    case 1546:  /* avx512f_xorv16sf */
    case 1545:  /* avx512f_andv16sf */
    case 1544:  /* *xortf3 */
    case 1543:  /* *iortf3 */
    case 1542:  /* *andtf3 */
    case 1541:  /* *xordf3 */
    case 1540:  /* *iordf3 */
    case 1539:  /* *anddf3 */
    case 1538:  /* *xorsf3 */
    case 1537:  /* *iorsf3 */
    case 1536:  /* *andsf3 */
    case 1532:  /* *xorv2df3 */
    case 1531:  /* *iorv2df3 */
    case 1530:  /* *andv2df3 */
    case 1529:  /* *xorv4df3 */
    case 1528:  /* *iorv4df3 */
    case 1527:  /* *andv4df3 */
    case 1526:  /* *xorv8df3 */
    case 1525:  /* *iorv8df3 */
    case 1524:  /* *andv8df3 */
    case 1523:  /* *xorv4sf3 */
    case 1522:  /* *iorv4sf3 */
    case 1521:  /* *andv4sf3 */
    case 1520:  /* *xorv8sf3 */
    case 1519:  /* *iorv8sf3 */
    case 1518:  /* *andv8sf3 */
    case 1517:  /* *xorv16sf3 */
    case 1516:  /* *iorv16sf3 */
    case 1515:  /* *andv16sf3 */
    case 1420:  /* *sminv2df3 */
    case 1419:  /* *smaxv2df3 */
    case 1418:  /* *sminv4df3 */
    case 1417:  /* *smaxv4df3 */
    case 1413:  /* *sminv8df3 */
    case 1409:  /* *smaxv8df3 */
    case 1408:  /* *sminv4sf3 */
    case 1407:  /* *smaxv4sf3 */
    case 1406:  /* *sminv8sf3 */
    case 1405:  /* *smaxv8sf3 */
    case 1401:  /* *sminv16sf3 */
    case 1397:  /* *smaxv16sf3 */
    case 1393:  /* *sminv2df3_finite */
    case 1389:  /* *smaxv2df3_finite */
    case 1385:  /* *sminv4df3_finite */
    case 1381:  /* *smaxv4df3_finite */
    case 1377:  /* *sminv8df3_finite */
    case 1373:  /* *smaxv8df3_finite */
    case 1369:  /* *sminv4sf3_finite */
    case 1365:  /* *smaxv4sf3_finite */
    case 1361:  /* *sminv8sf3_finite */
    case 1357:  /* *smaxv8sf3_finite */
    case 1353:  /* *sminv16sf3_finite */
    case 1349:  /* *smaxv16sf3_finite */
    case 1314:  /* sse2_divv2df3 */
    case 1313:  /* avx_divv4df3 */
    case 1309:  /* avx512f_divv8df3 */
    case 1308:  /* sse_divv4sf3 */
    case 1307:  /* avx_divv8sf3 */
    case 1303:  /* avx512f_divv16sf3 */
    case 1291:  /* *mulv2df3 */
    case 1287:  /* *mulv4df3 */
    case 1283:  /* *mulv8df3 */
    case 1279:  /* *mulv4sf3 */
    case 1275:  /* *mulv8sf3 */
    case 1271:  /* *mulv16sf3 */
    case 1259:  /* *subv2df3 */
    case 1255:  /* *addv2df3 */
    case 1251:  /* *subv4df3 */
    case 1247:  /* *addv4df3 */
    case 1243:  /* *subv8df3 */
    case 1239:  /* *addv8df3 */
    case 1235:  /* *subv4sf3 */
    case 1231:  /* *addv4sf3 */
    case 1227:  /* *subv8sf3 */
    case 1223:  /* *addv8sf3 */
    case 1219:  /* *subv16sf3 */
    case 1215:  /* *addv16sf3 */
    case 1122:  /* *mmx_concatv2si */
    case 1106:  /* *mmx_xorv2si3 */
    case 1105:  /* *mmx_iorv2si3 */
    case 1104:  /* *mmx_andv2si3 */
    case 1103:  /* *mmx_xorv4hi3 */
    case 1102:  /* *mmx_iorv4hi3 */
    case 1101:  /* *mmx_andv4hi3 */
    case 1100:  /* *mmx_xorv8qi3 */
    case 1099:  /* *mmx_iorv8qi3 */
    case 1098:  /* *mmx_andv8qi3 */
    case 1094:  /* mmx_gtv2si3 */
    case 1093:  /* mmx_gtv4hi3 */
    case 1092:  /* mmx_gtv8qi3 */
    case 1091:  /* *mmx_eqv2si3 */
    case 1090:  /* *mmx_eqv4hi3 */
    case 1089:  /* *mmx_eqv8qi3 */
    case 1088:  /* mmx_lshrv1di3 */
    case 1087:  /* mmx_ashlv1di3 */
    case 1086:  /* mmx_lshrv2si3 */
    case 1085:  /* mmx_ashlv2si3 */
    case 1084:  /* mmx_lshrv4hi3 */
    case 1083:  /* mmx_ashlv4hi3 */
    case 1082:  /* mmx_ashrv2si3 */
    case 1081:  /* mmx_ashrv4hi3 */
    case 1080:  /* *mmx_uminv8qi3 */
    case 1079:  /* *mmx_umaxv8qi3 */
    case 1078:  /* *mmx_sminv4hi3 */
    case 1077:  /* *mmx_smaxv4hi3 */
    case 1071:  /* *mmx_mulv4hi3 */
    case 1070:  /* *mmx_ussubv4hi3 */
    case 1069:  /* *mmx_sssubv4hi3 */
    case 1068:  /* *mmx_usaddv4hi3 */
    case 1067:  /* *mmx_ssaddv4hi3 */
    case 1066:  /* *mmx_ussubv8qi3 */
    case 1065:  /* *mmx_sssubv8qi3 */
    case 1064:  /* *mmx_usaddv8qi3 */
    case 1063:  /* *mmx_ssaddv8qi3 */
    case 1062:  /* *mmx_subv1di3 */
    case 1061:  /* *mmx_addv1di3 */
    case 1060:  /* *mmx_subv2si3 */
    case 1059:  /* *mmx_addv2si3 */
    case 1058:  /* *mmx_subv4hi3 */
    case 1057:  /* *mmx_addv4hi3 */
    case 1056:  /* *mmx_subv8qi3 */
    case 1055:  /* *mmx_addv8qi3 */
    case 1052:  /* *mmx_concatv2sf */
    case 1045:  /* mmx_gev2sf3 */
    case 1044:  /* mmx_gtv2sf3 */
    case 1043:  /* *mmx_eqv2sf3 */
    case 1034:  /* *mmx_sminv2sf3 */
    case 1033:  /* *mmx_smaxv2sf3 */
    case 1032:  /* *mmx_sminv2sf3_finite */
    case 1031:  /* *mmx_smaxv2sf3_finite */
    case 1030:  /* *mmx_mulv2sf3 */
    case 1029:  /* *mmx_subv2sf3 */
    case 1028:  /* *mmx_addv2sf3 */
    case 940:  /* smindf3 */
    case 939:  /* smaxdf3 */
    case 938:  /* sminsf3 */
    case 937:  /* smaxsf3 */
    case 570:  /* *bmi2_rorxdi3_1 */
    case 569:  /* *bmi2_rorxsi3_1 */
    case 528:  /* *bmi2_ashrdi3_1 */
    case 527:  /* *bmi2_lshrdi3_1 */
    case 526:  /* *bmi2_ashrsi3_1 */
    case 525:  /* *bmi2_lshrsi3_1 */
    case 495:  /* *bmi2_ashldi3_1 */
    case 494:  /* *bmi2_ashlsi3_1 */
    case 368:  /* *kxorhi */
    case 367:  /* *korhi */
    case 366:  /* *kandhi */
    case 365:  /* *kxorqi */
    case 364:  /* *korqi */
    case 363:  /* *kandqi */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 362:  /* *testqi_ext_3 */
    case 361:  /* *testqi_ext_3 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 2));
      break;

    case 360:  /* *testqi_ext_2 */
    case 359:  /* *testqi_ext_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 1), 0));
      break;

    case 358:  /* *testqi_ext_0 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      break;

    case 352:  /* udivmodhiqi3 */
    case 343:  /* divmodhiqi3 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 0), 1), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 0), 1), 0);
      recog_data.dup_num[1] = 2;
      break;

    case 351:  /* *udivmoddi4_noext */
    case 350:  /* *udivmodsi4_noext */
    case 349:  /* *udivmodhi4_noext */
    case 342:  /* *divmoddi4_noext */
    case 341:  /* *divmodsi4_noext */
    case 340:  /* *divmodhi4_noext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 2), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 2;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 3;
      break;

    case 348:  /* *udivmoddi4 */
    case 347:  /* *udivmodsi4 */
    case 346:  /* *udivmodhi4 */
    case 345:  /* udivmoddi4_1 */
    case 344:  /* udivmodsi4_1 */
    case 339:  /* *divmoddi4 */
    case 338:  /* *divmodsi4 */
    case 337:  /* *divmodhi4 */
    case 336:  /* divmoddi4_1 */
    case 335:  /* divmodsi4_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 2;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 3;
      break;

    case 334:  /* *umulsi3_highpart_zext */
    case 333:  /* *smulsi3_highpart_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 332:  /* *umulsi3_highpart_1 */
    case 331:  /* *smulsi3_highpart_1 */
    case 330:  /* *umuldi3_highpart_1 */
    case 329:  /* *smuldi3_highpart_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 328:  /* *umulqihi3_1 */
    case 327:  /* *mulqihi3_1 */
    case 326:  /* *mulditi3_1 */
    case 325:  /* *mulsidi3_1 */
    case 324:  /* *umulditi3_1 */
    case 323:  /* *umulsidi3_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      break;

    case 322:  /* *bmi2_umulsidi3_1 */
    case 321:  /* *bmi2_umulditi3_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0), 0), 0);
      recog_data.dup_num[0] = 2;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0), 1), 0);
      recog_data.dup_num[1] = 3;
      break;

    case 305:  /* *addsi3_zext_cc_overflow */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 1);
      recog_data.dup_num[1] = 2;
      recog_data.dup_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[2] = 1;
      break;

    case 304:  /* *adddi3_cc_overflow */
    case 303:  /* *addsi3_cc_overflow */
    case 302:  /* *addhi3_cc_overflow */
    case 301:  /* *addqi3_cc_overflow */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 2;
      recog_data.dup_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[2] = 1;
      break;

    case 300:  /* *adddi3_cconly_overflow */
    case 299:  /* *addsi3_cconly_overflow */
    case 298:  /* *addhi3_cconly_overflow */
    case 297:  /* *addqi3_cconly_overflow */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1);
      recog_data.dup_num[0] = 1;
      break;

    case 296:  /* adcxdi3 */
    case 295:  /* adcxsi3 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1), 0), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1), 0);
      recog_data.dup_num[1] = 4;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1), 0), 0);
      recog_data.dup_num[2] = 3;
      recog_data.dup_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1), 1);
      recog_data.dup_num[3] = 2;
      break;

    case 294:  /* *subsi3_carry_zext */
    case 293:  /* *addsi3_carry_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1), 0));
      break;

    case 292:  /* *subdi3_carry */
    case 291:  /* *adddi3_carry */
    case 290:  /* *subsi3_carry */
    case 289:  /* *addsi3_carry */
    case 288:  /* *subhi3_carry */
    case 287:  /* *addhi3_carry */
    case 286:  /* *subqi3_carry */
    case 285:  /* *addqi3_carry */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      break;

    case 284:  /* *subsi_3_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 283:  /* *subdi_3 */
    case 282:  /* *subsi_3 */
    case 281:  /* *subhi_3 */
    case 280:  /* *subqi_3 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 256:  /* *lea_general_3 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[4] = *(ro_loc[4] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 2653:  /* xop_pmacssdd */
    case 2652:  /* xop_pmacsdd */
    case 2651:  /* xop_pmacssww */
    case 2650:  /* xop_pmacsww */
    case 258:  /* *lea_general_4 */
    case 257:  /* *lea_general_4 */
    case 255:  /* *lea_general_2 */
    case 254:  /* *lea_general_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 320:  /* *mulvdi4_1 */
    case 319:  /* *mulvdi4_1 */
    case 318:  /* *mulvsi4_1 */
    case 317:  /* *mulvsi4_1 */
    case 316:  /* *mulvhi4_1 */
    case 315:  /* *mulvhi4_1 */
    case 314:  /* *mulvqi4_1 */
    case 313:  /* *mulvqi4_1 */
    case 279:  /* *subvdi4_1 */
    case 278:  /* *subvsi4_1 */
    case 277:  /* *subvhi4_1 */
    case 276:  /* *subvqi4_1 */
    case 253:  /* *addvdi4_1 */
    case 252:  /* *addvsi4_1 */
    case 251:  /* *addvhi4_1 */
    case 250:  /* *addvqi4_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 1));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 2;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 0);
      recog_data.dup_num[2] = 1;
      break;

    case 312:  /* *mulvdi4 */
    case 311:  /* *mulvsi4 */
    case 275:  /* *subvdi4 */
    case 274:  /* *subvsi4 */
    case 273:  /* *subvhi4 */
    case 272:  /* *subvqi4 */
    case 249:  /* *addvdi4 */
    case 248:  /* *addvsi4 */
    case 247:  /* *addvhi4 */
    case 246:  /* *addvqi4 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 2;
      recog_data.dup_loc[2] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 0);
      recog_data.dup_num[2] = 1;
      recog_data.dup_loc[3] = &XEXP (XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0), 1);
      recog_data.dup_num[3] = 2;
      break;

    case 434:  /* *xorqi_ext_2 */
    case 433:  /* *iorqi_ext_2 */
    case 432:  /* *xorqi_ext_1 */
    case 431:  /* *iorqi_ext_1 */
    case 387:  /* *andqi_ext_2 */
    case 386:  /* *andqi_ext_1 */
    case 245:  /* *addqi_ext_2 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      break;

    case 430:  /* *xorqi_ext_0 */
    case 429:  /* *iorqi_ext_0 */
    case 384:  /* andqi_ext_0 */
    case 244:  /* addqi_ext_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      break;

    case 560:  /* *ashrdi3_cconly */
    case 559:  /* *lshrdi3_cconly */
    case 558:  /* *ashrsi3_cconly */
    case 557:  /* *lshrsi3_cconly */
    case 556:  /* *ashrhi3_cconly */
    case 555:  /* *lshrhi3_cconly */
    case 554:  /* *ashrqi3_cconly */
    case 553:  /* *lshrqi3_cconly */
    case 511:  /* *ashldi3_cconly */
    case 510:  /* *ashlsi3_cconly */
    case 509:  /* *ashlhi3_cconly */
    case 508:  /* *ashlqi3_cconly */
    case 428:  /* *xordi_3 */
    case 427:  /* *iordi_3 */
    case 426:  /* *xorsi_3 */
    case 425:  /* *iorsi_3 */
    case 424:  /* *xorhi_3 */
    case 423:  /* *iorhi_3 */
    case 422:  /* *xorqi_3 */
    case 421:  /* *iorqi_3 */
    case 243:  /* *adddi_5 */
    case 242:  /* *addsi_5 */
    case 241:  /* *addhi_5 */
    case 240:  /* *addqi_5 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      break;

    case 235:  /* *addsi_3_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 234:  /* *adddi_3 */
    case 233:  /* *addsi_3 */
    case 232:  /* *addhi_3 */
    case 231:  /* *addqi_3 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      break;

    case 552:  /* *ashrsi3_cmp_zext */
    case 551:  /* *lshrsi3_cmp_zext */
    case 507:  /* *ashlsi3_cmp_zext */
    case 416:  /* *xorsi_2_zext */
    case 415:  /* *iorsi_2_zext */
    case 382:  /* *andsi_2_zext */
    case 271:  /* *subsi_2_zext */
    case 230:  /* *addsi_2_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 550:  /* *ashrdi3_cmp */
    case 549:  /* *lshrdi3_cmp */
    case 548:  /* *ashrsi3_cmp */
    case 547:  /* *lshrsi3_cmp */
    case 546:  /* *ashrhi3_cmp */
    case 545:  /* *lshrhi3_cmp */
    case 544:  /* *ashrqi3_cmp */
    case 543:  /* *lshrqi3_cmp */
    case 506:  /* *ashldi3_cmp */
    case 505:  /* *ashlsi3_cmp */
    case 504:  /* *ashlhi3_cmp */
    case 503:  /* *ashlqi3_cmp */
    case 409:  /* *xordi_2 */
    case 408:  /* *iordi_2 */
    case 407:  /* *xorsi_2 */
    case 406:  /* *iorsi_2 */
    case 405:  /* *xorhi_2 */
    case 404:  /* *iorhi_2 */
    case 403:  /* *xorqi_2 */
    case 402:  /* *iorqi_2 */
    case 381:  /* *andsi_2 */
    case 380:  /* *andhi_2 */
    case 379:  /* *andqi_2 */
    case 378:  /* *andqi_2_maybe_si */
    case 377:  /* *anddi_2 */
    case 270:  /* *subdi_2 */
    case 269:  /* *subsi_2 */
    case 268:  /* *subhi_2 */
    case 267:  /* *subqi_2 */
    case 229:  /* *adddi_2 */
    case 228:  /* *addsi_2 */
    case 227:  /* *addhi_2 */
    case 226:  /* *addqi_2 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 583:  /* *rotrqi3_1_slp */
    case 582:  /* *rotlqi3_1_slp */
    case 542:  /* *ashrqi3_1_slp */
    case 541:  /* *lshrqi3_1_slp */
    case 502:  /* *ashlqi3_1_slp */
    case 401:  /* *xorqi_1_slp */
    case 400:  /* *iorqi_1_slp */
    case 374:  /* *andqi_1_slp */
    case 266:  /* *subqi_1_slp */
    case 225:  /* *addqi_1_slp */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0);
      recog_data.dup_num[0] = 0;
      break;

    case 577:  /* *rotrsi3_1_zext */
    case 576:  /* *rotlsi3_1_zext */
    case 536:  /* *ashrsi3_1_zext */
    case 535:  /* *lshrsi3_1_zext */
    case 524:  /* *ashrsi3_cvt_zext */
    case 499:  /* *ashlsi3_1_zext */
    case 411:  /* kxnorhi */
    case 410:  /* kxnorqi */
    case 397:  /* *xorsi_1_zext */
    case 396:  /* *iorsi_1_zext */
    case 371:  /* *andsi_1_zext */
    case 308:  /* *mulsi3_1_zext */
    case 265:  /* *subsi_1_zext */
    case 222:  /* addsi_1_zext */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0), 1));
      break;

    case 219:  /* addqi3_cc */
    case 218:  /* *adddi3_cc */
    case 217:  /* *addsi3_cc */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 0));
      ro[2] = *(ro_loc[2] = &XVECEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0, 1));
      recog_data.dup_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 1), 1), 1);
      recog_data.dup_num[1] = 2;
      break;

    case 948:  /* pro_epilogue_adjust_stack_di_sub */
    case 947:  /* pro_epilogue_adjust_stack_si_sub */
    case 946:  /* pro_epilogue_adjust_stack_di_add */
    case 945:  /* pro_epilogue_adjust_stack_si_add */
    case 581:  /* *rotrhi3_1 */
    case 580:  /* *rotlhi3_1 */
    case 579:  /* *rotrqi3_1 */
    case 578:  /* *rotlqi3_1 */
    case 574:  /* *rotrdi3_1 */
    case 573:  /* *rotldi3_1 */
    case 572:  /* *rotrsi3_1 */
    case 571:  /* *rotlsi3_1 */
    case 540:  /* *ashrhi3_1 */
    case 539:  /* *lshrhi3_1 */
    case 538:  /* *ashrqi3_1 */
    case 537:  /* *lshrqi3_1 */
    case 532:  /* *ashrdi3_1 */
    case 531:  /* *lshrdi3_1 */
    case 530:  /* *ashrsi3_1 */
    case 529:  /* *lshrsi3_1 */
    case 523:  /* ashrsi3_cvt */
    case 522:  /* ashrdi3_cvt */
    case 519:  /* *ashrti3_doubleword */
    case 518:  /* *lshrti3_doubleword */
    case 517:  /* *ashrdi3_doubleword */
    case 516:  /* *lshrdi3_doubleword */
    case 501:  /* *ashlqi3_1 */
    case 500:  /* *ashlhi3_1 */
    case 497:  /* *ashldi3_1 */
    case 496:  /* *ashlsi3_1 */
    case 489:  /* *ashlti3_doubleword */
    case 488:  /* *ashldi3_doubleword */
    case 395:  /* *xorqi_1 */
    case 394:  /* *iorqi_1 */
    case 393:  /* *xorhi_1 */
    case 392:  /* *iorhi_1 */
    case 391:  /* *xordi_1 */
    case 390:  /* *iordi_1 */
    case 389:  /* *xorsi_1 */
    case 388:  /* *iorsi_1 */
    case 373:  /* *andqi_1 */
    case 372:  /* *andhi_1 */
    case 370:  /* *andsi_1 */
    case 369:  /* *anddi_1 */
    case 310:  /* *mulqi3_1 */
    case 309:  /* *mulhi3_1 */
    case 307:  /* *muldi3_1 */
    case 306:  /* *mulsi3_1 */
    case 264:  /* *subdi_1 */
    case 263:  /* *subsi_1 */
    case 262:  /* *subhi_1 */
    case 261:  /* *subqi_1 */
    case 260:  /* *subti3_doubleword */
    case 259:  /* *subdi3_doubleword */
    case 224:  /* *addqi_1 */
    case 223:  /* *addhi_1 */
    case 221:  /* *adddi_1 */
    case 220:  /* *addsi_1 */
    case 216:  /* *addti3_doubleword */
    case 215:  /* *adddi3_doubleword */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      break;

    case 212:  /* *floatunssixf2_i387_with_xmm */
    case 211:  /* *floatunssidf2_i387_with_xmm */
    case 210:  /* *floatunssisf2_i387_with_xmm */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 209:  /* floatdixf2_i387_with_xmm */
    case 208:  /* floatdidf2_i387_with_xmm */
    case 207:  /* floatdisf2_i387_with_xmm */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 3), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 2), 0));
      break;

    case 187:  /* fix_truncdi_i387_with_temp */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 3), 0));
      ro[5] = *(ro_loc[5] = &XEXP (XVECEXP (pat, 0, 4), 0));
      break;

    case 191:  /* fix_truncsi_i387_with_temp */
    case 190:  /* fix_trunchi_i387_with_temp */
    case 186:  /* fix_truncdi_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 3), 0));
      break;

    case 189:  /* fix_truncsi_i387 */
    case 188:  /* fix_trunchi_i387 */
    case 182:  /* fix_truncdi_i387_fisttp_with_temp */
    case 181:  /* fix_truncsi_i387_fisttp_with_temp */
    case 180:  /* fix_trunchi_i387_fisttp_with_temp */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (pat, 0, 2), 0));
      break;

    case 169:  /* *fixuns_truncdf_1 */
    case 168:  /* *fixuns_truncsf_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 2), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 3), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[4] = *(ro_loc[4] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 1167:  /* movdi_to_sse */
    case 674:  /* ffssi2_no_cmove */
    case 179:  /* fix_truncdi_i387_fisttp */
    case 178:  /* fix_truncsi_i387_fisttp */
    case 177:  /* fix_trunchi_i387_fisttp */
    case 163:  /* *truncxfdf2_mixed */
    case 162:  /* *truncxfsf2_mixed */
    case 160:  /* *truncdfsf_i387 */
    case 159:  /* *truncdfsf_mixed */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 1), 0));
      break;

    case 143:  /* extendsidi2_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (pat, 0, 2), 0));
      break;

    case 927:  /* *x86_movdicc_0_m1_neg */
    case 926:  /* *x86_movsicc_0_m1_neg */
    case 925:  /* *x86_movdicc_0_m1_se */
    case 924:  /* *x86_movsicc_0_m1_se */
    case 923:  /* *x86_movdicc_0_m1 */
    case 922:  /* *x86_movsicc_0_m1 */
    case 738:  /* *popcountdi2 */
    case 737:  /* *popcountsi2 */
    case 736:  /* *popcounthi2 */
    case 733:  /* *popcountdi2_falsedep_1 */
    case 732:  /* *popcountsi2_falsedep_1 */
    case 692:  /* *clzdi2_lzcnt */
    case 691:  /* *clzsi2_lzcnt */
    case 690:  /* *clzhi2_lzcnt */
    case 687:  /* *clzdi2_lzcnt_falsedep_1 */
    case 686:  /* *clzsi2_lzcnt_falsedep_1 */
    case 685:  /* *ctzdi2 */
    case 684:  /* *ctzsi2 */
    case 683:  /* *ctzhi2 */
    case 680:  /* *ctzdi2_falsedep_1 */
    case 679:  /* *ctzsi2_falsedep_1 */
    case 441:  /* *negdi2_1 */
    case 440:  /* *negsi2_1 */
    case 439:  /* *neghi2_1 */
    case 438:  /* *negqi2_1 */
    case 437:  /* *negti2_doubleword */
    case 436:  /* *negdi2_doubleword */
    case 185:  /* *fix_truncdi_i387_1 */
    case 184:  /* *fix_truncsi_i387_1 */
    case 183:  /* *fix_trunchi_i387_1 */
    case 140:  /* zero_extendqihi2_and */
    case 137:  /* zero_extendhisi2_and */
    case 136:  /* zero_extendqisi2_and */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      break;

    case 120:  /* *movqi_insv_2 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (pat, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      break;

    case 2831:  /* avx512cd_maskw_vec_dupv16si */
    case 2830:  /* avx512cd_maskb_vec_dupv8di */
    case 2798:  /* *avx512f_vec_dupv8df */
    case 2796:  /* *avx512f_vec_dupv8di */
    case 2794:  /* *avx512f_vec_dupv16sf */
    case 2792:  /* *avx512f_vec_dupv16si */
    case 2787:  /* avx2_vec_dupv4df */
    case 2769:  /* avx2_pbroadcastv4di_1 */
    case 2768:  /* avx2_pbroadcastv8si_1 */
    case 2767:  /* avx2_pbroadcastv16hi_1 */
    case 2766:  /* avx2_pbroadcastv32qi_1 */
    case 2765:  /* avx2_pbroadcastv2di */
    case 2764:  /* avx2_pbroadcastv4di */
    case 2763:  /* avx2_pbroadcastv4si */
    case 2762:  /* avx2_pbroadcastv8si */
    case 2761:  /* avx2_pbroadcastv8hi */
    case 2760:  /* avx2_pbroadcastv16hi */
    case 2759:  /* avx2_pbroadcastv16qi */
    case 2758:  /* avx2_pbroadcastv32qi */
    case 2757:  /* avx2_pbroadcastv8di */
    case 2756:  /* avx2_pbroadcastv16si */
    case 2563:  /* sse4_1_zero_extendv2siv2di2 */
    case 2562:  /* sse4_1_sign_extendv2siv2di2 */
    case 2555:  /* sse4_1_zero_extendv2hiv2di2 */
    case 2554:  /* sse4_1_sign_extendv2hiv2di2 */
    case 2553:  /* avx2_zero_extendv4hiv4di2 */
    case 2552:  /* avx2_sign_extendv4hiv4di2 */
    case 2547:  /* sse4_1_zero_extendv2qiv2di2 */
    case 2546:  /* sse4_1_sign_extendv2qiv2di2 */
    case 2545:  /* avx2_zero_extendv4qiv4di2 */
    case 2544:  /* avx2_sign_extendv4qiv4di2 */
    case 2542:  /* avx512f_zero_extendv8qiv8di2 */
    case 2540:  /* avx512f_sign_extendv8qiv8di2 */
    case 2539:  /* sse4_1_zero_extendv4hiv4si2 */
    case 2538:  /* sse4_1_sign_extendv4hiv4si2 */
    case 2531:  /* sse4_1_zero_extendv4qiv4si2 */
    case 2530:  /* sse4_1_sign_extendv4qiv4si2 */
    case 2529:  /* avx2_zero_extendv8qiv8si2 */
    case 2528:  /* avx2_sign_extendv8qiv8si2 */
    case 2523:  /* sse4_1_zero_extendv8qiv8hi2 */
    case 2522:  /* sse4_1_sign_extendv8qiv8hi2 */
    case 2401:  /* *vec_extractv4si_0_zext */
    case 2041:  /* *avx512f_us_truncatev8div16qi2 */
    case 2040:  /* *avx512f_truncatev8div16qi2 */
    case 2039:  /* *avx512f_ss_truncatev8div16qi2 */
    case 1863:  /* avx2_vec_dupv8sf_1 */
    case 1862:  /* avx2_vec_dupv4sf */
    case 1861:  /* avx2_vec_dupv8sf */
    case 1834:  /* sse2_cvtps2pd */
    case 1833:  /* vec_unpacks_lo_v16sf */
    case 1832:  /* *avx_cvtps2pd256_2 */
    case 1794:  /* sse2_cvtdq2pd */
    case 1793:  /* avx_cvtdq2pd256_2 */
    case 1792:  /* avx512f_cvtdq2pd512_2 */
    case 1785:  /* sse2_cvttsd2siq */
    case 1783:  /* sse2_cvttsd2si */
    case 1775:  /* avx512f_vcvttsd2usiq */
    case 1773:  /* avx512f_vcvttsd2usi */
    case 1767:  /* avx512f_vcvttss2usiq */
    case 1765:  /* avx512f_vcvttss2usi */
    case 1716:  /* sse_cvttss2siq */
    case 1714:  /* sse_cvttss2si */
    case 1703:  /* sse_cvttps2pi */
    case 1166:  /* sse2_movq128 */
    case 1120:  /* *vec_dupv4hi */
    case 811:  /* sqrt_extenddfxf2_i387 */
    case 810:  /* sqrt_extendsfxf2_i387 */
    case 482:  /* *one_cmplsi2_1_zext */
    case 471:  /* *negextenddfxf2 */
    case 470:  /* *absextenddfxf2 */
    case 469:  /* *negextendsfxf2 */
    case 468:  /* *absextendsfxf2 */
    case 467:  /* *negextendsfdf2 */
    case 466:  /* *absextendsfdf2 */
    case 149:  /* *extendqisi2_zext */
    case 147:  /* *extendhisi2_zext */
    case 117:  /* *movqi_extzv_2 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      break;

    case 111:  /* *movstricthi_xor */
    case 110:  /* *movstrictqi_xor */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 0), 1));
      break;

    case 132:  /* *swapdf */
    case 131:  /* *swapsf */
    case 130:  /* swapxf */
    case 107:  /* *swaphi_2 */
    case 106:  /* *swapqi_2 */
    case 105:  /* *swaphi_1 */
    case 104:  /* *swapqi_1 */
    case 103:  /* *swapdi */
    case 102:  /* *swapsi */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 0), 1));
      recog_data.dup_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0);
      recog_data.dup_num[0] = 1;
      recog_data.dup_loc[1] = &XEXP (XVECEXP (pat, 0, 1), 1);
      recog_data.dup_num[1] = 0;
      break;

    case 3106:  /* clzv8di2 */
    case 3104:  /* clzv16si2 */
    case 2818:  /* *avx512f_vec_dup_memv8df */
    case 2816:  /* *avx512f_vec_dup_memv8di */
    case 2814:  /* *avx512f_vec_dup_memv16sf */
    case 2812:  /* *avx512f_vec_dup_memv16si */
    case 2810:  /* *avx512f_vec_dup_gprv8di */
    case 2808:  /* *avx512f_vec_dup_gprv16si */
    case 2806:  /* *avx512f_broadcastv8di */
    case 2804:  /* *avx512f_broadcastv8df */
    case 2802:  /* *avx512f_broadcastv16si */
    case 2800:  /* *avx512f_broadcastv16sf */
    case 2791:  /* vec_dupv4df */
    case 2790:  /* vec_dupv4di */
    case 2789:  /* vec_dupv8sf */
    case 2788:  /* vec_dupv8si */
    case 2561:  /* avx2_zero_extendv4siv4di2 */
    case 2560:  /* avx2_sign_extendv4siv4di2 */
    case 2558:  /* avx512f_zero_extendv8siv8di2 */
    case 2556:  /* avx512f_sign_extendv8siv8di2 */
    case 2550:  /* avx512f_zero_extendv8hiv8di2 */
    case 2548:  /* avx512f_sign_extendv8hiv8di2 */
    case 2537:  /* avx2_zero_extendv8hiv8si2 */
    case 2536:  /* avx2_sign_extendv8hiv8si2 */
    case 2534:  /* avx512f_zero_extendv16hiv16si2 */
    case 2532:  /* avx512f_sign_extendv16hiv16si2 */
    case 2526:  /* *avx512f_zero_extendv16qiv16si2 */
    case 2524:  /* *avx512f_sign_extendv16qiv16si2 */
    case 2521:  /* avx2_zero_extendv16qiv16hi2 */
    case 2520:  /* avx2_sign_extendv16qiv16hi2 */
    case 2485:  /* absv2si2 */
    case 2484:  /* absv4hi2 */
    case 2483:  /* absv8qi2 */
    case 2481:  /* *absv8di2 */
    case 2480:  /* *absv4si2 */
    case 2479:  /* *absv8si2 */
    case 2477:  /* *absv16si2 */
    case 2476:  /* *absv8hi2 */
    case 2475:  /* *absv16hi2 */
    case 2474:  /* *absv16qi2 */
    case 2473:  /* *absv32qi2 */
    case 2409:  /* *vec_dupv2di */
    case 2408:  /* *vec_dupv4si */
    case 2407:  /* *vec_extractv2di_1 */
    case 2402:  /* *vec_extractv2di_0_sse */
    case 2400:  /* *vec_extractv2di_0 */
    case 2399:  /* *vec_extractv4si_0 */
    case 2026:  /* *avx512f_us_truncatev8div8hi2 */
    case 2025:  /* *avx512f_truncatev8div8hi2 */
    case 2024:  /* *avx512f_ss_truncatev8div8hi2 */
    case 2023:  /* *avx512f_us_truncatev8div8si2 */
    case 2022:  /* *avx512f_truncatev8div8si2 */
    case 2021:  /* *avx512f_ss_truncatev8div8si2 */
    case 2020:  /* *avx512f_us_truncatev16siv16hi2 */
    case 2019:  /* *avx512f_truncatev16siv16hi2 */
    case 2018:  /* *avx512f_ss_truncatev16siv16hi2 */
    case 2017:  /* *avx512f_us_truncatev16siv16qi2 */
    case 2016:  /* *avx512f_truncatev16siv16qi2 */
    case 2015:  /* *avx512f_ss_truncatev16siv16qi2 */
    case 2013:  /* vec_dupv2df */
    case 2009:  /* *vec_extractv2df_0_sse */
    case 2008:  /* sse2_storelpd */
    case 2007:  /* *vec_extractv2df_1_sse */
    case 2006:  /* sse2_storehpd */
    case 1912:  /* vec_extract_hi_v32qi */
    case 1911:  /* vec_extract_lo_v32qi */
    case 1910:  /* vec_extract_hi_v64qi */
    case 1909:  /* vec_extract_lo_v64qi */
    case 1908:  /* vec_extract_hi_v16hi */
    case 1907:  /* vec_extract_lo_v16hi */
    case 1906:  /* vec_extract_hi_v32hi */
    case 1905:  /* vec_extract_lo_v32hi */
    case 1904:  /* vec_extract_hi_v8sf */
    case 1903:  /* vec_extract_hi_v8si */
    case 1902:  /* vec_extract_lo_v8sf */
    case 1901:  /* vec_extract_lo_v8si */
    case 1900:  /* vec_extract_hi_v4df */
    case 1899:  /* vec_extract_hi_v4di */
    case 1898:  /* vec_extract_lo_v4df */
    case 1897:  /* vec_extract_lo_v4di */
    case 1896:  /* vec_extract_hi_v16si */
    case 1895:  /* vec_extract_hi_v16sf */
    case 1894:  /* vec_extract_lo_v16si */
    case 1893:  /* vec_extract_lo_v16sf */
    case 1891:  /* vec_extract_hi_v8di */
    case 1889:  /* vec_extract_hi_v8df */
    case 1885:  /* vec_extract_lo_v8di */
    case 1883:  /* vec_extract_lo_v8df */
    case 1872:  /* *vec_extractv4sf_0 */
    case 1864:  /* vec_dupv4sf */
    case 1858:  /* sse_storelps */
    case 1856:  /* sse_storehps */
    case 1831:  /* avx_cvtps2pd256 */
    case 1827:  /* avx512f_cvtps2pd512 */
    case 1825:  /* avx_cvtpd2ps256 */
    case 1821:  /* *avx512f_cvtpd2ps512 */
    case 1814:  /* fix_truncv4dfv4si2 */
    case 1810:  /* ufix_truncv8dfv8si2 */
    case 1806:  /* fix_truncv8dfv8si2 */
    case 1790:  /* ufloatv8siv8df */
    case 1789:  /* floatv4siv4df2 */
    case 1787:  /* floatv8siv8df2 */
    case 1757:  /* sse2_cvttpd2pi */
    case 1755:  /* sse2_cvtpi2pd */
    case 1754:  /* fix_truncv4sfv4si2 */
    case 1753:  /* fix_truncv8sfv8si2 */
    case 1749:  /* ufix_truncv16sfv16si2 */
    case 1745:  /* fix_truncv16sfv16si2 */
    case 1731:  /* ufloatv16siv16sf2 */
    case 1730:  /* floatv4siv4sf2 */
    case 1729:  /* floatv8siv8sf2 */
    case 1725:  /* floatv16siv16sf2 */
    case 1335:  /* sse2_sqrtv2df2 */
    case 1334:  /* avx_sqrtv4df2 */
    case 1330:  /* avx512f_sqrtv8df2 */
    case 1329:  /* sse_sqrtv4sf2 */
    case 1328:  /* avx_sqrtv8sf2 */
    case 1324:  /* avx512f_sqrtv16sf2 */
    case 1124:  /* *vec_extractv2si_1 */
    case 1123:  /* *vec_extractv2si_0 */
    case 1121:  /* *vec_dupv2si */
    case 1119:  /* mmx_pswapdv2si2 */
    case 1054:  /* *vec_extractv2sf_1 */
    case 1053:  /* *vec_extractv2sf_0 */
    case 1051:  /* *vec_dupv2sf */
    case 1050:  /* mmx_pswapdv2sf2 */
    case 1049:  /* mmx_floatv2si2 */
    case 1046:  /* mmx_pf2id */
    case 814:  /* *sqrtdf2_sse */
    case 813:  /* *sqrtsf2_sse */
    case 809:  /* sqrtxf2 */
    case 742:  /* *bswapdi2 */
    case 741:  /* *bswapsi2 */
    case 740:  /* *bswapdi2_movbe */
    case 739:  /* *bswapsi2_movbe */
    case 481:  /* *one_cmplqi2_1 */
    case 480:  /* *one_cmplhi2_1 */
    case 479:  /* *one_cmpldi2_1 */
    case 478:  /* *one_cmplsi2_1 */
    case 465:  /* *negxf2_1 */
    case 464:  /* *absxf2_1 */
    case 463:  /* *negdf2_1 */
    case 462:  /* *absdf2_1 */
    case 461:  /* *negsf2_1 */
    case 460:  /* *abssf2_1 */
    case 206:  /* *floatdidf2_i387 */
    case 205:  /* *floatdisf2_i387 */
    case 204:  /* *floatsidf2_i387 */
    case 203:  /* *floatsisf2_i387 */
    case 202:  /* *floatdidf2_sse */
    case 201:  /* *floatsidf2_sse */
    case 200:  /* *floatdisf2_sse */
    case 199:  /* *floatsisf2_sse */
    case 198:  /* floatdixf2 */
    case 197:  /* floatsixf2 */
    case 196:  /* floathixf2 */
    case 195:  /* floathidf2 */
    case 194:  /* floathisf2 */
    case 176:  /* fix_truncdi_fisttp_i387_1 */
    case 175:  /* fix_truncsi_fisttp_i387_1 */
    case 174:  /* fix_trunchi_fisttp_i387_1 */
    case 173:  /* fix_truncdfdi_sse */
    case 172:  /* fix_truncdfsi_sse */
    case 171:  /* fix_truncsfdi_sse */
    case 170:  /* fix_truncsfsi_sse */
    case 167:  /* *truncxfdf2_i387 */
    case 166:  /* *truncxfsf2_i387 */
    case 165:  /* truncxfdf2_i387_noop */
    case 164:  /* truncxfsf2_i387_noop */
    case 161:  /* *truncdfsf2_i387_1 */
    case 158:  /* *truncdfsf_fast_i387 */
    case 157:  /* *truncdfsf_fast_sse */
    case 156:  /* *truncdfsf_fast_mixed */
    case 155:  /* *extenddfxf2_i387 */
    case 154:  /* *extendsfxf2_i387 */
    case 153:  /* *extendsfdf2_i387 */
    case 152:  /* *extendsfdf2_sse */
    case 151:  /* *extendsfdf2_mixed */
    case 150:  /* extendqihi2 */
    case 148:  /* extendqisi2 */
    case 146:  /* extendhisi2 */
    case 145:  /* extendhidi2 */
    case 144:  /* extendqidi2 */
    case 142:  /* *extendsidi2_rex64 */
    case 141:  /* *zero_extendqihi2 */
    case 139:  /* *zero_extendhisi2 */
    case 138:  /* *zero_extendqisi2 */
    case 135:  /* zero_extendhidi2 */
    case 134:  /* zero_extendqidi2 */
    case 133:  /* *zero_extendsidi2 */
    case 116:  /* *movdi_extzv_1 */
    case 115:  /* *movsi_extzv_1 */
    case 114:  /* *movqi_extv_1 */
    case 113:  /* *movsi_extv_1 */
    case 112:  /* *movhi_extv_1 */
    case 101:  /* *movabsdi_2 */
    case 100:  /* *movabssi_2 */
    case 99:  /* *movabshi_2 */
    case 98:  /* *movabsqi_2 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 0));
      break;

    case 645:  /* *sibcall */
    case 644:  /* *sibcall */
    case 642:  /* *call */
    case 641:  /* *call */
    case 593:  /* *setcc_qi_slp */
    case 119:  /* movdi_insv_1 */
    case 118:  /* movsi_insv_1 */
    case 109:  /* *movstricthi_1 */
    case 108:  /* *movstrictqi_1 */
    case 97:  /* *movabsdi_1 */
    case 96:  /* *movabssi_1 */
    case 95:  /* *movabshi_1 */
    case 94:  /* *movabsqi_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (pat, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (pat, 1));
      break;

    case 3130:  /* storedi_via_fpu */
    case 3129:  /* loaddi_via_fpu */
    case 3110:  /* *conflictv8di */
    case 3108:  /* *conflictv16si */
    case 2951:  /* *avx512f_vcvtph2ps512 */
    case 2950:  /* vcvtph2ps256 */
    case 2949:  /* *vcvtph2ps_load */
    case 2913:  /* avx_pd256_pd */
    case 2912:  /* avx_ps256_ps */
    case 2911:  /* avx_si256_si */
    case 2751:  /* aesimc */
    case 2724:  /* xop_frczv8df2 */
    case 2723:  /* xop_frczv16sf2 */
    case 2722:  /* xop_frczv4df2 */
    case 2721:  /* xop_frczv8sf2 */
    case 2720:  /* xop_frczv2df2 */
    case 2719:  /* xop_frczv4sf2 */
    case 2718:  /* xop_frczdf2 */
    case 2717:  /* xop_frczsf2 */
    case 2642:  /* *avx512er_rsqrt28v8df */
    case 2638:  /* *avx512er_rsqrt28v16sf */
    case 2630:  /* *avx512er_rcp28v8df */
    case 2626:  /* *avx512er_rcp28v16sf */
    case 2622:  /* avx512er_exp2v8df */
    case 2618:  /* avx512er_exp2v16sf */
    case 2519:  /* sse4_1_phminposuw */
    case 2508:  /* sse4_1_movntdqa */
    case 2507:  /* avx2_movntdqa */
    case 2506:  /* avx512f_movntdqa */
    case 2487:  /* sse4a_movntdf */
    case 2486:  /* sse4a_movntsf */
    case 2425:  /* sse2_pmovmskb */
    case 2424:  /* avx2_pmovmskb */
    case 2423:  /* sse2_movmskpd */
    case 2422:  /* avx_movmskpd256 */
    case 2421:  /* sse_movmskps */
    case 2420:  /* avx_movmskps256 */
    case 1943:  /* avx512f_getexpv8df */
    case 1939:  /* avx512f_getexpv16sf */
    case 1802:  /* avx512f_ufix_notruncv8dfv8si */
    case 1799:  /* avx_cvtpd2dq256 */
    case 1795:  /* *avx512f_cvtpd2dq512 */
    case 1782:  /* sse2_cvtsd2siq_2 */
    case 1779:  /* sse2_cvtsd2si_2 */
    case 1756:  /* sse2_cvtpd2pi */
    case 1741:  /* *avx512f_ufix_notruncv16sfv16si */
    case 1737:  /* *avx512f_fix_notruncv16sfv16si */
    case 1736:  /* sse2_fix_notruncv4sfv4si */
    case 1735:  /* avx_fix_notruncv8sfv8si */
    case 1713:  /* sse_cvtss2siq_2 */
    case 1710:  /* sse_cvtss2si_2 */
    case 1344:  /* *rsqrt14v8df */
    case 1342:  /* *rsqrt14v16sf */
    case 1341:  /* sse_rsqrtv4sf2 */
    case 1340:  /* avx_rsqrtv8sf2 */
    case 1320:  /* *rcp14v8df */
    case 1318:  /* *rcp14v16sf */
    case 1316:  /* sse_rcpv4sf2 */
    case 1315:  /* avx_rcpv8sf2 */
    case 1208:  /* sse2_movntv2di */
    case 1207:  /* avx_movntv4di */
    case 1206:  /* avx512f_movntv8di */
    case 1205:  /* sse2_movntv2df */
    case 1204:  /* avx_movntv4df */
    case 1203:  /* avx512f_movntv8df */
    case 1202:  /* sse_movntv4sf */
    case 1201:  /* avx_movntv8sf */
    case 1200:  /* avx512f_movntv16sf */
    case 1199:  /* sse2_movntidi */
    case 1198:  /* sse2_movntisi */
    case 1197:  /* sse3_lddqu */
    case 1196:  /* avx_lddqu256 */
    case 1193:  /* avx512f_storedquv8di */
    case 1192:  /* avx512f_storedquv16si */
    case 1191:  /* sse2_storedquv16qi */
    case 1190:  /* avx_storedquv32qi */
    case 1188:  /* *avx512f_loaddquv8di */
    case 1186:  /* *avx512f_loaddquv16si */
    case 1185:  /* *sse2_loaddquv16qi */
    case 1184:  /* *avx_loaddquv32qi */
    case 1181:  /* sse2_storeupd */
    case 1180:  /* avx_storeupd256 */
    case 1179:  /* avx512f_storeupd512 */
    case 1178:  /* sse_storeups */
    case 1177:  /* avx_storeups256 */
    case 1176:  /* avx512f_storeups512 */
    case 1175:  /* *sse2_loadupd */
    case 1174:  /* *avx_loadupd256 */
    case 1172:  /* *avx512f_loadupd512 */
    case 1171:  /* *sse_loadups */
    case 1170:  /* *avx_loadups256 */
    case 1168:  /* *avx512f_loadups512 */
    case 1129:  /* mmx_pmovmskb */
    case 1038:  /* mmx_rsqrtv2sf2 */
    case 1035:  /* mmx_rcpv2sf2 */
    case 1027:  /* sse_movntq */
    case 983:  /* xsaveopt */
    case 982:  /* xsave */
    case 972:  /* rdpmc */
    case 886:  /* movmsk_df */
    case 885:  /* fxamdf2_i387_with_temp */
    case 884:  /* fxamsf2_i387_with_temp */
    case 883:  /* fxamxf2_i387 */
    case 882:  /* fxamdf2_i387 */
    case 881:  /* fxamsf2_i387 */
    case 852:  /* fistsi2 */
    case 851:  /* fisthi2 */
    case 850:  /* *fistsi2_1 */
    case 849:  /* *fisthi2_1 */
    case 846:  /* *fistdi2_1 */
    case 845:  /* rintxf2 */
    case 841:  /* *f2xm1xf2_i387 */
    case 818:  /* *cosxf2_i387 */
    case 817:  /* *sinxf2_i387 */
    case 812:  /* *rsqrtsf2_sse */
    case 808:  /* truncxfdf2_i387_noop_unspec */
    case 807:  /* truncxfsf2_i387_noop_unspec */
    case 779:  /* *rcpsf2_sse */
    case 768:  /* *tls_dynamic_gnu2_lea_64 */
    case 91:  /* kmovw */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XVECEXP (XEXP (pat, 1), 0, 0));
      break;

    case 590:  /* *setcc_si_1_and */
    case 85:  /* *movdi_or */
    case 84:  /* *movsi_or */
    case 83:  /* *movdi_xor */
    case 82:  /* *movsi_xor */
    case 77:  /* *popdi1_epilogue */
    case 76:  /* *popsi1_epilogue */
    case 73:  /* *pushdi2_prologue */
    case 72:  /* *pushsi2_prologue */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (pat, 0, 0), 1));
      break;

    case 1153:  /* *movv2df_internal */
    case 1152:  /* *movv4df_internal */
    case 1151:  /* *movv8df_internal */
    case 1150:  /* *movv4sf_internal */
    case 1149:  /* *movv8sf_internal */
    case 1148:  /* *movv16sf_internal */
    case 1147:  /* *movv1ti_internal */
    case 1146:  /* *movv2ti_internal */
    case 1145:  /* *movv2di_internal */
    case 1144:  /* *movv4di_internal */
    case 1143:  /* *movv8di_internal */
    case 1142:  /* *movv4si_internal */
    case 1141:  /* *movv8si_internal */
    case 1140:  /* *movv16si_internal */
    case 1139:  /* *movv8hi_internal */
    case 1138:  /* *movv16hi_internal */
    case 1137:  /* *movv32hi_internal */
    case 1136:  /* *movv16qi_internal */
    case 1135:  /* *movv32qi_internal */
    case 1134:  /* *movv64qi_internal */
    case 1026:  /* *movv2sf_internal */
    case 1025:  /* *movv1di_internal */
    case 1024:  /* *movv2si_internal */
    case 1023:  /* *movv4hi_internal */
    case 1022:  /* *movv8qi_internal */
    case 957:  /* *prefetch_3dnow */
    case 592:  /* *setcc_qi */
    case 591:  /* *setcc_si_1_movzbl */
    case 589:  /* *setcc_di_1 */
    case 214:  /* *leadi */
    case 213:  /* *leasi */
    case 129:  /* *movsf_internal */
    case 128:  /* *movdf_internal */
    case 127:  /* *movxf_internal */
    case 126:  /* *movtf_internal */
    case 125:  /* *pushsf */
    case 124:  /* *pushsf_rex64 */
    case 123:  /* *pushdf */
    case 122:  /* *pushxf */
    case 121:  /* *pushtf */
    case 93:  /* *movqi_internal */
    case 92:  /* *movhi_internal */
    case 90:  /* *movsi_internal */
    case 89:  /* *movdi_internal */
    case 88:  /* *movti_internal */
    case 87:  /* *movoi_internal_avx */
    case 86:  /* *movxi_internal_avx512f */
    case 81:  /* *popfldi1 */
    case 80:  /* *popflsi1 */
    case 79:  /* *pushfldi2 */
    case 78:  /* *pushflsi2 */
    case 75:  /* *popdi1 */
    case 74:  /* *popsi1 */
    case 71:  /* *pushhi2 */
    case 70:  /* *pushqi2 */
    case 69:  /* *pushsi2_rex64 */
    case 68:  /* *pushhi2_rex64 */
    case 67:  /* *pushqi2_rex64 */
    case 66:  /* *pushsi2 */
    case 65:  /* *pushdi2_rex64 */
    case 64:  /* *pushti2 */
    case 63:  /* *pushdi2 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (pat, 1));
      break;

    case 193:  /* x86_fldcw_1 */
    case 48:  /* x86_sahf_1 */
      ro[0] = *(ro_loc[0] = &XVECEXP (XEXP (pat, 1), 0, 0));
      break;

    case 2429:  /* sse_stmxcsr */
    case 1006:  /* rdgsbasedi */
    case 1005:  /* rdfsbasedi */
    case 1004:  /* rdgsbasesi */
    case 1003:  /* rdfsbasesi */
    case 998:  /* lwp_slwpcbdi */
    case 997:  /* lwp_slwpcbsi */
    case 993:  /* fnstsw */
    case 979:  /* fxsave64 */
    case 978:  /* fxsave */
    case 974:  /* rdtsc */
    case 959:  /* *prefetch_prefetchwt1_di */
    case 958:  /* *prefetch_prefetchwt1_si */
    case 760:  /* *load_tp_di */
    case 759:  /* *load_tp_si */
    case 758:  /* *load_tp_x32_zext */
    case 757:  /* *load_tp_x32 */
    case 667:  /* set_got_rex64 */
    case 192:  /* x86_fnstcw_1 */
    case 47:  /* x86_fnstsw_1 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      break;

    case 46:  /* *cmpxf_si_cc_i387 */
    case 45:  /* *cmpdf_si_cc_i387 */
    case 44:  /* *cmpsf_si_cc_i387 */
    case 43:  /* *cmpxf_hi_cc_i387 */
    case 42:  /* *cmpdf_hi_cc_i387 */
    case 41:  /* *cmpsf_hi_cc_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      break;

    case 40:  /* *cmpxf_si_i387 */
    case 39:  /* *cmpdf_si_i387 */
    case 38:  /* *cmpsf_si_i387 */
    case 37:  /* *cmpxf_hi_i387 */
    case 36:  /* *cmpdf_hi_i387 */
    case 35:  /* *cmpsf_hi_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (XEXP (pat, 1), 0, 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (XEXP (pat, 1), 0, 0), 1), 0));
      ro[3] = *(ro_loc[3] = &XEXP (XVECEXP (XEXP (pat, 1), 0, 0), 1));
      break;

    case 239:  /* *addsi_4 */
    case 238:  /* *addhi_4 */
    case 237:  /* *addqi_4 */
    case 236:  /* *adddi_4 */
    case 34:  /* *cmpuxf_cc_i387 */
    case 33:  /* *cmpudf_cc_i387 */
    case 32:  /* *cmpusf_cc_i387 */
    case 28:  /* *cmpdf_cc_i387 */
    case 27:  /* *cmpsf_cc_i387 */
    case 24:  /* *cmpxf_cc_i387 */
    case 22:  /* *cmpxf_0_cc_i387 */
    case 21:  /* *cmpdf_0_cc_i387 */
    case 20:  /* *cmpsf_0_cc_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (XVECEXP (pat, 0, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XEXP (XVECEXP (pat, 0, 0), 1), 1));
      break;

    case 31:  /* *cmpuxf_i387 */
    case 30:  /* *cmpudf_i387 */
    case 29:  /* *cmpusf_i387 */
    case 26:  /* *cmpdf_i387 */
    case 25:  /* *cmpsf_i387 */
    case 23:  /* *cmpxf_i387 */
    case 19:  /* *cmpxf_0_i387 */
    case 18:  /* *cmpdf_0_i387 */
    case 17:  /* *cmpsf_0_i387 */
      ro[0] = *(ro_loc[0] = &XEXP (pat, 0));
      ro[1] = *(ro_loc[1] = &XEXP (XVECEXP (XEXP (pat, 1), 0, 0), 0));
      ro[2] = *(ro_loc[2] = &XEXP (XVECEXP (XEXP (pat, 1), 0, 0), 1));
      break;

    case 16:  /* *cmpqi_ext_4 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0));
      break;

    case 15:  /* *cmpqi_ext_3 */
    case 14:  /* *cmpqi_ext_2 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 0), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 1));
      break;

    case 13:  /* *cmpqi_ext_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (pat, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (XEXP (pat, 1), 1), 0), 0));
      break;

    case 413:  /* kortestchi */
    case 412:  /* kortestzhi */
    case 357:  /* *testsi_1 */
    case 356:  /* *testhi_1 */
    case 355:  /* *testqi_1 */
    case 354:  /* *testqi_1_maybe_si */
    case 353:  /* *testdi_1 */
    case 12:  /* *cmpdi_minus_1 */
    case 11:  /* *cmpsi_minus_1 */
    case 10:  /* *cmphi_minus_1 */
    case 9:  /* *cmpqi_minus_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (XEXP (pat, 1), 0), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (XEXP (pat, 1), 0), 1));
      break;

    case 62:  /* *cmpiuxf_i387 */
    case 61:  /* *cmpiudf_i387 */
    case 60:  /* *cmpiusf_i387 */
    case 59:  /* *cmpixf_i387 */
    case 58:  /* *cmpidf_i387 */
    case 57:  /* *cmpisf_i387 */
    case 56:  /* *cmpiudf_sse */
    case 55:  /* *cmpiusf_sse */
    case 54:  /* *cmpidf_sse */
    case 53:  /* *cmpisf_sse */
    case 52:  /* *cmpiudf_mixed */
    case 51:  /* *cmpiusf_mixed */
    case 50:  /* *cmpidf_mixed */
    case 49:  /* *cmpisf_mixed */
    case 8:  /* *cmpdi_1 */
    case 7:  /* *cmpsi_1 */
    case 6:  /* *cmphi_1 */
    case 5:  /* *cmpqi_1 */
    case 4:  /* *cmpdi_ccno_1 */
    case 3:  /* *cmpsi_ccno_1 */
    case 2:  /* *cmphi_ccno_1 */
    case 1:  /* *cmpqi_ccno_1 */
      ro[0] = *(ro_loc[0] = &XEXP (XEXP (pat, 1), 0));
      ro[1] = *(ro_loc[1] = &XEXP (XEXP (pat, 1), 1));
      break;

    }
}
