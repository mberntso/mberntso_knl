#include <hbwmalloc.h>
#include <iostream>
#include <chrono> // for high_resolution_clock
#include <stdlib.h> // for malloc
using namespace std;

int main() {
	cout <<	"hbw_check_available: " << hbw_check_available() << endl;
	//hbw_set_policy(HBW_POLICY_PREFERRED); // default
	cout << "hbw_get_policy: "	<< hbw_get_policy() << endl;

	// setup vector experiment
	double *a, *b, *c;
	int size = 128000000;
	a=(double *) malloc(size*sizeof(double));
	b=(double *) malloc(size*sizeof(double));
	c=(double *) malloc(size*sizeof(double));

	// start recording execution time
	auto start = chrono::high_resolution_clock::now();

	// timed code
	for (int i=0; i<size; i++) {
                a[i] = i/10.0;
                b[i] = i/20.0;
                c[i] = 0.0;
        }

	for (int i=0; i<size; i++) {
                c[i]=a[i]/b[i];
        }
	
/*
	for (int i=0; i<size; i++) {
                cout << "c[" << i << "] = " << c[i] << endl;
        }
*/

	// stop recording execution time
	auto finish = chrono::high_resolution_clock::now();

	chrono::duration<double> elapsed = finish - start;
	cout << "Elapsed time: " << elapsed.count() << "s\n";

	// free allocated memory
	free(a);
	free(b);
	free(c);

	return 0;
}
